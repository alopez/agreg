### IMPORT DES LIBS QUI VONT BIEN 
# SUBPROCESS: Pour faire des appels systèmes
from     subprocess   import   call
# MATH: pour les racines, pi et autres opérations
import   math
# RANDOM: principalement pour randint/random
import   random
# ITERTOOLS: pour les opérations sur les listes
import   itertools
# YAML: pour parser les fichiers de configuration
from     yaml         import   load, dump
# DATETIME: pour gérer les dates
import   datetime


### CONTRÔLE SI ON UTILISE CVX/NUMPY
WITH_NICE_TOOLS = False


if WITH_NICE_TOOLS:
    from     scipy        import   stats
    import   numpy        as       np
    import   cvxpy        as       cvx


## PARCE QUE SIGNUM N'EST PAS DANS maths ???
def signum (x):
    # PS: c'est un hack sympathique
    # pour savoir le signe de x
    return (x >= 0) - (x < 0)


## LA DATE DE COMPILATION
CURRENT_DATE    = datetime.date.today ()

######
##
## LISTE DES DÉVELOPPEMENTS DE MATHÉMATIQUES
## ET CHARGEMENT DES DEADLINES 
##
######
with open ("devsmaths.yaml", "r") as devsmaths:
    DEVSMATHS = load (devsmaths) 

with open ("lecsmaths.yaml", "r") as lecsmaths:
    LECSMATHS = load (lecsmaths)

with open ("devsinfo.yaml", "r") as devsinfo:
    DEVSINFO  = load (devsinfo)

with open ("lecsinfo.yaml", "r") as lecsinfo:
    LECSINFO  = load (lecsinfo)

with open ("deadlines.yaml", "r") as deadlines:
    DEADLINES = load (deadlines)

###
##
## PERMET D'INVERSER LA RELATION DEV/LEC
##
###
def LtoD (LECS, DEVS):
    couplageR = {}

    for nom in LECS.keys ():
        couplageR[nom] = []

    for devnum,dev in enumerate(DEVS):
        for lec in dev["case"]:
            couplageR[lec].append(devnum)

    return couplageR

###
##
## RECHERCHE D'UNE COUVERTURE MINIMALE
##
###
def minimiseDEVS (LECS, DEVS):
    """ 
        Recherche (un ?) ensemble 
        minimal de DEVS qui couvre 
        deux fois chaque leçons

        Retourne tous les sous ensembles 
        qui peuvent couvrir toutes les leçons
        deux fois 
    """
    def nprint (x):
        # print (x)
        pass

    def devRecaseStar (devnum, lecnum):
        try:
            return DEVS[devnum]["note"][DEVS[devnum]["case"].index (lecnum)]
        except ValueError:
            return 0

    couplageR = LtoD (LECS,DEVS)

    n = len (DEVS)

    Adevs = [ [ int (x in lec) for x,_ in enumerate(DEVS) ] 
                for (lecnum, lec) in couplageR.items()  ]
    nprint ("Adevs = {} x {}".format (len (Adevs), len (Adevs[0])))
    Adevs = np.array (Adevs, dtype = float)

    Anots = [ [ devRecaseStar(devnum,lecnum) for devnum,_ in enumerate(DEVS) ] 
                for (lecnum, lec) in couplageR.items()  ]
    Anots = np.array (Anots, dtype = float)


    bdevs = [ 2 for (lecnum,_) in couplageR.items() ]
            # if lecnum[0] == "1" ]
    nprint ("bdevs = {}".format (len (bdevs)))
    bdevs = np.array (bdevs, dtype = float)

    bnots = [ 3 for (lecnum,_) in couplageR.items() ]
            # if lecnum[0] == "1" ]
    nprint ("bnots = {}".format (len (bnots)))
    bnots = np.array (bnots, dtype = float)


    nprint ("x = {}".format (n))

    x = cvx.Variable (n)

    bConstr = cvx.Bool (n)

    objective  = cvx.Minimize (cvx.sum_entries (x))
    constraint = [Adevs * x >= bdevs, Anots * x >= bnots, bConstr == x, ]

    prob = cvx.Problem (objective, constraint)
    opt  = prob.solve ()
    s    = prob.status

    nprint (opt)

    valx = np.copy (x.value)
    valx = np.transpose (valx)[0]

    nprint ([ "{:02f}".format (x) for x in valx ])

    for devnum, val in enumerate(valx):
        if val > 0.5:
            nprint ("GARDE  {:02d} : {}".format (devnum, DEVS[devnum]["titre"]))
        else:
            nprint ("RETIRE {:02d} : {}".format (devnum, DEVS[devnum]["titre"]))

    for (lecnum, lec) in couplageR.items ():
        nprint ("{} : {}".format (lecnum, [ x for x in lec if 0.5 < valx[x]]))

    return valx 

###
## AFFICHE L'INDEX ... C'EST TOTALEMENT HARDCODÉ
###
def buildIndex ():
    print ("\\chapter{Index}")
    print ("\\label{indexOfGlobalDoc}")
    print ("\\begin{niceblock}{Liens}")
    print ("\\item[$\\square$]\hyperref[statistiquesI]{\\textsc{Statistiques Info}}")
    print ("\\item[$\\square$]\hyperref[developpementsI]{\\textsc{Développements Info}}")
    print ("\\item[$\\square$]\hyperref[leconsI]{\\textsc{Leçons Info}}")
    print ("\\item[$\\square$]\hyperref[statistiquesM]{\\textsc{Statistiques Maths}}")
    print ("\\item[$\\square$]\hyperref[developpementsM]{\\textsc{Développements Maths}}")
    print ("\\item[$\\square$]\hyperref[leconsM]{\\textsc{Leçons Maths}}")
    print ("\\end{niceblock}")

#####
##
## CLASSE QUI GÈRE PLUS OU MOINS UNE "MATIÈRE",
## AVEC SES LEÇONS, SES DÉVELOPPEMENTS, SES
## DEADLINES
##
####
class Partie:
    def __init__ (self, leclist, devlist, name):
        self.DEVS = devlist
        self.LECS = leclist
        self.COUP = LtoD(self.LECS, self.DEVS)
        self.name = name

        if WITH_NICE_TOOLS: 
            self.optdevs  = minimiseDEVS (self.LECS, self.DEVS)
        else:
            self.optdevs  = [ 1 for x in devlist ] 

        self.STATS = {}

        self.statlabel = "statistiques"   + name[0]
        self.lecslabel = "lecons"         + name[0]
        self.devslabel = "developpements" + name[0]

    def lecRef (self, lecnum, content):
        print ("\\lecref{"+ lecnum + "}{" + content + "}",end="")

    def lecTitle (self, lecnum, titre):
        print ("\\lecon{"+ lecnum + "}{" + titre + "}", end="")

    def devRef (self, devnum, content):
        print ("\\devref{" +  "{0:02d}".format(devnum) + "}{" + content + "}{" +
                self.name[0] + "}", end="")

    def devTitle (self, devnum, title):
        print ("\\dev{" + "{0:02d}".format(devnum) + "}{" + title + "}{" +
                self.name[0] + "}", end="")

    def inputTexFile (self, texfile):
        # pass
        print ("\\input{" + texfile + "}")
        # with open (texfile, "r") as f:
            # print (f.read ())
        print ("\\cleardoublepage")


    def printPartie (self):
        print ("\\part{" + self.name + "}")
        self.computeStats ()
        # self.printPriorityChapter ()
        self.printStats ()
        self.printDevChapter ()
        self.printLecChapter ()


    def devRecaseStar (self, devnum, lecnum):
        """
            Étant donné un numéro de développement
            et un numéro de leçon, regarde combien
            quel est le nombre d'étoiles de recasage
        """
        try:
            return self.DEVS[devnum]["note"][self.DEVS[devnum]["case"].index (lecnum)]
        except ValueError:
            return 0

    def computeStats (self):
        """
            Remplit la variable "self.stats"
            avec tout un tas d'informations 
            utiles sur les leçons, les dévelppements
            et les deadlines
        """
        S = self.STATS
        cases     = [ len (dev["case"]) for dev in self.DEVS ] 
        lecsStats = [ len (self.COUP[l]) for l in self.LECS.keys () ]
        if WITH_NICE_TOOLS:
            descCase  = stats.cumfreq (cases, numbins=5)
            descLecs  = stats.cumfreq (lecsStats, numbins=5)

        S["lecons"] = {}
        S["lecons"]["nbrlecs"]   = len (self.LECS)
        S["lecons"]["nbrdevmoy"] = sum (lecsStats) / len (lecsStats)
        Moy                      = S["lecons"]["nbrdevmoy"]

        S["lecons"]["nbrdevect"] = math.sqrt (sum (
            [ (x - Moy)**2 for x in lecsStats ]) / len (lecsStats))
        S["lecons"]["sansdev"]   = len ([ x for x in lecsStats if x == 0])
        S["lecons"]["unseuldev"] = len ([ x for x in lecsStats if x == 1])
        S["lecons"]["rediges"] = len ([ d for d in self.LECS.values() if "tex" in d ])
        S["lecons"]["remains"] = S["lecons"]["nbrlecs"] - S["lecons"]["rediges"]

        S["devs"] = {}
        S["devs"]["nbrdevs"] = len (self.DEVS)
        S["devs"]["rediges"] = len ([ d for d in self.DEVS if "tex" in d ])
        S["devs"]["remains"] = S["devs"]["nbrdevs"] - S["devs"]["rediges"]
        S["devs"]["casemoy"] = sum (cases) / len (cases)

        if WITH_NICE_TOOLS:
            S["devs"]["nbropt"]  = len ([x for x in self.optdevs if x > 0.1 ]) 
        else:
            S["devs"]["nbropt"]  = "[Désactivé à la compilation]"

        def updateDeadline (dl):
            delta = dl["date"] - CURRENT_DATE - datetime.timedelta(days=1)
            dl["delta"] = delta
            if delta < datetime.timedelta(microseconds=1):
                dl["overdue"] = True
            else:
                dl["overdue"] = False
                dl["lecs/days"] = S["lecons"]["remains"] / delta.days
                dl["devs/days"] = S["devs"]["remains"]   / delta.days
                if "planned/lecs/days" in dl:
                    dl["planned/lecs/finished"] = S["lecons"]["remains"] / dl["planned/lecs/days"]

            return dl

        S["deadlines"] = [ updateDeadline(deadline) for deadline in DEADLINES
                           if self.name in deadline["concerne"] ]

    def printStats (self):
        stretch = "\\hspace{\\stretch{1}}"

        print ("\\chapter{Statistiques --- " + str (CURRENT_DATE) + "}")
        print ("\\label{" + self.statlabel + "}")

        print ("\\begin{niceblock}{Développements}")
        print ("\\item[Nombre de devs] {} {}".format (stretch,
            self.STATS["devs"]["nbrdevs"]))
        print ("\\item[Nombre optimal] {} {}".format (stretch,
            self.STATS["devs"]["nbropt"]))
        print ("\\item[Recasage moyen] {} {:0.2f}".format (stretch, 
            self.STATS["devs"]["casemoy"]))
        print ("\\item[Rédaction] \\hfill {} sur {}".format (
            self.STATS["devs"]["rediges"], self.STATS["devs"]["nbrdevs"]))
        print ("\\end{niceblock}")

        print ("\\begin{niceblock}{Leçons}")
        print ("\\item[Sans développement (abs)] {} {}".format (stretch,
            self.STATS["lecons"]["sansdev"]))
        print ("\\item[Un seul développement (abs)] {} {}".format (stretch,
            self.STATS["lecons"]["unseuldev"]))
        print ("\\item[Nombre moyen de développments] {} {:0.2f}".format (stretch,
            self.STATS["lecons"]["nbrdevmoy"]))
        print ("\\item[Écart-type $\\sigma$] {} {:0.2f}".format (stretch,
            self.STATS["lecons"]["nbrdevect"]))
        print ("\\item[Rédaction] \\hfill {} sur {}".format (
            self.STATS["lecons"]["rediges"], self.STATS["lecons"]["nbrlecs"]))
        print ("\\end{niceblock}")

        for dl in self.STATS["deadlines"]:
            print ("\\begin{niceblock}{{\\color{red} [DEADLINE] } " + dl["evenement"] + "}")
            print ("\\item[Date] {} {}".format (stretch, dl["date"]))
            print ("\\item[Dans] {} {} Jours".format (stretch,
                dl["delta"].days))
            if dl["overdue"]:
                print ("\\item[OVERDUE] {} {}".format (stretch, "..."))
            else:
                print ("\\item[Rédaction lecs] {} {:0.2f} Lec/Jour".format (stretch,
                    dl["lecs/days"]))
                print ("\\item[Rédaction devs] {} {:0.2f} Dev/Jour".format (stretch,
                    dl["devs/days"]))
                if "planned/lecs/days" in dl:
                    print ("\\item[Estimated days ({} L/J)] {} {:0.2f} Jours".format
                            (dl["planned/lecs/days"], stretch, dl["planned/lecs/finished"]))
            print ("\\end{niceblock}")


        print ("\\begin{niceblock}{Liens}")
        print ("\\item[$\\square$]\hyperref["+ self.devslabel +"]{\\textsc{Développements}}")
        print ("\\item[$\\square$]\hyperref["+ self.lecslabel +"]{\\textsc{Leçons}}")
        print ("\\end{niceblock}")

    def printCVXoutput (self):
        print ("\\begin{niceblock}{Optimal (CVX)}")
        for devnum,devscore in enumerate (self.optdevs):
            if devscore > 0.1:
                print ("\\item[Conserver]")
            else:
                print ("\\item[Supprimer]")
            print (self.DEVS[devnum]["titre"]) 
        print ("\\end{niceblock}")

    def printPriorityChapter (self):
        print ("\\chapter{Leçons à traiter en priorité}")

        lecs = [ lecnum for lecnum,lec in self.LECS.items ()
                        if "priority" in lec ] 

        lecs.sort (key = lambda x: self.LECS[x]["priority"], reverse=True)

        for x in lecs:
            L = self.LECS[x]
            print ("\\begin{niceblock}{" + L["titre"] + "}")
            print ("\\item[Priorité] \\hfill {}".format (L["priority"]))
            print ("\\end{niceblock}")

    def printLecChapter (self):
        print ("\\chapter{Leçons}")
        print ("\\label{" + self.lecslabel + "}")
        print ("\\vspace{-4em}")

        firstnum = "0"
        prevnum  = "0"
        for num,lecon in self.LECS.items ():
            if firstnum != num[0]:
                if firstnum != "0":
                    print ("\\end{niceblock}")
                    print ("\\clearpage")
                print ("\\begin{niceblock}{Leçons \\hfill " + num[0] + "XX }")
                firstnum = num[0]
            self.lecRef (num, lecon["titre"])
            print ("\\hspace{\\stretch{1}} ")
            numdevs = len (self.COUP[num])
            if numdevs <= 1:
                print ("\\textbf{" + "{}".format (numdevs) + "D}")
            else:
                print ("{}".format (numdevs) + "D")

        print ("\\end{niceblock}")

        print ("\\cleardoublepage")


        # On fait un petit tableau avec les leçons et les 
        # développements sélectionnés pour chaque leçon
        
        print ("\\begin{landscape}")
        print ("\\begin{multicols}{3}")
        print ("\\begin{description}")
        for num,lecon in self.LECS.items ():
            self.lecRef (num, lecon["titre"])
            print ("\\begin{compactitem}")
            for devnum in self.COUP[num]:
                dev = self.DEVS[devnum]
                if "tex" in dev:
                    print ("\\item[\\cmark] {}".format (self.DEVS[devnum]["titre"]))
                else:
                    print ("\\item[\\xmark] {}".format (self.DEVS[devnum]["titre"]))
            print ("\\end{compactitem}")
        print ("\\end{description}")
        print ("\\end{multicols}")
        print ("\\end{landscape}")

        print ("\\cleardoublepage")

        for num,lecon in self.LECS.items ():
            notesDevs = [ self.devRecaseStar (devnum, num) 
                            for devnum, dev in enumerate (self.DEVS)
                            if self.optdevs[devnum] > 0.1
                            and num in dev["case"]  ]
            notesDevs.sort (reverse=True)
            notesDevs = notesDevs[:2]
            moyenneNotesDevs = sum (notesDevs) / len (notesDevs)

            # if "tex" in lecon:
                # print ("\\cleardoublepage")

            self.lecTitle (num, lecon["titre"])

            print ("\\begin{devlist}{"+ self.devslabel +"}{"+  "{0:0.1f}".format (moyenneNotesDevs) +"}")
            if self.COUP[num] != []:
                max1 = max ([ i for i in range (len (self.DEVS)) if self.optdevs[i] >
                    0.1], key = (lambda d: self.devRecaseStar (d,num)))
                max2 = max ([ i for i in range(len (self.DEVS))  if self.optdevs[i] > 0.1
                    and i != max1], key = (lambda d: self.devRecaseStar (d,num)))
                for dev in self.COUP[num]:
                    colorCommand = ""
                    # if self.optdevs[dev] <= 0.1:
                        # colorCommand = "\\color{gray}"
                    # if self.optdevs[dev] > 0.1 and (dev == max1 or dev == max2):
                        # colorCommand = "\\color{red}"
                    self.devRef (dev, colorCommand + self.DEVS[dev]["titre"] 
                                    + "\\hfill" + 
                                    "$\\star$" * self.devRecaseStar (dev,num))
            else:
                print ("\\item[AUCUN DEVEL] ...")
            print ("\\end{devlist}")

            if "tex" in lecon:
                self.inputTexFile (lecon["tex"])

    def printDevChapter (self):
        print ("\\chapter{Développements}")
        print ("\\label{" + self.devslabel + "}")

        print ("\\begin{niceblock}{Table des développements \\hfill (0)}")
        for devnum,dev in enumerate(self.DEVS):
            # if devnum > 0 and devnum % 10 == 0:
                # print ("\\end{niceblock}")
                # print ("\\begin{niceblock}{Table des développements \\hfill " +
                        # "({})".format (devnum)
                        # + "}")
            lecs = dev["case"]
            self.devRef (devnum, dev["titre"])
            print ("\\hspace{\\stretch{1}} ")
            if "tex" in dev:
                print ("\\cmark")
            else:
                print ("\\xmark")
            print ("\\textbf{" + "{}".format (len (lecs)) + "L}")

        print ("\\end{niceblock}\\vspace{\\stretch{1}}")

        print ("\\cleardoublepage")

        for devnum,dev in enumerate(self.DEVS):
            lecs = dev["case"]

            self.devTitle (devnum, dev["titre"])

            print ("Référence : {}. ".format (dev["ref"]))
            print ("Recasé {} fois".format (len (lecs)))
            print ("\\begin{leclist}{" + self.lecslabel + "}")
            for lec in lecs: 
                self.lecRef (lec, self.LECS[lec]["titre"])
                print ("\\hfill")
                print ("$\\star$" * self.devRecaseStar (devnum, lec))
            print ("\\end{leclist}")

            if "tex" in dev:
                self.inputTexFile (dev["tex"])

if __name__ == "__main__":
    buildIndex ()
    Info = Partie (LECSINFO,  DEVSINFO,  "Informatique")
    Info.printPartie ()

    Math = Partie (LECSMATHS, DEVSMATHS, "Mathématiques")
    Math.printPartie ()

