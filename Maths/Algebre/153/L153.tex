\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs, wasysym}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{hyperref}
%\usepackage{enumitem}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows,matrix}
\usepackage{pdfpages}
\usepackage{epigraph}
\usepackage{palatino}
\usepackage{url}
\usepackage{faktor}
\usepackage{tikz}
\usepackage{tikz-cd}



%%%% Format

\setlength{\columnsep}{2cm}

\newcommand{\lname}{L153 - Polynômes d'endomorphisme en dimension finie.
Réduction d'endomorphisme en dimension finie. Applications}

\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}



\newcommand{\Lin}[1]{\mathcal{L}\left(#1\right)}
\newcommand{\Comm}[1]{\mathcal{C}\left(#1\right)}
\newcommand{\GLin}[1]{\operatorname{GL}\left(#1\right)}
\newcommand{\Image}{\operatorname{Im}}
\newcommand{\Ker}{\operatorname{Ker}}
\newcommand{\trace}{\operatorname{tr}}
\newcommand{\cycl}[2]{\left\langle #1 \right\rangle_{#2}}



\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}

\newcommand{\Dev}[1]{$\RHD$ \textbf{DEV} #1 $\LHD$}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{application}
\newtheorem{application}[definition]{Application}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}
 
\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}


%%%% Corps du fichier
\title{\lname}
\date{}



\begin{document}

\maketitle

\begin{contexte}
    \begin{itemize}
        \item Résoudre des équations polynômiales 
        \item Faire de la réduction 
        \item Proposer Jordan 
        \item Faire passer le lemme des noyaux bien avant,
            en justification de $k[u]$ 
        \item Donner des exemples concrets de réduction,
            des gens n'ayant pas de valeurs propres (pour 
            la différence scindé/pas scindé)
    \end{itemize}
\end{contexte}

\section{Polynômes d'endomorphisme}


\begin{application}[Polynôme annulateur]
    Si $P$ est un polynôme annulateur de $A$ alors 
    tout polynôme en $A$ peut se ramener à un polynôme 
    de degré inférieur à $\deg P$ de manière effective.
    Utile par exemple pour calculer $A^{10000}$. 
\end{application}

\subsection{Algèbre $k[u]$ et polynôme minimal}

\begin{definition}
    Soit $E$ un $k$-espace vectoriel 
    et $u \in \Lin{E}$.
    L'application $\phi_u : P \mapsto P(u)$
    est un morphisme de $k$-algèbres.
    On note $k[u] = \Image \phi_u$
    qui est une sous-algèbre commutative de 
    $\Lin{E}$.

    \begin{center}
        \begin{tikzcd}
            k[X] \arrow[r,"\phi_u", two heads] & k[u] \arrow[r,"i", hook] & \Lin{E}
        \end{tikzcd}
    \end{center}
\end{definition}

\begin{propriete}
    On déduit donc de la propriété universelle des 
    $k$-algèbres quotient~:

    \begin{equation*}
        \faktor{k[x]}{\Ker \phi_u} \simeq k[u]
    \end{equation*}

    Quand $E$ est de dimension finie, $\phi_u$
    n'est pas injective et $\Ker \phi_u$ est un 
    idéal non-trivial de $k[X]$, qui s'écrit donc 
    $(\pi_u)$ pour un certain polynôme unitaire $\pi_u$ non nul
    car $k[X]$ est principal.
    Par conséquent $\dim k[u] = \deg \pi_u$.
\end{propriete}

\begin{remarque}
    On peut adapter la même définition à $M_n(k)$
    et une matrice $M$. 
\end{remarque}

\begin{propriete}
    Si $\beta$ est une base de $E$ 
    et si $u \in \Lin{E}$ alors 
    $P([u]_\beta) = [P(u)]_\beta$.
\end{propriete}

\begin{exemple}
    L'algèbre commutative $k[f]$ (resp. $k[M]$)
    contient un grand nombre d'informations relatives à 
    l'endomorphisme \cite{mansuyAlgebre}.
    \begin{compactenum}[(i)]
    \item Si $M$ est semblable a une matrice diagonale alors 
        $M \in k[M^3]$ 
    \item Si $M$ est inversible, $M^{-1} \in k[M]$.
    \end{compactenum}
\end{exemple}

\begin{exemple}
    Quelques polynômes minimaux en lien avec des 
    propriétés géométriques
    \begin{compactenum}[(i)]
        \item $u$ est nilpotent d'indice $r$ 
            si et seulement si $\pi_u = X^r$
        \item $\pi_u$ est toujours différent de $1$ sauf 
            quand $E$ est de dimension nulle. 
        \item $u$ est une homothétie de rapport $\lambda \neq 0$
            si et seulement si $\pi_u = X - \lambda$
        \item $u$ est un projecteur non trivial si et seulement si 
            $\pi_u = X^2 - X$
        \item $u$ est une symétrie non triviale 
            si et seulement si $\pi_U = X^2 - 1$
    \end{compactenum}
\end{exemple}

\begin{remarque}
    Si $\pi_u$ est irréductible alors $k[u]$ est un corps,
    et $E$ peut être considéré comme un $k[u]$-espace-vectoriel.
\end{remarque}

\begin{application}
    Construction matricielle de $\mathbb{C}$ avec $J$
    de polynôme minimal $X^2 + 1$ dans $M_2(\mathbb{R})$
\end{application}

\begin{definition}[Commutant]
    Soit $u \in \Lin{E}$,
    on note $\Comm{u}$ l'ensemble des $v \in \Lin{E}$
    tels que $u \circ v = v \circ u$.
    C'est une sous-algèbre de $\Lin{E}$
    qui contient $k[u]$.
\end{definition}


\subsection{Polynôme caractéristique}


\begin{definition}[Polynôme caractéristique]
    Si $u \in \Lin{E}$ alors on définit 
    $\chi_u = \det (XI_n - M)$
    avec $M$ une matrice de $u$. Cette définition 
    est invariante par changement de base, et le 
    déterminant est calculé dans un espace de matrices 
    sur un anneau.
\end{definition}

\begin{exemple}
    En dimension 2 $\chi_M = X^2 - \trace M X + \det M$,
    et $\chi_M (M) = 0$
\end{exemple}

\begin{remarque}
    Le calcul de $\chi_M$ provient d'un déterminant, et 
    il sera donc facile de calculer les polynômes 
    caractéristiques de matrices diagonales ou triangulaires.
    De plus il existe un algorithme de calcul via le pivot de Gauss.
\end{remarque}

\begin{propriete}
    Soit $\lambda \in k$ on a l'équivalence suivante~:

    \begin{equation*}
        \chi_M (\lambda) = 0 
        \iff
        M - \lambda I \text{ n'est pas inversible }
    \end{equation*}
\end{propriete}

\begin{exemple}[Matrice compagnon]
    Si $P = X^n - \sum_{i = 0}^{n-1} a_i X^i$ 
    et que $M = C(P)$ la matrice compagnon associée au polynôme 
    $P$ alors~:

    \begin{equation*}
        \pi_M = \chi_M = P
    \end{equation*}
\end{exemple}


\section{Étude des sous-espaces stables}

\begin{definition}
    Un sous espace $F$ est $u$-stable si 
    et seulement si $u(F) \subset F$.
\end{definition}

\begin{propriete}
    Si $F$ est $u$-stable alors l'endomorphisme  
    induit $u_F : F \to F$ est bien défini.
\end{propriete}

\begin{application}
    Si $u \in \Lin{E}$ a un polynôme minimal $\pi_u$
    irréductible, tout sous-espace stable par $u$
    admet un supplémentaire stable par $u$. On peut donc 
    trouver une matrice de $u$ sous la forme~:


    \begin{center}
        \begin{tikzpicture}
            \matrix (M) [matrix of math nodes,left delimiter=(,right delimiter=),column sep=2mm,row sep=2mm]
                { 
                    E_1 &  &  (0)  \\
                        &      ~ &   \\
                    (0) &       &  E_k  \\
                };
            \draw (M-1-1.north west) rectangle (M-1-1.south east);
            \draw (M-3-3.north west) rectangle (M-3-3.south east);
            \draw[loosely dotted, thick] (M-2-2.north west) -- (M-2-2.south east);
        \end{tikzpicture}
    \end{center}

    Où les seuls sous-espaces $u$-stables de $E_i$ sont $\{0\}$ et $E_i$.
\end{application}

\begin{remarque}
    Si $F$ est $u$-stable alors $\pi_{u_F} \mid \pi_u$, 
    de plus si $E = F \oplus G$ avec $F$ et $G$ $u$-stables,
    $\pi_u = \text{ppcm}(\pi_{u_F},\pi_{u_G})$.
\end{remarque}

\begin{lemme}[Lemme des noyaux]
    Soit $(P_k)_{1 \leq k \leq N}$ une famille de polynômes 
    deux à deux premiers entre eux et un endomorphisme $u \in \Lin{E}$, on a~:

    \begin{equation*}
        \ker \left( \prod_{k = 1}^N P_k (u) \right)
        = 
        \bigoplus_{k = 1}^N \ker P_k (u) 
    \end{equation*}

    De plus le projecteur sur chacun des sous-espaces est un polynôme
    en $u$.
\end{lemme}

\begin{application}
    L'endomorphisme $T$ de $M_n(k)$ 
    qui à $A$ associe ${}^t A$ a pour polynôme 
    minimal $X^2 - 1$ si $-1 \neq 1$ dans $k$. On a alors~:

    \begin{equation*}
        M_n (k) = S_n (k) \oplus A_n (k)
    \end{equation*}

    Et les projections sont $\pi_1 (A) = \frac{1}{2}( A + T(A))$
    et $\pi_2(A) = \frac{1}{2}(A - T(A))$. Ce théorème
    se généralise à toute symétrie, cela même en dimension infinie 
    (parité dans $\mathcal{F}(X,\mathbb{R})$).
\end{application}

\begin{exemple}[Étude via le spectre] 
    Si $\pi_u = (X - \lambda_1) \cdots (X - \lambda_p)$ 
    scindé à racines simples, alors pour $P \in k[X]$~:

    \begin{equation*}
        P(u) = \sum_{k = 0}^p P(\lambda_k) L_k(u)
    \end{equation*}

    Où $L_k$ est le $k$-ème polynôme interpolateur de 
    Lagrange sur les valeurs $\lambda_1, \dots, \lambda_p$.
\end{exemple}


\subsection{Par l'intérieur}


\begin{definition}[Sous-espace cyclique]
    Soit $u \in \Lin{E}$ et $x \in E$, 
    on considère le morphisme d'algèbres $P \mapsto P(u)(x)$.
    Comme pour le polynôme minimal, on déduit de la non-injectivité 
    l'existence d'un polynôme $\pi_{u,x}$ tel que~:
    \begin{equation*}
        \faktor{k[X]}{(\pi_{u,x})} \simeq \{ P(u)(x) ~|~ P \in k[X] \} =
        \cycl{x}{u} 
    \end{equation*}
\end{definition}

\begin{remarque}
    L'espace $\cycl{x}{u}$ est naturellement un sous espace vectoriel de $E$
    $u$-stable de dimension $\deg \pi_{u,x}$. De plus c'est le plus 
    petit espace vectoriel $u$-stable contenant $x$.
\end{remarque}

\begin{remarque}
    Une base de $\cycl{x}{u}$ est $(x,u(x), \dots, u^{l-1}(x))$
    où $l = \deg \pi_{u,x}$, ce qui justifie l'appellation.
    De plus dans cette base $u_{\cycl{x}{u}}$ est $C(\pi_{u,x})$.
\end{remarque}

\begin{remarque}
    Si $x \in E$ et $u \in \Lin{E}$ alors $\pi_{u,x} \mid \pi_u$.
\end{remarque}

\begin{definition}[Endomorphisme cyclique]
    Un endomorphisme $u \in \Lin{E}$ est cyclique si et seulement s'il 
    existe $x \in E$ tel que $E = \cycl{x}{u}$. 
    Dans un espace de dimension $n$ cela revient à dire que $\deg \pi_u = n$.
\end{definition}

\begin{exemple}
    En dimension 2, un endomorphisme $u$ est soit cyclique soit une homothétie.
\end{exemple}

\begin{propriete}
    $u$ est cyclique si et seulement si $\chi_u = \pi_u$,
    si et seulement s'il existe une matrice de $u$
    de la forme $C(\pi_u)$
\end{propriete}

\begin{propriete}[Sous-espace]
    Un sous espace $u$-stable d'un espace cylique 
    est cyclique
\end{propriete}

\begin{propriete}[Cyclicité et espaces stables]
    Si $u$ est cyclique alors $u$ n'admet qu'un nombre 
    fini de sous-espaces stables. La réciproque 
    est vraie quand $k$ est infini.
\end{propriete}

\begin{propriete}[Polynôme minimal ponctuel]
    Si $u \in \Lin{E}$ alors il existe $x \in E$ tel que 
    $\pi_{u,x} = \pi_u$
\end{propriete}

\begin{theoreme}[Invariants de similitude]
    Soit $u \in \Lin{E}$, il existe une unique famille de polynômes 
    unitaires $Q_1, \dots, Q_l$ qui vérifie $Q_l \mid \dots \mid Q_1$
    telle que la matrice suivante soit une matrice de $u$~:

    \begin{center}
        \begin{tikzpicture}
            \matrix (M) [matrix of math nodes,left delimiter=(,right delimiter=),column sep=2mm,row sep=2mm]
                { 
                    C(Q_1) & & &  (0)     \\
                           & & &          \\
                           & & &          \\
                    (0)    & & &  C(Q_l)  \\
                };
            \draw[loosely dotted, thick] (M-1-1.south east) -- (M-4-4.north west);
        \end{tikzpicture}
    \end{center}
\end{theoreme}

\begin{application}
    Si $A$ est semblable à $B$ dans une extension $K$ de $k$,
    alors $A$ est semblable à $B$ dans $k$.
\end{application}

\begin{theoreme}[\Dev{Bi-commutant}]
    Soit $u \in \Lin{E}$, alors  
    $\Comm{\Comm{u}} = k[u] $
\end{theoreme}

\begin{lemme}[Caractérisation des endomorphismes cycliques]
    Si $u \in \Lin{E}$ alors
    \begin{equation*}
        k[u] = \Comm{u}
        \iff
        u \text{ est cyclique }
    \end{equation*}
\end{lemme}


\begin{propriete}
    Les invariants de Smith de la matrice $M - XI$ 
    dans l'anneau $k[X]$ sont de la forme $1, \dots, 1, Q_l, \dots, Q_1$.
    De plus les polynômes $Q_1, \dots, Q_l$ 
    sont exactement les invariants de similitude de $M$.
    Comme l'anneau est euclidien cela donne une méthode effective de calcul 
    des invariants de similitude.
\end{propriete}

\begin{propriete}[Cayley-Hamilton]
    Le polynôme minimal divise le polynôme caractéristique.
    De plus ils ont les mêmes facteurs irréductibles.
\end{propriete}

\begin{application}
    Le polynôme minimal du morphisme de frobénius 
    de $\mathbb{F}_{p^n}$ est $X^n - 1$, c'est donc 
    aussi le polynôme caractétirstique et 
    le déterminant de l'endomorphisme de frobénius 
    est donc $(-1)^{n+1}$
\end{application}


\subsection{En scindant des polynômes}

\begin{propriete}[Lien racine valeur-propre]
    Les propriétés suivantes sont équivalentes 
    \begin{inparaenum}[(i)]
    \item $\lambda$ est valeur propre de $u$
    \item $\pi_u (\lambda) = 0$
    \item $\chi_u (\lambda) = 0$
    \end{inparaenum}
\end{propriete}

\begin{propriete}[Diagonalisabilité]
    Les propriétés suivantes sont équivalentes:
    \begin{compactenum}[(i)]
    \item $u$ est diagonalisable
    \item $\pi_u$ est scindé à racines simples
    \item $\chi_u$ est scindé et 
        la multiplicité algébrique de $(X - \lambda_i)$
        est égale à la dimension de $\Ker (u - \lambda_i id_E)$
    \end{compactenum}
\end{propriete}

\begin{exemple}
    $u$ est diagonalisable dans $\Lin{\mathbb{F}_q}$
    (comme $\mathbb{F}_p$ espace vectoriel) si et 
    seulement si $X^q - X$ annule $u$
\end{exemple}

\begin{propriete}[Trigonalisabilité]
    Les propriétés suivantes sont équivalentes:
    \begin{compactenum}[(i)]
    \item $u$ est trigonalisable 
    \item $\pi_u$ est scindé 
    \item $\chi_u$ est scindé 
    \end{compactenum}
\end{propriete}

\begin{theoreme}[\Dev{Décomposition de Dunford}]
    Soit $u \in \Lin{E}$ avec $\pi_u$ scindé,
    il existe un unique couple $(d,v)$ avec 
    $d$ diagonalisable, $v$ nilpotent, 
    $d$ et $v$ commutent et $u = d + v$.
    De plus $d$ et $v$ sont des polynômes en $u$.
\end{theoreme}

\begin{propriete}
    [Ici on cache la semi-simplicité]
    Si on suppose $\pi_u$ scindé, alors~:
    \begin{compactenum}
        \item Si $\pi_u$ est à racine 
            simples $k[u]$ est isomorphe à un produit 
            de corps et le seul élément nilpotent de $k[u]$ est $0$
        \item Les seuls éléments idempotents de $k[u]$ sont 
            les sommes de projecteurs sur les sous-espaces 
            caractéristiques de $u$
    \end{compactenum}
\end{propriete}



\section{Dans $k = \mathbb{R}$ ou $\mathbb{C}$}

\begin{propriete}[Topologie et classes de similitude]
    On note $\sigma (u)$ la classe de similitude de $u$
    dans $\Lin{E}$.
    \begin{compactitem}
        \item $u$ est diagonalisable sur $\mathbb{C}$ 
            si et seulement si 
            $\sigma(u)$ est fermée
        \item $u$ est trigonalisable sur $\mathbb{R}$
            si et seulement si 
            $\overline{\sigma(u)}$ contient une matrice 
            diagonale
        \item $u$ est nilpotente si et seulement si 
            $0 \in \overline{\sigma(u)}$
    \end{compactitem}
\end{propriete}

\begin{propriete}
    L'ensemble des matrices compagnon est un ouvert
    connexe dense de $\Lin{E}$
\end{propriete}

\begin{application}
    Le polynôme minimal n'est pas continu en la matrice 
\end{application}

\begin{propriete}
    ~
    \begin{compactenum}
    \item L'intérieur des matrice diagonalisables sont les 
        matrices avec toutes les valeurs propres distinctes 
    \item L'ensemble des matrices diagonalisables est dense
        dans les les matrices à coefficients dans $\mathbb{C}$ 
    \item $\GLin{E}$ est un ouvert dense de $\Lin{E}$
    \item L'ensemble des matrices trigonalisables est fermé
        si $k = \mathbb{R}$ (trivial quand $k = \mathbb{C}$)
    \item Le rang est semi-continu inférieurement
    \end{compactenum}
\end{propriete}


\begin{definition}
    Toute fonction analytique $f$ de rayon de convergence $+\infty$
    s'étend sur $\Lin{E}$ et pour chaque $u \in \Lin{E}$ il existe 
    un polynôme $P$ tel que $P(u) = f(u)$.

    De plus si $f$ est injective, il existe un polynôme $Q$ 
    tel que $Q(f(u)) = u$.
\end{definition}

\begin{exemple}
    $\exp A$ est diagonalisable si et seulement si $A$ 
    est diagonalisable 
\end{exemple}

\begin{application}
    Étude des EDO linéaires à coefficients 
    constants.
    Calcul d'exponentielle en utilisant la réduction 
    (méthode de "je diagonalise la dérivation")
\end{application}


\begin{application}[Théorème de Kronecker]
    Soit $P$ un polynôme dans $\mathbb{Z}[X]$ dont 
    toutes les racines sont de module inférieur à $1$.
    Alors toutes les racines de $P$ sont des racines 
    de l'unité.
\end{application}


\begin{propriete}[Localisation des valeurs propres]
    Localisation des valeurs propres de $M$ via les disques
    de Gershgorin
\end{propriete}

\begin{theoreme}[Brauer \cite{mansuyAlgebre}]
    Deux permutations sont conjugées dans $\mathfrak{S}_n$
    si et seulement si les matrices de permutations associées 
    sont conjugées dans $\GLin{E}$.
\end{theoreme}

\begin{theoreme}[Diagonalisation des homographies]
    étude asymptotique des suites $u_{n+1} = f(u_n)$
    en utilisant de la réduction 
\end{theoreme}


\begin{definition}[Invariance]
    Un polynôme $P$ est invariant quand pour tout $g \in M_n (\mathbb{C})$ 
    inversible et $M \in M_n (\mathbb{C})$ $P(g^{-1} M g) = P(M)$.
\end{definition}

\begin{theoreme}[Polynômes invariants sur $M_n(\mathbb{C})$]
    Soit $P$ un polynôme invariant sur $M_n(\mathbb{C})$, 
    il existe un unique polynôme $Q$ tel que
    
    \begin{equation*}
        P(M) = Q( \sigma_1(M), \dots, \sigma_n(M))
    \end{equation*}

    Avec $\chi_M = X^n - \sigma_1 (M) X^{n-1} + \dots + (-1)^n \sigma_n (M)$.
\end{theoreme}

\newpage

\appendix

\bibliographystyle{alpha}
\bibliography{ressources}

\url{http://www.math.cmu.edu/~ldietric/doc/idempotents.pdf}
~\\~
\url{http://math.univ-lille1.fr/~serman/agreg/alglineaireCours.pdf}

\end{document}


