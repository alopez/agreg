\begin{references}
    \item[FGN Algèbre 1] page 14
\end{references}

\subsection{Développement}

On définit $B_n$ comme le nombre de partitions de $\llbracket 1, n \rrbracket$.
C'est-a-dire le cardinal de $A_n$ où 

\begin{equation}
    A_n = \left\{ X \subseteq \mathcal{P} (\llbracket 1, n \rrbracket) ~|~
            \bigsqcup_{x \in X} x = \llbracket 1, n \rrbracket \right\}
\end{equation}

On pose par convention $B_0 = 1$.

\begin{description}
    \item[Calculs de premiers termes]

        On calcule aisément $B_1 = 1$, et 
        $B_2 = 2$.

        On constate que $A_3 = \left\{ \{ \emptyset, \{ 1,2,3\} \},
            \{ 1, \{ 2,3\} \}, \{ 2, \{ 1,3\} \}, \{ 3, \{ 1,2
        \}\}\right\}$.
        Et donc $A_3 = 4$.

    \item[Formule de récurrence]

        Soit $n \in \mathbb{N}^*$ et $k \in \llbracket 1, n \rrbracket$.
        On note $E_k$ l'ensemble des $X \in A_{n+1}$ tels que 
        l'ensemble contenant $n+1$ est de taille $k$.

        \begin{equation}
            A_{n+1} = \bigsqcup_{k = 1}^{n+1} E_k
        \end{equation}

        Et donc 

        \begin{equation}
            B_{n+1} = \sum_{k = 1}^{n+1} |E_k|
        \end{equation}

        Or, il est évident que $|E_k| = { n \choose k - 1 } \times B_{n+1 - k}$
        puisqu'on sélectionne les voisins de $n+1$, et une partition des
        éléments restants.

        Ainsi

        \begin{equation}
            B_{n+1} = \sum_{k = 1}^{n+1} { n \choose k - 1} B_{n+1 - k} 
            = \sum_{k = 0}^n { n \choose k } B_{n - k}
            = \sum_{k = 0}^n { n \choose k } B_k
        \end{equation}

    \item[Construction de la série génératrice]
        

        \begin{equation}
            f(x) = \sum_{n \geq 0} B_n \frac{x^n}{n!} 
        \end{equation}

        Montrons que cette fonction est bien définie sur $[0,1[$.
        Pour cela il suffit de remarquer que $B_n \leq n!$ et 
        constater que c'est une série entière de rayon de convergence
        supérieur à $1$.

        En effet, on a l'injection suivante de $A_n$ dans $S_n$
        qui à une partition $X$ associe le produit de cycles à supports
        disjoints où chaque cycle est un cycle sur un élément de $X$.

    \item[Équation différentielle vérifiée par la série]

        Sur l'intervalle $[0,1[$ la fonction $f$ est $C^\infty$ 
        et on peut dériver terme à termes.

        \begin{align}
            f'(x) &= \sum_{n \geq 1} B_n n \frac{x^{n-1}}{n!} \\
                  &= \sum_{n \geq 0} B_{n+1} \frac{x^n}{n!} \\
                  &= \sum_{n \geq 0} \sum_{k = 0}^n { n \choose k } B_k \frac{x^n}{n!}  \\
                  &= \sum_{n \geq 0} \sum_{k = 0}^n B_k \frac{x^n}{k! (n-k)!}  \\
                  &= \sum_{n \geq 0} x^n \sum_{k = 0}^n \frac{B_k}{k!} \frac{1}{(n-k)!}  \\
                  &= \left(\sum_{n \geq 0} \frac{B_n}{n!} x^n\right)
            \left(\sum_{n \geq 0} \frac{1}{n!} x^n\right) & \text{ Produit de
            Cauchy} \\
                &= f(x) e^x
        \end{align}

    \item[Étude de la solution]

        On résout facilement cette EDO linéaire d'ordre 1 

        \begin{equation}
            f(x) = \frac{f(0)}{e} e^{e^x}
            = \frac{1}{e} e^{e^x}
        \end{equation}

        On va donc étudier $e^{e^x}$, 

        \begin{align}
            e^{e^x} &= \sum_{n \geq 0} \frac{(e^x)^n}{n!} \\
                    &= \sum_{n \geq 0} \frac{e^{nx}}{n!} \\
                    &= \sum_{n \geq 0} \sum_{k \geq 0} \frac{(nx)^k}{n!} 
        \end{align}

        On veut utiliser Fubini, et pour cela on 
        étudie la somme suivante~:

        \begin{align}
            \sum_{n \geq 0} \sum_{k \geq 0} \left|\frac{(nx)^k}{n!}  \right|
            &= \sum_{n \geq 0} \frac{\left(e^{|x|}\right)^n}{n!}  \\
            &= e^{e^{|x|}} < +\infty
        \end{align}

        Donc on peut continuer notre calcul 
        \begin{align}
            \sum_{n \geq 0} \sum_{k \geq 0} \frac{(nx)^k}{n!} &= 
            \sum_{k \geq 0} \sum_{n \geq 0} \frac{(nx)^k}{n!} \\
            &= \sum_{k \geq 0} \frac{x^k}{k!} \left(\sum_{n \geq 0}
            \frac{n^k}{n!} \right)\\
            &= \sum_{n \geq 0} \frac{x^n}{n!} \left(\sum_{k \geq 0}
            \frac{k^n}{k!} \right)\\
        \end{align}


    \item[Conclusion]

        On peut alors conclure car $f(x) = \frac{1}{e} e^{e^x}$
        sur $[0,1[$, par unicité du DSE on déduit que le rayon 
        de convergence de $f$ est en réalité $+\infty$ et que 

        \begin{equation}
            \frac{B_n}{n!} = \frac{1}{e n!} \sum_{k \geq 0} \frac{k^n}{k!} 
        \end{equation}

        \begin{equation}
            B_n = \frac{1}{e} \sum_{k \geq 0} \frac{k^n}{k!} 
        \end{equation}
\end{description}

\subsection{Remarques}

\begin{description}
    \item[Donner un équivalent est difficile]
        
    \item[On peut trouver en triffouillant] 
        $\ln B_n \sim n \ln n$
        ce qui ne donne pas d'équivalent de $B_n$
        mais dit en gros que ça devrait pas être 
        trop loin de $n^n$ au final.
\end{description}
