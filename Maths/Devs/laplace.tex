\begin{references}
    \item[ZQ]
    \item[Rouvière] page 344
\end{references}


\subsection{Développement}

On considère $I = [a,b[$ un intervalle de $\mathbb{R}$,
$\phi : I \to \mathbb{R}$ et $f : I \to \mathbb{C}$.

On pose

\begin{equation}
    F(\lambda) = \int_I e^{- \lambda \phi(x)} f(x) dx 
\end{equation}

On suppose que pour $\lambda \geq \lambda_0$ 
on a $F(\lambda)$ qui converge absolument, c'est-à-dire 
que $e^{-\lambda \phi(x) f(x)}$ est intégrable.

L'objectif est d'étudier le comportement de 
$F$ quand $\lambda \to +\infty$.


\begin{description}
    \item[Trouvons la masse] 

        Le terme exponentiel écrase tout assez vite quand $\lambda 
        \to +\infty$. La contribution majoritaire va donc 
        se faire quand $\phi(x)$ est minimale. Ce qui justifie
        le bloc suivant.

        \begin{quote}
            Supposons que $\phi$ vérifie les hypothèses suivantes~:
            $\phi' > 0$ sur $I - \{ a \}$, $\phi'(a) = 0$ et $\phi''(a) > 0$.
            On ajoute que $f$ est continue en $a$ et $f(a) \neq 0$.
        \end{quote}

    \item[Étude de $\phi$]
        Alors localement $\phi$ s'écrit 

        \begin{equation}
            \phi(x) = \phi(a) + 0 + \frac{1}{2} \phi''(a) (x - a)^2 + o \left((x - a)^2\right)
        \end{equation}

        On a donc un terme en $e^{-\lambda \phi(a)}$,
        et un terme qui se comporte au voisinage de $a$ comme $(x-a)^2$.

        Cela justifie d'étudier dans un premier temps 
        le terme en $(x - a)^2$, et pour simplifier encore 
        de considérer $a = 0$...

    \item[Le cas particulier]

        On considère donc $\boxed{\phi (x) = x^2}$. Les précédentes 
        remarques invitent à découper l'intégrale au voisinage de zéro, 
        et en dehors. 

        On se sert de la continuité de $f$ au voisinage de $0$
        pour obtenir $\eta > 0$ et $M > 0$ tels que 

        \begin{equation}
            \forall x \in I, x \leq \eta \implies |f(x)| \leq M
        \end{equation}

        On a alors 

        \begin{equation}
            \int_0^\eta e^{- \lambda x^2} f(x) dx 
            = 
            \frac{1}{\sqrt{\lambda}} \int_0^\eta e^{- u^2}
            f\left(\frac{u}{\sqrt{\lambda}}\right) du
        \end{equation}

        Par convergence dominée, on déduit alors 

        \begin{equation}
            \int_0^{\eta \sqrt{\lambda}} e^{-u^2}
            f\left(\frac{u}{\sqrt{\lambda}}\right) du
            \longrightarrow \int_0^+\infty e^{-u^2} f(0) du 
        \end{equation}

        En utilisant la valeur de l'intégale de Gauss on déduit 
        donc 

        \begin{equation}
            \int_0^\eta e^{- \lambda x^2} f(x) dx 
            \sim 
            \frac{f(0)}{2} \sqrt{\frac{\pi}{\lambda}}
        \end{equation}

    \item[Dans le cas général]

        On veut se ramener au cas particulier, et au vu du développement 
        limité, il est très naturel de poser le changement de variable 
        suivant

        \begin{equation}
            \phi(x) = \phi(a) + 0 + \frac{1}{2} \phi''(a) (x - a)^2 + o \left((x - a)^2\right)
        \end{equation}

        \begin{equation}
            \Phi (x) = \sqrt{\phi(x) - \phi(a)}
        \end{equation}

        \begin{enumerate}[(i)]
            \item On constate que $\Phi$ est bien $C^1$ sur $I - \{ a \}$
                par simple composition 
                et que 

                \begin{equation}
                    \Phi'(x) = \frac{\phi'(x)}{2 \sqrt{\phi(x) - \phi(a)}} > 0
                \end{equation}

            \item En utilisant le développement limité, on considère la limite 
                comme suit
                \begin{equation}
                    \frac{\phi'(x)}{\sqrt{2} \sqrt{(x - a)^2 (\phi''(a) + o(1))}}
                    = 
                    \frac{\phi'(x)}{\sqrt{2} (x - a) \sqrt{\phi''(a) + o(1))}}
                \end{equation}

                Comme $\phi'(x) = 0 + \phi''(a) \frac{(x - a)}{2} + o(x - a)$

                On en déduit que

                \begin{equation}
                    \Phi'(x) \longrightarrow \sqrt{\frac{\phi''(a)}{2}} > 0
                    \quad \quad (x \rightarrow 0)
                \end{equation}
        \end{enumerate}

        On a donc $\Phi$ qui est $C^1$ sur $I$ et de différentielle 
        inversible. C'est donc un $C^1$-difféomorphisme (inversion locale)
        et on conclut donc à un "lemme de Morse en une variable"~:

        \begin{equation}
            \phi(x) = \phi(a) + \Phi(x)^2
        \end{equation}
        
        On note $\psi$ l'application réciproque, en posant $x = \psi(u)$ 
        on a le changement de variables 

        \begin{equation}
            F(\lambda) 
            = 
            \int_a^b e^{-\lambda (\phi(a) + \Phi(x)^2} f(x) dx 
            = 
            e^{-\lambda \phi(a)}
            \int_{\Phi(I)} e^{-t u^2} f(\psi(u)) |\psi'(u)| du 
        \end{equation}

        On déduit alors qu'il existe $c > 0$ tel que 

        \begin{equation}
            F(\lambda) = 
            e^{-\lambda \phi(a)}
            \int_0^c e^{-t u^2} f(\psi(u)) \psi'(u) du 
        \end{equation}

        En appliquant le théorème démontré pour le cas particulier, on 
        a alors

        \begin{equation}
            F(\lambda) \sim 
            e^{-\lambda \phi(a)}
            \frac{f(\psi(0)) \psi'(0)}{2} \sqrt{\frac{\pi}{\lambda}}
            = 
            e^{-\lambda \phi(a)}
            f(a) \sqrt{\frac{1}{2\phi''(a)}}
            \sqrt{\frac{\pi}{\lambda}}
        \end{equation}

    \item[Application à $\Gamma$]

        \begin{equation}
            \Gamma(t+1) = \int_0^\infty x^t e^{-x} dx 
            = 
            \int_0^\infty 
            \exp \left( 
                -[x - t\ln (x)]
            \right)
        \end{equation}

        Ce n'est pas une intégrale sous la bonne forme 
        pour appliquer notre théorème. Toutefois, on 
        peut essayer de calculer le minimum de la fonction 
        pour effectuer un changement de variable qui "suit" 
        ce minimum.

        On pose $g_t(x) = x - t\ln x$
        on calcule  $g_t'(x) = 1 - t/x$.
        Cela montre que $g_t$ est strictement convexe 
        sur $\mathbb{R}^+$ avec un minimum en $1$, c'est-à-dire
        quand $x = t$.

        On pose donc $u = x/t$, avec $dx = t du$, ce qui donne

        \begin{equation}
            \Gamma(t+1) 
            = 
            \int_0^\infty 
            \exp \left( 
                -[tu - t\ln (tu)]
            \right)
            t du 
            = 
            t^{t+1}
            \int_0^\infty 
            \exp \left( 
                -t[u - \ln (u)]
            \right)
            du 
        \end{equation}

        Il suffit alors de couper l'intégrale au niveau 
        de $1$, et d'utiliser deux fois le théorème 
        pour obtenir~:

        \begin{equation}
            \Gamma(t+1)
            \sim 
            t^{t+1} \times 
            2 \times 
            e^{-t \phi(1)}
            \sqrt{\frac{1}{2\phi''(1)}}
            \sqrt{\frac{\pi}{t}}
        \end{equation}

        Or il est clair que $\phi(1) = 1$
        et $\phi''(1) = 1$, donc 

        \begin{equation}
            \Gamma(t+1)
            \sim 
            t^t
            e^{-t}
            \sqrt{2\pi t}
        \end{equation}

        Ce qui donne la formule de Stirling

        \begin{equation}
            n! \sim \left(\frac{n}{e}\right)^n \sqrt{2\pi n}
        \end{equation}

\end{description}







\subsection{Annexes}


\begin{description}
    \item[Améliorations]
        On peut se ramener à $\phi$ admettant 
        un minimum local strict en $a$
        et avoir le même équivalent

\end{description}


