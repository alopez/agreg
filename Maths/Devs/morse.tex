\begin{references}
\item[Rouvière] page 344, exercice 114
\item[Rouvière] page 201, exercice 66
\end{references}


\begin{description}
    \item[Notations]
        On considère $f : U \to \mathbb{R}$ de classe $\mathcal{C}^3$,
        qui vérifie $df_0 = 0$ et 
        $d^2f_0$ est non dégénérée de signature $(p, n - p)$.
    \item[Objectif]
        Ramener l'étude de $f$ en $0$ à celle du polynôme 
        $x_1^2 + \dots + x_p^2 - x_{p+1}^2 - \dots - x_n^2$
        via un $\mathcal{C}^1$ difféomorphisme
\end{description}

Naturellement, on écrit la formule de Taylor avec reste intégral 
à l'origine~:

\begin{equation}
    f(x) - f(0) = \int_0^1 (1 - t) d^2 f_{tx} (x,x) dt 
\end{equation}

Cette équation peut se ré-écrire
\begin{equation}
    f(x) - f(0) = {}^t x Q(x) x
\end{equation}

\todo[inline]{MORSE: Dérivable oui, mais pour quelle notion de variété ?}
Où $Q(x)$ est la forme quadratique définie ci-dessous, qui varie
de manière $\mathcal{C}^1$ en $x$.

\begin{equation}
    Q(x) = \int_0^1 (1 - t) D^2 f_{tx} dt 
\end{equation}

L'objectif est donc de réduire la forme $Q(x)$
au voisinage de $x = 0$.

Une première étape est d'écrire localement
$Q(x) = {}^t M(x) Q(0) M(x)$ avec $M(x)$ 
assez régulière. Par la suite, une réduction de $Q(0)$
via le théorème d'inertie de Sylvester 
nous amènera au changement de variable désiré.


\subsection{Existence de la fonction $M$}

On pose pour simplifier les notations $A = Q(0) \in S_n \cap GL_n$.
Naturellement, on considère l'application $\phi : M \mapsto {}^t M A M$,
qui part de $\mathcal{M}_n$ vers $S_n$ (deux espaces vectoriels, donc variétés).
Cette application étant polynômiale, elle est en particulier $\mathcal{C}^1$.

De plus $\phi(I_n) = A$. On va donc rechercher un voisinage de l'identité 
où $\phi$ est un $\mathcal{C}^1$-difféomorphisme.

\begin{equation}
    D\phi_{I_n} (H) = {}^t H A + AH = {}^t (AH) + AH
\end{equation}

En effet $A$ est  symétrique. Comme $A$ est inversible,
on déduit directement $\ker D\phi_{I_n} = A^{-1} A_n(\mathbb{R})$.

\begin{center}
    \includegraphics{images/morse_restriction.pdf}
\end{center}

Ainsi, il n'est pas possible d'utiliser directement le théorème 
d'inversion locale. Toutefois $\mathcal{M}_n = A^{-1} S_n \oplus A^{-1} A_n$,
et donc en considérant $\psi$ la restriction de $\phi$ à $A^{-1} S_n$,
un sous espace vectoriel qui contient l'identité on a cette fois
$D\psi_{I_n}$ injective par construction.

De plus, par un argument de dimension, $D \psi_{I_n}$ est surjective,
et donc inversible.


On peut alors utiliser le théorème d'inversion locale pour trouver 
un voisinage $V$ de $I_n$ dans $A^{-1} S_n$ tel que $\psi_{|V}$ soit 
un $\mathcal{C}^1$-difféomorphisme sur son image (qui contient $A$).


Alors, quitte à supposer $V \subseteq GL_n$ (ce qui est possible 
car $GL_n$ est un ouvert de $\mathcal{M}_n$) on a par construction

\begin{equation}
    \forall B \in \psi (V),
    B = {}^t \psi^{-1}(B) A \psi^{-1}(B)
\end{equation}

Avec $\psi^{-1}$ un $\mathcal{C}^1$ difféomorphisme, et de plus 
$\psi^{-1}(B)$ inversible donc représentant un changement de base.

\subsection{Application à la réduction de $f$}

On peut alors réduire $f$ comme souhaité. 
D'une part le théorème d'inertie de Sylvester donne une matrice $P \in GL_n$
telle que 
\begin{equation}
    {}^t P Q(0) P = \begin{pmatrix} I_p & (0) \\ (0) & - I_{n-p} \end{pmatrix}
\end{equation}

D'autre part comme $Q(x)$ est $\mathcal{C}^1$ en $x$, il existe un voisinage $W$ 
de $0$ dans $\mathbb{R}^n$ tel que $Q(W) \subseteq V$. Alors en utilisant 
$\psi$ on peut écrire

\begin{equation}
    \forall x \in W,  Q(x) = {}^t (\psi^{-1}(Q(x))) Q(0) \psi^{-1}(Q(x))
\end{equation}

Ainsi, le "changement de variable dépendant de $x$" $M_x  = \psi^{-1} (Q(x)) P$ 
amène effectivement à la forme suivante

\begin{equation}
    \forall x \in W,
    {}^t x Q(x) x = {}^t (M_x x) \begin{pmatrix} I_p & 0 \\ 0 & - I_{n-p}
    \end{pmatrix} (M_x x)
\end{equation}

Reste à vérifier que le changement de variable $x \mapsto M_x x$ est bien 
un $\mathcal{C}^1$ difféomorphisme.

Pour cela on constate déjà qu'il est $\mathcal{C}^1$ car $M_x$ a la bonne 
régularité. De plus $M_0 = P$ donc $M_0 0 = 0$.

\begin{equation}
    D (M_x x)_0 \cdot h = M_0 h + (D (M_x)_0 \cdot h) h = M_0 h + o (\| h \|)
\end{equation}

Mais $M_0 = P$ et est donc inversible, donc quitte à utiliser le théorème d'inversion
on peut supposer que $M_x x$ est un $\mathcal{C}^1$ difféomorphisme
de $W$ voisinage de $0$ vers $(M_x x) (W)$ voisinage de $0$.

\begin{center}
    \includegraphics[width=10cm]{images/morse_diffeo.png}
\end{center}


\subsection{Application en dimension deux}

On peut appliquer ce lemme dans l'exercice $111$ du Rouvière 
pour regarder la position par rapport au plan tangent.

On considère $S$ la surface d'équation $f(x,y) = z$ avec $f$ 
de classe $\mathcal{C}^3$ au voisinage d'un point $a$.
On suppose que la forme $d^2 f_a$ est non dégénérée, et 
on veut discuter de la position relative de $S$ par rapport 
à son plan tangent.

On commence par se ramener en $a = 0$ via une simple translation
(qui ne change pas la Hessienne).

On déduit alors du lemme de morse qu'il existe un $\mathcal{C}^1$
difféomorphisme local $h = (x,y) \mapsto (u(x), v(y))$ tel que

\begin{equation}
    \delta (h) = f(h) - (f(0) + Df_{0} (h)) = \varepsilon_1 u(x)^2 + \varepsilon_2 v(x)^2
\end{equation}

\begin{center}
    \includegraphics{images/morse_dim2_positif.pdf}
\end{center}

Ainsi, une signature $(+,+)$, $\delta(h) > 0$ localement, donc 
$S$ est strictement au dessus de son plan tangent en $a$.
Si la signature est $(-,-)$ par symétrie on a une surface 
strictement en dessous de son plan tangent en $a$.

Pour une signature $(+,-)$, on constate que localement 
$\delta(h)$ peut être strictement positif et strictement 
négatif, donc la sous variété intersecte de plan tangent 
(une partie est supérieure, l'autre inférieure).

\begin{center}
    \includegraphics{images/morse_dim2_alterne.pdf}
\end{center}


\subsection{Post requis}

\begin{enumerate}
    \item En fait ce théorème reste vrai pour $f$ de classe seulement
        $\mathcal{C}^1$ et possédant une différentielle seconde non dégénérée en
        $0$
    \item Si $D^2f$ est dégénérée, alors il faut aller plus loin dans le
        développement et on ne peut pas dire grand chose.
        Exemple $x^2 + x^3$ ne peut pas s'écrire de cette manière.
\end{enumerate}
