\begin{references}
    \item Objectif Agrégation 
    \item Demazure
\end{references}

\subsection{Polynômes dans les corps finis}

Soit $q$ une puissance d'un ombre premier $p$ et $P \in \cfin{q}[X]$.

\begin{propriete}
    Si $P' = 0$ alors il existe $Q \in \cfin{q}[X]$
    tel que $P = Q(X)^p$. 
    Sinon, soit $P \wedge P' = 1$ et $P$ est sans facteurs 
    carré, soit $P \wedge P' \neq 1$ et $P$ possède 
    un facteur carré.
\end{propriete}

\begin{proof}
    Si $P' = 0$ il existe un $R$ tel que $P = R(X^p)$
    par identification des coefficients.

    Par la suite, en calculant les racines $p$-èmes 
    des puissances $q$-èmes des coefficients de $R$,
    on obtient un polynôme $Q$ tel que $Q(X)^p = R(X^p)$,
    et on a conclu.

    Sinon, le pgcd est un polynôme non nul, et donc 
    il vaut soit $1$ soit pas $1$, et c'est le cas classique
    où tout se passe bien.
\end{proof}

Ainsi, on peut se limiter à étudier des polynômes sans 
facteurs carré, car on peut explicitement calculer $Q$
dans le cas où la dérivée est nulle, et les pgcds dans 
les autres cas.

\subsection{Développement}

On suppose que $P$ est un polynôme dans $\cfin{q}[X]$
sans facteurs carré. On recherche un diviseur irréductible 
de $P$, on note $P_1, \dots, P_r$ les diviseurs irréductibles de $P$.

Pour cela on considère l'application 

\begin{equation}
    \begin{array}{llll}
        \psi_P : & \faktor{\cfin{q}[X]}{(P)} & \longrightarrow & \faktor{\cfin{q}[X]}{(P)}
        \\
        & Q & \mapsto & Q^q 
    \end{array}
\end{equation}

Cette application est $\cfin{q}$-linéaire car c'est une itérée 
du morphisme de Frobénius sur $\cfin{q}$.

D'un autre côté, le lemme Chinois fournit un isomorphisme 
\begin{equation}
    \faktor{\cfin{q}[X]}{(P)} \simeq 
    \left(\faktor{\cfin{q}[X]}{(P_1)} \right)
    \times \cdots \times
    \left(\faktor{\cfin{q}[X]}{(P_r)}\right)
\end{equation}

Cela permet d'étudier $\Fix \psi_P$.

Puisque les $P_i$ sont irréductibles, les 
quotients à gauche sont des corps. Donc 
$Q^q - Q = 0$ dans $\faktor{\cfin{q}[X]}{(P)}$
si et seulement si $Q^q - Q = 0$ dans 
chacun de ces corps. Or ce sont des extensions 
de $\cfin{q}$, dans lequel le polynôme $Y^q - Y$
possède exactement $q$ racines.

Donc l'isomorphisme permet de déduire qu'il y 
a exactement $q^r$ éléments dans $\Fix \psi_P$
qui est de dimension $r$.

\begin{center}
\fbox{On peut donc compter le nombre de diviseurs 
irréductibles de $P$}
\end{center}

\begin{description}
    \item[Si $r = 1$] alors $P$ est irréductible et c'est terminé
    \item[Si $r \geq 2$] alors on va trouver explicitement 
        un diviseur non-trivial de $P$, puis procéder par récurrence.

        Pour cela on remarque que si $Q$ est dans $\Fix \psi_P$
        alors il existe des $\alpha_i$ dans $\cfin{q}$ tels 
        que $Q = \alpha_i$ dans $\faktor{\cfin{q}[X]}{(P_i)}$.

        Mais alors pour $\alpha \in \cfin{q}$, $Q - \alpha$ est nul seulement dans 
        les corps où $\alpha_i = \alpha$. Ainsi $P \wedge Q - \alpha$ 
        est précisément le produit de ces $P_i$. 
        En faisant varier $\alpha$ dans $\cfin{q}$ on a donc parcouru
        tous les diviseurs irréductibles de $P$ une seule et unique fois,
        ce qui montre

        \begin{equation}
            P = \prod_{\alpha \in \cfin{q}} P \wedge (Q - \alpha)
        \end{equation}

        Seulement, si $Q$ n'est pas un polynôme constant modulo $P$
        alors les $\alpha_i$ ne sont pas tous égaux, et donc 
        un des pgcds permet d'obtenir un facteur non trivial de $P$.

        Reste à trouver un polynôme $Q$ dans $\Fix \psi_P$ 
        qui n'est pas constant. Comme sa dimension est supérieure 
        à deux, et que les polynômes constants sont fixés, 
        il existe un polynôme non constant dans $\Fix \psi_P$.
        En calculant via l'algorithme du pivot de Gauss 
        une base du noyau, on trouve donc automatiquement 
        un polynôme non constant.
\end{description}

\subsection{Question d'effectivité}

\begin{description}
    \item[Division euclidienne]
        "Division euclidienne rapide par la méthode de Newton"
        Ça fait du temps linéaire par rapport à la multiplication 
        de deux polynômes ... c'est drôlement bien !

    \item[Simplification du polynôme $P$]

        \todo[inline]{Combien d'opérations faut-il faire pour 
            rendre un polynôme sans facteurs carrés dans 
            un corps fini ?}
    
    \item[Calcul de la matrice]
        c'est facile, on prend la base canonique
        de l'espace vectoriel $1, X, \dots, X^{n-1}$ 
        et on calcule à la puissance $q$ modulo 
        $P$.

        Cette opération prend $n$ fois le temps 
        d'une division euclidienne, qui se fait en $n^2$ en gros.
        Donc le précalcul est en $n^3$.

    \item[Détermination du noyau]
        pour cela on utilise le pivot de Gauss.
        On échelonne en lignes pour déterminer 
        une base où le noyau est directement visible. 
        Cette base est effectivement calculée. Cela 
        prend 
        \begin{equation}
            \mathcal{O}(n^3)
        \end{equation}

        Avec $n = \deg P$ bien entendu,
        puisque la matrice est de taille le degré de $P$.

        La multiplication des polynômes se faisant aussi 
        en gros en $n^2$, on fait donc le calcul 
        d'une base du noyau en $n^5$.

    \item[Estimation de la complexité totale]
        On sélectionne un polynôme dans le noyau, en temps 
        linéaire.

        On fait $q$ choix pour pour $\alpha$, puis
        pour chaque $\alpha$ on fait un calcul de pgcd
        de polynômes de taille $n$. Le calcul d'un pgcd 
        demande en gros $n$ divisions euclidiennes, 
        donc on va dire qu'on fait $n^{2q}$ opérations.

    \item[Remarque corps finis]
        Il faut prendre en compte la taille des entiers !
        Si $q$ devient très grand c'est totalement irréaliste,
        et il faut ajouter du $q^x$ partout... 

        En particulier, la dernière opération devient 
        plutôt en $q^q$ ! Et là c'est grave !
            

\end{description}

\subsection{Amélioration probabiliste}

Plutôt que de parcourir $\cfin{q}$ pour et effectuer le calcul 
d'un pgcd pour chacun, on va fournir une factorisation de $P$
avec $3$ termes.

Si $Q$ est fixé par $\psi_P$, alors $Q$ correspond à un élément 
de $\cfin{q}$ dans chacun des corps du lemme Chinois.
En particulier, soit $Q = 0$, soit $Q^{\frac{q-1}{2}} = \pm 1$,
les cas étant exactement ceux du symbole de Legendre.

Ainsi, on déduit que $Q$, $Q^{\frac{q-1}{2}} - 1$ et 
$Q^{frac{q-1}{2}} + 1$ sont divisibles respectivement 
par ceux des $P_i$ où c'est nul, un résidu quadratique
ou un non-résidu quadratique.

On a donc l'écriture

\begin{equation}
    P = (Q \wedge P) \times (Q^{\frac{q-1}{2}} - 1 \wedge P)
                     \times (Q^{\frac{q-1}{2}} + 1 \wedge P)
\end{equation}

En tirant aléatoirement un élément de $\Fix \psi_P$,
ce qui est possible en tirant aléatoirement les coordonnées 
dans une base calculée par le pivot de Gauss,
on doit ensuite simplement calculer $3$ pgcds.

Le cas où les trois facteurs sont triviaux est précisément 
celui où tous les $\alpha_i$ sont de même nature.

\todo[inline]{Regarder le Demazure pour comprendre le truc}
Cela arrive avec probabilité $2/2^r$ car un tirage uniforme 
dans $\Fix \psi_P$ donne un tirage uniforme dans 
les sous corps via la bijection, et il y a la moitié 
des gens qui sont des résidus quadratiques.


