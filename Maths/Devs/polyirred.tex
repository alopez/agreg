
\begin{references}
\item[Rombaldi] page 422
    (fait les trucs un peu à la main ...)
\item[Demazure] page 220 
    (fait mieux les choses ... j'ai l'impression)
\item[Gourdon] 
    le fait aussi dans un "problème"
\end{references}

On fixe $p$ premier et on veut étudier $I_n(p)$, le nombre de polynômes 
unitaires irréductibles de degré $n$ sur $\cfin{p}$.
On note $\mathcal{U}_n(p)$ l'ensemble des polynômes unitaires irréductibles 
de degré $n$ sur $\cfin{p}$.

\subsection{Préliminaires}

Pour $\lambda \in \cfin{p}$ le polynôme $X - \lambda$ est irréductible 
unitaire de degré $1$, on conclut alors $I_1(p) = p$.

Pour le polynômes de degré $2$, il suffit de dénombrer les polynômes 
\emph{réductibles} unitaires, qui ont nécessairement une racine dans $\cfin{p}$.
Soit celle-ci est double, soit il y en a deux distinctes. On déduit alors

\begin{equation}
    I_2(p) = p^2 - \left(p + \frac{p(p-1)}{2}\right) = \frac{p(p-1)}{2}
\end{equation}

\subsection{Cas général}

On va démontrer qu'il existe des polynômes irréductibles unitaires de tout degré
supérieur à $1$ dans $\cfin{p}$, et préciser leur nombre.

On fixe $n$ un entier naturel non nul et on note $P_n = X^{p^n} - X$.

\begin{lemme}
    Pour tout $P \in \mathcal{U}_d$, ce polynôme divise $P_n$ si et seulement 
    si son degré $d$ divise $n$.
\end{lemme}

\begin{proof}
    Un polynôme $P$ de degré $d$ divise $P_n$ dans $\cfin{p}$ si et seulement si la classe de
    $P_n$ dans le corps $\cfin{p^d} = \faktor{\cfin{p}}{(P)}$ est $0$. Ce qui équivaut à 
    $X^{p^n} = X$ dans $\cfin{p^d}$.

    Mais on sait que pour les éléments de $\cfin{p}$, $x^{p^n} = x$ (théorème de
    Fermat). Ainsi, on déduit que pour tout polynôme $Q$, la classe de $Q^{p^n}$
    est égale à la classe de $Q$ dans $\cfin{p^d}$. Cela reste une équivalence.


    \begin{description}
        \item[Sens implique]

            En conséquence, pour tout $x \in \cfin{p^d}^*$, on a $x^{p^n - 1} = 1$.
            Or le groupe $\cfin{p^d}^*$ est cyclique d'ordre $p^d - 1$, et si on note
            $\omega$ un générateur, on trouve $\omega^{p^n - 1} = 1$ et donc $p^d - 1 | p^n -
            1$.

            Cela force $d | n $.

        \item[Sens implique version 2]
            Supposons $P$ irréductible de degré $d$ qui divise $X^{p^n} - X$, 
            alors en considérant $x$ une racine de $P$ dans une clôture
            algébrique on a $\cfin{q}(x)$ qui est un corps, avec
            $[\cfin{q}(x):\cfin{q}] = d$. Or, $P$ divise $X^{p^n} - X$, donc 
            cette racine $x$ est une racine de $X^{p^n} - X$ ce qui prouve 
            que $\cfin{q}(x)$ est un sous corps de $\cfin{q^n}$.

            Mais alors 
            \begin{equation}
                n = [\cfin{q^n} : \cfin{q}] = [\cfin{q^n} : \cfin{q}(x)]
                [\cfin{q} (x) : \cfin{q}] = k d
            \end{equation}

        \item[Sens réciproque]
            On sait que $n = qd$, et que $X^{p^d} = X$ dans $\cfin{p^d}$, 
            par récurrence immédiate on a $X^{p^{dk}} = X$ dans $\cfin{p^d}$
            puis $X^{p^n} = X$, ce qui permet de conclure.
    \end{description}
\end{proof}

\begin{lemme}
    Le polynôme $P_n$ est sans facteur carré, dans $\cfin{p}$, et on a la
    factorisation suivante~: 
    \begin{equation}
        X^{p^n} - X = \prod_{d | n } \prod_{P \in \mathcal{U}_d(p)} P 
    \end{equation}
\end{lemme}

\begin{proof}
    On peut simplement calculer le polynôme dérivé de $P_n$
    $P_n'(X) = p^n X^{p^n - 1} - 1 = -1$. Cela prouve $P_n' \wedge P_n = 1$ 
    et donc $P_n$ est sans facteur carré.

    Le lemme précédent permet directement de conclure pour la formule 
    en utilisant le fait qu'un polynôme irréductible n'apparaît qu'une 
    unique fois dans $P_n$.
\end{proof}

On peut donc conclure

\begin{equation}
    p^n = \sum_{d | n } \sum_{P \in \mathcal{U}_d(p)} \deg P
        = \sum_{d | n } d \times I_d(p) 
\end{equation}


On peut alors constater 

\begin{equation}
    I_n (p) \leq \frac{p^n}{n}
\end{equation}

Et en ré-injectant dans l'équation en déduire

\begin{equation}
    p^n - n I_n (p) \leq \sum_{ d | n \text{ et } d < n } p^d \leq 
    \frac{1 - p^{n/2 + 1}}{1 - p} 
    \leq p^{n/2 + 1}
\end{equation}

Donc on peut conclure

\begin{equation}
    \frac{p^n - p^{n/2 + 1}}{n} \leq I_n (p) \leq p^n / n
\end{equation}

Cela permet non seulement de déduire qu'il existe des polynômes irréductibles 
de tout degrés, mais aussi l'équivalent suivant

\begin{equation}
    I_n(p) \sim \frac{p^n}{n}
\end{equation}

\subsection{Commentaires}

La formule permet de calculer $I_n(p)$ par récurrence, et en réalité on 
peut même utiliser la formule d'inversion de Möbius pour écrire directement

\begin{equation}
    n I_n(p)  = \sum_{d | n } \mu\left( \frac{n}{d} \right) p^d 
\end{equation}
