\begin{references}
    \item[Gilles Dowek]
\end{references}


\subsection{Trois formulations équivalentes}

\begin{enumerate}[(i)]
    \item $T \vDash A$ implique $T \vdash A$
    \item $T \not\vdash A$ implique $T \not\vDash A$
    \item $T \not\vdash \bot$ implique $T \not\vDash \bot$
        ie, $T$ a un modèle 
\end{enumerate}

\begin{proof}
    Les deux premières propositions sont clairement équivalentes,
    la troisième est impliquée par la deuxième de manière aussi simple. 
    La réciproque est facile~: Si $T \not\vdash A$ alors 
    $T, \neg A \not \vdash \bot$ en raisonnant par l'absurde,
    et donc $T, \neg A$ a un modèle, donc $T$ a un modèle qui satisfait $\neg
    A$, donc $A$ n'est pas valide pour $T$.
\end{proof}

\subsection{Le modèle syntaxique}

On suppose $T$ cohérente dans le reste du développement.

On pose $M = \left\{ t\sigma ~|~ t \in T_X(\Sigma), \sigma \text{ close }
\right\}$. 

\begin{equation}
    \sem{f}{M}(t_1, \dots, t_n) = f(t_1, \dots, t_n) 
    \quad \quad 
    \sem{P}{M}(t_1, \dots, t_n) = T \vdash P(t_1, \dots t_n)
\end{equation}

\begin{exemple}
    Prenons $T = \{ P(c) \vee Q(c), \exists x. P(x) \}$
    On a 

    \begin{equation}
        \sem{P(c)}{M} = \sem{Q(c)}{M} = \bot 
        \quad \quad 
        \sem{P(c) \vee Q(c)}{M} = \sem{\exists x. P(x)}{M} = \bot
    \end{equation}

    Cela pose problème ... 
\end{exemple}

\subsection{Si la théorie est saturée}

Supposons de plus que $T$ est saturée. C'est-à-dire 
\begin{enumerate}
    \item $T$ est complète 
    \item Si $T \vdash \exists x. \phi$
        alors il existe un terme clos $t$ 
        tel que $T \vdash (t/x)\phi$.
\end{enumerate}

Le modèle $M$ satisfait alors la théorie $T$.

\begin{proof}
    Par induction sur $\phi$ on montre que 
    $T \vdash \phi \iff \sem{\phi}{M} = \top$.


    \begin{description}
        \item[Formules atomiques] 
            C'est évident par construction de $M$

        \item[Conjonction]
            Si $T \vdash \phi \wedge \psi$ alors 
            via la règle $\wedge$-elim on déduit 
            $T \vdash \phi$ et $T \vdash \psi$.

            Par hypothèse de récurrence $\sem{\phi}{M} = \sem{\psi}{M} = \top$
            puis $\sem{\phi \wedge \psi}{M} = \top$.

            Réciproquement on procède de même avec la règle 
            $\wedge$-intro.

        \item[Négation]
            Si $T \vdash \neg \phi$, 
            alors on ne peut pas avoir $T \vdash \phi$
            par cohérence. Donc par hypothèse de récurrence
            $\sem{\phi}{M} = \bot$, ce qui prouve 
            $\sem{\neg\phi}{M} = \top$.


            Si $\sem{\neg\phi}{M} = \top$, alors 
            $\sem{\phi}{M} = \bot$ et donc 
            $T$ ne prouve pas $\phi$.
            Comme $T$ est complète, cela force $T$ 
            à prouver $\neg \phi$.


        \item[Existence] 
            On procède par équivalence.

            $T \vdash \exists \phi$ si et seulement si 
            $\exists t \in M, T \vdash (t/x) \phi$
            si et seulement si $\exists t \in M, \sem{\phi}{M}(t) = \top$
            si et seulement si $\sem{\exists x. \phi}{M} = \top$.
    \end{description}
\end{proof}

\subsection{Saturation d'une théorie cohérente}


On suppose $\Sigma$ dénombrable.
On pose $U$ un ensemble dénombrable de constantes.
On numérote les formules par $\phi_i$,
les constantes via $c_i$.

On construit par itérativement une théorie $T_i$ 
sur le langage $\Sigma \uplus U$ comme suit.

\begin{equation}
    T_0 = T
\end{equation}

\begin{enumerate}
    \item Si $T_i \vdash \phi_{i+1}$ alors $T_{i+1} = T_i \cup \{ \phi_{i+1} \}$.
    \item Si $T_i \not \vdash \phi_{i+1}$ alors $T_{i+1} = T_i \cup \{ \neg \phi_{i+1}\}$.
    \item Si $\phi_{i+1} = \exists x. \phi$ alors ajouter en plus 
        l'axiome $(c_{i+1}/x) \phi_{i+1}$ à la théorie $T_i$
\end{enumerate}

On pose alors $T' = \bigcup_i T_i$.

\begin{description}
    \item[$T'$ est saturée] Par construction, une formule $\phi$ 
        est soit un axiome, soit n'est pas démontrable. 
        De plus on a bien les témoins de Henkin.

    \item[Les $T_i$ sont cohérentes]
        Par récurrence sur $i$.
        C'est le cas pour $i = 0$ par hypothèse.
        Si $T_i$ est cohérente, alors $T_{i+1}$ est cohérente
        car 

        \begin{enumerate}
            \item Si $T_i \vdash \phi_{i+1}$ alors 
                $T_{i+1} \vdash \bot$ implique $T_i, \phi_{i+1} \vdash \bot$
                puis $T_i \vdash \bot$ en substituant la règle axiome 
                par la preuve de $\phi_{i+1}$.

            \item Si $T_i \not\vdash \phi_{i+1}$ alors 
                $T_{i+1} \vdash \bot$ implique 
                $T_i, \neg \phi_{i+1} \vdash \bot$ 
                puis $T_i \vdash \phi$ (par récurrence 
                sur la dérivation)
                puis $T_i \vdash \bot$.

            \item Si $T_i, (c_{i+1}/x)\phi \vdash \bot$
                alors en utilisant la règle $\exists$-elim 
                on peut déduire $\bot$ depuis la théorie $T_i$.
        \end{enumerate}

    \item[La théorie $T'$ est cohérente]
        En effet, une théorie est incohérente si et seulement si une partie 
        finie de la théorie est incohérente. Donc $T'$ incohérente 
        si et seulement si une $T_i$ est incohérente.
\end{description}

On peut donc conclure.


