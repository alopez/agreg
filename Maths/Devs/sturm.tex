\begin{references}
    \item[ZQ]
    \item[Gourdon]
\end{references}

\subsection{Pré-requis}

On veut étudier les équations de la forme 

\begin{equation}
    y'' + py' + qy = 0
\end{equation}


\begin{description}
    \item[Le cas particulier]
        Le cas particulier c'est quand $p$ et $q$ sont constants.
        On retrouve alors les phénomènes d'oscillateur harmonique 
        amorti, ou bien hyperbolique amorti.

    \item[Résolution théorique]
        Si $p$ et $q$ sont continues, alors on a affaire à un 
        Cauchy-Lipschitz linéaire à coefficients continus,
        qui admet donc une unique solution \emph{globale}
        sur l'intervalle de définition pour toute paire 
        de conditions initiales.


    \item[Principe des zéros isolés]
        Les zéros d'une solution non nulle d'une telle équation sont nécessairement 
        isolés, car si $\alpha$ est un zéro de $y$ 
        qui n'est pas isolé, alors on a une suite
        $\alpha_k$ de zéros avec $\alpha_k \to \alpha$.

        Mais alors les taux d'accroissements pris en les $\alpha_k$
        montrent que $(y(\alpha_k) - y(\alpha)) / (\alpha_k - \alpha) = 0$
        et donc $y'(\alpha) = 0$. Or il existe une unique solution 
        au problème de cauchy $y(\alpha) = 0$, $y'(\alpha) = 0$, 
        et c'est la solution nulle. 

    \item[Application des zéros isolés]

        Si $y$ non nulle possède une infinité de zéros. 
        L'ensemble des zéros de $y$ étant \emph{fermé},
        \emph{discret}, dans chaque compact il ne peut y 
        en avoir qu'un nombre fini. Donc on peut 
        les ordonner (il sont dénombrables), 
        et il y a des zéros à des distances 
        arbitrairement grandes.

    \item[Wronsiken]

        Le wronskien de deux solutions, noté $w(y_1, y_2)$
        est le déterminant des deux vecteurs $(y_1,y_1')$
        et $(y_2, y_2')$. S'il est nul en un point, les deux 
        solutions sont proportionnelles globalement (CL)
        et réciproquement, si les deux sont proportionnelle 
        alors il est nul tout le temps.

    \item[Lemme des pentes]

        Si $y > 0$ sur $]x_1,x_2[$ avec $y(x_1) = y(x_2) = 0$ 
        alors on a nécessairement $y'(x_1) > 0$ et $y'(x_2) < 0$.

        En effet, l'étude du taux d'accroissement nous indique 
        que $y'(x_1) \geq 0$ et $y'(x_2) \leq 0$, et s'ils étaient
        nuls alors $y$ serait nulle, et c'est absurde.

    \todo[inline]{Prouver qu'on peut se ramener à étudier 
    le cas $y'' + qy = 0$ dans STURM ...}

\end{description}

\subsection{Développement}

\begin{description}
    \item[Le théorème d'entrelacement de Sturm]
        On pose $I = [a,b]$ un intervalle de $\mathbb{R}$ (possiblement 
        $\mathbb{R}$ entier). On considère deux fonctions $q \leq r$ 
        continues sur $I$.

        \begin{equation}
            \tag{$E_1$} y'' + qy = 0
            \label{eqn:super1}
        \end{equation}

        \begin{equation}
            \tag{$E_2$} y'' + ry = 0
            \label{eqn:super2}
        \end{equation}

        On considère $y$ une solution non nulle de $(E_1)$
        et $z$ une solution non nulle de $(E_2)$.
        Montrons qu'entre deux zéros de $y$ il y a un zéro de $z$.

        \begin{proof}

        Soient $x_1 < x_2$ deux zéros consécutifs de $y$ dans $I$,
        supposons par l'absurde que $z$ n'ait pas de zéro dans $[x_1,x_2]$,
        on peut par sans restriction de généralité
        prendre $z > 0$ sur cet intervalle.

        Alors, comme ce sont deux zéros consécutifs de $y$, on peut 
        supposer sans restriction de généralité que $y > 0$ sur 
        $]x_1, x_2[$.


        En utilisant le lemme des pentes on déduit alors 
        \begin{equation}
            y'(x_1) > 0 > y'(x_2)
        \end{equation}

        Considérons alors le Wronskien mixte de $y$ et $z$

        \begin{equation}
            w = yz' - zy'
        \end{equation}

        C'est une application dérivable, et on remarque 
        que

        \begin{align*}
            w'(x) &= y'z' + yz'' - z'y' - y''z \\
                  &= -yrz + rqy \\
                  &= yz (q - r)
        \end{align*}

        Donc le wronskien est négatif sur $[x_1,x_2]$.
        Toutefois, on remarque que $w(x_1) = - z(x_1)y'(x_1)$
        est strictement négatif, et que $w(x_2) = -z(x_2) y'(x_2)$
        est strictement positif. C'est absurde !

        On a donc bien un zéro entre $x_1$ et $x_2$ pour $z$.
        \end{proof}

    \item[Amélioration]
        Supposons de plus que $z(x_1) = 0$, alors on peut 
        appliquer le même raisonnement pour déduire 
        qu'il existe un \emph{autre} zéro dans $[x_1, x_2]$ !

    \item[Conséquence]
        On peut appliquer ce théorème à une unique équation 
        par exemple $(E_1)$ en prenant deux solutions non nulles $y_1$ et $y_2$.

        On déduit alors qu'entre deux zéros de $y_1$ il y a un zéro 
        de $y_2$. Mais en étudiant le wronskien de $y_1$ et $y_2$, si elles ne 
        sont pas colinéaires, il est non nul, et donc les zéros sont
        \emph{strictement entrelacés} car il s'annule sur un zéro commun 
        à $y_1$ et $y_2$.

    \item[Théorème de Sturm périodique]

        On suppose désormais $q$ continue et périodique de période $\omega > 0$.
        On va montrer qu'il n'y a que deux comportements possibles~:
        \begin{enumerate}[(i)]
            \item Toute solution réelle possède au plus un zéro (hyperbolique)
            \item Toute solution réelle possède une infinité de zéros
                (sinusoïdal)
        \end{enumerate}

        \begin{proof}
            Soit $y$ une solution de $(E_1)$ qui possède deux zéros 
            $x_1 < x_2$. On pose $z(x) = y (x + n\omega)$
            avec $n \omega > x_2 - x_1$.
    
            La fonction $z(x)$ est clairement une solution de $(E_1)$
            puisque

            \begin{equation}
                z''(x) = y''(x + n\omega) = - q(x + n\omega) y(x + n\omega) = -
                q(x) z(x)
            \end{equation}

            Par le théorème d'entrelacement, on déduit qu'il existe
            $x_1 \leq x_3 \leq x_2$ zéro de $z$.

            Or, ce zéro pour $z$ dit que $x_3 + n\omega$ est un zéro pour $y$.
            Mais 

            \begin{equation}
                x_3 + n\omega > x_3 + x_2 - x_1 \geq x_2
            \end{equation}

            Donc on a trouvé un zéro strictement au dessus de $x_1$ et $x_2$
            pour $y$.

            L'ensemble des zéros de $y$ n'étant pas majoré, on déduit 
            qu'il est infini.

            Enfin, si $y_2$ est une autre solution de $(E_1)$, le principe
            des zéros entrelacés permet de déduire que $y_2$ possède aussi 
            une infinité de zéros ! On est donc bien dans le cas $(ii)$.
        \end{proof}

    \item[Le cas particulier où $q \leq 0$]
        Dans ce cas là, même sans supposer $q$ périodique, on peut 
        montrer que les solutions possèdent un unique zéro, comme 
        le montre le modèle $y'' - y = 0$.

        Supposons par l'absurde que $y$ ait au moins deux zéros,
        entre ces deux zéros on a $y'' = - qy$ qui est de signe constant,
        et sans restriction de généralité on peut supposer $y > 0$ 
        donc $y'' > 0$ entre ces deux zéros. 

        On déduit donc que $y'$ est strictement croissante sur cet 
        intervalle. Toutefois, par le lemme des pentes, 
        on déduit que $y'(x_1) > 0$ et $y'(x_2) < 0$, ce qui est absurde !

    \item[Le cas particulier où $q \geq 0$ et $q \neq 0$]

        Il suffit de fabriquer une solution avec $2$ zéros pour 
        conclure qu'on est dans le deuxième cas des équations périodiques.

        On possède $a$ tel que $q(a) > 0$ car il n'est pas nul.
        On prend $y$ solution du problème de cauchy $(E_1)$
        $y(a) = 1$, $y'(a) = 0$.

        Alors on a $y''(a) = -q(a)y(a) = -q(a) < 0$ 
        et donc $y''(a) < 0$ sur un voisinage de $a$,
        donc $y'$ strictement décroissante sur un voisinage de $a$.
        Or on sait que $y'(a) = 0$, donc il existe 
        $a_1 < a < a_2$ tels que 


        \begin{equation}
            y'(a_2) < 0 = y'(a) < y'(a_1)
        \end{equation}

        Supposons par l'absurde qu'il n'y ait pas de zéros 
        dans $[a, +\infty[$. On sait alors que $y$ y est de signe
        constant, mais comme $y(a) > 0$, on déduit que $y > 0$
        sur $[a,+\infty[$. Ainsi, on déduit que $y'(x)$ est décroissante 
        sur $[a,+\infty[$.

        \begin{equation}
            0 < y(x) = y(a_2) + \int_{a_2}^x y'(t) dt
            \leq y(a_2) + (x - a_2) y'(a_2) \to +\infty \quad \quad (x \to
            \infty)
        \end{equation}

        Ce qui est totalement absurde.
        En procédant de même pour $]+\infty,a]$ on déduit qu'il y a au moins 
        deux zéros pour $y$. 

\end{description}
