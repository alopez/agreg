
\begin{references}
\item[Rombaldi] \hfill Pour les choses sur $S_n$
\item[FGN Algèbre 2] \hfill Pour le déterminant de smith
\end{references}

On a un morphisme $P : S_n \to GL_n(k)$
qui correspond à l'action du groupe symétrique sur les vecteurs de $k^n$.
On sait que si $\sigma$ est conjugée à $\tau$ alors $P_\sigma$ est 
conjugée à $P_\tau$. On se demande s'il y a une réciproque.

On suppose $P_\sigma = Q P_\tau Q^{-1}$.

On note $\delta_\sigma^k$ le nombre de cycles de taille $k$ dans la
décomposition en cycles à support disjoints de $\sigma$, \emph{en comptant 
les cycles de taille $1$}.

Il suffit de montrer que $\delta_\sigma^k = \delta_\tau^k$ pour conclure,
car on connaît les classes de conjugaison dans $S_n$ grâce au type de la 
signature.

\subsection{Fixateur}

On étudie le fixateur de $P_\sigma$ et $P_\tau$,
c'est à dire $\Fix P = \Ker (P - id_E)$.
Comme les deux endomorphismes sont congugués on trouve~:

\begin{equation}
    \dim \Fix P_\sigma = \dim \Fix P_\tau
\end{equation}

Supposons alors que $\sigma$ se décompose en cycles à support disjoints  $c_1
\dots c_l$. Une base de $\Fix P_\sigma$ est alors $(\varepsilon_1, \dots,
\varepsilon_l)$ où $\varepsilon_i$ est le vecteur indicateur du support de
$c_i$.

\begin{enumerate}[(i)]
    \item Cette famille est libre car les $c_i$ sont à support disjoints
    \item Elle est bien fixée par l'action de $\sigma$
    \item Elle est génératrice, car si $P_\sigma x = x$,
        pour tout $c_i$, et $j$ dans le support de $c_i$ 
        on obtient $x_j = x_{c_i(j)}$. Donc le vecteur 
        est constant sur chaque support par transitivité.
\end{enumerate}


On obtient donc, si $l$ est le nombre de cycles dans la décomposition en cycles
à support disjoints de $\sigma$~:

\begin{equation}
    \dim \Fix P_\sigma = l
\end{equation}


On remarque que l'égalité des dimensions s'étend aux itérées de $\sigma$ et
$\tau$~:

\begin{equation}
    \dim \Fix P_{\sigma^k} =\dim \Fix (P_\sigma)^k = \dim \Fix (P_\tau)^k = \dim \Fix P_{\tau^k}
\end{equation}

\subsection{Décompte des cycles}



\begin{lemme}
    Si $c$ est un cycle de longueur $r$ alors 
    $c^m$ possède exactement $r \wedge m$ cycles de longueur $r / (r \wedge m)$.
\end{lemme}

\begin{proof}
    \begin{align}
        (c^m)^k (x) = x & \iff c^{mk} (x) = x \\
                        & \iff r | mk \\
                        & \iff r / (r \wedge m) | k m / (r \wedge m) \\
                        & \iff r / (r \wedge m) | k 
    \end{align}

    Par définition de l'ordre d'un élément, on obtient que l'ordre de $x$
    est $r / (r \wedge m)$, ceci étant vrai pour chaque $x$, on déduit 
    qu'il y a exactement $r \wedge m$ cycles de taille $r / (r \wedge m)$
    dans la décomposition de $c^m$ en cycles à support disjoints.
\end{proof}

Ainsi, on peut compter le nombre de cycles dans la décomposition 
en cycles à support disjoints de $\sigma^m$: chaque cycle de taille $i$
se découpe en exactement $(i \wedge m)$ cycles \footnote{Les supports étant
    disjoints on peut raisonner indépendamment sur chaque cycle}.

\begin{equation}
    \dim \Fix P_{\sigma^m} = 
    \sum_{i = 1}^{n} (i \wedge m) \delta_\sigma^i
\end{equation}

On a donc le système d'équations suivant~:

\begin{equation}
    \forall m \in \mathbb{N}^*,
    \sum_{i = 1}^n (i \wedge m) \delta_\sigma^i 
    = \sum_{i = 1}^n (i \wedge m) \delta_\tau^i
\end{equation}

\subsection{Résolution du déterminant}

On déduit alors que si l'on pose $A_{i,j} = (i \wedge j)$ et $X_i =
\delta_\sigma^i - \delta_\tau^i$ on veut résoudre $AX = 0$.
Si l'unique solution est $X = 0$ alors on déduit $\sigma$ conjuguée à $\tau$.

Or la matrice $A$ correspond à un déterminant de Smith.

\begin{lemme}[Déterminant de Smith]
    $\det A = \prod_{i = 1}^k \phi(i) > 0$
\end{lemme}
\begin{proof}
    On remarque que 
    $A_{i,j} = (i \wedge j) = \sum_{ d | (i \wedge j) } \phi (d)$

    Mais $d$ divise le pgcd si et seulement s'il divise les deux, donc 
    $A_{i,j} =  \sum_{d = 1}^n  \phi (d) \chi_{ d | i} \chi_{d | j}$.

    En posant $B_{i,j} = \chi_{ i | j }$ on obtient $A = {}^t B D( \phi(1)
    \dots \phi (n) )B$.

    Or la matrice $B$ est triangulaire supérieure et de diagonale $1$ donc
    on obtient le déterminant désiré.
\end{proof}

En conclusion les deux permutations ont même type et sont donc bien conjuguées.
