
\begin{references}
\item[Rombaldi] page 130
\item[FGN Algèbre 2] page 185
\end{references}

\subsection{Développement}

On se place sur $\mathbb{C}$ et en dimension finie,
on va démontrer que tout sous groupe d'exposant 
fini de $GL_n(\mathbb{C})$ est fini.

On pose $G$ un sous groupe d'exposant fini $N$ de $GL_n(\mathbb{C})$.

\begin{description}
    \item[Caractérisation des éléments de $G$]

        Tous les éléments de $G$ sont diagonalisables 
        puisqu'ils sont annulés par $X^N - 1$ qui est scindé 
        à racines simples dans $\mathbb{C}$.

    \item[Injection de $G$ dans un ensemble fini]
        On pose $X$ l'ensemble des traces des éléments de $G$,
        qui est donc fini. 

        On voudrait poser $\phi : g \mapsto \trace g$. Seulement 
        cette application n'est pas nécessairement injective.

        On se sert donc de la structure d'espace vectoriel 
        de $M_n (\mathbb{C})$
        et on pose $F$ l'espace vectoriel engendré par $G$,
        $\mathcal{B} = (g_1, \dots, g_p)$ une base de $F$ composée d'éléments de $G$.

        On introduit l'application $\phi : g \mapsto (\trace (g g_1), \dots, \trace (g g_p))$.

        Elle est à valeurs dans $X^p$, qui est toujours un ensemble fini,
        et on va montrer qu'elle est injective\footnote{
            C'est en effet assez naturel puisqu'on est en train 
            de calculer une projection sur une base de $F$ via des 
            "produits scalaires" (bien que rien ne soit à priori orthogonal ici).
        }.


    \item[Caractérisation des endomorphismes nilpotents]

        Un endomorphisme $u \in \Lin{E}$ est nilpotent si et seulement 
        si les traces de ses itérées sont nulles 

        \begin{proof}
            Si $u$ est nilpotent, son unique valeur propre est $0$, et il en va 
            de même pour ses itérées. On déduit alors que les traces sont toutes 
            nulles.

            Réciproquement, si les valeurs propres de $u$ non nulles sont les $\lambda_1, \dots,
            \lambda_p$ avec multiplicité $\alpha_1, \dots, \alpha_p$, on a

            \begin{equation}
                0 = \trace (u^k) = \sum_i \alpha_i \lambda_i^k
            \end{equation}

            On a donc un système linéaire de type $A X = 0$ avec $A$ 
            la matirce de Vandermonde des $(\lambda_1, \dots, \lambda_p)$.
            Ce système étant inversible, on déduit que les $\alpha_i$ sont nuls,
            et donc la seule valeur propre est bien zéro.
        \end{proof}

    \item[Injectivité de $\phi$]

        Si $g, g' \in G$ vérifient $\phi (g) = \phi (g')$ alors 
        $g' g^{-1} - id_E$ est nilpotent.

    \begin{proof}
        Par linéarité de la trace, et comme les $(g_i)$ forment une base de $F$
        on déduit que $\trace (g u) = \trace (g' u)$ pour tout $u \in F$.

        Or $g' g^{-1}$ est dans $G$, donc dans $F$ et ainsi 

        \begin{equation}
            \trace (g' g^{-1})^{k+1}
            = 
            \trace (g' g^{-1} (g' g^{-1})^k )
            = 
            \trace (g g^{-1} (g' g^{-1})^k )
            =
            \trace (g' g^{-1})^k
        \end{equation}

        Par récurrence immédiate on déduit alors $\trace (g' g^{-1})^k = \trace id_E = n$

        Ainsi  en posant $v = g' g^{-1})$ on a 
        \begin{equation}
            \trace (v - id_E)^r
            = 
            \sum_{k = 0}^r \binom{r}{k} (-1)^{r - k} \trace ( v^k)
            = 
            0
        \end{equation}

        Donc l'endomorphisme $g' g^{-1} - id_E$ est bien nilpotent.
    \end{proof}



    En conclusion $\phi$ est injective, car $\phi (g) = \phi(g')$ 
    implique $g' g^{-1} - id_E$ est nilpotente, mais elle est aussi diagonalisable 
    comme somme deux deux matrices diagonalisables qui commutent. On déduit 
    alors $g' = g$.

    \item[Conclusion]

    $G$ s'injecte par $\phi$ dans $X^p$, ce qui permet de déduire que $G$ est fini.

\end{description}


\subsection{Annexes}

\begin{description}
    \item[Équivalences]

        On a les équivalences suivantes 
        \begin{enumerate}[(i)]
            \item G est fini
            \item G est d'exposant fini
            \item Tout élément de G est diagonalisable, et l'ensemble des 
                traces est fini
        \end{enumerate}

        De plus, dans le cas où $G$ est commutatif on déduit une paramétrisation de $G$
        par $\mathbb{U}_N$ car les matrices sont alors co-diagonalisables.

    \item[Majoration de la taille de $G$] 
        On a $|G| \leq N^{n^3}$ car $|G| \leq |X|^m$
        où $m$ est la dimension de l'espace engendré par $G$
        et $X$ est l'ensemble des traces des éléments de $G$.

        Sauf que $m \leq n^2$, et que $|X| \leq N^n$, car 
        les éléments sont des racines $N$-èmes de l'unité.

    \item[Annexe historique]
        La conjecture de Burnside était~: «~Est ce que les groupes possédant un
        nombre fini de générateurs et dont tous les éléments sont d'ordre fini
        sont tous finis ?~». Et sa version plus faible
        «~Est-ce que les groupes d'exposant fini possédant un nombre fini de
        générateurs sont tous finis ?~»

        On a démontré ici que tout sous groupe d'exposant fini 
        de $GL_n(\mathbb{C})$ est
        fini. Un énnoncé équivalent est que toute repérsentation 
        d'un groupe d'exposant fini dans un espace vectoriel complexe
        de dimension finie est d'image finie. 

        Ceci met en évidence une difficulté à trouver un contre exemple à la
        conjecture de Burnside~: il faut qu'un tel contre-exemple 
        soit un groupe dont aucune represéntation de degré fini 
        n'est fidèle !

        Ceci explique que c'est seulement en 1964 qu'on réfute la version 
        forte, et en 1968 qu'on réfute la version faible.
\end{description}
