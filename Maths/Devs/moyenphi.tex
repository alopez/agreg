\begin{references}
\item[FGN Algèbre 1] page 156
\item[Rombaldi]      dans arithmétique
\end{references}

\subsection{Prérequis}

\begin{description}
\item[Rappels basiques sur $\phi$]
    On a l'isomorphisme d'anneaux 
    \begin{equation}
        \left( \faktor{Z}{nZ} \right)^\times
        \simeq
        \prod \left( \faktor{Z}{p_i^{\alpha_i} Z} \right)^\times
    \end{equation}

    Ce qui donne la multiplicativité de $\phi$
    et l'expression 
    \begin{equation}
        \phi (n) = \prod \phi (p_i^{\alpha_i})
    \end{equation}

    De plus, $\phi (p^\alpha) = (p-1) p^{\alpha - 1}$ 
    pour $\alpha > 0$, car 
    
    \begin{equation}
        \phi (p^\alpha) = 
        \left| \left\{
            1 \leq k \leq p^\alpha ~\big|~ k \wedge p^\alpha = 1
        \right\}\right|
        =
        p^\alpha
        -
        \left| \left\{
            1 \leq k \leq p^\alpha ~\big|~ p | k 
        \right\}\right|
        =
        p^\alpha
        -
        p^{\alpha - 1}
    \end{equation}

\item[Le petit truc qui fait plaisir sur $\phi$]
    ... et qui se prouve par un simple dénombrement
    \begin{equation}
        n = \sum_{d | n } \phi (d)
    \end{equation}


\item[Expressions pratiques pour $\phi$]
    On en déduit les expressions suivantes

    \begin{equation}
        \phi (n) = \prod (p - 1) p^{\alpha_i - 1} = n \prod \left(1 -
        \frac{1}{p} \right)
    \end{equation}

\item[Encadrement]

    Dans le Rombaldi, la propriété non-tirivale suivante est démontrée
    proprement~:
    \begin{equation}
        \forall n \geq 2, \sqrt{n} - 1 < \phi (n) \leq n - 1
    \end{equation}

\item[Limites]

    De manière plus ou moins immédiate on a $\lim \phi(n) = +\infty$
    \footnote{Sans avoir besoin de l'encadrement précédent, on peut simplement 
        remarquer qu'avoir moins de $k$ inversibles c'est plus possible quand 
        on est trop grand}.

    On sait aussi que $\lim \sup \frac{\phi(n)}{n} = 1$ car il y a une 
    infinité de nombres premiers. Un résultat tout aussi simple dit que 
    de l'autre côté on a bien $\lim \inf \frac{\phi(n)}{n} = 0$.

    Pour cela, on écrit~:

    \begin{equation}
        \frac{\phi(n)}{n} = \prod \left( 1 - \frac{1}{p} \right) 
    \end{equation}

    On trouve donc $n$ nombres premiers supérieurs à $2$ distincts tels que $1 -
    \frac{1}{p} < \frac{1}{2}$, et donc le produit est plus petit que
    $\frac{1}{2^n}$. 

\item[Formule du Crible / De Poincarré]

    On considère un ensemble fini d'évènements $A_k$, alors 
    on constate que 

    \begin{equation}
        \mathbb{P} \left( \bigcup A_k \right) = 
        \mathbb{E} \left( \mathbf{1}_{\bigcup A_k} \right)
        = 
        \mathbb{E} \left( 1 - \mathbf{1}_{\bigcap A_k^c} \right)
        = 
        \mathbb{E} \left( 1 - \prod (1 - \mathbf{1}_{A_k} ) \right)
        = 
        1 - \sum_{ \emptyset \subseteq I \subseteq \{ 1, \dots n \}} 
                (-1)^{|I|} \times \mathbb{P} \left( \bigcap A_k \right)
    \end{equation}

    En simplifiant le terme en $1$ on obtient alors 
    \begin{equation}
        \mathbb{P} \left( \bigcup A_k \right) = 
        \sum_{ \emptyset \subsetneq I \subseteq \{ 1, \dots n \}} 
                (-1)^{|I|+1} \times \mathbb{P} \left( \bigcap A_k \right)
    \end{equation}

\item[Fonction de Möbius]
    On note $\mu : \mathbb{N} \to \{ -1, 0, 1 \}$ 
    la fonction qui à $n$ associe $0$ si $n$ possède un facteur 
    carré, $1$ si $n = 1$, et $(-1)^r$ si $n$ possède $r$ facteurs 
    premiers distincts.

    On a alors la formule d'inversion dite «~De Möbius~» 

    \begin{equation}
        \sum_{ d | n} \mu (d) = \begin{cases}
                                    1 & \text{ si } n = 1 \\
                                    0 & \text{ sinon }
                                \end{cases}
    \end{equation}

    Cette formule s'obtient simplement via un binôme de Newton
    de type $(1 - 1)^n$. En effet, cette somme 
    est sur les choix des exposants qui valent $\pm 1$
    des nombres premiers $p$ qui divisent $n$.
    On a donc pour $n > 1$.

    \begin{equation}
        \sum_{ d | n } \mu (d) = 
        \sum_{ \emptyset \subseteq I \subseteq D_n }
            (-1)^{|I|}
        =
        \sum_{k = 0}^{|D_n|}
        { |D_n| \choose k } (-1)^k 
        = 
        (1 - 1)^{|D_n|} = 0
    \end{equation}

\end{description}

\subsection{Développement}

\begin{description}

    \item[Objectif]

On introduit $r_n$ la probabilité 
que deux entiers inférieurs à $n$ soient premiers entre eux,
et $A_n$ l'ensemble associé à cet évènement.
La relation entre les deux objets est bien évidemment 

\begin{equation}
    r_n = \frac{|A_n|}{n^2}
\end{equation}

L'objectif est de calculer $A_n$, et de détermier un équivalent 
quand $n$ devient grand.

    \item[Introduction des évènements plus simples]

Mais on peut écrire facilement $A_n^c$ comme une 
union d'évènements plus simples~:

\begin{equation*}
    A_n^c = \left\{ (a,b) \leq n ~|~ \exists p \in P_{\leq n}, p | a \wedge p | b \right\}
\end{equation*}

On note $U_p = \{ (a,b) \leq n ~|~ p | a \wedge p | b \}$.

    \item[Utilisation de la formule du Crible]

\begin{equation*}
    \left| A_n^c \right|
    = 
    n^2 - \sum_{\emptyset \subsetneq I \subseteq P_{\leq n}}
        (-1)^{|I| + 1} 
        \left|
            \bigcap_{p \in I} U_p
        \right|
\end{equation*}

    \item[Calcul des intersections]

Mais il est facile de remarquer que si on pose 
$\alpha_I = \prod_{i \in I} p_i$ alors~:

\begin{align*}
        \left|
            \bigcap_{p \in I} U_p
            \right| &= 
        \left| \{ \left(a,b\right) \leq n ~|~ \forall p \in I, p | a \wedge p | b \} \right|
        \\
        &= 
        \left| \{ \left(\alpha_I k,\alpha_I l\right) \leq n ~|~ \alpha_I k = a
            \wedge \alpha_I
        l = b \} \right|
        \\
        &= 
        \left\lfloor 
            \frac{n}{\alpha_I} 
        \right\rfloor^2
\end{align*}

    \item[Utilisation de la fonction de Möbius]

On peut alors conclure en reconnaissant la fonction $\mu$ cachée dans la somme~:

\begin{align*}
    | A_n | 
    &= 
    n^2 + \sum_{\emptyset \subsetneq I \subseteq P_{\leq n}}
        (-1)^{|I|} 
        \left\lfloor 
            \frac{n}{\alpha_I} 
        \right\rfloor^2 \\
    &= 
        n^2 + \sum_{d = 2}^n
        \mu(d)
        \left\lfloor 
            \frac{n}{d} 
        \right\rfloor^2 \\
    &= 
        \sum_{d = 1}^n
        \mu(d)
        \left\lfloor 
            \frac{n}{d} 
        \right\rfloor^2 \\
\end{align*}

    \item[Méthode d'estimation du développement asymptotique]

On veut un développmement asymptotique de $r_n$, 
pour cela on va simplement 
remarquer que le terme général est équivalent 
à $\mu(d) / d^2$.

On pose donc $s_n = \sum_{d = 1}^n \mu(d)/d^2$.
Cette somme converge absolument, tout comme $r_n$.

    \item[Comparaison des deux séries]

Or 

\begin{equation*}
    \left\lfloor 
        \frac{n}{d} 
    \right\rfloor 
    \geq
    \frac{n}{d}  - 1
\end{equation*}

Ainsi

\begin{equation*}
    \left\lfloor 
        \frac{n}{d} 
    \right\rfloor^2
    \geq
    (\frac{n}{d}  - 1)^2
\end{equation*}

Et donc on peut majorer 
l'intérieur de la différence des sommes~:

\begin{align*}
    | r_n - s_n | 
    &\leq 
    \sum_{d = 1}^n 
        \left| \frac{1}{n^2} 
        \left\lfloor 
            \frac{n}{d} 
        \right\rfloor^2
        - \frac{1}{d^2} \right|
    \\
    &\leq 
    \sum_{d = 1}^n 
        \left| \frac{2}{dn}
        - \frac{1}{n^2} \right|
    \\
    &\leq 
    \frac{2}{n}
    \sum_{d = 1}^n 
        \frac{1}{d}
    + \frac{1}{n}
    \\
    &\leq O \left(\frac{\ln n}{n} \right)
\end{align*}

On déduit donc, que $r_n$ et $s_n$ ont 
la même somme. 

    \item[Calcul effectif]

Pour calculer la somme de $s_n$ on procède comme suit


\begin{enumerate}[(i)]
    \item Calculons le produit des sommes $\zeta(2)$ et 
        $s_\infty$. Ce sont deux sommes absolument convergentes 
        c'est-à-dire que les familles $(\mu(d) / d^2)$ et 
        $(1 / n^2)$ sont sommables.

        Ainsi, la famille $ (\mu(d)/ (dn)^2)$ est sommable,
        et on a en utilisant des sommations par paquets~:

        \begin{align}
            \left( \sum_{ d \geq 1} \frac{\mu(d)}{d^2} \right)
            \left( \sum_{ n \geq 1} \frac{1}{n^2} \right)
            &= 
            \sum_{n,d \geq 1} \frac{\mu(d)}{(dn)^2}
            \\
            &= 
            \sum_{ d \geq 1} \sum_{ n \geq 1 } \frac{\mu(d)}{(dn)^2} \\
            &= 
            \sum_{ d \geq 1} \sum_{ d | p } \frac{\mu(d)}{p^2} \\
            &= 
            \sum_{ d \geq 1} \frac{1}{p^2} \sum_{ d | p } \mu(d) \\
            &= 
            1
        \end{align}

        \begin{remarque}
            On a prouvé par la même occasion que 
            $1/n^2$ est l'inverse de $\mu(d)/d^2$
            pour $\star$. Il serait amusant de généraliser 
            cela ... (lien entre inverse classique pour les 
            séries et inverse pour $\star$).
        \end{remarque}

    \item Les deux sommes sont donc inverses, mais on sait que $\zeta(2) =
        \frac{\pi^2}{6}$, 
        on a donc 
        \begin{equation}
            \boxed{
                \sum_{ d \geq 1} \frac{\mu(d)}{d^2} = \frac{6}{\pi^2}
            }
        \end{equation}
\end{enumerate}

\end{description}

\subsection{Lien avec l'ordre moyen}

On constate que 

\begin{align}
    | \{ (a,b) \in \llbracket 1, n \rrbracket ~|~ a \wedge b = 1 \} | 
    &= 
    2 | \{ (a,b) \in \llbracket 1, n \rrbracket ~|~ a \wedge b = 1 \text{ et } b
    < a \} | \\
    &= 2 \sum_{a = 1}^n | \{ b \in \llbracket 1, a \rrbracket ~|~ a \wedge b = 1
\} | \\
    &= 2 \sum_{k = 1}^n \phi (k)
\end{align}

On déduit donc l'ordre moyen de $\phi(n)$

\begin{equation}
    \boxed{\frac{1}{n} \sum_{k = 1}^n \phi(k) \sim \frac{3}{\pi^2} n }
\end{equation}

\begin{remarque}
    On a environ $\frac{3}{\pi^2} \approx 0.31$,
    ce qui veut dire qu'en moyenne il y a $31 \%$ d'inversibles 
    dans un $Z/nZ$.
\end{remarque}

\subsection{Annexe Möbius}

\begin{description}
    \item[Formule d'inversion]
        On munit l'espace des suites réelles (indicées à partir de $1$) 
        du produit $\star$ (de convolution) 

        \begin{equation}
            (u \star v) (n) = \sum_{ kl = n} u(k) v(l)
                            = \sum_{ d | n } u(d) v\left(\frac{n}{d}\right)
        \end{equation}

        Ce produit est associatif, commutatif, et possède un neutre 
        évident~: la suite $(1, 0, \dots)$.

        La question est de trouver un inverse à la suite $\omega = (1, 1, \dots
        )$. Son inverse est en fait $\mu$ ... C'est ce que dit la formule
        d'inversion de Möbius !

        Cela permet d'inverser pleins de formules, puisque 

        \begin{equation}
            u \star \omega = v 
            \iff
            u = v \star \mu
        \end{equation}

        En pratique, on écrit ça dans le sytle plus sommatoire
        (mais totalement équivalente)

        \begin{equation}
            g(n) = \sum_{d | n} f(d) 
            \iff
            f(n) = \sum_{d | n} \mu(d) g\left( \frac{n}{d} \right)
        \end{equation}


\end{description}
