\begin{references}
    \item[Crochemore]
\end{references}

\begin{ouverture}

    Dans les recherches Google on fait des fautes 
    de frappe, et donc on veut parfois chercher
    des motifs avec une distance d'édition raisonnable.
    C'est cool, c'est sympathique, et la distance d'édition est née.
    On traite dans un premier temps la distance d'édition avec un 
    unique mot, que l'on généralise aussitôt à l'extraction des 
    mots à distance $k$ du motif dans un dictionnaire. 

    Cette dernière étape se fait en suivant le paradigme général
    suivant~: représenter des données comme des modèles de calcul. 
    Ainsi, comme l'automate de Simon transforme un motif en un 
    automate, on transforme ici le motif en un automate de Levenshtein
    et le dictionnaire en un Trie (ou mieux encore).
    C'est à mettre en parallèle avec la construction de l'arbre 
    des suffixes. 

    On se place sur un alphabet fini $\Sigma$.

\end{ouverture}

\subsection{Introduction}

\begin{definition}[Édition]
    Soit $u,v \in \Sigma^*$ et $a,b \in \Sigma$, 
    une édition est de la forme~:

    \begin{compactenum}[(i)]
    \item Ajout $uv \to u a v$
    \item Suppression $u a v \to uv$
    \item Modification $u a v \to u b v$
    \end{compactenum}

    La relation $(\to)$ sur $\Sigma^*$ définit 
    un système de réécriture. 
\end{definition}

\begin{definition}[Distance d'édition]
    On définit la distance d'édition 
    $d_E (u,v)$ comme la taille de la plus 
    petite dérivation $u \to^* v$ si 
    elle existe, et $+\infty$ sinon.
\end{definition}

\begin{exemple}
    Il existe une édition de taille $3$ qui 
    mène de «~rotis~» à «~sortie~» 
    \begin{equation*}
            \textnormal{rotis}
        \to \textnormal{sotis}
        \to \textnormal{sortis}
        \to \textnormal{sortie}
    \end{equation*}
\end{exemple}

\begin{remarque}
    Ce système de ré-écriture n'est pas facile 
    à étudier. Il existe beaucoup de dérivations 
    d'un mot vers un autre, et l'ordre des modifications 
    n'est pas contrôlé. 
\end{remarque}

\begin{lemme}[Algorithme Naïf]
    L'algorithme naïf qui trouve le plus court 
    chemin pour aller du mot $|u|$ au mot $|v|$
    se fait en taille linéaire en la taille du graphe, 
    que l'on peut tronquer aux mots de taille inférieure 
    à $|u|+|v|$, et donc  en $\mathcal{O} \left((|u|+|v|)2^{|u|+|v|}\right)$.
\end{lemme}

\begin{definition}[Alignement]
    Un alignement de deux mots $u$ et $v$ dans $\Sigma^*$
    est la donnée de deux mots $\hat{u}$ et $\hat{v}$ 
    dans $\left( \Sigma \uplus \{ \bot \} \right)^*$ 
    qui vérifient~:
    
    \begin{inparaenum}[(a)]
    \item $\pi_\Sigma (\hat{u}) = u$
    \item $\pi_\Sigma (\hat{v}) = v$
    \item $|\hat{u}| = |\hat{v}|$
    \end{inparaenum}

    Avec $\pi_\Sigma$ le morphisme qui efface les $\bot$
    et conserve toutes les autres lettres.
\end{definition}

\begin{exemple}
    Voici un alignement possible des mots «~rotis~»
    et «~sortie~»
    \begin{center}
        \begin{tabular}{cccccc}
            r & o & $\bot$ & t & i & s   \\
            s & o & r    & t & i & e 
        \end{tabular}
    \end{center}
\end{exemple}

\begin{definition}[Taille d'alignement]
    Soit $u,v \in \Sigma^*$ et $\hat{u}, \hat{v}$
    un alignement de $u,v$.
    On définit la mesure $\hat{d} (\hat{u}, \hat{v})$
    comme le nombre de lettres différentes dans $\hat{u}$ et 
    $\hat{v}$, auquel on ajoute le nombre de positions où 
    $\hat{u}$ et $\hat{v}$ possèdent tous deux un $\bot$.
\end{definition}

\begin{exemple}
    En reprenant l'alignement précédent, la distance 
    obtenue est $3$.
    \begin{center}
        \begin{tabular}{cccccc|r}
            r & o & $\bot$ & t & i & s   \\
            s & o & r      & t & i & e   \\ \hline 
            1 & 0 & 1      & 0 & 0 & 1 & 3 
        \end{tabular}
    \end{center}
    
    En revanche si on ajoute des caractères blancs,
    on peut constater qu'on augmente la taille d'alignement. 
    \begin{center}
        \begin{tabular}{ccccccc|r}
            r & o & $\bot$ & t & i & s & $\bot$   \\
            s & o & r      & t & i & e & $\bot$  \\ \hline 
            1 & 0 & 1      & 0 & 0 & 1 & 1 & 4
        \end{tabular}
    \end{center}
\end{exemple}



\begin{definition}[Distance d'alignement]
    On définit la distance $d_A$ sur les mots de $\Sigma^*$ 
    comme~:

    \begin{equation*}
        d_A(u,v) = \min \left\{ \hat{d} (\hat{u}, \hat{v}) ~|~ (\hat{u},\hat{v})
        \text{ alignement de } (u,v) \right\}
    \end{equation*}
\end{definition}

\begin{remarque}
    Il existe toujours un alignement trivial de taille $|u|+|v|$,
    et donc la distance d'alignement est toujours finie.
\end{remarque}


\begin{lemme}[Traduction 1]
    Si $u \to^k v$ alors il existe un alignement 
    $\hat{u},\hat{v}$ de taille inférieure à $k$.
\end{lemme}

\begin{lemme}[Traduction 2]
    Si $\hat{u},\hat{v}$ est un alignement de $u,v$
    de taille $k$, alors il existe une dérivation 
    $u \to^k v$.
\end{lemme}

\begin{remarque}[Embedding-projection pair]
    Si on note $E$ l'ensemble des traces d'édition 
    et $A$ l'ensemble des agencements.
    Les lemmes de traductions $1$ et $2$ fournissent 
    de manière effective deux fonctions $f : E \to A$ et 
    $g : A \to E$ qui vérifient~:
    
    
    \begin{minipage}{0.49\linewidth}
        \begin{compactitem}
        \item $f \circ g = Id$
        \item $g \circ f \leq Id$
        \item $f$ et $g$ sont des fonctions croissantes
            (ie: "continues")
        \end{compactitem}
    \end{minipage}
    \begin{minipage}{0.49\linewidth}
        \begin{center}
            \begin{tikzcd}
                E \arrow[r, "f", bend left=45] & 
                A \arrow[l, "g", bend left=45]
            \end{tikzcd}
        \end{center}
    \end{minipage}

    On a donc une construction très classique de théorie 
    des domaines, qui permet de construire des formes normalisées.
\end{remarque}

\begin{theoreme}[Caractérisation de la distance d'édition]
    \begin{equation*}
        \forall u,v \in \Sigma^*, d_E (u,v) = d_A (u,v) < +\infty
    \end{equation*}

    On a montré plus précisément que si on considère 
    les interprétations sémantiques dans $E$ et $A$ de la 
    paire $(u,v)$, on obtient~:

    \begin{equation*}
        \llbracket (u,v) \rrbracket_E = 
        \llbracket (u,v) \rrbracket_A
    \end{equation*}
\end{theoreme}

\begin{remarque}
    Ce résultat est légèrement imprécis, et l'analogie 
    doit en rester une. En effet, pour être particulièrement 
    clair, il faut considérer l'interprétation de $(u,v)$ dans $E$
    comme \emph{l'ensemble} des dérivations de $u$ vers $v$. 
    De la même manière l'interprétation dans $A$ comme l'ensemble
    des alignements de $u$ sur $v$.

    Cela étant fait, on possède un \emph{préordre} sur $E$ et $A$,
    qui à un ensemble de dérivations (resp. d'alignements)
    associe la plus petite (resp. celui de taille minimale).
    La paire $f,g$ reste la même, mais s'applique à des ensembles, et on 
    obtient alors~:

    \begin{equation*}
        g \circ f \simeq Id \wedge f \circ g \simeq Id \quad \text{ pour les
        préordres de } A \text{ et } E
    \end{equation*}
\end{remarque}

\subsection{Programme dynamique}

\begin{lemme}[Équation de récurrence]
    Soit $u,v \in \Sigma^+$. Si $w \in \Sigma^*$ 
    on note 
    $w_i$ le préfixe de taille $i$ de $w$.
    
    \begin{equation*}
        d_A (u_{i+1}, v_{i+1}) 
        = 
        \min 
        \begin{cases}
            1 + d_A (u_i, v_{i+1}) \\
            1 + d_A (u_{i+1}, v_{i}) \\
            \delta_{u_{i+1}}^{v_{i+1}} + d_A (u_i, v_i)
        \end{cases}
    \end{equation*}
\end{lemme}

\begin{proof}
    Considérons un alignement optimal de $u,v$. Alors 
    les dernières lettres de $\hat{u},\hat{v}$ sont dans 
    une des trois configurations suivantes (par optimalité)~:

    \begin{compactenum}[(i)]
    \item $\bot, v_{i+1}$
    \item $u_{i+1}, \bot$
    \item $u_{i+1}, v_{i+1}$
    \end{compactenum}

    Ce qui permet de conclure en utilisant l'optimalité de l'alignement.
\end{proof}

\begin{lemme}[Initialisation]
    Soit $v \in \Sigma^+$ (attention, non vide)

    \begin{equation*}
        d(a, v) = 
        \begin{cases}
            |v| - 1 & \text{ si } a \in v \\
            |v|     & \text{ sinon } 
        \end{cases}
    \end{equation*}
\end{lemme}

\begin{algorithm}
    \caption{Distance d'Édition}
    \begin{algorithmic}
       \REQUIRE $u,v \in \Sigma^+$
       \STATE $T \leftarrow $ tableau $(|u|,|v|)$
       \STATE $T[0,0] \leftarrow (u_0 == v_0)$
       \FOR{ $i = 1$ \TO $|u|$} 
           \IF{$u_i == v_0$}
                \STATE $T[i,0] \leftarrow 1$
           \ELSE  
                \STATE $T[i,0] \leftarrow T[i-1,0]$.
           \ENDIF
       \ENDFOR
       \STATE $T[0,0] \leftarrow (u_0 == v_0)$
       \FOR{ $j = 1$ \TO $|v|$} 
           \IF{$v_j == u_0$}
                \STATE $T[0,j] \leftarrow 1$
           \ELSE  
                \STATE $T[0,j] \leftarrow T[0,j-1]$.
           \ENDIF
       \ENDFOR
       \FOR{ $i = 1$ \TO $|u|$}
           \FOR{ $j = 1$ \TO $|v|$}
           \STATE $T[i,j] \leftarrow \min \left(1 + T[i-1,j], 1 + T[i,j-1], \delta +
           T[i-1,j-1]\right)$
           \ENDFOR
       \ENDFOR
    \end{algorithmic}
\end{algorithm}

\begin{theoreme}[Résolution]
    On peut résoudre le problème de la distance d'édition 
    avec un algorithme dynamique en temps et en espace $\mathcal{O}(|u||v|)$.
\end{theoreme}

\begin{remarque}[Optimisation spatiale]
    Il est possible de modifier légèrement l'algorithme 
    pour ne retenir que la dernière ligne du tableau, et 
    ainsi faire baisser la complexité en espace à $\mathcal{O}(|u|)$.
\end{remarque}

\subsection{Annexe automates de Levenshtein}

\begin{definition}[Automate de Levenshtein]
    Soit $u$ un mot $k$ un entier plus grand que $1$, 
    l'automate non déterministe 
    de Levenshtein est un automate $A$ tel que 
    $\mathcal{L}(A) = \{ v \in \Sigma^* ~|~ d_E (u,v) \leq k \}$.
    Sa construction est très simple en temps $\mathcal{O}(kn)$.
\end{definition}

\begin{exemple}
    TODO: à faire sur un mot court comme «~sortie~».
\end{exemple}

\begin{remarque}
    L'automate va permettre de faire quelque chose de très 
    pertinent~: si on cherche dans une base de données 
    les mots similaires à un mot donné, l'automate de 
    Levenshtein va permettre de les énumérer en un 
    temps très raisonnable. 

    Toutefois, le non-déterminisme fait exploser
    la complexité... Et déterminiser n'est pas évident.
\end{remarque}

\begin{propriete}[Construction en temps linéaire]
    On peut construire un automate de Levenshtein déterministe 
    en temps linéaire.
\end{propriete}

\begin{proof}
    Soit $u \in \Sigma^+$ avec $n = | u |$.
    On pose $Q = \{ (m_1, \dots, m_n) ~|~ 0 \leq m_i \leq k \}$
    remarquons que ce n'est pas de taille linéaire par rapport 
    à $|u|$, mais bien en $k^n$.

    Toutefois, la table de transitions se compresse aisément 
    grâce à l'algorithme dynamique, et il n'y a pas besoin 
    d'enregistrer les états.
    
    L'idée c'est simplement d'appliquer en un coup 
    l'algorithme dynamique sur la ligne (qui est l'état $q$)
    avec la lettre $c$. Cela se calcule en temps linéaire en $|u|$.
\end{proof}

\begin{remarque}
    On peut au lieu de faire cela 
    considérer la composition de 
    l'automate déterministe classique du motif $m$
    avec un transducteur qui représente les différentes éditions 
    possibles, puis minimiser.... Mais pour quelle complexité ?
\end{remarque}

\subsection{Annexe recherche de motif}

\begin{itemize}
    \item L'arbre des suffixes d'un texte 
        est une sorte d'automate fini déterministe. 
        Que se passe-t-il si on considère son intersubsection 
        avec un automate ? Est-ce que cela donne tous les facteurs 
        du texte qui vérifient une expression rationnelle ? 

    \item Problème dual, l'automate de Simon permet-il 
        de trouver un ensemble de textes pour lesquels 
        le mot est facteur ? (donnés via une expression rationnelle) ?
\end{itemize}

\newpage

\subsection*{Bibliographie}

\begin{itemize}
    \item \url{http://blog.notdot.net/2010/07/Damn-Cool-Algorithms-Levenshtein-Automata}.
    \item \url{http://julesjacobs.github.io/2015/06/17/disqus-levenshtein-simple-and-fast.html}
    \item \url{https://github.com/julesjacobs/levenshtein} 
    \item ??? Jewels of Stringology ???
\end{itemize}
