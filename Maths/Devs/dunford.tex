
\begin{references}
\item[Rombaldi] page 606
\end{references}

On suppose que $\mathbb{K}$ est algébriquement clos. 
On pose $u \in \Lin{E}$, avec $E$ un $\mathbb{K}$-ev de dimension $n$.

Comme $\mathbb{K}$ est algébriquement clos, le polynôme caractéristique 
de $\chi_u$ est scindé et s'écrit 

\begin{equation}
    \chi_u = \prod_{k = 1}^p (X - \lambda_k)^{\alpha_k}
\end{equation}
On pose $P = \prod_{k = 1}^p (X - \lambda_k)$.

On note $u = d + v$ la décomposition de Dunford de l'endomorphisme $u$.

On sait que le polynôme minimal de $d$ est $P$, et que $d$ est un polynôme en
$u$.
L'idée est de rechercher $d$ comme solution de l'équation 
$P(w) = 0$ avec $w$ dans $\mathbb{K}[u]$. Pour cela on utilise la méthode de
Newton 

\begin{equation}
    \begin{cases}
        w_0 = u \\
        w_{k+1} = w_k - P(w_k) (P'(w_k))^{-1}
    \end{cases}
\end{equation}

La question est de savoir si cette définiton a bien un sens.
On montre par induction sur $k$ les propriétés suivantes~:

\begin{enumerate}[(i)]
    \item $w_k$ est un polynôme en $u$
    \item $P'(w_k)$ est inversible dans $\mathbb{K}[u]$
    \item $P(w_k)$ est nilpotent 
\end{enumerate}

\subsection{Initialisation}

\begin{enumerate}[(i)]

    \item On remarque que $u$ est un polynôme en $u$.

    \item Comme $P$ est scindé à racines simples, $P$ et $P'$ n'ont aucune 
racine commune, et donc il en va de même pour $\chi_u$ et $P'$.
Les polynômes étant scindés, et non nuls ils sont donc premiers entre eux et 
par le théorème de Bézout il existe $U,V$ tels que

\begin{equation}
    U \chi_u + V P'  = 1
\end{equation}

En appliquant cette relation à $u$ on déduit que $P'$ est inversible et que son 
inverse est un polynôme en $u$.

    \item $P(u)^k = P^k (u)$, en particulier, pour $k$ supérieur à tous les
        $\alpha_i$ on déduit $\chi_u | P^k$ puis $P(u)^k = P^k (u) = 0$.


\end{enumerate}

\subsection{Hérédité}

\begin{enumerate}[(i)]
    \item On constate que $w_{k+1}$ est bien défini, et est dans $\mathbb{K}[u]$
comme somme de produits d'éléments de $\mathbb{K}[u]$.

    \item En utilisant la formule de Taylor il existe un polynôme $Q \in
        \mathbb{K}[X,Y]$ vérifiant

        \begin{equation}\label{eqn:dunford:taylor1}
            P'(Y) - P'(X) = (Y - X) Q(X,Y)
        \end{equation}

        En effet on a la formule suivante quand $p \geq 2$.

        \begin{equation}
            P'(Y) = \sum_{j = 0}^{p-1} \frac{(P')^{(j)}}{j!} (X) \times (Y -
            X)^j
        \end{equation}

        En appliquant la formule \ref{eqn:dunford:taylor1} à $w_{k+1}$ et $w_k$
        on obtient alors

        \begin{equation}
            P'(w_{k+1}) - P'(w_k) \in P(w_k) (P'(w_k))^{-1} \mathbb{K}[u]
        \end{equation}

        Par hypothèse de récurrence, on déduit que $R_k(u) = P'(w_{k+1}) - P'(w_k)$ est 
        nilpotent. Ce qui prouve que $P'(w_{k+1}) = P'(w_k) + R_k(u)$ est
        inversible dans $\mathbb{K}[u]$ \footnote{$(a - b)^{-1} = (a (I -
        a^{-1}b))^{-1} = (I - a^{-1}b)^{-1} a^{-1} = \sum a^{-k-1}b^k$}

    \item On utilise une fois de plus la formule de Taylor sur les polynômes

        \begin{equation}
            P(Y) = P(X) + P'(X) (Y - X) + (Y - X)^2 Q(X,Y)
        \end{equation}

        On a donc $P(w_{k+1}) \in  (P(w_k) (P' (w_k))^{-1})^2 \mathbb{K}[u]$,
        ce qui permet de conclure sur sa nilpotence.
\end{enumerate}

\subsection{Convergence}

Maintenant que la suite est bien définie, on veut montrer qu'elle converge vers 
la partie diagonalisable de la décomposition de Dunford.

\begin{description}
    \item[La suite est stationnaire] 

        En étudiant l'équation de récurrence, on remarque que 
        $P(w_{k+1}) \in P(w_k)^2 \mathbb{K}[u]$. On constate donc 
        que $P(w_k) \in P(u)^{2^k} \mathbb{K}[u]$ et donc 
        que la suite stationne en un nombre d'étapes 
        \emph{logarithmique} par rapport à la taille du plus gros 
        espace caractéristique.

    \item[La suite converge vers $d$]

        Si la suite stationne à partir du rang $n$. On a $w_n$ diagonalisable 
        car annulé par un polynôme scindé à racines simples, et on a
        $u - w_n = w_0 - w_n = \sum_{j = 0}^{n-1} w_j - w_{j+1}$. Or 
        chacune des différences est un polynôme en $u$ qui est nilpotent, 
        en particulier cette somme est nilpotente, et donc on a une
        décomposition $u = w_n + w'$ ... C'est la décomposition de Dunford 
        par unicité de celle-ci.
\end{description}

\subsection{Annexe effectivité}


\begin{description}
    \item[Ordres de grandeurs du nombre d'opérations]

\begin{equation}
    \begin{array}{llr}
        \textbf{Opération}         & \textbf{Nombre d'opérations} \\ %\hline
        \textrm{Somme de polynômes}             & \mathcal{O} (k + k') \\
        \textrm{Dérivation de polynôme}        & \mathcal{O} (k) \\
        \textrm{Produit de polynômes}           & \mathcal{O} (k \log k)    & \textrm{ via FFT } \\
        \textrm{Bézout}            & \mathcal{O} (k \log^2 k)  & \textrm{ via
        euclide étendu } \\
        \textrm{Somme de matrices} & \mathcal{O} (d^2) \\
        \textrm{Produit de matrices} & \mathcal{O} (d^3) \\
        \textrm{Calcul de déterminant} & \mathcal{O} (d^3) \\
    \end{array}
\end{equation}
    
    \item[Complexité globale de l'algorithme]

        L'algorithme fait en gros $\log n$ itérations (degré de nilpotence),
        où l'on calcule $A_j - P(A_j) (P')^{-1} (A_j)$. Le calcul de $(P')^{-1}$
        se fait en $\mathcal{O}(n^n)$ (oupsi ...).

        \todo[inline]{On aurait pu remarquer dès le début 
            que le $U$ fournit par Bézout marche en fait pour
            inverer $P'(M)$ pour toute matrice $M$
            qui vérifie $P^d (M) = 0$ ... Ce qui évite 
            d'avoir à faire le calcul compliqué}

    \item[Pourquoi on demande scindé sur le corps ?]
        On peut demander plus faible~: les racines de $P$ dans une clôture
        algébrique sont séparables sur $\mathbb{K}$, ou encore 
        les fecteurs irréductibles de $P$ dans $\mathbb{K}[X]$ sont de dérivée
        non nulle.

        Cela permet de faire tous les calculs dans une extension où le polynôme 
        est scindé à racines simples... Sinon on a le contre exemple suivant.

        On pose $\mathbb{K} = \cfin{p}(T)$ (le corps des 
        fractions rationnelles) et $P = X^p - T$. On a bien $P$ 
        irréductible, mais dans une clôture algébrique, $P$ ne possède 
        qu'une seule racine de multiplicité $p$ car 
        $\alpha^p - T = 0$ et $\beta^p - T = 0$ implique
        $(\alpha - \beta)^p = 0$ et par injectivité de Frobenius\footnote{Une puissance 
        est nulle si et seulement si l'élément est nul} on déduit $\alpha =
        \beta$.

        \todo[inline]{Terminer cette preuve chiante d'un contre exemple dans 
        un corps non parfait}
    
    \item[Que faire quand le polynôme n'est pas scindé mais le corps sympa ?] 
        C'est pas grave, ça marche quand même dans un surcorps, 
        et donc on obtient une décomposition de type "diagonalisable dans un
        sucrorps" plus nilpotente. En fait on obtient semi-simple plus
        nilpotente du coup.

    \item[Doit-on scinder le polynôme ?]
        NON ! Tous les calculs peuvent se faire via des calculs de divisions 
        euclidienne et produits de polynômes. Le calcul du polynôme "sans
        facteurs carrés" se fait via $P / (P \wedge P')$ dans un corps 
        de caractéristique nulle, et via un calcul un peu plus explicite 
        dans un corps de caractéristique non nulle. (cf la première 
        partie de Berlekamp).

\end{description}
