\begin{references}
    \item[Spizgrals L3]
    \item[Rombaldi] page 155
\end{references}
On étudie les sous groupes compacts de $GL_n(\mathbb{R})$,
en particulier on veut démontrer le théorème suivant.

\begin{theoreme}
    Tout sous groupe compact de $GL_n( \mathbb{R})$ est 
    conjugué à un sous groupe de $O_n( \mathbb{R})$.
\end{theoreme}

\subsection{Préliminaires}

On rappelle que si $q$ et $q'$ sont deux formes quadratiques 
équivalentes, alors $O(q)$ est conjugué à $O(q')$, 
c'est le petit calcul trivial qui dit que 

\begin{center}
\fbox{\begin{minipage}{0.9\linewidth}
\begin{equation}
    q'(x,y) = q(u(x), u(x)) \implies 
    u O(q') u^{-1} = O(q)
\end{equation}
\end{minipage}
}
\end{center}

\begin{lemme}[Carathéodory]
    Soit $E$ un $\mathbb{R}$ espace vectoriel de dimension $n$,
    $A$ une partie de $E$.

    \begin{equation}
        \textrm{Conv}(A) = 
        \left\{
            \sum_{i = 1}^{n+1} \alpha_i x_i
            ~\big|~
            x_i \in A
        \right\}
    \end{equation}
\end{lemme}

\begin{proof}
    Soit $x = \sum_{i = 1}^p \alpha_i x_i$ un élément 
    de $\textrm{Conv}(A)$ avec $p$ minimal.
    
    Supposons par l'absurde que $p \geq n+2$.
    En fixant $x_1$, on sait que la famille 
    $(x_i - x_1)_{i > 1}$ est liée dans $E$
    car de taille plus grande que $n+1$\footnote{L'idée est 
        de se servir du fait que $(x_1, \dots, x_p)$ 
        n'est pas un repère affine, et pour cela, il faut 
    se ramener au cas linéaire en fixant un des points}.

    Il existe donc des $\lambda_i$ tels que

    \begin{equation}
        \sum_{i = 2}^p \lambda_i (x_i - x_1) = 0
    \end{equation}

    C'est à dire, 
    \begin{equation}
        \sum_{i = 2}^p \lambda_i x_i = \left(\sum_{i = 2}^p \lambda_i \right)x_i
    \end{equation}

    On pose $\lambda_1 = -\sum_{i = 2}^p \lambda_i$
    pour que $\sum \lambda_i = 0$.
    
    On remarque alors que 
    
    \begin{equation}
        \sum_{i = 1}^p (\alpha_i + \lambda_i) x_i = x
    \end{equation}

    Malheureusement, ce n'est pas nécessairement une combinaison convexe. 
    D'un côté, on sait que $\sum (\alpha_i + \lambda_i) = 1$ ce qui est bien,
    mais de l'autre $\alpha_i + \lambda_i$ peut être négatif ...

    On va donc pondérer par un paramètre $t$ les $\lambda_i$ afin de
    s'assurer que tous les termes de cette combinaison soient positifs.

    \begin{equation}
        \forall t \in \mathbb{R},
        \sum_{i = 1}^p (\alpha_i + t \lambda_i) x_i = x
    \end{equation}

    En posant $t = \inf \{ \tau ~|~ \forall i, \alpha_i + \tau \lambda_i \geq 0
    \}$ on peut presque conclure.

    \begin{enumerate}[(i)]
        \item L'ensemble sur lequel on considère l'inf n'est pas vide 
            car il contient $0$
        \item L'ensemble sur lequel on considère l'inf est minoré 
            par le minimum des $-\alpha_i / \lambda_i$ pour $\lambda_i$ non nul.
    \end{enumerate}

    En réalité, cet inf est atteint pour un des $-\alpha_j /
    \lambda_j$\footnote{ C'est assez clair si on regarde l'ensemble 
    comme une intersection d'intervalles de $\mathbb{R}$}.

    En posant $t = - \alpha_j / \lambda_j$ on a donc non seulement une 
    combinaison convexe, mais on annule un terme dans la somme.

    \begin{equation}
        \sum_{i = 1 \wedge i \neq j}^p (\alpha_i + t \lambda_i) x_i = \sum_{i = 1}^p (\alpha_i + t \lambda_i) x_i = x
    \end{equation}

    Ce qui contredit la minimalité de $p$.
\end{proof}

\begin{remarque}
    En conséquence \emph{l'enveloppe convexe d'un compact en dimension 
    finie est compacte}, en effet on remarque que l'application continue
    suivante est aussi \emph{surjective}

    \begin{equation}
        \begin{array}{cccc}
            \Phi : & A^{n+1} \times \{ (\alpha_i) ~|~ \alpha_i \geq 0, \sum
            \alpha_i = 1 \} & \longrightarrow & \textrm{Conv}(A) \\
                            & ((x_i),(\alpha_i)) & \mapsto & \sum \alpha_i x_i
        \end{array}
    \end{equation}
    
    Donc comme image d'un compact par une application continue,
    $\textrm{Conv}(A)$ est compacte.
\end{remarque}


\subsection{Cas des groupes finis}

\begin{remarque}
    C'est vrai pour tout groupe fini de manière évidente 
    en considérant le produit scalaire renormalisé 
    comme dans la preuve de Machke 

    \begin{equation}
        \phi (x,y) = \frac{1}{|G|} \sum_{ g \in G} \langle g x ~|~ g y \rangle 
    \end{equation}

    C'est bien un produit scalaire, et celui-ci rend tous les éléments 
    de $G$ \emph{orthogonaux}. 

    On a donc $G \subseteq O(\phi)$,
    mais comme $\phi$ est un produit scalaire, 
    $\phi$ est congrue à $\langle \cdot | \cdot \rangle$.
    Ainsi, $u^{-1} G u \subseteq u^{-1} O(\phi) u = O(E)$.
\end{remarque}

La méthode générale est donc de trouver une forme quadratique 
définie positive $\phi$ invariante par $G$. Après cela, en utilisant 
le changement de base $u$ adapté à $\phi$ on passe de $G \subseteq O (\phi)$
à $u^{-1} G u \subseteq O (\mathbb{R})$ pour le produit scalaire canonique.

On peut comprendre la méthode comme suit~: $G$ agit sur l'espace des 
formes quadratiques sur $E$ via\footnote{C'est une action à \emph{droite} !!!} 
$q(\cdot, \cdot) \mapsto q (g \cdot, g \cdot)$.
Pour construire une forme invariante par l'action à droite de $G$ 
on considère la moyenne de l'orbite du produit scalaire canonique par $G$.
C'est clairement quelque chose qui est invariant, c'est un produit 
scalaire, et donc on peut se ramener au petit calcul de changement de base.

\subsection{Kakutani}

\begin{lemme}
    Soit $H$ un sous groupe compact de $GL(E)$, $K$ un compact
    convexe non vide de $E$ stable par $H$. Alors $H$ a un point fixe dans $K$
\end{lemme}

\begin{proof}

    On fixe une norme euclidienne $\| \cdot \|_2$ sur $E$
    et on pose 

    \begin{equation}
        \| x \| = \max_{h \in H} \| h (x) \|_2 
    \end{equation}

    Cette définition est légitime car $H$ est compact et 
    que les opérateurs sont continus.

    \begin{enumerate}[(i)]
        \item Cela définit une norme sur $E$ 
            (vérifications laissées au lecteur)
        \item Cette norme est invariante par $H$
            car $\| h (x) \| = \max_{h ' \in H } \| hh' (x) \|_2 = \| x \|$
            car $H$ est un groupe
        \item 
            Cette norme est strictement convexe, en effet 
            si $x \neq y$ en utilisant la stricte convexité 
            de $\| \cdot \|_2$ on peut conclure~:

            \begin{equation}
                \left\| \frac{x + y}{2} \right\|
                = \left\| h_0 \left( \frac{x + y}{2} \right) \right\|_2
                = \left\| \frac{ h_0 (x) + h_0 (y)}{2} \right\|_2
                < \frac{\| h_0 (x) \|_2 + \| h_0 (y) \|_2}{2} 
                \leq \frac{\| x \| + \| y \|}{2}
            \end{equation}

    \end{enumerate}


    Choisissons alors $s \in K$ avec $\| s \|$ minimale, c'est possible 
    car $K$ est compact non vide. Comme $K$ est convexe, et par stricte 
    convexité de $\| \cdot \|$ cet élément est unique. 

    Puisque $K$ est stable par $H$ $h(s) \in K$. De plus $\| \cdot \|$ est invariant 
    par $H$, on déduit que $\| h(s) \| = \|s\|$, c'est-à-dire via 
    l'unicité évoquée plus haut, que $s$ 
    est un point fixe de $H$ dans $K$.
\end{proof}

\subsection{Utilisation}

\todo[inline]{Tout refaire sans matrices ... c'est inutile}

L'idée dans le cas non fini est de trouver un point fixe pour l'action de $G$
via le théorème de Kakutani. Pour cela on va utiliser fortement la représentation 
matricielle est l'espace $S_n$ des matrices symétriques. 

On fait donc agir\footnote{C'est encore une action à \emph{droite} ... il faut
changer ?} $G$ représenté matriciellement sur $S_n$ via

\begin{equation}
    \rho : g \cdot s \mapsto {}^t g  s g 
\end{equation}

Le sous ensemble des matrices symétriques définies positives 
est convexe et stable sous l'action de $G$. Toutefois il n'est pas 
compact ce qui ne permet pas d'appliquer directement le lemme.

Le compact naturel à considérer au vu des explications précédentes 
est l'orbite de $I_n$ par l'action de $G$. C'est bien un compact 
contenu dans l'ensemble des matrices symétriques définies positives.
On considère donc $K = \textrm{Conv} (G \cdot I_n)$, par le théorème 
de Carathéodory c'est un convexe compact, contenu dans SDP
car SDP est convexe.


\todo[inline]{Préciser l'action affine préserve l'enveloppe convexe ...} 
L'ensemble $K$ reste stable sous l'action de $G$ car 
$g \cdot K = g \cdot (\textrm{Conv} (G \cdot I_n)) = \textrm{Conv} (g \cdot G \cdot I_n)
= K $. En effet l'action de $G$ est affine. 

On a donc bien $H = \rho (G)$ un sous groupe compact de $GL(E)$,
$K$ compact convexe non vide, stable par $H$. En utilisant le point 
fixe de Kakutani on possède donc bien un élément $s \in K$ fixé par $H$.


En sélectionnant une base orthogonale pour $s$, et en notant $P$ la matrice 
de changement de base associée, on constate via la formule du changement 
de base pour les formes quadratiques 

\begin{equation}
    {}^t P s P = Id_n
\end{equation}

Cela se traduit par $P^{-1} g P \in O (\mathbb{R})$, via 

\begin{equation}
    {}^t (P^{-1} g P) (P^{-1} g P)
    = 
    {}^t P {}^t g {}^t P^{-1} P^{-1} g P 
    = {}^t P s P 
    = I_n
\end{equation}

\subsection{Remarques post développement}

En réalité, on peut continuer sur la lancée "je prends 
le barycentre sur l'orbite de l'identité" pour conclure, 
en "reprouvant localement" le théorème de point fixe.

La solution est la suivante dans les grandes lignes

\begin{enumerate}
    \item On peut recouvrir $G$ par un nombre fini 
        de boules ouvertes de rayon $R$, telles
        que pour deux éléments $g,g'$ dans une boule,
        la distance des formes quadratiques $q_g$, $g_{g'}$
        soit inférieure à $\varepsilon$.

        Avec bien entendu $q_g (x) = \langle g x ~|~ g x \rangle$
        C'est possible par compacité et parce qu'on est en dimension 
        finie du coup il n'y a aucun problème avec les normes 
        en général.
    
    \item Étant donné un tel recouvrement $U_1, \dots, U_n$
        on sélectionne dans chaque $U_i$ un élément $g_i$.
        On pose alors 
        \begin{equation}
            \phi_\varepsilon (x,y) = \frac{1}{n} \sum_{i = 1}^n \langle g_i x
                ~|~ g_i y \rangle
        \end{equation}

        Bien entendu le $n$ dépend du $\varepsilon$.

    \item On constate immédiatement que les éléments de $G$, bien que n'étant 
        pas des isométries pour $\phi_\varepsilon$, sont des
        \emph{quasi}-isométries, puisque 

        \begin{equation}
            |  \phi_\varepsilon (g x, g x) - \phi_\varepsilon (x,x) | 
            \leq \varepsilon
        \end{equation}

        Il suffit de remarquer que si $g_i \in U_i$, alors $g_i g \in U_i g$,
        or la multiplication par $g$ est une bijection de $G$ dans $G$, donc 
        $U_i g = U_j$ pour un certain $j$, et réciproquement. Chaque différence 
        étant alors majorée par $\varepsilon$, on conclut.

    \item On remarque que $\phi_\varepsilon$ est toujours dans un compact. En
        effet c'est le barycentre d'éléments dans l'orbite du produit scalaire 
        usuel pour l'action de $G$. Or, $G$ est compact, et son action 
        est continue, donc $\phi_\varepsilon$ évolue dans un \emph{compact}
        contenu dans l'ensemble des formes quadratiques définies positives.

    \item En considérant une suite extraite, on fait converger
        $\phi_\varepsilon$ vers $\phi$, ce qui permet en passant 
        à la limite de conclure que tout élément de $G$ est une isométrie 
        pour $\phi$, qui est un produit scalaire, et hop on a conclu.
\end{enumerate}

C'est encore plus fait à la main, mais cela illustre parfaitement l'idée 
générale de la moyenne, et l'utilisation de l'enveloppe convexe devient 
particulièrement claire.

\subsection{Applications et conséquences}


\todo[inline]{Compléter et détailler cette liste}
\begin{enumerate}
    \item Sous groupes compacts maximaux ?
    \item Enveloppe convexe de la boule unité ?
    \item autres ?
\end{enumerate}

\begin{description}
    \item[Exercice FGN Algèbre 3 page 231] 
        Une norme $N$ telle que le groupe d'isométries $G_N$ associé à $N$
        agit transitivement sur $S_N$ la sphère unité pour cette norme 
        est euclidienne.

        En effet, $G_N$ est un sous groupe compact car fermé borné,
        donc $G_N \subseteq O(q)$ pour une certaine forme quadratique définie
        positive $q$.

        Mais alors, si $G$ agit transitivement sur $S_N$, on a 
        \begin{equation}
            \forall x \in S_N,
            \exists g \in G_N,
            \exists y \in S_N,
            q(x) = q(g(y)) = q(y)
        \end{equation}

        Ainsi $q$ est constante sur $S_N$, mais par "quadraticité"

        \begin{equation}
            q \left( \frac{x}{N(x)} \right) = \frac{1}{N(x)^2} q(x) 
        \end{equation}

        Donc pour tout $x$ on a $q(x) = N(x)^2 \alpha$,
        avec $\alpha$ la constante de $q$ sur $S_N$. Ainsi,
        $N(x)$ est proportionnelle à $\sqrt{q}$ qui est une norme euclidienne.
\end{description}
