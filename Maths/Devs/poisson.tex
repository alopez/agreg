\begin{references}
    \item[ZQ] page 93
    \item[FGN Analyse 2]
\end{references}


\subsection{Démonstration de la formule sommatoire}

On veut démontrer la formule de Poisson, c'est-à-dire 

\begin{equation}
    \sum_{-\infty}^{+\infty} F(x+2n \pi) = 2\pi \sum_{-\infty}^{+\infty} \hat{F}(2\pi
    n) e^{inx}
\end{equation}

\begin{description}
    \item[Énoncé des hypothèses]

        On suppose $F \in L^1 \cap C^0$ afin de pouvoir définir $\hat{F}$
        et d'avoir assez de régularité sur les coefficients de Fourier de $F$.

        On suppose de plus 
        \begin{equation}
            \sum_{-\infty}^{+\infty} |\hat{F}(n)| < +\infty
        \end{equation}

        \begin{equation}
            \exists M > 0,
            \alpha > 1, 
            \forall x \in \mathbb{R},
            |F(x)| \leq M(1+|x|)^{-\alpha}
        \end{equation}

        Ce qui donne une garantie sur la décroissance de la fonction
        suffisante pour pouvoir sommer.

    \item[Introduction du périodisé]
        Afin de faire le lien entre les coefficients de Fourier de $F$
        et sa transformée de Fourier, on périodise la fonction.
        Pour cela on considère

        \begin{equation}
            f(x) = \sum_{-\infty}^{+\infty} F(x + 2\pi n)
        \end{equation}

        Cette série est normalement convergente car $F$ décroit 
        suffisamment vite. En effet
        si $|x| \leq A$, on a convergence normale car $\alpha > 1$, 
        via une comparaison avec une série de Riemann~:

        \begin{equation}
            |F(x + 2\pi n)|
            \leq M(1+|x + 2\pi n|)^{-\alpha}
            \leq M\left(\frac{1}{1 + (2\pi n - A)}\right)^{\alpha}
        \end{equation}

        On a donc $f$ qui est bien définie, et comme il y a convergence 
        uniforme sur tout compact, $f$ est aussi continue.

        De plus, on a par définition $f(x + 2\pi) = f(x)$
        donc $f$ est $2\pi$-périodique !

    \item[Calcul des coefficients de Fourier de $f$]

        \begin{align}
            c_m (f) &= \frac{1}{2\pi} \int_0^{2\pi} f(t) e^{-imt} dt \\
                    &= \frac{1}{2\pi} \int_0^{2\pi} \sum_{-\infty}^{+\infty}
                        F(t + 2\pi n) e^{-imt} dt \\
                    &= \frac{1}{2\pi}  \sum_{-\infty}^{+\infty}\int_0^{2\pi}
                        F(t + 2\pi n) e^{-imt} dt 
                        & \text{ Via Fubini car il y a convergence normale }
                        \\
                    &= \frac{1}{2\pi}  \sum_{-\infty}^{+\infty}\int_0^{2\pi}
                        F(t + 2\pi n) e^{-mi(t + 2\pi n)} dt 
                        & \text{ Par $2\pi$ périodicité }
                        \\
                    &= \frac{1}{2\pi}\sum_{-\infty}^{+\infty}  \int_{2\pi n}^{2\pi (n+1)}
                        F(t) e^{-imt} dt 
                        & \text{ Changement de variable }
                        \\
                    &= \frac{1}{2\pi} \int_{-\infty}^{+\infty}
                        F(t) e^{-mit} dt 
                        \\
                    &= \frac{1}{2\pi} \hat{F}(m)
        \end{align}

        On a donc bien un lien entre les coefficients de Fourier du
        périodisé de $F$ et sa transformée de Fourier ... C'est logique !

    \item[Conclusion]
        On sait que $\sum |\hat{F}(n)| < +\infty$, 
        et donc via l'égalité précédente
        $\sum |c_n(f)| < +\infty$.

        Comme la fonction est \emph{continue} et 
        que sa série de Fourier converge absolument, on 
        l'égalité 

        \begin{equation}
            f(x) = \sum_{m \in \mathbb{Z}} c_m(f) e^{imx}
                 = \frac{1}{2\pi} \sum_{m \in \mathbb{Z}} \hat{F}(m) e^{imx}
        \end{equation}
    
        Par construction de $f$, on déduit alors l'égalité attendue

        \begin{equation}
            2\pi \sum_{m \in \mathbb{Z}} F(x + 2m \pi) 
            = 
            \sum_{m \in \mathbb{Z}} \hat{F}(m) e^{imx}
        \end{equation}
        
    \item[Dans la classe de Schwartz, tout se passe bien]
        En effet, automatiquement $f$ et $\hat{f}$ sont 
        à décroissance rapide, et les hypothèses sont vérifiées.

    \item[Application à une Gaussienne]

        Si on pose $f(x) = e^{-x^2/(4\alpha t)}$ avec $\alpha,t > 0$ 
        (la Gaussienne centrée d'écart type $2\alpha t$) qui est vérifie
        clairement les hypothèses de l'énoncé, 
        on trouve alors  pour $x \in \mathbb{R}$

        \begin{equation}
            2\pi \sum_{n \in \mathbb{Z}}
            \exp\left( 
                - \frac{(x - 2n \pi)^2}{4 \alpha t} 
            \right)
            = 
            \sum_{n \in\mathbb{Z}}
            \hat{f} e^{inx}
        \end{equation}

        Or la transformée de Fourier d'une Gaussienne est connue, c'est 

        \begin{equation}
            \hat{f}(\xi) = \frac{1}{\sqrt{2 \pi (2 \alpha t)}} \exp\left( -
            \frac{\xi^2 2 \alpha t}{2} \right)
        \end{equation}

        Ce qui prouve que 

        \begin{equation}
            \sqrt{\frac{\pi}{\alpha t}} \sum_{n \in \mathbb{Z}}
            \exp\left( 
                - \frac{(x - 2n \pi)^2}{4 \alpha t} 
            \right)
            = 
            \sum_{n \in\mathbb{Z}}
            e^{-\alpha n^2 t} e^{inx}
        \end{equation}

    \item[Conclusion sur la fonction $\Theta$ de Jacobi]
        en posant $\alpha = \pi$ et $x = 0$, on déduit 
        immédiatement 
        \begin{equation}
            \frac{1}{\sqrt{t}} \Theta \left(\frac{1}{t}\right)
            = \Theta (t)
        \end{equation}

        Avec $\Theta(x) = \sum_{n \in \mathbb{Z}} e^{-\pi n^2 x}$.


    \item[Résolution de l'équation de la Chaleur]

        Pour $t > 0$ on pose sur le Tore

        \begin{equation}
            p_t (x) = \sum_{n \in \mathbb{Z}} e^{-n^2t}e^{inx}
        \end{equation}

        En utilisant la formule de Poisson sur les gaussiennes 
        avec $\alpha = 1$ on déduit 

        \begin{equation}
            p_t (x) = 
            \sqrt{\frac{\pi}{t}} \sum_{n \in \mathbb{Z}}
            \exp\left( 
                - \frac{(x - 2n \pi)^2}{4 t} 
            \right)
        \end{equation}

        C'est une fonction de classe $C^\infty$ (convergence normale
        et dérivation terme à terme) sur $\mathbb{R}_+^* \times
        \mathbb{T}$.
        Si on se donne une condition initiale $f \in \mathcal{C}(\mathbb{T})$
        alors la convolée $f \star p_t (x)$ est de classe $C^\infty$
        sur le même ensemble.

        De plus, on constate que $p_t$ vérifie (sur son domaine 
        de définition) l'équation (via une dérivation terme à terme)

        \begin{equation}
            \frac{\partial p_t}{\partial t} = \frac{\partial^2 p_t}{\partial
            x^2}
        \end{equation}

        Ainsi, en posant $u$ comme suit

        \begin{equation}
            \begin{cases}
                u(0,x) = f(x) \\
                u(t,x) = (f \star p_t) (x)
            \end{cases}
        \end{equation}

        On construit une solution de l'équation de la Chaleur sur
        $\mathbb{R}_+^* \times \mathbb{T}$ (car la convolution 
        passe bien à la dérivation).

        Il suffirait que $p_t$ est une approximation de l'unité 
        pour pouvoir conclure que cette solution est continue 
        sur $\mathbb{R}_+ \times \mathbb{T}$ via les théorèmes 
        généraux sur la convolution.

        Or, on peut utiliser l'expression sous forme de somme 
        de gaussiennes~:

        \begin{align}
            f \star p_t (x) 
            &= \frac{1}{2\pi} \int_0^{2\pi} f(y) 
            \sqrt{\frac{\pi}{t}} \sum_{n \in \mathbb{Z}}
            \exp\left( 
                - \frac{(x - 2n \pi)^2}{4 t} 
            \right) \\
            &= \frac{1}{\sqrt{4\pi t}}
            \sum_{n \in \mathbb{Z}}
            \int_0^{2\pi} f(y) 
            \exp\left( 
                - \frac{(x - y - 2n \pi)^2}{4 t} 
            \right) \\
            &= \frac{1}{\sqrt{4\pi t}}
            \sum_{n \in \mathbb{Z}}
            \int_{2n\pi}^{2(n+1)\pi} f(y - 2n\pi) 
            \exp\left( 
                - \frac{(x - y)^2}{4 t} 
            \right) \\
            &= \frac{1}{\sqrt{4\pi t}}
            \sum_{n \in \mathbb{Z}}
            \int_{2n\pi}^{2(n+1)\pi} f(y) 
            \exp\left( 
                - \frac{(x - y)^2}{4 t} 
            \right) \\
            &= \frac{1}{\sqrt{4\pi t}}
            \int_{-\infty}^{+\infty} f(y) 
            \exp\left( 
                - \frac{(x - y)^2}{4 t} 
            \right) \\
            &= f \star_\mathbb{R} g_{4 t} 
        \end{align}

        On a donc bien une convolution avec un noyau gaussien, qui 
        est une approximation de l'unité, et donc on a la continuité de $u$ !

        Mieux, on retrouve un principe du maximum puisque pour $t > 0$
        (de diffusion de la chaleur)

        \begin{equation}
            \| u(t,\cdot) \|_\infty \leq \| f \star_\mathbb{R} g_{4 t} \|_\infty 
                \leq \| f \|_\infty \| g_{4 t} \|_1 = \|f \|_\infty
        \end{equation}
\end{description}
