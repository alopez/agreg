\begin{references}
    \item[Allaire Kaber]
    \item[Ciralet] p 95 / p 102
    \item[FGN Alèbre 3] page 169
\end{references}

\subsection{Méthode itérative}

On suppose $A \in GL_n(\mathbb{R})$
et on cherche à résoudre $Ax = b$.

L'idée est de découper $A = M - N$
avec $M$ "facilement inversible".

Ainsi,

\begin{equation}
    Ax = b 
    \iff
    (M - N)x = b
    \iff
    x  = M^{-1}(b + Nx)
\end{equation}

On a donc transformé une équation 
"classique" en une recherche de point fixe.

Très naturellement, on s'intéresse alors 
à la suite 

\begin{equation}
    \begin{cases}
        x_0 = b \\
        x_{k+1} = M^{-1}(b + Nx_k) 
    \end{cases}
\end{equation}

Si cette suite converge, c'est vers 
une solution du système. On se demande 
donc de manière très naturelle sous 
quelles conditions cette suite converge.

\subsection{Householder et rayon spectral}

\begin{theoreme}[Householder]
    Pour une matrice carrée $A$ les 
    trois quantités suivantes sont 
    égales

    \begin{enumerate}[(i)]
        \item $\rho (A)$
        \item $\inf \{ \|A\| ~|~ \| \cdot \| \text{ norme matricielle } \}$
        \item $\inf \{ \|A\| ~|~ \| \cdot \| \text{ norme subordonnée } \}$
    \end{enumerate}
\end{theoreme}

\begin{proof}
    Montrons dans un premier temps que $\rho(A)$ minore 
    toute norme matricielle. Soit $\| A \|$ une 
    norme matricielle, et $v$ un vecteur propre 
    associée à une valeur propre de module maximal 
    de $A$. Alors on a

    \begin{equation}
        \| A v {}^t v \| \leq \| A \| \| v {}^t v\|
    \end{equation}
    
    Or $A v = \lambda v$, donc 
    $\| A v {}^t v \| = | \lambda | \| v {}^t v\|$.

    Comme $v$ est un vecteur propre, cette matrice 
    est non nulle\footnote{Pour qu'elle soit nulle,
    il faut que $v$ soit nul}. Elle est donc de norme 
    non nulle, et on obtient bien 

    \begin{equation}
        \rho(A) = |\lambda| \leq \| A \|
    \end{equation}


    Reste à montrer que l'on peut obtenir 
    $\rho(A) + \varepsilon$ pour une norme subordonnée.

    Pour cela, on considère $A$ comme une matrice à coefficients 
    dans $\mathbb{C}$, elle est alors trigonalisable.
    On a donc $PAP^{-1} = T$.

    On considère ensuite pour un $\delta > 0$ la matrice 
    de changement de base suivante~: 
    $e_i \mapsto \delta^i e_i$. Alors on constate que 

    \begin{equation}
        T e_j' = \sum_{i = 1}^j \delta^{j-i} t_{i,j} e_i
    \end{equation}


    \begin{equation}
        D_\delta T D_\delta^{-1}
        = 
        \begin{pmatrix}
            t_{1,1} &        &   \delta (*) \\
                    & \ddots &              \\
            (0)     &        & t_{n,n}      \\
        \end{pmatrix}
    \end{equation}

    En particulier, les coefficients de la matrice $T$ dans 
    la base $(e_1', \dots, e_n')$ sont multipliés par au moins 
    $\delta$ sauf sur la diagonale.
    
    On pose alors $\| x \| = \| D_\delta P x\|_\infty$.
    C'est une norme sur $\mathbb{C}^n$, mais aussi sur $\mathbb{R}^n$.

    On considère $\VERT \cdot \VERT$ la norme subordonnée 
    associée.
    Alors 

    \begin{align}
        \VERT A  \VERT
        &= 
        \sup_{x \neq 0} \frac{\| B x\|}{\| x\|} \\
        &=
        \sup_{x \neq 0} \frac{\| D_\delta P A x \|_\infty}{\|
        D_\delta P x\|_\infty} \\
        &= 
        \sup_{y \neq 0} \frac{\| D \delta P A P^{-1} D_\delta^{-1} x
        \|_\infty}{\| y \|} \\
        &\leq
        \max_{1 \leq i \leq n} 
        \sum_{j = 1}^n | D_\delta T D_\delta^{-1} |
        \leq |t_{i,i}| + \varepsilon
    \end{align}

    Or $|t_{i,i}| \leq \rho (T) = \rho (A)$ car $T$ 
    est triangulaire.
\end{proof}

\subsection{Application à l'étude de la convergence}

L'étude de la suite va être simplifié par l'introduction 
du vecteur d'erreur $e_k = x_k - u$ avec $u$ solution 
de l'équation $Ax = b$.

En effet, on obtient alors 

\begin{equation}
    e_{k+1} = x_{k+1} - u = M^{-1} (Nx_k + b) - u
            = M^{-1} (Nx_k + b) - M^{-1}(Nu + b)
            = M^{-1} N e_k
\end{equation}

On sait que notre méthode converge si et seulement si l'erreur tends 
vers $0$.

\begin{theoreme}
    La méthode converge pour tout $b$ si et seulement si $\rho(M^{-1}N) < 1$.
\end{theoreme}

\begin{proof}
    Si le rayon spectral est inférieur à $1$, on possède une norme induite
    $\VERT \cdot \VERT$ telle que $\VERT A \VERT < 1$, alors
    il est aisé de vérifier que 

    \begin{equation}
        \| e_k \| = \| (M^{-1}N)^k e_0 \| \leq \VERT M^{-1}N \VERT^k \| e_0 \| 
        \longrightarrow 0
    \end{equation}

    En revanche, si le rayon spectral est supérieur ou égal à $1$,
    il existe une valeur propre complexe $\lambda$ de module 
    supérieur ou égal à $1$ et on note $v$ un vecteur propre associé.

    Alors par construction si $e_0 = v$ (c'est-à-dire $b = v + u$)

    \begin{equation}
        e_k = \lambda^k v \not \rightarrow 0
    \end{equation}

    Pour que ce soit tout à fait correct, il faut prendre 
    $b$ dans $\mathbb{R}^n$, par exemple en posant 
    $v = v_1 + iv_2$ et en remarquant que comme $A$ est réelle,
    $Av = Av_1 + iAv_2$, donc que $v_1$ est un vecteur propre
    \emph{réel} associé à $\lambda$. 
\end{proof}

\begin{remarque}
    L'erreur suit une décroissance exponentielle
    (dite "linéaire en le nombre de chiffres significatifs")
    de raison $\rho(M^{-1}N)$. Ainsi, plus le rayon 
    spectral de cette matrice est faible, plus la vitesse 
    va converger rapidement.
\end{remarque}

\subsection{Comment choisir sa décomposition ?}

On considère par exemple la méthode de \fbox{Jacobi}, 
qui consiste, si la diagonale est non nulle,
à prendre $M = D$ et $N = E + F$. \textbf{En faisant le dessin du Ciralet}.


Le schéma numérique est alors le suivant~:

\begin{equation}
    a_{i,i} \times x_{k+1}^i = - \sum_{j \neq i} a_{i,j} x_k^j + b_i
\end{equation}

Les $x_{k+1}^i$ se calculent donc assez simplement,
Le nombre d'opérations pour passer de $x_k$ à $x_{k+1}$
est de $O(n^2)$ additions et multiplications, et $n$ divisions.

\begin{exemple}
    Si le rayon spectral est $1/2$, afin 
    d'avoir une précision en $1/2^{10}$ c'est à dire 
    au millième près il faut faire $10$ itérations.

    Globalement, pour une précision $\varepsilon$
    fixée, et un rayon spectral $\rho$
    il faut à peu près ce nombre de calcul~:

    \begin{equation}
        (\log_\rho \varepsilon) n^2
    \end{equation}

    Dans les cas pratiques, c'est beaucoup plus 
    efficace que le pivot de Gauss qui est en $n^3$.
\end{exemple}

\begin{remarque}
    Cette méthode bénéficie d'une parallélisation évidente,
    puisque toutes les coordonnées de $x_{k+1}$ sont 
    calculées de manière indépendantes !
    En utilisant une machine avec beaucoup d'unités de calculs 
    (par exemple un GPU) on peut donc obtenir 
    pour des matrices de taille raisonnable des 
    performances de l'ordre de $n$ par itération !
\end{remarque}

\begin{remarque}
    Une autre approche consiste à penser 
    de manière séquentielle et utiliser 
    le plus possible les informations déjà calculées,
    c'est à dire poser 

    \begin{equation}
        a_{i,i} \times x_{k+1}^i = 
            - \sum_{j < i} a_{i,j} x_{k+1}^j
            - \sum_{j > i} a_{i,j} x_{k}^j
            + b_i
    \end{equation}

    Cela ne nécessite alors plus qu'un seul segment 
    mémoire pour le calcul de $x_k$, qui se fait \emph{en place}.
    C'est très très intéressant car on divise par deux 
    l'utilisation de mémoire, mais aussi parce qu'on 
    est beaucoup plus susceptible d'être dans le cache ....

    C'est la méthode de \fbox{Gauss-Seidel}. La décomposition 
    est alors associée à la paire de matrices 

    \begin{equation}
        \begin{cases}
            M = D - E \\
            N = F
        \end{cases}
    \end{equation}

    Avec $D$ la diagonale de $A$, $E$ qui vaut 
    moins la partie inférieure, et $F$ qui 
    vaut moins la partie supérieure.
\end{remarque}



\subsection{Une comparaison sur le cas tridiagonal}

\begin{remarque}
    Le cas tridiagonal est très intéressant 
    puisqu'il intervient dès qu'on essaie 
    de discrétiser des équations différentielles
    avec des laplaciens. Ou bien en faisant 
    des schémas numériques de type saute-mouton.
\end{remarque}

\begin{theoreme}[Ciralet page 105]
    Pour une matrice tridiagonale, la méthode de 
    Gauss-Seidel est deux fois plus rapide
    que celle de Jacobi.
    C'est-à-dire que le rayon spectral $\rho_G$ 
    pour Gauss-Seidel est le carré du rayon 
    spectral $\rho_J$ pour Jacobi.
\end{theoreme}

\begin{proof}
    Soit $A$ une matrice tridiagonale,
    alors on va montrer que les valeurs propres
    non nulles de $(D - E)^{-1}F$ sont les carrés 
    des valeurs propres non nulles de $D^{-1}(E+F)$.

    Pour cela on remarque que les valeurs propres 
    des deux matrices sont les racines des polynômes 
    suivants 

    \begin{equation}
        \det (\lambda D - E - F) \quad \quad 
        \det (\lambda D - \lambda E - F) 
    \end{equation}

    Or si $\lambda$ est une racine non nulle du premier 
    polynôme, alors 

    \begin{align}
        \det (\lambda^2 D - \lambda^2 E - F)
        &=
        \lambda^n \det (\lambda D - \lambda E - \lambda^{-1} F)\\
        &= 
        \lambda^n \det (D_\lambda (\lambda D - E - F) D_\lambda^{-1})\\
        &= 
        \lambda^n \det (\lambda D - E - F)\\
        &= 0
    \end{align}

    Où $D_\lambda$ est la même matrice que dans la preuve de Householder.

    En faisant le calcul à l'envers on a la réciproque,
    et donc le rayon spectral pour la méthode de Gauss-Seidel 
    est le carré du rayon spectral pour la méthode de Jacobi,
    ce qui donne une vitesse de convergence doublée.
\end{proof}

\subsection{Post requis}

\todo[inline]{Conditionnement d'un système d'équation ?}
\todo[inline]{Méthode de calcul de valeurs propres ?}
\todo[inline]{Méthode de la relaxation ?}

