\begin{references}
    \item[Ciralet] page 182, 189
    \item[Devillier]
\end{references}

\subsection{Préliminaires}

\begin{description}
    \item[Rappels sur la convexité] Voir le Beck
    \item[Définition de $\alpha$-convexité]
    \item[Problèmes "elliptiques" ?]
\end{description}

\subsection{Développement}

Considérons $J : \mathbb{R}^n \to \mathbb{R}$, $C^1$ et elliptique~:

\begin{equation}
    \langle \nabla J(x) - \nabla J(y) ~|~ x - y \rangle \geq \alpha \| x - y
    \|^2
\end{equation}

On cherche à trouver le minimum de $J$ sur $\mathbb{R}$.

\begin{description}
    \item[La fonction $J$ est strictement convexe]

        C'est clair puisque $J$ est strictement convexe si et seulement 
        si pour $x \neq y$

        \begin{equation}
            \langle \nabla J(x) - \nabla J(y) ~|~ x - y \rangle > 0
        \end{equation}

        Ce qui est le cas puisque $\alpha > 0$.

    \item[La fonction $J$ est coercive]

        Pour cela on utilise Taylor 

        \begin{equation}
            J(x) - J(y) = \int_0^1 \langle \nabla J(y + t(x - y)) ~|~ x - y \rangle dt
        \end{equation}

        Cela permet d'écrire ensuite 

        \begin{equation}
            J(x) - J(y) = \langle \nabla J(y) ~|~ x - y \rangle
            +
            \int_0^1 \langle \nabla J(y + t(x - y)) - J(y)~|~ x - y \rangle dt
        \end{equation}

        En utilisant alors le caractère elliptique

        \begin{equation}
            J(x) - J(y) \geq \langle \nabla J(y) ~|~ x - y \rangle
            +
            \int_0^1 \frac{\alpha}{t} \| t (x - y)\|^2  dt
        \end{equation}

        \begin{equation}
            \boxed{
                J(x) - J(y) \geq \langle \nabla J(y) ~|~ x - y \rangle
                +
                \frac{\alpha}{2} \|x - y\|^2
            }
        \end{equation}

        En particulier, en prenant $y = 0$ on déduit la coercivité 

        \begin{equation}
            J(x) \geq \langle \nabla J(0) ~|~ x \rangle
                +
                \frac{\alpha}{2} \| x \|^2
                \rightarrow +\infty
        \end{equation}

    \item[La fonction $J$ possède un unique minimum local/minimum global]

        Comme $J$ est coercive, l'ensemble $K = \{ x \in \mathbb{R}^n ~|~ J(x)
        \leq J(0) \}$ est compact (dimension finie !).  Ainsi, $J$ admet 
        un minimum sur $K$ par continuité, et par construction ce minimum est
        global. On le suppose atteint en $x^\star$.

        Prenons $x'$ un minimum global, alors $\nabla J(x') = 0$.
        Ainsi l'équation précédente permet de dire 

        \begin{equation}
            J(x^\star) - J(x') \geq  \frac{\alpha}{2} \|x^\star - x'\|^2
        \end{equation}

        Donc $x^\star = x'$ et le minimum global est unique\footnote{C'est en réalité
        trivial puisque $J$ était strictement convexe... Donc le barycentre de
        deux points minimaux distincts en donne un strictement inférieur ...}

        La preuve permet de conclure même si $x'$ est seulement un minimum local
        puisque~:

        \begin{equation}
            0 \geq J(x^\star) - J(x') \geq \frac{\alpha}{2} \|x^\star - x'\|^2
        \end{equation}

        Donc le minimum $x^\star$ est \emph{caractérisé} par $\nabla J(x^\star)
        = 0$.

    \item[Construction de la méthode itérative]

        On pose $x_0 \in \mathbb{R}^n$ quelconque, et on prend l'équation 

        \begin{equation}
            x_{n+1} = x_n - t_n \nabla J (x_n)
        \end{equation}

        Avec $t_n$ qui permet de minimiser la fonction réelle 
        $g_n : t \mapsto J (x_n - t \nabla J(x_n))$.

        \begin{enumerate}[(i)]
            \item Intuitivement, on considère une direction de descente
                vers le minimum, et pour cela on considère l'information 
                 à l'ordre $1$, c'est à dire le gradient, qui donne 
                 la direction de plus grande pente. 

                 Une fois cette direction choisie, il faut savoir 
                 "jusqu'où aller", et quoi de plus naturel 
                 que de minimiser notre fonction sur la droite affine
                 donnée par cette pente pour se rapprocher du minimum ?

            \item La fonction $g_n$ est $C^1$ par composition, 
                et reste strictement convexe comme restriction d'une fonction 
                convexe à une droite affine. De même, $g_n$ est coercive. 
                Elle a donc un unique minimum caractérisé par l'équation
                $g_n'(t) = 0$.

                La suite $x_n$ est donc bien définie.
        \end{enumerate}

    \item[Les pas sont orthogonaux]

        En effet, par construction (développer ce calcul d'une ligne)~:

        \begin{equation}
            g_n'(t_n) = 0 \iff \langle \nabla J (x_{n+1}) ~|~ \nabla J(x_n)
                \rangle = 0
        \end{equation}

        Cela permet non seulement de comprendre géométriquement la suite 
        considérée, mais va permettre de prouver les propriétés de convergence.

    \item[La suite $J(x_n)$]

        On reprend l'équation encadrée au tout début et on l'applique 
        à deux termes consécutifs de la suite~:

        \begin{equation}
            J(x_n) - J(x_{n+1}) \geq \langle \nabla J(x_{n+1}) ~|~ x_n - x_{n+1} \rangle
            +
            \frac{\alpha}{2} \|x_n - x_{n+1}\|^2
        \end{equation}

        Or, $x_n - x_{n+1}$ est précisément un vecteur colinéaire à $\nabla
        J(x_n)$, donc le produit scalaire s'annule et 

        \begin{equation}
            J(x_n) - J(x_{n+1}) \geq
            \frac{\alpha}{2} \|x_n - x_{n+1}\|^2
        \end{equation}

        Donc la suite $J(x_n)$ décroit strictement. Comme elle est minorée par
        $J(x^\star)$, elle converge.

    \item[La suite $x_n$ converge vers $x^\star$]

        De l'équation précédente on déduit déjà que $x_{n+1} - x_n \rightarrow
        0$, donc que la suite $x_n$ \emph{s'essouffle}.

        Sur le compact $K$ considéré auparavant, la fonction $\nabla J$ 
        est continue, donc uniformément continue (Heine).

        Ainsi, il est clair que $\nabla J(x_{n+1}) - \nabla J(x_n) \rightarrow
        0$ par uniforme continuité.

        Enfin, 

        \begin{equation}
            \| \nabla J(x_n) \|^2 = 
            \langle \nabla J (x_n) ~|~ \nabla J (x_n)
                \rangle 
                = 
                \langle \nabla J (x_n) ~|~ \nabla J (x_n) - \nabla J(x_{n+1})
                \rangle 
                \leq \| \nabla J (x_n) \| \| \nabla J (x_n) - \nabla J(x_{n+1})
                \|
        \end{equation}

        Donc $\nabla J(x_n) \rightarrow 0$ !

        Soit $x_\infty$ une valeur d'adhérence de $x_n$ qui existe car $K$ est
        compact. Par continuité on déduit $\nabla J(x_\infty) = 0$, 
        et donc $x_\infty = x^\star$ car on avait caractérisé le minimum.

        Il y a donc une unique valeur d'adhérence, $x^\star$ et 
        $x_n \rightarrow x^\star$.

\end{description}


\subsection{Questions}

\begin{description}
    \item[Application à la résolution]

        En minimisant $(Au,u) - (b,u)$ on trouve 
        une solution du système $Ax = b$ ! (ciralet)

    \item[Comment trouver $t_n$ ?]
        Application pratique sur $(Au,u) - (b,u)$, on recherche 
        une solution d'un trinôme du second degré ! Hop hop hop calcul
        explicite. (Ciralet page 191)


    \item[Comment savoir quand stopper la recherche ?]
\end{description}

