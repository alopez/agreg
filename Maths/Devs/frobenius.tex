

\begin{references}
    \item[Mansuy Mneimné] page 125
    \item[Gourdon Algèbre] page 290
\end{references}

On désire caractériser l'action du groupe $\GLin{E}$ par conjugaison 
sur $\Lin{E}$.


\begin{lemme}[Admis]
    Il pour tout endomorphisme $u$ il existe un vecteur $x \in E$
    tel que $\pi_{u,x} = \pi_u$.
\end{lemme}

\begin{proof}
    On procède en plusieurs étapes
    \begin{enumerate}[(i)]
        \item Si $\pi_u = P^\alpha$ avec $P$ irréductible.
            Alors on a la suite strictement croissante 
            \begin{equation}
                \{ 0 \} \subsetneq \ker P(u) 
                        \subsetneq \dots
                        \subsetneq \ker P^\alpha(u) = E
            \end{equation}

            On peut donc sélectionner $x \in E - \ker P^{\alpha-1}(u)$.
            Par construction $\pi_{u,x} | P^\alpha$, mais 
            comme $P^{\alpha-1}(u)(x) \neq 0$, on a $\pi_{u,x} = \pi_u$.

        \item Si $\pi_{u,x} \wedge \pi_{u,y} = 1$, alors 
            $\langle x + y \rangle_u = \langle x \rangle_u \oplus \langle y
            \rangle_u$ et $\pi_{u, x+y} = \pi_{u,x} \pi_{u,y}$.

            On commence par remarquer que
            $\langle x + y \rangle_u \subseteq \langle x \rangle_u + \langle y
            \rangle_u$.

            De plus, la somme est directe car si $z$ est un vecteur dans
            l'intersection, son polynôme minimal ponctuel doit diviser 
            les deux polynômes et donc être constant égal à $1$, ce qui force
            $z = 0$.

            Enfin, l'astuce 

            \begin{equation}
                0 = \pi_{u,x} \pi_{u, x+y} (u) (x+y) 
                = \pi_{u, x+y} (u) (y)
            \end{equation}

            Donc $\pi_{u,y} | \pi_{u,x+y}$, on a la même chose pour $x$,
            et on déduit donc que $\pi_{u, x+y} = \pi_{u,x} \pi_{u,y}$.

            Cela donne donc la bonne dimension pour les espaces qui étaient 
            inclus, et donc sont égaux.

        \item Pour conclure dans le cas général il suffit de décomposer $\pi_u$
            en facteurs de type $P^\alpha$, et de recombiner les résultats 
            via le deuxième point.
    \end{enumerate}
\end{proof}


\subsection{Existence}

On considère un vecteur $x$ tel que $\pi_u = \pi_{u,x}$,
on note $k = \deg \pi_u = \deg \pi_{u,x}$, et $E_x = \langle x \rangle_u$
l'espace vectoriel engendré par les $u^i (x)$.

On constate que $E_x$ est de dimension $k$, qu'il est stable par $u$
et que $\pi_{u_{| E_x}} = \pi_{u,x} = \pi_u$.

\textbf{On recherche alors un supplémentaire stable à $E_x$.} 
On complète la base $(x, u(x), \dots, u^{k-1}(x))$ de $E_x$ 
notée $(e_1, \dots, e_k)$ 
en une base $(e_1, \dots, e_n)$ de $E$.

On pose alors $G = \Gamma^\circ$ où $\Gamma = \{ e_k^* \circ u^i ~|~ i \in
\mathbb{N} \}$. De manière évidente, $\Gamma$ est stable par ${}^t u$ et donc 
$G$ est un sev stable par $f$.

Pourquoi a-t-on posé ceci ? Et bien en fait 

\begin{equation}
    \Gamma^\circ = \left(\left\langle e_k^* \right\rangle_{{}^t u}\right)^\circ 
\end{equation}

En effet, 
\begin{equation}
    \left\langle e_k^* \right\rangle_{{}^t u}
    = 
    \left\{ P\left({}^t u\right) \left(e_k^*\right) ~|~ P \in k[X] \right\}
    = 
    \left\{ e_k^* \circ P(u) ~|~ P \in k[X] \right\}
\end{equation}

\begin{description}
    \item[Montrons $F \cap G = \{ 0\}$] 
        Soit $y \in F \cap G$, alors
        $e_j^* (y) = 0$ si $j > k$ car $y \in F$. De plus
        $e_k^* (y) = 0$ et par récurrence $e_j^* (y) = 0$ pour $j \leq k$.
        On a donc $y = 0$.

    \item[Montrons $\dim F + \dim G = n$]
        On sait que $\dim G = n - \dim \textrm{Vect } \Gamma$.
        Il suffit de montrer que $\dim \textrm{Vect } \Gamma = k$ pour conclure.
        Or on a montré que $\dim \textrm{Vect } \Gamma = \dim \left\langle e_k^*
        \right\rangle_{{}^t u}$. C'est donc un espace vectoriel de dimension 
        $\deg \pi_{{}^t u}$, mais le polynôme minimal de ${}^t u$ est le 
        même que celui de $u$, donc les dimensions sont bonnes.

\end{description}

On a donc $E = F \oplus G$.
On note $P_1$ le polynôme minimal de $u_{|F}$ et $P_2$ le polynôme minimal de
$u_{|G}$. On a $P_2 | \pi_u = P_1$ et en appliquant l'hypothèse de récurrence
à $u_{|G}$ on obtient la suite des espaces attendue.


\subsection{Unicité}


Supposons l'existence de deux suites de sous espaces $F_1 \dots F_r$ et 
$G_1 \dots G_s$ associées aux polynômes $(P_i)$ et $(Q_i)$. 

On remarque que $P_1 = Q_1 = \pi_u$, car 
le polynôme minimal de $u$ est le pgcd des polynômes minimaux des $u_{|F_i})$
qui se divisent tous (et de même pour $Q_1$).

On montre par récurrence que pour $j \leq \min (r,s)$ on a bien $P_i =
Q_i$. L'initialisation a déjà été faite.

Considérons alors un $j \geq 2$.

On remarque 

\begin{equation}
    P_j(u) (E) = \oplus P_j(u)( F_i) = \oplus P_j(u)(G_i)
\end{equation}

En passant aux dimensions on obtient 

\begin{equation}
    \sum_{i = 1}^{j-1} \dim P_j (u)(F_i) 
    = 
    \sum_{i = 1}^s \dim P_j (u) (G_i)
\end{equation}


\todo[inline]{Préciser cette preuve}
Mais on sait que \footnote{Considérer le morphisme de $K[u]$ vers l'espace
    associé et regarder son noyau}.

\begin{equation*}
    \dim P_j(u)(F_i) = \deg \frac{P_i}{P_j \wedge P_i}
\end{equation*}

\begin{equation*}
    \dim P_j(u)(G_i) = \deg \frac{Q_i}{P_j \wedge Q_i}
\end{equation*}


On peut donc déduire par récurrence que $\dim P_j(u)(F_i) = \dim P_j (u)(G_i)$
pour $i < j$. Cela permet alors de déduire

\begin{equation}
    \forall i \geq j, \dim P_j(u)(G_i) = 0
\end{equation}

En particulier on observe $Q_j | P_j$. Par symétrie dans la preuve, on déduit 
de même $P_j | Q_j$ et comme les polynômes sont unitaires $Q_j = P_j$.


CQFD.

\subsection{Utilisations, résultats annexes}

\begin{description}
    \item[Résultat de classification]
        Cela permet de totalement comprendre l'action de 
        $GL(E)$ sur $L(E)$ par conjugaison.

    \item[Invariance par extension de corps]
        TODO 

    \item[Vers la réduction de Jordan]

    \item[Endomorphismes cycliques]

        Les endo cycliques sont très important,
        il y a pleins de résultats super cools 
        et il faut les marquer.
\end{description}
