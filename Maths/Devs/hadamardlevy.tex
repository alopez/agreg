\begin{references}
    \item[Zavidovique]
    \item[Zuily Queffélec] page 399
\end{references}

\subsection{Prérequis}

\begin{lemme}[Gronwall]
    Si $\phi$ et $\psi$ sont des fonctions positives 
    sur $I = [a,b]$ qui vérifient 
    \begin{equation}
        \phi(t) \leq A + B \int_a^t \psi(s)\phi(s) ds
    \end{equation}
    Alors 
    \begin{equation}
        \phi(t) \leq A \exp\left( B \int_a^t \psi(s) ds \right)
    \end{equation}
\end{lemme}

\begin{proof}
    Considérer le quotient des deux majorations, c'est une fonction 
    dérivable strictement décroissante, et donc la majoration 
    déduite est toujours au dessus de la majoration supposée ...
\end{proof}

\begin{theoreme}[Cauchy-Lipschitz]
    Le système $X' = F(t,X)$ où $F$ est localement-lip 
    en la seconde variable admet une unique solution 
    vérifiant $X(t_0) = x_0$ et cette solution est $C^1$.
\end{theoreme}

\begin{theoreme}[Sortie de tout compact]
    Sortie de tout compact ZQ
\end{theoreme}

\begin{theoreme}[Inégalité des Accroissements Finis]
    Si $f : I \to E$ et $g : I \to \mathbb{R}$ vérifient 
    $\| f'(t) \| \leq g'(t)$, alors 
    \begin{equation}
        \| f(b) - f(a) \| \leq g(b) - g(a)
    \end{equation}
\end{theoreme}

\begin{remarque}
    On déduit le cas vectoriel vers vectoriel 
    en appliquant ce théorème à $F : I \to E$
    $F(t) = f(tx + (1-t)y)$ en fixant $x$ et $y$.
\end{remarque}


\begin{lemme}[Fonction propres]
    Une fonction $f$ vérifie que la pré-imaa pré-imagee de tout compact 
    est compacte si et seulement si $|f(x)| \to +\infty$
    quand $x \to +\infty$.
\end{lemme}

\subsection{Développement}

On considère $f \in \mathcal{C}^2(\mathbb{R}^n)$. On montre l'équivalence entre
les deux propriétés suivantes

\begin{enumerate}[(i)]
    \item $f$ est un $\mathcal{C}^1$ difféomorphisme global
    \item $df_x$ est inversible pour tout $x$ et $f$ est propre,
        c'est-à-dire que la pré-image de tout compact est compacte
\end{enumerate}

On commence par remarquer qu'un sens est évident. En effet 
si $f$ est un difféomorphisme global, alors $df_x$ est inversible 
pour tout $x$, et comme $f^{-1}$ est continue, l'image de tout compact 
par $f^{-1}$ est donc compacte, ce qui permet de conclure.

Pour le sens réciproque, on suppose $f(0) = 0$.
Il suffit de construire $g$ surjective de $\mathbb{R}^n$ dans $\mathbb{R}^n$
telle que $f \circ g = id$ pour déduire que $f$ est un difféomorphisme global
(puisqu'injectif et global).

\begin{description}
    \item[Construction d'un flot]
        On fixe $y \in \mathbb{R}^n$ et on pose 

        \begin{equation}
            \begin{cases}
                \dot{x} (t) = (df_{x(t)})^{-1} y = F(x,t) \\
                x(0) = 0
            \end{cases}
        \end{equation}
    
    \item[L'application $F(x,t)$ est C1]
        par composition d'applications $C^1$ !

    \item[L'application $t \mapsto x(t,y)$ est bien définie]

        En effet, on peut utiliser Cauchy-Lipschitz car $F(x,t)$ est $C^1$
        et donc à $y$ fixé il existe une solution maximale sur $I = [0, T^*[$. 

        
    \item[La solution était en réalité globale]

        De plus, pour $t \in I$ on a 

        \begin{equation}
            \frac{d}{dt} f(x(t,y)) = d f_{x(t,y)} \dot{x}(t,y)
                = d f_{x(t,y)} (df_{x(t,y)})^{-1} y = y
        \end{equation}

        Ainsi, comme $f(x(0,y)) = 0$, on déduit en résolvant 
        une équation différentielle linéaire d'ordre $1$ que
        \begin{equation}
            \forall t \in [0, T^*[,
                    f(x(t,y)) = ty
        \end{equation}

        En particulier $x(t,y) \in f^{-1} (B(0,T^* y)$.

        Supposons par l'absurde que $T^* < +\infty$,
        on a alors contrôlé $x(t,y)$ dans un compact, ce qui 
        est absurde au vu du théorème de sortie de tout compact !

        Ainsi $T^* = +\infty$.

    \item[Définition de la fonction $g$]

        On pose $g(y) = x(1,y)$ ce qui est possible au vu des résultats
        précédents.

        On sait de plus que $f \circ g = id$ sur $\mathbb{R}^n$, mais il 
        reste à montrer que $g$ est surjective ! Pour cela on va 
        d'abord montrer que $g$ est continue.

    \item[La fonction $g$ est en réalité continue]

        C'est la partie dure de ce développement.

        Déjà, on remarque que si $0 \leq t \leq 1$
        alors $x(t,y) \in f^{-1} (B(0, |y|))$ (via les magouilles d'avant).
        
        Fixons $y_0$.
        Supposons alors $|y - y_0| \leq 1$, et posons $K_0 = f^{-1} (B(0, |y_0|
        + 1))$. Il est clair que les $x(t,y)$ et $x(t,y_0)$ sont dans $K_0$,
        mais on va poser $B_0$ une boule contentant $K_0$ afin d'avoir un 
        compact \emph{connexe}. 


        \begin{align}
            \dot{x} (t,y_0) - \dot{x} (t,y)
            &= (df_{x(t,y_0)})^{-1} y_0 - (df_{x(t,y)})^{-1} y \\
            &= (df_{x(t,y_0)})^{-1} (y_0 - y)
            + \left(df_{x(t,y_0)}^{-1} df_{x(t,y)}^{-1}\right) y
        \end{align}

        L'application $x \mapsto df_x$ est continue par hypothèse,
        et la fonction inverse aussi, donc $x \mapsto (df_x)^{-1}$ est 
        continue, donc de norme triple bornée par un $M_{y_0}$ sur le 
        compact $B_0$.

        Pour le second terme, on remarque que cela se ré-écrit
        $F(x(s,y_0)) - F(x(s,y))$. Mais comme $F$ est $C^1$, on peut 
        utiliser l'inégalité des accroissements finis sur le compact
        \emph{convexe} $B_0$ 
        
        \begin{equation}
            \| F(x(t,y_0)) - F(x(t,y)) \|
            \leq 
            C(y_0) \| x (t,y_0) - x(t,y) \|
        \end{equation}

        On a alors montré~:

        \begin{equation}
            \left\| \int_0^s \dot{x} (t,y_0) - \dot{x} (t,y) dt \right\|
            \leq 
            \int_0^s M_{y_0} \| y - y_0 \| dt
            + 
            \int_0^s C(y_0) \| x(t,y_0) - x(t,y) \| dt
        \end{equation}

        On déduit donc 

        \begin{equation}
            \| x(t,y_0) - x(t,y) \| 
            \leq M_{y_0} \| y - y_0 \| + C(y_0) \int_0^s \| x(t,y_0) - x(t,y) \| dt
        \end{equation}

        On peut alors utiliser le lemme de Gronwall à $y$ fixé sur la fonction
        $\phi (t) = \|
        x(t,y_0) - x(t,y) \|$ pour déduire

        \begin{equation}
            \| x(t,y_0) - x(t,y) \| 
            \leq 
            M_{y_0} \| y - y_0 \| \exp\left( C(y_0) t \right)
        \end{equation}

        Ce qui permet de conclure vu que la fonction $g$ est continue.

    \item[La fonction $g$ est donc surjective]
        Pour cela on montre que $g(\mathbb{R}^n)$ est ouvert et fermé
        (car il est clairement non vide).

        \begin{enumerate}[(i)]
            \item On a l'égalité $g(\mathbb{R}^n) = g (f (g (\mathbb{R}^n)))$
                ce qui montre qu'une suite dans $g(\mathbb{R}^n)$ qui converge 
                converge bien dans $g(f(\mathbb{R}^n))$ c'est à dire 
                dans $g(\mathbb{R}^n)$ via la continuité de $f$ et $g$.

            \item Soit $x_0 \in g(\mathbb{R}^n)$, alors on a $g(y_0) = x_0$ 
                pour un certain $y_0$. 

                \textbf{FAIRE UN PUTAIN DE DESSIN ICI}

                En particulier $f(x_0) = y_0$, et par le théorème d'inversion
                locale, on a des voisinages ouverts de $x_0$ et de $y_0$
                où $f$ est un $C^1$-difféo.

                Or, $g$ étant continue, on peut restreindre le voisinage 
                autour de $y_0$ afin que $g$ l'envoie dans le voisinage de
                $x_0$.

                Les fonctions $g$ et $f^{-1}$ définies sur ces domaines 
                et co-domaines sont deux inverses de $f$, et donc coïncident.

                En particulier, l'image par $g$ est l'image par un $C^1$-difféo
                et est donc ouverte.

                Ainsi il existe un voisinage ouvert de $x_0$ inclus dans
                $g(\mathbb{R}^n)$.
        \end{enumerate}

        On a donc $g(\mathbb{R}^n) = \mathbb{R}^n$.

    \item[On peut alors conclure]
        Exactement comme dit avant.
\end{description}

\subsection{Questions subsidiaires} 

\begin{exemple}
    Un exemple d'application 
\end{exemple}

\begin{definition}[Flot]
\end{definition}

\begin{theoreme}[Théorèmes sur le flot]
\end{theoreme}
