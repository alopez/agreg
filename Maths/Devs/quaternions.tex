\begin{references}
    \item[Perrin] Partie quaternions
\end{references}

\subsection{Prérequis}

\begin{lemme}[Isométries]
    On fait un bref rappel des éléments dans $O(\mathbb{R})$.
    \begin{quote}
    \begin{description}
        \item[Symétrie] c'est semblable à $I_p$, $-I_{n-p}$
        \item[Réflexion orthogonale]
            c'est diagonalisable, de spectre $\{ \pm 1 \}$
            avec un seul $-1$.
        \item[Un retournement] c'est pareil qu'un renversement 
            ou une rotation d'angle $\pi$, c'est 
            de spectre $\{ \pm 1 \}$ avec exactement 
            \emph{deux} $-1$.
        \item[SO3] tous les gens dans $SO_3$ sont des rotations !
    \end{description}
    \end{quote}
\end{lemme}

\begin{lemme}
    On montre que $O_n(\mathbb{R})$ est engendré 
    par les produits de $n$ réflexions
\end{lemme}

\begin{proof}
    Soit $u$ un endomorphisme orthogonal, et considérons $\Fix u$
    l'espace vectoriel des points fixes de $u$.
    On pose $p_u = n - \dim \Fix u$, et on montre 
    par récurrence que $u$ est un produit 
    d'au plus $p_u$ réflexions orthogonales.

    \begin{description}
        \item[Si $p_u = 0$] alors $u = id$, c'est terminé.
        \item[Sinon.]
            $E = \Fix u \oplus (\Fix u)^\bot$, 
            et on a un $x \neq 0$ dans $(\Fix u)^\bot$.

            On pose $y = u(x)$, et comme $x \not \in \Fix u$,
            $x \neq u(x) = y$. Toutefois, comme $\Fix u$
            est $u$-stable et $u$ orthogonal, $(\Fix u)^\bot$
            est $u$-stable et $y \in (\Fix u)^\bot$.

            On considère la réflexion orthogonale définie par le vecteur $x - y$.
            Elle vérifie trivialement les propriétés suivantes

            \begin{itemize}
                \item Elle fixe $\Fix u$, car $x - y \in (\Fix u)^\bot$
                \item Elle fixe $x + y$ car $x + y$ est orthogonal 
                    à $x - y$
                \item Elle envoie $y$ sur $x$, car 
                    $\tau (x - y) = y - x$ et $\tau (x + y) = x + y$.
            \end{itemize}

            Ainsi $\tau u$ est un élément de $O_n(\mathbb{R})$
            avec $\dim \Fix (\tau u) > \dim \Fix u$ puisque 
            $\Fix u \subseteq \Fix (\tau u)$ et 
            $\tau (u (x)) = x$.

            On peut donc se ramener à l'hypothèse de récurrence
            sur $\tau u = \tau_1 \dots \tau_r$, puis 
            $u = \tau \tau_1 \dots \tau_r$.
    \end{description}
\end{proof}

\begin{lemme}
    On déduit que $SO_n(\mathbb{R})$ est engendré 
    par les produits de $n$ \emph{retournements}
\end{lemme}

\begin{proof}
    On découpe la preuve en deux, car le cas $n = 3$ est
    particulièrement facile.

    \begin{description}
        \item[Le cas $n = 3$] 
            Si $\tau$ est une réflexion, alors $-\tau$ est un 
            retournement, car on est en dimension 3.

            Or, un élément dans $SO_n(\mathbb{R})$
            ne peut être qu'un produit \emph{pair} de réflexions 
            à cause de son déterminant positif.

            Donc en remplaçant $\tau$ par $-\tau$ dans 
            l'expression de $u$ on obtient bien 
            $u$ comme un produit de $n$ retournements

        \item[Le cas $n > 3$]

            Cette fois il faut prouver un lemme 
            un peu plus fort, qui montre qu'un 
            produit de deux réflexions peut 
            s'écrire comme un produit de 
            deux retournements.

            Soit $u = \tau_1 \tau_2$, 
            d'hyperplans respectifs $H_1$, $H_2$.
            On considère $V$ un sous espace vectoriel 
            de $H_1 \cap H_2$ de dimension $n - 3$ possible 
            car $n \geq 3$.

            On a $u_{|V} = id_V$ par composition, 
            et donc $u$ laisse stable $V^\bot$.
            Or $V^\bot$ est de dimension précisément $3$,
            et on peut écrire $u_{|V^\bot}$ comme un 
            produit de deux retournements. En prolongeant
            ces retournements par l'identité sur $V$, on peut 
            conclure.
    \end{description}
\end{proof}

\begin{remarque}
    $SO_3 (\mathbb{R})$ est un connexe (par arcs) compact.
\end{remarque}

\begin{lemme}
    Si $h \in H$ est quaternion pur alors $h^2 = -1$.
\end{lemme}

\begin{lemme}
    Les quaternions de norme $1$ forment un ensemble 
    connexe.
\end{lemme}

\begin{lemme}
    Parler des endomorphismes orthogonaux, des 
    isomorphismes de $H$ avec les matrices 
    parce que sinon le développement ne tient pas 
    debout
\end{lemme}

\subsection{Développement}

On note $G$ le groupe des quaternions 
de norme $1$. 

\begin{theoreme}
    Il existe un isomorphisme "calculable"
    $\quad \faktor{G}{\{ \pm 1 \}} \simeq SO_3(\mathbb{R})$
\end{theoreme}

Le point clef de la preuve est de remarquer 
que $G$ agit sur $H$ par automorphisme intérieur.

\begin{equation}
    \begin{array}{llllll}
        S : & G & \to     & \textrm{Aut} (H) \\
            & h & \mapsto & S_h : & H & \to H \\
            &   &         &       & q & \mapsto h q h^{-1} 
    \end{array}
\end{equation}

\begin{proof} C'est bien une action de $G$
    sur $H$ par automorphisme intérieur.
    \begin{description}
        \item[$S_h$ est un automorphisme]
            son inverse étant $S_{h^{-1}}$
        \item[C'est bien une action]
            En effet $hh' \cdot q = h h' q h'^{-1} h^{-1} = h \cdot (h' \cdot
            q)$. De plus $1 \cdot q = q$.
    \end{description}
\end{proof}

\begin{remarque}
    On remarque que $h q h^{-1} = h q \bar{h}$
    car $h$ est de norme $1$.
\end{remarque}

\begin{proof}
    On va transformer cette action en une action de $G$ sur 
    $\mathbb{R}^3$ via le groupe spécial orthogonal.

    \begin{enumerate}[(i)]
        \item L'action par automorphisme intérieur correspond 
            a un morphisme $G \to GL_4 (\mathbb{R})$.
        \item Pour $h \in G$ l'application $S_h$ respecte la norme.
            \begin{equation}
                N (S_h(q)) = N (h q \bar{h}) = N(h) N(q) N (\bar{h}) = N(q)
            \end{equation}
            Ainsi $S_h \in O_4 (N)$.

        \item L'application $S$ a pour noyau $Z(H) \cap G = \mathbb{R} \cap G =
            \{ \pm 1 \}$.

        \item Pour la norme $N$, l'espace des quaternions purs $P$ 
            est l'orthogonal de $\mathbb{R}$. Comme $S_h$ fixe $\mathbb{R}$
            et est un endomorphisme orthogonal, on déduit que $P$ est stable 
            par $S_h$, pour tout $h \in G$.

            On pose $s_h = (S_h)_{| P}$, qui correspond donc à un élément 
            de $O (N_{|P}) \simeq O(3, \mathbb{R})$.

        \item
            On désire montrer que ce morphisme est en réalité 
            vers $SO_3(\mathbb{R})$. Pour cela 
            on munit $O(3, \mathbb{R})$ de la topologie induite 
            par $\mathcal{M}_3 (\mathbb{R})$ et on constate alors 
            que l'application $S$ est continue~: il suffit de remarquer 
            que $S_h(q)$ est un polynôme de degré $2$ en les coordonnées 
            de $h$, et linéaire en les coordonnées de $q$. 
            \todo[inline]{Préciser cette chose}
            
            En remarquant que $G$ est isomorphe à $S^3$ 
            (la sphère unité en dimension $4$) est connexe,
            on constate alors que l'image de $G$ par l'homéomorphisme
            puis par le déterminant est connexe. Or $G$ étant 
            un groupe cela force $G \subseteq SO_3 (\mathbb{R})$.

        \item 
            Reste à montrer la surjectivité vers $SO_3(\mathbb{R})$.
            Pour cela on utilise le fait que $SO_3(\mathbb{R})$
            est engendré par les \emph{renversements}.

            On considère donc un élément $h \in P \cap G$,
            et on construit le retournement d'axe $\mathbb{R} h$.

            En réalité on montre même $S_h = r_h$, car 
            $S_h$ laisse clairement stable l'axe désiré, 
            et que $(S_h)^2 = S_{h^2} = S_{-1} = Id_H$ car 
            $h$ est un quaternion \emph{pur}.

            Ainsi on a montré que $S_h$ était un élément de 
            $O(3,\mathbb{R})$ avec un axe fixe, et donc 
            une rotation autour de cet axe. De plus $S_h$
            est une involution, donc c'est nécessairement 
            le renversement d'axe $h$.
    \end{enumerate}

    En conclusion, on a bien un morphisme surjectif 
    $\phi : G \twoheadrightarrow SO_3(\mathbb{R})$
    de noyau $\{ \pm 1 \}$, ce qui donne l'isomorphisme 
    attendu.
\end{proof}

\subsection{Post-requis}

\begin{description}
    \item[On peut aussi décrire les isométries négatives]
        En effet, sur l'exemple d'une réflexion 
        de droite $q$ et d'hyperplan $q^\bot$ on sait 
        que la réflexion $\tau_q$ est donnée par 
        
        \begin{equation}
            \tau_q (x) = x - 2 \frac{\phi (q,x)}{\phi (q,q)} q
        \end{equation}

        Or ici le produit scalaire est 
        simplement $\frac{1}{2} \left( q \bar{x} + \bar{q} x\right)$
        et la norme de $q$ vaut $1$. Ainsi 

        \begin{equation}
            \tau_q (x) = x - (q \bar{x} - \bar{q}) x
                       = - q \bar{x} q
        \end{equation}

        Ainsi, en faisant agir $q$ sur les quaternions 
        purs, on obtient $\tau_q (x) = q x q$.
        Ce qui veut dire qu'il "suffit de ne pas 
        conjuguer".

        Comme on considère la réflexion de droite $q$,
        $q$ est de plus un quaternion pur, et on peut 
        se ramener à $s_q$ via 
        $\tau_q (x) = - s_q (x)$.
    \item[Application aux automorphismes de $H$]
        tout automorphisme de $H$ est intérieur, c'est-à-dire 
        de la forme $S_q$.

        Éléments de preuve
        \begin{itemize}
            \item Un automorphisme doit conserver le centre,
                donc il fixe $\mathbb{R}$, mais alors 
                restreint à $\mathbb{R}$ c'est un automorphisme
                de $\mathbb{R}$, qui est donc nécessairement 
                l'identité.
            \item On utilise ensuite la caractérisation 
                $q \in P \iff q^2 \in \mathbb{R}^-$
                pour déduire que $u(q)^2 = u(q^2) \in \mathbb{R}^-$.
                Ainsi $P$ est laissé stable par $u$.

            \item Mais pour $q \in P$, $N(q) = -q^2$, ainsi l'équation 
                précédente dit que $N(u(q)) = N(q)$, et donc 
                $u$ préserve la norme.
                Ainsi $u_{|P} \in O(3, \mathbb{R})$.

            \item L'image de $(i,j,k)$ par $u$ est donc une base 
                orthonormée, et quitte à poser $k' = - u(k)$
                $(u(i), u(j), k')$ est orthonormée directe.

            \item En vertu du théorème démontré, il existe 
                un quaternion $q$ unitaire tel que $u(i) = s_q (i)$,
                $u(j) = s_q (j)$ et $k' = s_q (k)$.

                Or $u(k) = u(ij) = u(i)u(j) = s_q(i)s_q(j) = s_q(ij) = s_q(k)$
                donc $u(k) = s_q (k)$. On constate alors $u_{|P}$ était déjà une rotation. 

            \item Les deux applications $\mathbb{R}$ linéaires coïncident sur 
                une base, donc $s_q = u$.
        \end{itemize}
\end{description}

\subsection{Annexe effectivité}

\begin{description}
    \item[Pourquoi "calculable"]
        On peut exprimer effectivement ce morphisme 
        de $G$ vers $SO_3(\mathbb{R})$ au niveau matriciel.
        Si $h = a + bi + cj + dk$.

        \begin{equation}
            S_h (1) = h \bar{h} = N(h) = 1
        \end{equation}

        \begin{align}
            S_h (i) &= h i \bar{h}  \\
                    &= 
            (- b + ai + c ji + d ki)(a - bi - cj - dk)
            \\
            &= (- b + ai + c k + dj) (a - bi - cj - dk)  
            \\
            &= (a^2 + b^2 - c^2 - d^2) i + 2(ad + bc)j
            + 2(bd - ac)k 
        \end{align}
        
        Cela permet d'expliciter comment construire 
        la matrice associée.
        \footnote{Le quotient par $\{ \pm 1\}$ ne pose 
        pas de souci précisément parce qu'il ne 
        change pas le résultat de ce calcul}

    \item[Pourquoi c'est "utile"]
        Une fois donné un quaternion unitaire $q$ et un vecteur $(x,y,z)$
        effectuer la rotation c'est simplement calculer $q (xi + yj + zk)
        \bar{q}$. 

        Cette description de $SO_3(R)$ qui est \emph{beaucoup} plus 
        stable numériquement que la manière matricielle. 
        De plus elle est compacte~: quatre nombres au lieu de $9$.
        Cela donne de plus un nombre inférieur d'opérations 
        élémentaires à effectuer pour le calcul de compostions 
        de rotations !

    \item[Calcul effectif version plus plus] 

        Soit $(x,y,z)$ un axe de rotation dans
        $\mathbb{R}^3$ et $\theta \in [0, 2\pi[$
        un angle pour cette rotation.

        Le quaternion pur $q'$ suivant est associé
        à la rotation d'angle $\pi$ 
        de même axe~: $xi + yj + zk$.

        Pour obtenir une rotation d'angle 
        $\theta$, on ajoute une partie 
        réelle, et pour conserver une norme 
        $1$ on normalise le vecteur précédent. 

        \begin{equation}
            q = \cos \frac{\theta}{2}
              + \sin \frac{\theta}{2}
              \underbrace{\left( x i + y j + z k \right)}_{q'}
        \end{equation}

        Montrons que ce $q$ calcule bien la rotation désirée.
        Déjà, $q$ est de norme $1$, et correspond 
        donc bien à une rotation.

        \begin{equation}
            S_q (p) = q p \bar{q}
            = \cos^2 \frac{\theta}{2}  p
            + \cos \frac{\theta}{2} \sin
            \frac{\theta}{2} (q'p - p q')
            - \sin^2 \frac{\theta}{2}
            q' p q'
        \end{equation}

        Elle possède le bon axe de rotation
        car $S_q (q') = q'$.
        
        On remarque que $-q' = \bar{q'}$
        car $q'$ est un quaternion pur.
        Ainsi tout quaternion $p$
        représentant un vecteur orthogonal 
        à l'axe de rotation vérifie~: 
        $-q' p q' = -p$ par définition de $q'$.

        Un rapide calcul utilisant ce fait 
        permet de déduire $pq' = - q'p$.

        Ceci permet d'écrire pour un tel $p$
        \begin{equation}
            q p \bar{q}
            = \cos^2 \frac{\theta}{2}  p
            + 2 \cos \frac{\theta}{2} \sin
            \frac{\theta}{2} q'p
            - \sin^2 \frac{\theta}{2}
            p
            = \cos \theta p + \sin \theta q'p
        \end{equation}

        Reste à remarquer que $q'p$ est un 
        quaternion pur, et que $(q', p, q'p)$
        correspond à une base orthonormée 
        directe de l'espace pour conclure.
\end{description}
