\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs,stmaryrd}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{complexity}
\usepackage{hyperref}
%\usepackage{enumitem}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}
\usepackage{palatino}
\usepackage{pdfpages}
\usepackage{epigraph}
\usepackage{algorithm}
\usepackage{algorithmic}

%%%% Format

\setlength{\columnsep}{2cm}

\newcommand{\lname}{Bicommutant d'un endomorphisme}
\newcommand{\tr}{\operatorname{Tr}}

\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}



\newcommand{\Lin}[1]{\mathcal{L}\left(#1\right)}
\newcommand{\Comm}[1]{\mathcal{C}\left(#1\right)}
\newcommand{\GLin}[1]{\operatorname{GL}\left(#1\right)}
\newcommand{\Image}{\operatorname{Im}}
\newcommand{\Ker}{\operatorname{Ker}}
\newcommand{\trace}{\operatorname{tr}}
\newcommand{\cycl}[2]{\left\langle #1 \right\rangle_{#2}}





\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}
 
\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}


%%%% Corps du fichier
\title{\lname}
\author{\textsc{Aliaume Lopez}}


\begin{document}

\maketitle

\vspace{1em}
\hrule
\vspace{1em}

\begin{tabular}{ll}
    \textbf{Difficulté~:} & $\star$ \\
    \textbf{Technicité~:} & $\star\star$ \\
    \textbf{Durée~:} & $\star$ \\
    \textbf{Recasages~:} & \cite{L903}
\end{tabular}

\vspace{1em}
\hrule
\vspace{1em}

\begin{ouverture}
    Commuter c'est bien, mais commuter entre nous, c'est mieux,
    on peut se demander s'il faut faire une tour. En fait
    tout s'effondre au niveau deux.
\end{ouverture}

Tout au long du développement on fixe un espace $E$
de dimension finie, un endomorphisme $u$.


\section{Préliminaires}

\begin{definition}[Notations]
    On note $\pi_1, \dots, \pi_l$ les invariants de similitude 
    de $u$, $E_1, \dots E_l$ les sous espaces cycliques 
    associés et $x_1, \dots, x_l$ leurs générateurs respectifs.

    \begin{equation*}
        E = \bigoplus_{i = 1}^l E_i
    \end{equation*}
\end{definition}

\begin{lemme}[Caractérisation de $C(u)$]
    L'isomorphisme suivant est obtenu en
    considérant $\phi : v \mapsto (v(x_1), \dots, v(x_l))$~:
    \begin{equation*}
        C(u) \simeq \Ker \pi_1(u) \times \dots \times \Ker \pi_l (u) 
    \end{equation*}
\end{lemme}

\begin{proof}
    ~
    \begin{description}
        \item[La fonction est bien définie]
            Comme $v$ commute avec $u$, $v$ laisse stable 
            tout sous espace vectoriel $u$-stable, en particulier 
            les $E_i$.

        \item[C'est une application linéaire injective]
            Elle est trivialement linéaire, et l'injectivité 
            se déduit alors en considérant son noyau.
            Pour cela on remarque que $u^i (v (x_j)) = v (u^i (x_j))$,
            donc si $\phi(v) = 0$, alors $v$ s'annule sur une base 
            de $E$ et est donc nulle.
            
        \item[L'application $\phi$ est surjective]
            Soit $y_1, \dots, y_n \in \Ker \pi_1 (u) \times \dots \times \Ker
            \pi_l (u)$.
            On définit un endomorphisme $v$ comme suit~:

            \begin{equation*}
                \begin{cases}
                    v(x_i) = y_i & 1 \leq i \leq l \\
                    v(u^j (x_i)) = u^j (y_i) & 1 \leq i \leq l \wedge 1 \leq j
                    \leq \deg \pi_i - 1
                \end{cases}
            \end{equation*}

            Cela définit bien $v$ sur une base, et il reste à montrer que $v$
            commute avec $u$. 
    
            Soit $x \in E$, on peut écrire de manière unique 
            $x = \sum_{i = 1}^l P_i (u) (x_i)$ avec $\deg P_i < \deg \pi_i$. 
            On a alors~:


            \begin{equation*}
                uv (x) = \sum_{i = 1}^l (XP_i) (u) (y_i)
            \end{equation*}

            \begin{equation*}
                vu (x) = \sum_{i = 1}^l v ((XP_i) (u) (x_i))
            \end{equation*}
            
            On ne peut pas ici utiliser la commutation, puisque la définition 
            ne fait commuter qu'avec des degrés plus petit que $\deg \pi_i - 1$.
            On fait donc la division euclidiennne de $X P_i$ par $\pi_i$.

            \begin{equation*}
                X P_i = Q_i \pi_i + R_i
            \end{equation*}

            Et comme $y_i$ et $x_i$ sont dans $\Ker \pi_i (u)$, on a 
            bien $XP_i (u) (x_i) = R(x_i)$ (resp. $y_i$). 

            On peut alors écrire~:
            \begin{equation*}
                vu (x) = \sum_{i = 1}^l v((R_i) (u) (x_i))
                       = \sum_{i = 1}^l R_i (u) (y_i) 
                       = uv (x)
            \end{equation*}
    \end{description}
\end{proof}

\section{Bicommutant}

\begin{theoreme}[Du bicommutant]
    \begin{equation*}
        k[u] = C(C(u))
    \end{equation*}
\end{theoreme}

\begin{proof}
    L'inclusion $k[u] \subseteq C(C(u))$ est directe
    car $C(C(u))$ est une sous-algèbre qui contient $u$.

    Soit $w \in C(C(u))$ montrons que $w \in k[u]$.
    Pour cela, on utilise le fait que $w$ commute avec 
    les projecteurs sur les sous-espaces cycliques. 
    On pose $p_i$ le projecteur sur $E_i$ parallèlement aux 
    autres espaces de la décomposition de Frobénius.
    
    On constate que $p_i$ commute avec $u$ de manière assez 
    évidente. Donc $w$ commute avec $p_i$.

    \begin{quote}
        L'image de $p_i$ est stable par $w$, donc 
        pour $1 \leq i \leq l$ on sait que $w (E_i) \subseteq E_i$
    \end{quote}

    L'idée est d'utiliser l'espace cyclique de taille maximale $E_l$.
    Puisque $w(x_l) \in E_l$ on a par définition un polynôme $R$
    tel que $w(x_l) = R(u)(x_l)$.
    
    On va maintenant utiliser l'isomorphisme $\phi$ introduit 
    auparavant pour déduire que $w = R(u)$ sur tout l'espace.

    Soit $1 \leq i_0 \leq l - 1$, on considère
    le $l$-uplet $(x_1, \dots, x_{l-1}, x_{i_0})$. On a bien 
    $x_{i_0} \in \Ker \pi_l(u)$ car $\pi_l$ annule $u$ sur tout 
    l'espace. L'isomorphisme $\phi$ permet alors de 
    construire $v$ qui commute avec $u$ et tel que~:
    
    \begin{equation*}
        \begin{cases}
            v (x_i) = x_i & 1 \leq i \leq l - 1 \\
            v (x_l) = x_{i_0} 
        \end{cases}
    \end{equation*}

    Par construction on a donc~:
    
    \begin{align*}
        w (x_{i_0}) &= wv (x_l) \\
                    &= v w (x_l) \\
                    &= v (R(u)(x_l)) \\
                    &= R(u) (v (x_l)) \\
                    &= R(u) (x_{i_0})
    \end{align*}

    On sait donc que $w$ commute avec $u$ et $w (x_i) = R(u)(x_i)$,
    on peut donc conclure que $w = R(u)$ en le vérifiant 
    sur la base adaptée aux $E_i$.
\end{proof}


\appendix

\bibliographystyle{abstract}
\bibliography{ressources}



\end{document}

