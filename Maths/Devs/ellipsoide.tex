\begin{references}
    \item[FGN Algèbre 3] page 239
\end{references}

\subsection{Prérequis}

\begin{lemme}
    Le déterminant est logarithmiquement 
    strictement convexe 
    sur les matrices définies positives
\end{lemme}

\begin{proof}
    Soit $A,B \in S_n^{++} (\mathbb{R})$, 
    on utilise le théorème de réduction 
    des formes quadratiques.
    Il existe $P \in GL_n(\mathbb{R})$
    et $D$ diagonale telle que~:

    \begin{equation}
        A = {}^t P P  \quad \quad B = {}^t P D P
    \end{equation}

    Comme $B$ est dans $S_n^{++}$, $D$ est à diagonale strictement 
    positive.

    On considère $\alpha, \beta > 0$ tels que $\alpha + \beta = 1$.

    \begin{equation}
        \det \alpha A + \beta B
        = \det {}^tP (\alpha I_n + \beta D) P
        = (\det P)^2 \det (\alpha I_n + \beta D)
    \end{equation}

    Or $\det (\alpha I_n + \beta D) = \prod_{i = 1}^n (\alpha + \beta
    \lambda_i)$

    Par concavité du logarithme $\log \alpha + \beta \lambda_i \geq \alpha \log
    1 + \beta \log \lambda_i$. On peut donc déduire

    \begin{equation}
        \det (\alpha I_n + \beta D) \geq \prod_{i = 1}^n \lambda_i^\beta
    \end{equation}

    On peut alors conclure

    \begin{equation}
        \det (\alpha A + \beta B)
        \geq (\det P)^2 (\det D)^\beta = (\det {}^t P P)^{2 \alpha + 2 \beta}
        (\det D)^\beta
        = (\det A)^\alpha (\det B)^\beta
    \end{equation}

    Si $A \neq B$, alors un des $\lambda_i$ est différent de $1$, et 
    la stricte concavité du log permet alors de conclure à l'inégalité 
    stricte.
\end{proof}

\begin{lemme}
    L'ensemble des formes quadratiques 
    positives est \emph{convexe}
    et \emph{fermé}
\end{lemme}

\subsection{Développement}

On prend $K$ un compact d'intérieur non vide de $\mathbb{R}^n$.
On va montrer qu'il existe un unique ellipsoïde centré en $0$ de 
volume minimal contenant $K$.

\begin{remarque}
    Un ellipsoïde centré en l'origine est défini par 
    une inéquation du type $q(x) \leq 1$ avec $q$
    une forme quadratique définie positive.
\end{remarque}

On munit $\mathbb{R}^n$ de sa structure euclidienne usuelle.
On note $Q$ l'ensemble des formes quadratiques, $Q_+$ les positives,
et $Q_{++}$ les définies positives.

Pour une telle forme quadratique on pose $\mathcal{E}_q$ l'ellipsoïde 
défini par $q$.

Comme $q$ est définie positive, il existe une base orthonormale pour $q$,
dans laquelle $q(x) = \sum_{i = 1}^n a_i x_i^2$.

Dans cette base

\begin{equation}
    V_q = \int\int \dots \int \chi_{ q(x) \leq 1 } dx_1 \dots dx_n
\end{equation}

En considérant le changement de variable linéaire inversible
$x_i = \frac{t_i}{\sqrt{a_i}}$ on récupère 

\begin{equation}
    V_q = \int \dots \int \chi_{ \sum_i x_i^2 } \frac{d t_1 \dots d
    t_n}{\sqrt{a_1 \dots a_n}}
\end{equation}

Mais le déterminant de toute matrice de $q$ dans une base orthonormale 
est précisément $a_1 \cdots a_n$ par simple changement de base.
Ainsi, notons $D(q)$ ce déterminant qui ne dépend pas de la base.

\begin{equation}
    V_q = \frac{V_0}{\sqrt{D(q)}}
\end{equation}

Où $V_0$ est le volume de la boule unité pour la norme euclidienne canonique.

On veut donc trouver une unique forme quadratique $q \in Q_{++}$ telle 
que $D(q)$ soit \emph{maximal} tel que $K \subseteq \mathcal{E}_q$.

On pose $N(q) = \sup_{ \| x \| \leq 1} |q(x)|$ qui est une norme sur 
l'espace $Q$. On considère de manière naturelle 
l'espace 

\begin{equation}
    \mathcal{A} = \left\{ q \in Q_+ ~|~ \forall x \in K, q(x) \leq 1 \right\}
\end{equation}

On commence par montrer l'unicité avant l'existence.
L'unicité repose sur un argument de convexité.

\begin{description}
    \item[Convexité]
        Soient $q$ et $q'$ dans $\mathcal{A}$, 
        $\lambda q + (1 - \lambda) q'$ est bien 
        dans $Q_+$ car c'est un ensemble convexe.
        
        De plus si $x \in K$
        alors 
        \begin{equation}
            (\lambda q + (1 -\lambda) q')(x)
            \leq 
            \lambda + (1 - \lambda)
            = 1
        \end{equation}

        Donc $K$ est toujours contenu dans les 
        ellipsoïdes des combinaisons convexes de $\mathcal{A}$.
        
    \item[Unicité]

        Supposons qu'on ait deux formes $q$ et $q'$ 
        qui réalisent le minimum pour $D(q)$ sur $\mathcal{A}$.
        Comme le déterminant est strictement logarithmiquement convexe,
        sur l'ensemble 
        des matrices définies positives, on a

        \begin{equation}
            D \left( \frac{q + q'}{2} \right) 
            > D(q)^{1/2} D(q')^{1/2}  = D(q)
        \end{equation}

        C'est absurde, car le barycentre est dans $\mathcal{A}$
        et donc $q$ n'atteint pas le maximum de $D$.
\end{description}

Montrons désormais l'existence.

\begin{description}
    \item[Fermé]
        C'est évident puisqu'une limite de $q_n$ dans $\mathcal{A}$
        est dans $Q_+$, et que 
        pour un $x \in K$, $q_n(x) \leq 1$ donc par passage 
        à la limite $\forall x \in K, q(x) \leq 1$.

    \item[Borné]
        Comme $K$ est d'intérieur non vide, il existe 
        $a \in K$ et $r > 0$ tel que $B(a,r) \subseteq K$.

        Soit $q \in \mathcal{A}$, si $\| x \| \leq r$ alors 
        $a + x \in K$ donc $q(a+x) \leq 1$.
        D'autre part $q(-a) = q(a) \leq 1$.

        Par l'inégalité de Minkowski on obtient alors 
        \begin{equation}
            \sqrt{q(x)}
            = 
            \sqrt{q(x + a - a)}
            \leq 
            \sqrt{q(x+a)}
            + 
            \sqrt{q(-a)}
            \leq 2
        \end{equation}

        Ainsi, si $\| x \| \leq r$, $|q(x)| \leq 4$. On 
        déduit donc par dilatation que $N(q) \leq \frac{4}{r^2}$.
    \item[Non vide]

        Comme $K$ est compact, il est borné par un $M > 0$.
        Posons alors $q(x) = \frac{\|x\|^2}{M^2}$, c'est une 
        forme quadratique qui vérifie bien $q \in \mathcal{A}$
\end{description}

\subsection{Post requis}

\begin{description}
    \item[Et si jamais l'ensemble n'était pas d'intérieur vide ?]
        Et bien on ne peut pas (toujours) atteindre l'optimum.
        Considérons par exemple le segment $[-1,1]$ compact de $\mathbb{R}^2$,
        des ellipses de plus en plus effilées possèdent un volume de plus en
        plus faible et contiennent toujours ce segment. La limite n'étant pas 
        une ellipse, on a un contre exemple.

        \begin{center}
            \begin{tikzpicture}
                \draw[dashed,->] (-2,0) -- (2,0);
                \draw[dashed,->] (0,-1) -- (0,1);
                \draw[thick]  (-1,0) -- (1,0);

                \foreach \o in {1,...,3}
                {
                    \draw[opacity={1 - \o / 5}] (0,0) ellipse ({\o} and {1 / \o});
                };
            \end{tikzpicture}
        \end{center}


    \item[L'ellipsoïde pour un triangle équilatéral dans le plan]
        \begin{enumerate}
            \item L'ellipsoïde minimal passe par un sommet,
                sinon on peut considérer une dilatation qui amène 
                un des sommets sur le bord de l'ellipse, 
                la forme quadratique associée possède un volume strictement 
                inférieur et contient encore le triangle.

                \begin{center}
                    \begin{tikzpicture}
                        \draw[dashed,->] (-4,0) -- (4,0);
                        \draw[dashed,->] (0,-2) -- (0,2);
                        \draw (0,0) ellipse (2.2cm and 1.1cm);
                        \draw (10:1) -- (130:1) -- (250:1) -- (10:1);
                        \draw[red] (10:1.14) -- (130:1.14) -- (250:1.14) -- (10:1.14);
                        \draw[red,->] (10:0.2) -- (10:0.5);
                        \draw[red,->] (130:0.2) -- (130:0.5);
                        \draw[red,->] (250:0.2) -- (250:0.5);

                        \draw[fill, red] (250:1.14) circle (0.05);
                    \end{tikzpicture}
                \end{center}

            \item L'ellipsoïde de volume minimal est le cercle circonscrit.
                En effet, l'ellipsoïde est invariant par l'action 
                du groupe d'isométries du triangle équilatéral 
                par unicité du-dit ellipsoïde de volume minimal.
                
                \begin{center}
                    \begin{tikzpicture}
                        \draw[dashed,->] (-4,0) -- (4,0);
                        \draw[dashed,->] (0,-2) -- (0,2);
                        \draw (0:1) -- (120:1) -- (240:1) -- (0:1);
                        
                        \draw (0,0) ellipse (2.2cm and 0.9cm);
                        \draw[red, rotate=120] (0,0) ellipse (2.2cm and 0.9cm);
                        \draw[red,->] (30:2) arc (30:60:1);
                    \end{tikzpicture}
                \end{center}

                En particulier, l'ellipse ne peut pas posséder deux valeurs 
                propres distinctes, c'est donc un cercle. Or elle passe par 
                un point du triangle, et est de même centre, c'est donc 
                le cercle circonscrit.
        \end{enumerate}

    \item[Ellipsoïde pour un polygône régulier dans le plan]
        On fait exactement la même preuve que pour le triangle !

    \item[Généralisation aux polyèdres] 
        L'ellipse doit être stable par le groupe de symétrie 
        du polyèdre régulier, cela va donner un certain nombre
        de conditions sur les valeurs propres, et au moins 
        dans $\mathbb{R}^3$ on obtient la sphère.

    \item[Volume général de l'ellipsoïde en dimension supérieure ?] 
        On a déjà calculé le volume de la boule unité 
        en dimension $n$ dans les remarques du développement sur Brouwer.
        On rappelle

        \begin{equation}
            V_n = \frac{2 \pi^{n/2}}{n \Gamma(n/2)}
        \end{equation}
        
        Ainsi, on constate aisément que $V_n \rightarrow 0$, 
        ce qui est logique quand on connaît un peu les propriétés
        de répartition de la masse dans les espaces de grande dimension. 

        En particulier, pour un hypercube de côté $1$, dont le volume 
        est naturellement $1$, le diamètre de l'ellipsoïde (qui est une sphère)
        \emph{doit tendre vers l'infini} quand $n$ grandit !
\end{description}

\subsection{Application non triviale}

On peut retrouver le développement sur les sous groupes compacts 
de $GL_n(\mathbb{R})$ en utilisant John. C'est fait dans l'exercice
suivant du FGN Algèbre 3 (page 239 + un pouillème). 

Soit $G$ un groupe compact de $GL_n(\mathbb{R})$.
Le groupe $G$ agit naturellement sur les parties compactes 
de $E$ d'intérieur non vide contenant zéro via $g \cdot K = g(K)$. 

D'un autre côté, $G$ agit naturellement sur l'espace 
des formes quadratiques via $(g \cdot q) (x) = q (g^{-1} x)$
(pour que ce soit une action à gauche). 

On recherche une forme quadratique $q$ invariante pour l'action 
de $G$ (c'est ce qu'on recherche dans le développement "classique"),
et pour cela on va considérer l'ellipsoïde associée à une partie 
liée à la première action.

On pose $K = G \cdot B$ où $B$ est la boule unité (compacte 
en dimension finie). Cette partie est par construction invariante 
par l'action de $G$. Mais alors, considérons $q_K$ qui définit 
l'ellipsoïde de volume minimal pour $K$.

On constate que $g \cdot q$ est une forme quadratique 
dont l'ellipse contient aussi $K$. Mieux, comme $G$
est compact, le déterminant de ses éléments est de module $1$ 
\footnote{Considérer une suite ... C'est évident}.
Ainsi les éléments de $G$ \emph{préservent les volumes} !!
De manière plus pragmatique, $D(g \cdot q) = D (q)$, ce qui force 
par unicité $g \cdot q = q'$.


On a donc $G \subseteq O(q)$ et par conséquent, comme $q$ 
est définie positive on déduit $uGu^{-1} \subseteq O(E)$.
