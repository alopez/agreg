\begin{references}
    \item[COT] exercices de probabilités, page 72
    \item[Probabilités pour les non-probabilistes, Walter Appel]
\end{references}

\subsection{Développement}

On considère une suite $Z_k$ de variables aléatoires 
définies par récurrence comme suit

\begin{equation}
    \begin{cases}
        Z_0 = 1 \\
        Z_{k+1} = \sum_{i = 1}^{Z_k} X_{i,k}
    \end{cases}
\end{equation}

Où les $X_{i,k}$ sont des variables aléatoires indépendantes 
identiquement distribuées selon la même loi qu'une certaine 
variable $X$.

\begin{description}
    \item[Approche Modélisation]
        On veut modéliser le caractère pérène d'un allèle dominant 
        dans une population, en fonction de sa capacité de reproduction 
        modélisée par la loi de $X$. On peut aussi penser à faire 
        de l'épidémiologie avec ça.

        L'objectif est de calculer la probabilité d'extinction. 

    \item[Premières remarques]

        De manière très naturelle, si $Z_k = 0$ pour un certain $k$,
        alors pour $n \geq k$, $Z_n = 0$.

        Ainsi la probabilite d'extinction s'écrit 
        \begin{equation}
            \mathbf{P} (ext) = \mathbf{P} \left( \bigcup_k \{ Z_k = 0 \} \right)
        \end{equation}

        Par continuité décroissante de la limite, on déduit donc~:
        \begin{equation}
            \mathbf{P} (ext) = \lim \mathbf{P} \left(\bigcup_{k \leq N} \{ Z_k =
            0 \} \right) = \lim \alpha_k = \alpha
        \end{equation}

    \item[L'outil théorique]

        On introduit l'outil théorique qui va faire marcher toute l'étude~: la
        série génératrice.

        \begin{equation}
            G_{Z_n} (t) = \mathbf{E} (t^{Z_n}) = \sum_{k \in \mathbb{N}}
            \mathbf{P} (Z_n = k) t^k
        \end{equation}

        C'est une série entière à termes positifs de rayon de convergence plus 
        grand que $1$. Et elle caractérise la loi de $Z_n$.

        Son lien avec la suite $\alpha_n$ est le suivant~:

        \begin{equation}
            G_{Z_n} (0) = \mathbf{P} (Z_n = 0) = \alpha_n
        \end{equation}

    \item[L'équation de récurrence]
    
        \begin{align}
            G_{Z_{n+1}} (t) 
            &= \mathbf{E} \left( t^{\sum_{i = 1}^{Z_n} X_{i,n}} \right)  \\
            &= \mathbf{E} \left( \sum_{N \in \mathbb{N}} t^{\sum_{i = 1}^{N}
            X_{i,n}} \chi_{Z_n = N} \right)  \\
            &= \sum_{N \in \mathbb{N}} \mathbf{E} \left( t^{\sum_{i = 1}^{N}
            X_{i,n}} \chi_{Z_n = N} \right)  \\
            &= \sum_{N \in \mathbb{N}} \prod_{i = 1}^N \mathbf{E} \left(
            t^{X_{i,n}} \right) \mathbf{P} \left( Z_n = N \right)  \\
            &= \sum_{N \in \mathbb{N}} \left(
            G_X (t) \right)^N \mathbf{P} \left( Z_n = N \right)  \\
            &= \left(G_{Z_n} \circ G\right)(t) 
        \end{align}

        On a donc~:

        \begin{equation}
            G_{Z_n} (t) = G^{\circ n}_X (t)
        \end{equation}

        En particulier on déduit~:

        \begin{equation}
            \begin{cases}
                \alpha_0     = 0 \\
                \alpha_{n+1} = G(\alpha_n)
            \end{cases}
        \end{equation}

    \item[Quelques propriétés de $G$, et hypothèses sur $X$]

        \begin{enumerate}[(i)]
            \item On sait déjà que $G$ est une série entière de rayon de 
                convergence plus grand que $1$, donc $C^\infty$ sur $[0,1[$.


            \item Comme la série $G$ est à coefficients positifs et que $G(1) = 1$ 
                on déduit que $G$ est continue en $1$.
                (permutation de sup)

                \textbf{La suite $\alpha_n$ converge donc vers un point fixe de $G$}

            \item Une autre permutation de suprema permet de déduire que $G$ est 
                croissante sur $[0,1]$.

                \textbf{La suite $\alpha_n$ converge donc vers le plus petit point 
                fixe de $G$}

            \item On sait de plus que $G$ est convexe comme supremum de
                fonctions convexes. On peut éviter le cas où $G$ est une droite,
                et supposer que la série contient au moins un terme d'ordre
                $\geq 2$. On constate alors que $G'' > 0$ sur $]0,1[$ 
                et donc $G$ est \emph{strictement convexe}.
                \footnote{Dans le cas où $G$ donne une droite, on peut directement 
                comprendre que soit le seul point fixe est $1$, soit le 
                plus petit point fixe est zéro.}

            \item On sait par positivité que $\lim G'(t)$ existe 
                et peut valoir $+\infty$. On remarque aisément 
                que $G'(t) = \mathbf{E}(X)$ dans $\overline{\mathbb{R}_+}$.

                En effet, on a d'une part
                \begin{equation}
                    \sum_{n \leq N} \mathbf{P}(X = n) \frac{t^n - 1}{t - 1} \leq
                    \sup_{t \in [0,1]} 
                    \sum_{n \leq N} \mathbf{P}(X = n) \frac{t^n - 1}{t - 1}
                    =
                    \sum_{n \leq N} \mathbf{P}(X = n) n 
                    \leq
                    \mathbf{E}(X)
                \end{equation}

                Et d'autre part les inégalités inverses
                \begin{equation}
                    \sum_{n \leq N} \mathbf{P} (X = n) \frac{t^n - 1}{t- 1}
                    \leq G'(t)
                \end{equation}
        \end{enumerate}

    \item[Traitons le cas $\mathbf{E}(X) \leq 1$.]
        Dans ce cas, on sait que $G$ est strictement croissante,
        et donc que $G$ est strictement au dessus de sa tangente en $1$
        pour $t \neq 1$. C'est-à-dire~:

        \begin{equation}
            \forall t \in [0,1[, 
                    G(t) > G'(1) (t - 1) + G(1)
        \end{equation}

        On obtient alors en faisant une soustraction par $t$~:

        \begin{equation}
            G(t) - t > G'(1) ( t - 1) - (t - 1)
        \end{equation}

        Et donc 

        \begin{equation}
            G(t) - t > (t-1) (G'(1) - 1) \geq 0
        \end{equation}

        Ainsi, on déduit qu'il n'y a pas de point fixe 
        strictement plus petit que $1$.


    \item[Traitons le cas $\mathbf{E}(X) > 1$.]

        Au voisinage de $1$, que l'espérance soit finie ou non,
        on a $G$ qui est au dessous de sa corde, et donc au dessous de
        l'identité. Comme $G(0) \geq 0$ on déduit alors 
        que $G$ possède au moins un point fixe strictement plus 
        petit que $1$ via le théorème des valeurs intermédiaires.

        Si jamais il y avait deux tels points fixes, $s_1$ et $s_2$, 
        alors $G' - 1$ s'annule en ses deux points, mais 
        comme $G$ est strictement convexe, $G'$ est strictement 
        croissante sur $]0,1[$ et c'est absurde.
\end{description}

\subsection{Remarques}

Peut-on faire exactement la même chose avec des fonctions caractéristiques ?
