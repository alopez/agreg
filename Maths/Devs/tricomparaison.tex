\begin{references}
    \item[TODO]
\end{references}

\begin{ouverture}
    Des algorithmes de tri en $\mathcal{O}(n \log n)$ sont 
    connus et réputés pour être optimaux. Quel argument permet 
    de constater cette optimalité ? Quid des algorithmes en 
    temps «~linéaire~» (tri par paquets, tri par base, ...) ?
\end{ouverture}

\subsection{Introduction}

\begin{definition}
    Soit $T$ un arbre binaire qui possède $n$ feuilles,
    on peut définir la profondeur moyenne de $T$ comme ceci~:

    \begin{compactenum}[(i)]
    \item On construit $X$ une variable aléatoire uniforme sur $\llbracket 1,
        n \rrbracket$
    \item On contsruit la fonction profondeur $p$ qui à $i$ associe la
        profondeur 
        de la $i$ème feuille.
    \item La profondeur moyenne est alors $\mathbb{E}(p (X))$
    \end{compactenum}

    Une expression directe est la suivante~:

    \begin{equation*}
        m(T) = \frac{1}{n} \sum_{i = 1}^{n} p(i)
    \end{equation*}
\end{definition}

\begin{theoreme}
    La profondeur moyenne d'un arbre qui possède $n$ feuilles 
    est supérieure à $\log_2 n$.
\end{theoreme}

\begin{proof}
    Par induction sur la structure de l'arbre.

    \begin{description}
        \item[Si l'arbre est une feuille]
            Alors sa profondeur est $0$,
            la profondeur moyenne est aussi $0$
            et $\log_2 1 = 0$ donc $m(T) \geq \log_2 n$
        \item[Si l'arbre possède une racine non triviale]
            Notons $T_1$ et $T_2$ les deux sous arbres 
            (potentiellement réduits à une feuille) de $T$.
            Notons $n_1$ le nombre de feuilles de $T_1$
            et $n_2$ le nombre de feuilles de $T_2$, par construction 
            on a $n_1 + n_2 = n$.

            Par hypothèse de récurrence $m(T_1) \geq \log_2 n_1$
            et $m(T_2) \geq \log_2 n_2$.

            \begin{align*}
                m (T) &= \frac{1}{n} \sum_{i = 1}^{n} p (i) \\
                      &= \frac{1}{n} \sum_{i = 1}^{n_1} (p_1 (i) + 1 )
                + \frac{1}{n} \sum_{i = n_1 + }^{n_2} (p_2 (i) + 1) \\
                      &= \frac{1}{n} \left( n_1 m(T_1) + n_1 + n_2 m(T_2) + n_2 \right) \\
                      &= 1 + \frac{n_1}{n} m (T_1) + \frac{n - n_1}{n} m(T_2) \\
                      &\geq 1 + \frac{n_1}{n} \log_2 (n_1) 
                          + \frac{n - n_1}{n} \log_2 (n - n_1) \\
            \end{align*}

            Étudions alors la fonction $f$ définie sur $]0, n[$\footnote{
                On traite bien tous les cas, car $n_1 > 0$ et $n_1 < n$,
                puisque chaque sous arbre possède au moins une feuille
            }par~:

            \begin{equation*}
                f(x) = 1 + \frac{x}{n} \log_2 x
                    + \frac{n - x}{n} \log_2 (n - x) \\
            \end{equation*}

            On calcule 
            \begin{equation*}
                f'(x) = \frac{\log_2 x}{n} + \frac{1}{n} 
                        - \frac{\log_2 (n - x)}{n} 
                        - \frac{1}{n}
            \end{equation*}

            Ce qui donne après simplification~:

            \begin{equation*}
                f'(x) = \frac{1}{n} \log_2 \frac{x}{n - x}
            \end{equation*}

            On a donc le tableau de variation suivant~:

            \begin{equation*}
                \begin{array}{r|ccc}
                    x     & 0 & n/2 & n \\ \hline 
                    f'(x) & - & 0 & + \\ 
                \end{array}
            \end{equation*}

            Et on constate alors que le minimum de cette fonction est en $n/2$,
            ce qui signifie~:

            \begin{equation*}
                m(T) \geq f(n/2) = 1 + \frac{1}{2} \log_2 \frac{n}{2} 
                + \frac{1}{2} \log_2 \frac{n}{2} 
            \end{equation*}

            Or $1 = \log_2 2$ donc $f(n/2) = \log_2 n$, ce qui permet 
            de conclure.
    \end{description}
\end{proof}

\begin{lemme}
    La profondeur maximale d'un arbre qui possède $n$ feuilles 
    est supérieure à $\log_2 n$.
\end{lemme}

\subsection{Tris}

\begin{lemme}[Stirling]
    La formule de Stirling donne 
    un équivalent de $n!$~:
    \begin{equation*}
        n! \sim 
        \left( \frac{n}{e} \right)^n \sqrt{2 \pi n} 
    \end{equation*}
    
    Et permet donc de justifier~.
    \begin{equation*}
        \log_2 n! \sim n \log_2 n
    \end{equation*}
\end{lemme}


\begin{definition}[Arbre de décision]
    Soit $n \in \mathbb{N}$, 
    un arbre de décision pour les données 
    de taille $n$ est un arbre qui vérifie~:

    \begin{compactenum}
    \item Les nœuds sont étiquetés par 
        des comparaisons de la forme $i < j$
        (avec $0 \leq i,j \leq n - 1$)
    \item Les feuilles sont étiquetées 
        par des éléments de $\mathfrak{S}_n$
        ou par $\bot$
    \end{compactenum}
\end{definition}

\begin{definition}[Tri par comparaison]
    Soit $P$ un algorithme de tri, et $n \in \mathbb{N}$.
    Comme $P$ est un algorithme déterministe, on peut 
    sur une entrée $T$ (un tableau de $n$ entiers)
    construire la suite des comparaisons $i < j$ effectuées 
    par l'algorithme, et enregistrer dans une permutation 
    l'ensemble des opérations effectuées sur le tableau. 
    On note $\trace_P T$ cette trace.

    Si l'ensemble des traces de $P$ forme un arbre binaire,
    alors $P$ est dit \emph{tri par comparaison}. On note 
    $A_P^n$ l'arbre formé par les traces de $P$
    sur les tableaux de taille $n$.
\end{definition}

\begin{propriete}
    Un arbre sémantique d'un algorithme de tri 
    par comparaison possède au moins $n!$ feuilles
\end{propriete}

\begin{proof}
    La fonction $f$ qui à une permutation $\sigma$
    dans $\mathfrak{S}_n$ 
    associe la trace $\trace_P \sigma$ est injective.

    En effet, la feuille (dernier élément) de $\trace_P \sigma$
    correspond à la permutation $\sigma^{-1}$, puisque 
    c'est précisément la suite d'opérations qui permet de 
    trier $\sigma$. 
\end{proof}

\begin{propriete}[Bornes inférieures]
    Un algorithme de tri par comparaison fait 
    en moyenne $\Omega \left( n \log n \right)$ 
    comparaison.  
    Un algorithme de tri par comparaison fait 
    dans le pire des cas $\Omega \left( n \log n \right)$ 
    comparaison.  
\end{propriete}

\begin{proof}
    On combine simplement les lemmes précédent 
    avec la formule de Stirling.
\end{proof}

