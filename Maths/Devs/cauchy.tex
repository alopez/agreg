\begin{references}
    \item[ZQ]
\end{references}

\subsection{Pré-requis}

\begin{lemme}[Gronwall]
    Si $\phi$ et $\psi$ sont des fonctions positives 
    sur $I = [a,b]$ qui vérifient 
    \begin{equation}
        \phi(t) \leq A + B \int_a^t \psi(s)\phi(s) ds
    \end{equation}
    Alors 
    \begin{equation}
        \phi(t) \leq A \exp\left( B \int_a^t \psi(s) ds \right)
    \end{equation}
\end{lemme}

\begin{proof}
    Considérer le quotient des deux majorations, c'est une fonction 
    dérivable strictement décroissante, et donc la majoration 
    déduite est toujours au dessus de la majoration supposée ...
\end{proof}

On considère une équation différentielle de type 
$y' = F(t,y(t))$. Avec $F : I \times \Omega \to \mathbb{R}^n$.

\begin{definition}[Cylindre de sécurité]
    Étant donné $t_0 \in I$, 
    c'est un cylindre $[t_0 - T, t_0 + T] \times B(y_0, r_0)$ 
    de $I \times \Omega$ 
    contenant $t_0$, tel que si $y$ est 
    une solution de l'équation et $y(t_0) = y_0$ alors 
    pour tout $t \in [t_0 - T, t_0 + T]$
    $y(t) \in B (y_0, r_0)$.
    Les boules étant fermées.
\end{definition}

Comment construire un cylindre de sécurité ?

\begin{exemple}
    Si $F$ est continue, alors on considère un cylindre 
    $[t_0, - T, t_0 + T] \times B(y_0, r_0)$ 
    avec $T > 0$ quelconque.

    Comme ce cylindre est compact, la fonction $F$ est bornée 
    par une constante $M$.

    Supposons qu'il existe une solution qui sorte 
    du cylindre. Elle le fait à un temps $\tau \in [t_0 - T, t_0 + T]$.


    Mais alors~:


    \begin{equation}
        r_0 = \| y(\tau) - y_0 \| = 
        \| y(\tau) - y (t_0) \|= 
        \left\| \int_{t_0}^\tau y'(u)du  \right\|
        = 
        \left\| 
            \int_{t_0}^\tau F(u, y(u)) du 
        \right\|
        \leq M|\tau - t_0| \leq MT
    \end{equation}

    On constate que c'est indépendant de $y$ !
    Donc en posant $T' = \min (T, \frac{r_0}{M})$,
    on est assuré que les solutions restent dans le 
    cylindre (ce qui correspond à restreindre le temps 
    pour éviter que ça parte en couille).
\end{exemple}

\begin{definition}[Localement-Lip]
    La fonction $F(t,x)$ est localement lipschitzienne 
    si et seulement si pour toute paire $(x_0, t_0)$, 
    il existe un voisinage $V$ de ce point dans $I \times \Omega$
    tel que sur ce voisinage 
    \begin{equation}
        \| F(t,x) - F(t,y) \| \leq C \| x - y \| \quad \quad 
        \forall (t,x) \in V, (t,y) \in V
    \end{equation}
    
    C'est-à-dire, en fixant un temps, on est localement lip.
\end{definition}

\subsection{Développement}

On veut démontrer Cauchy-Lipschitz local.
On suppose $F$ localement-lipschitzienne. 
On se fixe $(t_0, y_0)$ comme condition initiale, et 
on va montrer qu'il existe une unique solution 
définie sur un intervalle $[t_0 - T, t_0 + T]$ de $I$.

\begin{description}
    \item[Construction d'un cylindre (qui n'est pas encore de sécurité)]

        Comme $F$ est localement-lipschitzienne, 
        il existe un voisinage $V$ de $(t_0, y_0)$ 
        qui vérifie les bonnes hypothèses, mais ce voisinage 
        étant ouvert, il contient un pavé.

        On le note $C = [t_0 - T, t_0 + T] \times B(y_0, r)$,
        sur lequel la fonction est bornée par $M$ et $L$ lip.

    \item[Construction d'une équation fonctionnelle]

        On construit la fonction $G$ qui 
        transforme une application de $\mathcal{C}([t_0 -T, t_0 + T], B(y_0,
        r))$ en une application dans le même ensmemble. 

        \begin{equation}
            G : y \mapsto y_0 + \int_{t_0}^x F(s, y(s)) ds
        \end{equation}

        En effet, 

        \begin{equation}
            \| G(y)(t) - y_0 \| 
            =
            \left\|
                \int_{t_0}^t F(s, y(s)) ds
            \right\|
            \leq M | t - t_0 | \leq MT
        \end{equation}

        Donc en posant $T' = \min (T, \frac{r}{M})$, on 
        construit effectivement un cylindre de sécurité !

        On a une application d'un espace complet 
        dans un espace complet... Il faut lui trouver un point fixe.

        En effet, on constate que $y$ est solution si et seulement si 
        $y = G(y)$.

        En effet, si $y$ est solution, en intégrant on obtient $y = G(y)$,
        et si $y = G(y)$, alors $y$ est $C^1$ et $y(t_0) = y_0$,
        et en dérivant on remarque que $y$ est bien solution du problème.

    \item[Construction du point fixe]

        On montre qu'il existe $k$ tel que $G^k$ est contractante.

        Pour cela on montre le lemme plus général 

        \begin{equation}
            | G^k(y)(t) - G^k(z)(t) \| \leq M \frac{L^{k-1}|t - t_0|^k}{k!} \| y
            - z \|_\infty
        \end{equation}

        En effet, c'est vrai pour $k = 0$, comme on l'a déjà montré.
        Pour la récurrence

        \begin{align}
            \| G^{k+1} (y)(t) - G^{k+1}(z)(t) \|
            &=
            \| G (G^k(y)) (t) - G (G^k(z))(t) \| \\
            &\leq 
            \left\|
            \int_{t_0}^t F(s,G^{k}(y)(s)) 
                       - F(s,G^{k}(z)(s)) ds
            \right\| \\
            &\leq 
            \int_{t_0}^t 
                L \|G^{k}(y)(s) - G^k(z)(s)\| ds \\
                &\leq
            \int_{t_0}^t ML \frac{L^{k-1}|s - t_0|^k}{k!} \| y - z\|_\infty ds \\
            &\leq 
            M \frac{L^k |t - t_0|^{k+1}}{(k+1)!} \| y - z \|_\infty
        \end{align}

        Comme $|t| \leq T$, on peut choisir $k$ assez grand pour que 
        $G^k$ soit strictement contractante.

        On peut alors utiliser le théorème de point fixe de Picard, qui 
        justifie qu'il existe un unique point fixe pour cette 
        fonction $G^k$.

    \item[Retour à $G$]

        Si $G^k$ possède un unique point fixe $y$, alors $G^k (y) = y$,
        mais alors $G^k (G(y)) = G(y)$, donc par unicité, $G(y) = y$,
        et cet unique point fixe est aussi un point fixe de $G$.

    \item[Conclusion]
        On a construit un intervalle où la solution existe, 
        est unique, et voilà

    \item[Point fixe de Picard]
        Soit $E$ un espace de Banach, $f$ une application $K$-contractante 
        avec $K < 1$.
        Alors $f$ admet un unique point fixe.

        \begin{description}
            \item[Unicité]
                Si $f(a) = a$ et $f(b) = b$,
                alors on a $\| f(a) - f(b) \| \leq K \| b - a \|$
                ce qui est absurde si $a \neq b$.

            \item[Existence]
                Considérons $x_0 \in E$, et la suite $x_{k+1} = f(x_k)$.
                Par construction

                \begin{equation}
                    \| x_{k+2} - x_{k+1} \| \leq K \| x_{k+1} - x_k \|
                                            \leq K^k \| x_1 - x_0 \| 
                \end{equation}
                
                Comme $K < 1$, la série des $x_{k+1} - x_k$ est donc 
                absolument convergente, ainsi comme $E$ est complet,
                elle est convergente, et alors on constate que 
                la suite $x_k$ converge car~:

                \begin{equation}
                    x_{k+1} = x_0 + \sum_{i \leq k} x_{i+1} - x_i
                \end{equation}

                Mais alors, on a par continuité de $f$
                $f(x_\infty) = x_\infty$, et donc $x_\infty$ est bien 
                un point fixe de $f$.
        \end{description}

    \item[Remarque]
        On peut trouver l'unicité indépendamment avec le lemme de 
        Gronwall.


\end{description}


