\begin{references}
    \item[Krivine]
\end{references}

On veut montrer la propriété de confluence de la $\beta$-réduction.

\begin{center}
    \begin{tikzcd}
        t \arrow[r] \arrow[d] & u \arrow[d, dashed] \\
        v \arrow[r, dashed] & w
    \end{tikzcd}
    Les flèches représentent des réductions pour $\to_\beta^*$
\end{center}

\begin{description}
    \item[Méthode 1] 
        Montrer que $\to_\beta$ est fortement confluente.
        Faux pour $(\lambda x. x x) ((\lambda x. x) y)$.

    \item[Méthode 2]
        Montrer que $\to_\beta$ termine et est localement 
        confluente.
        Faux pour $\Omega$.
\end{description}

\subsection{La méthode qui marche}

Définir une relation $\to_\beta \subseteq \implies \subseteq \to_\beta^*$

\begin{multicols}{2}
    \begin{prooftree}
        \AxiomC{}
        \UnaryInfC{$t \implies t$}
    \end{prooftree}

    \begin{prooftree}
        \AxiomC{$t \implies t'$}
        \UnaryInfC{$\lambda x. t \implies \lambda x. t$}
    \end{prooftree}

    \begin{prooftree}
        \AxiomC{$t \implies t'$}
        \AxiomC{$u \implies u'$}
        \BinaryInfC{$tu \implies \lambda t'u'$}
    \end{prooftree}

    \begin{prooftree}
        \AxiomC{$t \implies t'$}
        \AxiomC{$u \implies u'$}
        \BinaryInfC{$(\lambda x. u) t \implies \lambda u[t/x]$}
    \end{prooftree}
\end{multicols}

\begin{remarque}
    La relation $\implies$ est la plus petite pré-congruence 
    qui vérifie la dernière règle.
\end{remarque}

\begin{lemme}
    On a bien les inclusions désirées
\end{lemme}

\begin{proof}
    Il est clair que $\to_\beta$ est inclue dans $\implies$.
    De plus $\to_\beta^*$ est une précongruence
    et vérifie la dernière règle. Ce qui prouve l'inclusion désirée.
\end{proof}


\begin{lemme}
    La règle suivante est admissible 
    \begin{prooftree}
        \AxiomC{$t \implies t'$}
        \AxiomC{$u \implies u'$}
        \BinaryInfC{$ u[t/x] \implies u'[t'/x]$}
    \end{prooftree}
\end{lemme}

\begin{proof}
    Par induction sur la preuve de $u \implies u'$, 
    puis analyse de cas sur la dernière règle appliquée.

    \textbf{\huge{Ne traiter que la règle 4}}

    \begin{description}
        \item[Règle 1] Alors $u' = y = u$ et on a bien 
            le résultat attendu.

        \item[Règle 2] On a $u = \lambda y. v$,
            et $v \implies v'$. 

            Par HR on a $v[t/x] \implies v'[t'/x]$
            puis cela passe sous la $\lambda$-abstraction.

        \item[Règle 3] Pareil, c'est encore 
            utiliser la pré-congruence.

        \item[Règle 4] 
            \begin{prooftree}
                \AxiomC{$w' \implies w'$}
                \AxiomC{$v  \implies  v'$}
                \BinaryInfC{$(\lambda y. w) v \implies \lambda w'[v'/y]$}
            \end{prooftree}

            Alors on a bien 
            \begin{align*}
                u[t/x] &= (\lambda y. w)[t/x] v[t/x] \\
                       &= (\lambda y. w[t/x]) v[t/x] 
            \end{align*}

            Par hypothèse de récurrence on a donc 

            \begin{prooftree}
                \AxiomC{$w[t/x] \implies w'[t'/x]$}
                \AxiomC{$v[t/x] \implies v'[t'/x]$}
                \BinaryInfC{$(\lambda y. w[t/x]) v[t/x] \implies
            w'[t'/x][v'[t'/x] / y]$}
            \end{prooftree}

            Mais on a alors la réduction désirée en remarquant 
            que le terme obtenu est bien $u'[t'/x]$.
    \end{description}
\end{proof}

\begin{lemme}
    La réduction $\implies$ est fortement confluente.
\end{lemme}

\begin{proof}
    Par induction sur $t$.

    \textbf{\huge{Ne traiter que le cas 2}}

    \begin{description}
        \item[Variable] C'est évident 
        \item[Abstraction] C'est par congrunence
        \item[Application]
            On suppose $t = (\lambda x. w) h$ qui est 
            le seul cas intéressant.

            On a alors plusieurs possibliités de réduction pour $t$

            \begin{description}
                \item[Cas 1]

                    \begin{prooftree}
                        \AxiomC{$w \implies w'$}
                        \AxiomC{$h \implies h'$}
                        \BinaryInfC{$t \implies u = (\lambda x. w') h'$}
                    \end{prooftree}

                    \begin{prooftree}
                        \AxiomC{$w \implies w''$}
                        \AxiomC{$h \implies h''$}
                        \BinaryInfC{$t \implies v = (\lambda x. w'') h''$}
                    \end{prooftree}

                    Alors on applique l'hypothèse de récurrence $w,w',w''$
                    $h,h',h''$. On peut alors conclure 
                    par pré-congruence.

                \item[Cas 2] 
                    On garde $u$ suivant la même dérivation, 
                    mais cette fois $v$ utilise la règle $4$.

                    \begin{prooftree}
                        \AxiomC{$w \implies w''$}
                        \AxiomC{$h \implies h''$}
                        \BinaryInfC{$t \implies v = w''[h''/x]$}
                    \end{prooftree}

                    Alors on applique l'hypothèse de récurrence sur $w,w',w''$
                    et $h,h',h''$. On peut conclure 
                    parce que $u \implies \hat{w}[\hat{h}/x]$
                    et via le lemme précédent on a bien 
                    $v \implies \hat{w}[\hat{h}/x]$.

                \item[Cas 3]
                    Cela se traite de la même manière, en utilisant 
                    le lemme précédent.
            \end{description}
    \end{description}
\end{proof}

\begin{theoreme}
    La $\to_\beta$ réduction est confluente.
\end{theoreme}

\begin{proof}
    On a $\implies = \to_\beta^*$ et donc $\to_\beta^*$ 
    est fortement confluente, ce qui veut exactement dire 
    que $\to_\beta$ est confluente.
\end{proof}

\subsection{Post requis}

Cela permet de définir la notion de calcul, les formes normales sont toutes 
égales et tout est bien dans le meilleur des mondes.
