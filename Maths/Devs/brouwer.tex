\begin{references}
    \item[Gonnord Tosel]
\end{references}

\subsection{Notations}

On note $B^n$ la boule unité fermée de $\mathbb{R}^n$
et $S^{n-1}$ la sphère associée. Le produit 
scalaire usuel est noté $\langle \cdot ~|~ \cdot \rangle$
et la mesure de Lebesgue est notée $\lambda$.

\subsection{Lemme de Milnor}

On fixe $K$ un compact de $\mathbb{R}^n$, 
$U$ un voisinage ouvert de $K$, $\Omega$ un ouvert 
tel que l'on ait $\overline{\Omega}$ compact et 

\begin{equation}
    K \subseteq \Omega \subseteq \overline{\Omega} \subseteq U
\end{equation}

On considère $v \in \mathcal{C}^1 (U, \mathbb{R}^n)$,
et on pose $v_t = id + t v$ pour $t \in \mathbb{R}$.

\begin{description}
    \item[Localement, $D v_t (x)$ est inversible] 

        En effet, pour tout $t$ l'application $v_t$ est différentiable 
        et $D v_t (x) = Id + t D v (x)$. Par ailleurs $Dv$ est continue,
        puisque $v$ est $\mathcal{C}^1$, et comme $\overline{\Omega}$
        est compact, il existe un $M > 0$ tel que 

        \begin{equation}
            \forall x \in \overline{\Omega}, \| D v(x) \| \leq M
        \end{equation}

        En posant $2M \alpha = 1$, on constate que 
        \begin{equation}
            \forall |t| < \alpha,
            \forall x \in \overline{\Omega},
            \| D v_t (x) - Id \| = \| t D v(x) \| \leq \frac{1}{2}
        \end{equation}

        Donc $D v_t (x)$ est inversible sous ces hypothèses,
        pour tout $t \in ] -\alpha, \alpha[$ et $x \in \overline{\Omega}$.

        Remarquons que comme $D v_t (x)$ est toujours inversible,
        le déterminant n'est jamais nul, et en particulier 
        il ne peut pas changer de signe quand $t$ varie pour un $x$ 
        donné (connexité de l'intervalle $] -\alpha, \alpha[$, 
        et continuité du tout en la variable $t$).

        Mieux comme $D v_0 (x) = Id$ on déduit que le déterminant 
        est toujours strictement positif.

    \item[Localement, $v_t$ est injective]

        Comme $v$ est $\mathcal{C}^1$ sur $U$ elle y est localement 
        Lipschitzienne via l'inégalité des accroissements finis. 
        Comme $\overline{\Omega}$ est compact, on déduit que 
        $v$ est en fait Lipschitzienne sur $\overline{\Omega}$,
        de constante de Lipschitz $k$.


        En posant $\beta = \frac{1}{2k}$,
        si $v_t (x) = v_t (y)$ avec $(x,y) \in \overline{\Omega}$, 
        
        \begin{equation}
            \forall |t|<\beta,
            \| x - y \| = \| v_t (x) - v_t (y) + t(v(y) - v(x)) \|
                        \leq |t| \| v(y) - v(x) \|
                        \leq \beta k \| x - y \| 
                        \leq \frac{\| x - y \|}{2}
        \end{equation}

        Donc $x = y$.
        Ainsi, la fonction $v_t$ restreinte à $\overline{\Omega}$
        est injective pour $|t| < \beta$.

    \item[Localement, $v_t$ est un $\mathcal{C}^1$-difféo]

        La fonction $v_t$ restreinte à $\overline{\Omega}$ 
        est donc un $\mathcal{C}^1$ difféomorphisme 
        de $\Omega$ sur $v_t(\Omega)$ pour tout $|t| < \min (\alpha, \beta)$.


    \item[Expression du volume de $K$]

        On a donc d'après le théorème de changement de variable,
        et parce que le déterminant est égal à sa valeur absolue~: 

        \begin{equation}
            \int_K \det (Dv_t (x)) dx 
            = 
            \int_{ v_t (K) } 
            1 dx 
            = 
            \lambda ( v_t (K))
        \end{equation}


        Comme le déterminant est une expression polynomiale 
        en $t$, l'intégrale sur $x$ reste un polynôme en $t$,
        et donc $\lambda (v_t (K))$ coincide avec un 
        polynôme pour $|t| < \min (\alpha, \beta)$.

\end{description}

\subsection{Application à la non-rétraction}

\begin{lemme}
    Il n'existe aucune fonction $f : B^n \to S^{n-1}$
    de classe $\mathcal{C}^1$ telle que pour tout $x \in S^{n-1}$
    $f(x) = x$.
\end{lemme}

Supposons par l'absurde qu'il existe une telle fonction $f$.
On construit $U$ un voisinage ouvert de $B^n$ dans $\mathbb{R}^n$,
$v = f - id$, et $\Omega$ vérifiant les hypothèses du lemme de Milnor.

\begin{remarque}
    Par définition, $\mathcal{C}^1$ veut dire qu'il existe un ouvert 
    autour de la boule sur lequel $f$ est définie et $\mathcal{C}^1$,
    il n'y a donc pas de problème.
\end{remarque}

\begin{description}
    \item[Application du lemme de Milnor]
        On possède un $\gamma > 0$ tel que pour tout $|t| < \gamma$
        $v_t$ est un $\mathcal{C}^1$ difféomorphisme de $\Omega$
        sur $v_t (\Omega)$
    \item[Montrons que $v_t ( B^n) = B^n$]
        Pour cela on écrit l'équation clef

        \begin{equation}
            v_t (x) = x + t v(x) = x + t (f(x) - x) = (1 - t) x + t f(x)
        \end{equation}


        On remarque alors que $v_t (S^{n-1}) = S^{n-1}$
        puisque $f$ fixe $S^{n-1}$.

        Reste à montrer que $v_t (B^n) = B^n$ (boules ouvertes).
        On constate déjà l'inclusion $\subseteq$ par la formule clef.

        Pour l'autre inclusion, on remarque déjà que l'image de 
        la boule ouverte est ouverte parce que c'est un $\mathcal{C}^1$-difféo.
        Montrons alors que l'image de la boule ouverte est aussi fermée 
        \emph{dans la boule ouverte}. 

        Soit $y_i = v_t (x_i)$ une suite d'éléments dans l'image
        convergeant vers une limite $y$ dans la boule ouverte. 
        Par compacité de $B^n$, la suite $x_i$ a une valeur d'adhérence $x$
        dans $B^n$. Par continuité de $v_t$ on a alors $v_t (x) = y$.
        Toutefois comme $y$ est dans la boule ouverte, $x$ ne peut pas 
        non-plus y être car ce sont des points fixés (et on aurait $x = y$).

        Ainsi, on a montrér que $v_t (B^n)$ (image de la boule \emph{ouverte})
        est bien un ouvert fermé de la boule ouverte, et par connexité
        c'est égal.

    \item[Expression du volume via $D f$]
        En utilisant le lemme de Milnor pour $|t| < \gamma$ on obtient
        \begin{equation}
            \int_{B^n} \det (D v_t (x)) dx 
            = \lambda (v_t (B^n)) = \lambda (B^n)
        \end{equation}

        Mais la fonction de gauche est une fonction 
        polynomiale en $t$ et la fonction de droite aussi. Elles 
        sont donc égales sur $\mathbb{R}$ entier, et en appliquant 
        cette égalité en $t = 1$

        \begin{equation}
            \int_{B^n} \det (D v_1 (x)) dx 
            = \lambda (B^n)
        \end{equation}

        Or $D v_1 (x) = Id + D v (x) = D f (x)$.

        \begin{equation}
            \int_{B^n} \det (D f (x)) dx 
            = \lambda (B^n)
        \end{equation}

    \item[Or $\det D f(x)$ est systématiquement nul] 
        car $x \mapsto \langle f(x) ~|~ f(x) \rangle$
        est constante égale à $1$ sur $B^n$. Et sa différentielle 
        est donc nulle sur $B^n$,
        or cette différentielle est 
        $2 \langle D f(x) \cdot h ~|~ f(x) \rangle$. 

        Ainsi, l'image de $D f(x)$ est contenue dans 
        l'orthogonal de $f(x)$, ce qui prouve 
        que $D f(x)$ n'est pas surjective 
        (par exemple on ne peut pas atteindre $f(x)$).
\end{description}

\subsection{Équivalence entre non-rétraction et Brouwer}

\begin{description}
    \item[Brouwer implique non-rétraction]

        Si $f : B^n \to S^{n-1}$ est une rétraction continue.
        Alors $-f$ est aussi une fonction continue, qui cette fois n'admet 
        \emph{aucun point fixe}, d'après le théorème de Brouwer c'est absurde.

    \item[Non rétraction implique Brouwer]
        On suppose $f$ de classe $\mathcal{C}^1$ pour simplifier
        et sans point fixe.
        On définit $g$ qui à tout $x$ associe le point d'intersection 
        entre $[f(x),x)$ et la sphère.

        \begin{center}
            \includegraphics{images/brouwer_app.pdf}
        \end{center}

        On en trouve une expression raisonnable (polynomiale en $f$)
        et donc on a construit une rétraction $\mathcal{C}^1$
        ce qui n'est pas possible.

        Plus précisément il existe une fonction $t \geq 0$
        telle que 

        \begin{equation}
            g(x) = x + t(x) \frac{x - f(x)}{\| x - f(x) \|}
        \end{equation}

        Or la relation $\|g(x) \|^2 = 1$ permet de déduire

        \begin{equation}
            t(x)^2 + \| x \|^2 + 2 \frac{\langle x ~|~ x - f(x)\rangle}{\| x -
            f(x) \|} t(x) = 1
        \end{equation}

        C'est un polynôme de degré $2$ en $t(x)$, son discriminant 
        est strictement positif, son expression est donc 
        une jolie racine carrée, ce qui rend le tout $\mathcal{C}^1$.

    \item[Généralisation au cas continu]
        Pour le cas continu, on utilise une approximation par des Polynômes 
        pour pouvoir conclure.
        
        Puisque $f - Id$ est continue sur le compact $B^n$
        il existe un $\varepsilon > 0$ tel que 
        $\| f - Id \| \geq 2 \varepsilon$.

        Par ailleurs, il existe un polynôme $Q$ tel que 
        $\| f - Q \| \leq \varepsilon$.
        
        En posant $P = \frac{Q}{1 + \varepsilon}$ on obtient

        \begin{enumerate}
            \item $P (B^n) \subseteq B^n$
            \item On en déduit donc $\| Q - P \| = \varepsilon \| P \| \leq
                \varepsilon$
            \item Ainsi $\| f - P \| < 2 \varepsilon$
            \item De plus $P(x) \neq x$ car $\| P(x) - x \| \geq \| f(x) - x \|
                - \| P(x) - f(x) \| > 0$
        \end{enumerate}
        
        On a donc bien, en partant d'une fonction sans point fixe
        continue, construit une fonction sans point fixe $\mathcal{C}^1$.

\end{description}

\subsection{Remarques}

\begin{description}
    \item[Calcul du volume de la boule unité]
        On montre que 
        \begin{equation}
            V_n = \frac{2 \pi^{n/2}}{n \Gamma(n/2)}
        \end{equation}

        \begin{proof}
            On montre ça par récurrence.
            C'est vrai pour les cas $1$ et $2$. 
            Par la suite un petit coup de Fubini permet de conclure.

            \begin{align}
                V_{n+1} 
                &= 
                \int_{\mathbb{R}^{n+1}} \chi_{\leq 1} x dx \\
                &= 
                \int_{\mathbb{R}}^{n+1} \chi_{\|y \| \leq \sqrt{1 - x^2}}  dx dy \\
                &= 
                \int_{-1}^1 V_n \times \left(\sqrt{1 - x^2}\right)^n dx  \\
                &= 
                V_n \times 
                \int_{-1}^{1} \left(\sqrt{1 - x^2}\right)^n dx  \\
                &= 
                2 \times V_n \times 
                \int_0^{1} \left(\sqrt{1 - x^2}\right)^n dx  \\
                &= 
                2 \times V_n \times 
                \int_0^{1} \left(\sqrt{1 - u}\right)^n u^{-1/2} \frac{du}{2}  \\
                &= 
                V_n \times 
                \int_0^{1} \left(1 - u\right)^{\frac{n}{2}} u^{-1/2} du  \\
                &= 
                V_n \times \beta \left( \frac{n+2}{2}, \frac{1}{2} \right) \\
                &= 
                V_n \times \frac{\Gamma\left( \frac{n+2}{2}\right) 
                \Gamma\left( \frac{1}{2} \right)}{\Gamma\left( \frac{n+2}{2} +
                \frac{1}{2} \right)} \\
                &= 
                V_n \sqrt{\pi} \frac{\Gamma\left( \frac{n+2}{2} \right)}{\Gamma\left( \frac{n+3}{2}\right)}
            \end{align}

            On en déduit la formule attendue par récurrence.
            On a utilisé la définition de la fonction $\beta$ et la propriété 
            qui la relie à $\Gamma$ (cf formule des compléments).

            \begin{equation}
                \beta (x,y) = \int_0^1 u^{x -1} (1 - u)^{y - 1} du
            \end{equation}

            \begin{equation}
                \beta (x,y) = \frac{\Gamma (x) \Gamma (y)}{\Gamma (x+y)}
            \end{equation}
        \end{proof}
\end{description}

\todo[inline]{Déterminant et volume en général}
\todo[inline]{Généralisation à Shauder}
\todo[inline]{Application à Cauchy-Peano-Arzela~: Chambert-Loir, Analyse 1, p 79}


\subsection{Théorèmes de point fixe dans la nature}

\begin{theoreme}[Knaster-Tarski]
    Soit $E$ un treillis complet et $f$ une fonction 
    croissante. Alors l'ensemble des points 
    fixes de $f$ est non vide est forme un treillis 
    complet.
\end{theoreme}

\begin{proof}
    Pour l'existence d'un plus petit point fixe seulement.

    Considérer l'ensemble $A = \{ x \in E ~|~ f(x) \leq x \}$,
    et $a = \inf A$ qui existe car $E$ est un treillis complet.
    
    On constate que $f(a) \leq a$ assez vite, et donc $a \in E$,
    par la suite $f(a) \in E$ puis $a \leq f(a)$ par définition 
    de l'inf. En particulier, cet inf donne le plus petit 
    point fixe de manière évidente.

    Remarque: l'ensemble $A$ est l'ensemble des \emph{pré}-points-fixes.
\end{proof}

\begin{remarque}
    C'est un exo classique, peut-être l'avez-vous déjà croisé 
    en considérant $E = [0,1]$ et une fonction croissante ...
\end{remarque}

\begin{remarque}
    Contrairement aux théorèmes de point fixe qui arrivent après, 
    Knaster-Tarski ne permet pas de l'obtenir "explicitement" 
    via une suite récurrente. En cela il se rapproche de Brouwer.

    Par exemple, considérons $\mathbb{N} \cup \{ \omega, \omega + 1 \}$
    qui est un treillis complet pour l'ordre usuel 
    et la fonction $f$ qui fait $f(n) = n+1$, $f(\omega) = \omega + 1$
    et $f(\omega + 1) = \omega + 1$. Alors 
    la limite de la suite $f^k (n)$ est $\omega$ qui n'est pas un point 
    fixe !

    On s'en sort en faisant des inductions transfinies, mais 
    ça perd un peu l'esprit "constructif".
\end{remarque}

\begin{theoreme}[Point fixe de Scott]
    Soit $E$ un ensemble avec un plus petit élément $\bot$ 
    où toute chaine ordonnée possède un 
    supremum et $f$ une fonction croissante qui préserve les supremums 
    de chaines ordonnées.

    Alors $f$ possède un plus petit point fixe, qui est obtenu comme 
    \begin{equation}
        x = \bigsqcup_i f^i (\bot)
    \end{equation}
\end{theoreme}

\begin{remarque}
    Ce théorème est crucial en informatique théorique. 
    Il faut voir $E$ comme un ordre partiel pour la relation "est plus précis
    que". Ainsi, l'élément $\bot$ représente l'absence d'information 
    sur le système étudié, et une chaine croissante 
    représente une observation de plus en plus précise sur un même élément.

    On peut associer à la structure d'ordre de $E$ une structure 
    topologique (la topologie de Scott) dans laquelle les ouverts 
    sont précisément les ensembles $O$ de $E$ qui sont clos par supremum,
    et fermés par le bas, au sens suivant~: si une suite possède un supremum
    dans $O$, alors il existe un élément de cette suite dans $O$. Cela 
    revient à dire que toute propriété observable (Observable = O = Ouvert) 
    est effectivement ... Observable (en temps fini).

    La propriété demandée sur $f$ est alors tout simplement la continuité 
    pour cette topologie. 

    Un exemple concret est la sémantique d'une boucle "while", 
    qui est atteinte comme un plus petit point fixe.
\end{remarque}


\todo[inline]{Ajouter le théorème de point fixe dans un compact}
\begin{theoreme}[Point fixe dans un compact]
\end{theoreme}

\todo[inline]{Ajouter le théorème de point fixe de Picard-Banach}
\begin{theoreme}[Picard-Banach]
\end{theoreme}
