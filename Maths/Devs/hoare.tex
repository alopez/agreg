\begin{references}
    \item[Glynn Winskel]
\end{references}

\begin{definition}
    On considère le langage logique de l'arithmétique
    au premier ordre. On différencie dans les formules 
    deux types de variables, les variables \emph{de programme}
    et les variables \emph{logiques}. L'interprétation 
    d'une formule est donc l'interprétation standard dans 
    $\mathbb{N}$ qui nécessite l'environment $\sigma$ 
    du programme et une interprétation $I$ des variables 
    logiques. On prend comme convention que 
    $\bot \models^I A$ pour tout $I$ et tout $A$.

    Classiquement, on écrit $\models A$ pour 
    dire que pour tout $\sigma,I$ $\sigma \models^I A$.
\end{definition}

\begin{definition}
    On écrit $\models \Hoare{P}{c}{Q}$ pour 
    signifier que $\forall \sigma, \forall I, 
    \sigma \models^I P \implies \sem{c}{\sigma} \models^I Q$.
\end{definition}

\begin{definition}[Logique de Hoare]
    ~\\~
    \begin{multicols}{2}
    \begin{prooftree}
        \AxiomC{}
        \UnaryInfC{$\Hoare{P}{\IMPSKIP}{P}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{}
        \UnaryInfC{$\Hoare{P[x \mapsto e]}{\lassign{x}{e}}{P}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Hoare{P}{c}{Q'}$}
        \AxiomC{$\Hoare{Q'}{c'}{Q}$}
        \BinaryInfC{$\Hoare{P}{c ; c'}{Q}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Hoare{P \wedge e \neq 0}{c}{Q}$}
        \AxiomC{$\Hoare{P \wedge e =    0}{c'}{Q}$}
        \BinaryInfC{$\Hoare{P}{\lif{e}{c}{c'}}{Q}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Hoare{P \wedge e \neq 0}{c}{P}$}
        \UnaryInfC{$\Hoare{P}{\lwhile{e}{c}}{P \wedge e = 0}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\models P \implies P'$}
        \AxiomC{$\Hoare{P'}{c}{Q'}$}
        \AxiomC{$\models Q' \implies Q$}
        \TrinaryInfC{$\Hoare{P}{c}{Q}$}
    \end{prooftree}
    \end{multicols}
\end{definition}


\begin{center}
\fbox{\begin{minipage}{0.8\linewidth}
\begin{theoreme}
    Pour toute formule $A$ plus faible 
    précondition de $c$ et $B$, le séquent 
    $\vdash \Hoare{A}{c}{B}$
    est démontrable.
\end{theoreme}
\end{minipage}}
\end{center}

\begin{lemme}
    $ \rho[ x \mapsto \sem{e}{\rho}] \models^I B$
    si et seulement si 
    $\rho \models^I B[ x \mapsto e]$
\end{lemme}

\begin{proof}
    On procède par récurrence sur le programme $c$.

    Soit $A$ une formule telle que 
    $\rho \models^I A$ si et seulement si 
    $\sem{c}{\rho} \models^I B$, montrons 
    que $\Hoare{A}{c}{B}$ est démontrable.
    \begin{description}
\item[Skip] 

    On a $\sem{c}{\rho} = \rho$,
    donc $\rho \models^I A$ si et seulemnt 
    si $\rho \models^I B$, ce qui prouve 
    $\models A \iff B$.

    On peut donc conclure via la règle skip
    avec $B$ puis la règle d'affaiblissement.

    \begin{prooftree}
        \AxiomC{$ \models A \implies B$}
        \AxiomC{}
        \UnaryInfC{$ \Hoare{B}{\IMPSKIP}{B}$}
        \AxiomC{$ \models B \implies B$}
        \TrinaryInfC{$ \Hoare{A}{\IMPSKIP}{B}$}
    \end{prooftree}

\item[Affectation]
    
    On sait que $\sem{c}{\rho} = \rho[x \mapsto \sem{e}{\rho}]$.
    Ainsi $\rho \models^I A$ si et seulement si 
          $\rho[ x \mapsto \sem{e}{\rho}] \models^I B$,
    en utilisant le lemme, on déduit que $\rho \models^I A$
    si et seulement si $\rho \models^I B[x \mapsto e]$

    Cela signifique que $\models A \iff B[x \mapsto e]$,
    et donc on peut faire la preuve 

    \begin{prooftree}
        \AxiomC{$ \models A \implies B[x \mapsto e]$}
        \AxiomC{}
        \UnaryInfC{$ \Hoare{B[x \mapsto e]}{\lassign{x}{e}}{B}$}
        \AxiomC{$ \models B \implies B $}
        \TrinaryInfC{$ \Hoare{A}{\lassign{x}{e}}{B}$}
    \end{prooftree}

\item[Séquence]
    
    On introduit $C$ une plus faible précondition 
    pour $(c_2, B)$, on a donc la dérivation $\vdash \Hoare{C}{c_2}{B}$
    par hypothèse de récurrence.

    On sait que $\sem{c}{\rho} = \sem{c_2}{\sem{c_1}{\rho}}$,
    si $\sem{c_1}{\rho} \neq \bot$ et $\bot$ sinon.

    Dans le premier cas on a bien
    $\rho \models^I A$ si et seulement si 
    $\sem{c_2}{\sem{c_1}{\rho}} \models^I B$
    et par construction cela veut dire que
    $\sem{c_1}{\rho} \models^I C$.

    Dans le second cas on a $\rho \models^I A$ 
    si et seulement si $\bot \models^I B$,
    si et seulement si $\bot \models^I C$,
    si et seulement si $\sem{c_1}{\rho} \models^I C$.

    On conclut donc que $A$ est une plus faible 
    précondition de $(c_1, C)$.

    Alors par hypothèse de récurrence on 
    a $\vdash \Hoare{A}{c_1}{C}$.

    On peut donc conclure via la règle de séquence.

    \fbox{De manière générale 
        $\sem{c_1;c_2}{\rho} \models^I A$
        si et seulement si 
        $\sem{c_2}{\sem{c_1}{\rho}} \models^I A$}

\item[Condition]

    \textbf{Ne pas faire la condition, c'est pas intéressant}

\item[Boucle]

    On va montrer que 
    $\models \Hoare{A \wedge e \neq 0}{c_1}{A}$,
    puis que $A \wedge e = 0$
    implique $B$.

    En effet, 
    $\rho \models^I A \wedge e \neq 0$
    si et seulement si $\rho \models^I A$
    et $\sem{e}{\rho} \neq 0$, 
    si et seulement si $\sem{c}{\rho} \models^I B$
    et $\sem{e}{\rho} \neq 0$
    et $\sem{c}{\rho} = \sem{c_1;c}{\rho}$.
    Si et seulement si 
    $\sem{e}{\rho} \neq 0$
    et $\sem{c}{\sem{c_1}{\rho}} \models^I B$.
    Si et seulement si 
    $\sem{c_1}{\rho} \models^I A$ 
    et $\sem{e}{\rho} \neq 0$.

    On peut appliquer l'hypothèse de récurrence à une 
    plus faible précondition $C$ de $A$ pour $c_1$, 
    et utiliser $\models A \wedge e \neq 0 \implies C$
    pour $\vdash \Hoare{A \wedge e \neq 0}{c_1}{A}$.

    De plus,
    si $\rho \models^I A \wedge e = 0$ alors 
    $\rho \models^I B \wedge e = 0$ par 
    la sémantique du while.

    On peut donc utiliser cela pour 
    déduire $\vdash \Hoare{A}{c}{B}$.
\end{description}



\end{proof}
