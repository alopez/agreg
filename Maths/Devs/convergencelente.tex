\begin{references}
    \item[FGN Analyse 1] page 99
\end{references}


\subsection{Prérequis}

\begin{enumerate}
    \item Savoir faire un DL d'une fonction classique

        \begin{equation}
            (1 + x)^\alpha = 1 + \alpha x + \frac{\alpha(\alpha - 1)}{2} x^2 +
            \dots 
        \end{equation}
    
        \begin{equation}
            \sin x = x - \frac{x^3}{3} + \frac{x^5}{120} - \frac{x^7}{7!} \dots
        \end{equation}

    \item Sommation des relations de comparaison 
    \item Théorème de Cesaro 
\end{enumerate}

\subsection{Développement}

On fixe $c > 0$, on note $I = [0,c]$ et 
on considère $f : I \to I$ une fonction 
continue. On suppose de plus qu'au voisinage 
de zéro il existe un $\alpha > 1$ et $a > 0$ tels que

\begin{equation}
    f(x) = x - a x^\alpha + o \left(x^\alpha\right)
\end{equation}

On pose $u_0 \in I$ et $u_{n+1} = f(u_n)$.

\begin{description}
    \item[Si $u_0$ est assez petit, la suite $u_n$ converge vers zéro.]
        En effet, on peut passer à la limite dans le développement asymptotique 
        par continuité de $f$ pour trouver 

        \begin{equation}
            f(0) = 0
        \end{equation}

        Par la suite, on remarque que $f(x) - x$ est strictement négatif sur un 
        voisinage épointé de zéro, car 

        \begin{equation}
            f(x) - x = - ax^\alpha + o\left( x^\alpha\right)
        \end{equation}

        Ainsi, il existe un $\eta > 0$ tel que 

        \begin{equation}
            \forall 0 < x < \eta, f(x) < x
        \end{equation}

        La suite $u_n$ est alors décroissante si $u_0 < \eta$. 
        Elle est de plus minorée par $0$, elle converge donc.
        Par continuité de $f$ elle converge vers un point fixe,
        qui est nécessairement $0$~: c'est le seul point fixe de 
        $f$ sur $[0, \eta[$ (puisque sinon $f(x) < x$).

    \item[Déterminons un équivalent de $u_n$]

        On essaie de savoir à quelle vitesse décroit $u_n$, pour cela 
        on remarque que 

        \begin{equation}
            u_{n+1} - u_n = - a u_n^\alpha + o \left(u_n^\alpha\right)
        \end{equation}

        Cette décroissance est donc d'autant plus lente que $u_n$ devient 
        petit car $a > 0$ et $\alpha > 1$.

        Pour se ramener à un pas presque constant on décide de renormaliser 
        l'échelle~: on pose $v_n = u_n^\beta$, et on recherche un $\beta$
        tel que $v_{n+1} - v_n$ soit constant.


        \begin{align}
            v_{n+1} - v_n &= \left(u_n - a u_n^\alpha + o
            \left(u_n^\alpha\right)\right)^\beta - u_n^\beta \\
            &= u_n^\beta \left( \left(1 - a u_n^{\alpha - 1} + o \left(u_n^{\alpha -
        1}\right)\right) - 1 \right) \\
            &= u_n^\beta \left( -a \beta u_n^{\alpha - 1} + o \left(u_n^{\alpha -
            1}\right)\right) \\
            &\sim - a \beta u_n^{\alpha + \beta - 1} 
        \end{align}

        On considère donc $\beta = 1 - \alpha$, ce qui était plus ou moins
        logique, vu que la décroissance était à la puissance $\alpha$.

        On a alors~:

        \begin{equation}
            v_{n+1} - v_n \longrightarrow a (\alpha - 1) > 0
        \end{equation}

        On utilise le théorème de sommation des équivalents positifs 
        (ou le théorème de Cesaro) pour déduire 

        \begin{equation}
            \sum_{k < n} v_{k+1} - v_k \sim n a(\alpha - 1)
        \end{equation}

        Ce qui montre alors que

        \begin{equation}
            u_n^\beta - u_0^\beta \sim n a (\alpha - 1)
        \end{equation}

        Comme les suites tendent vers $+\infty$, il est clair que le terme 
        constant $u_0^\beta$ ne joue pas, et donc 

        \begin{equation}
                u_n^\beta \sim n a (\alpha - 1)
        \end{equation}

        Cela montre alors 

        \begin{equation}
            \boxed{
                u_n \sim (n a (\alpha - 1))^{\frac{1}{1 - \alpha}}
            }
        \end{equation}
    \item[Considérons un cas particulier]

        On se place sur $I = \left[0, \frac{\pi}{2}\right]$ qui est stable 
        par la fonction $\sin$, le développement en zéro de cette fonction 
        est 

        \begin{equation}
            \sin x = x - \frac{x^3}{6} + o\left(x^3\right)
        \end{equation}

        C'est-à-dire $a = \frac{1}{6} > 0$ et $\alpha = 3 > 1$.
        De plus, si $x > 0$, $\sin x < x$, donc la suite converge pour 
        $u_0 \in I$ vers zéro.

        Enfin, en appliquant notre théorème~:

        \begin{equation}
            u_n \sim \left (n \frac{1}{6} \left( 3 - 1 \right)
            \right)^{\frac{1}{1 - 3}}
            = 
            \sqrt{\left( \frac{3}{n} \right)}
        \end{equation}

    \item[Si on veut un terme de plus ...]
    
        On peut itérer la méthode !
        On écrit alors
        \begin{equation}
            \sin x = x - \frac{x^3}{6} + \frac{x^5}{120} + o (x^5)
        \end{equation}

        Et on considère de nouveau 
        \begin{equation}
            \frac{1}{u_{n+1}^2} - \frac{1}{u_n^2}
        \end{equation}

        On trouve alors~:

        \begin{equation}
            \sin u_n = u_n \left( 1 - \frac{u_n^2}{6} + \frac{u_n^4}{120} +
            o(u_n^4) \right)
        \end{equation}
        
        D'où  après calcul~:
        
        \begin{align}
            \frac{1}{ u_{n+1}^2} 
            =
            \frac{1}{(\sin u_n)^2}
            &= 
            \frac{1}{ u_n^2} \frac{1}{ \left( 1 - \frac{u_n^2}{6} + \frac{u_n^4}{120} +
            o(u_n^4) \right)^2} \\
            &=
            \frac{1}{ u_n^2} \left( 
                1 - 2 \left(\frac{-u_n^2}{6} + \frac{u_n^4}{120}\right)
                  + 3 \left(\frac{-u_n^2}{6} + \frac{u_n^4}{120}\right)^2
                  + o(u_n^4)
            \right) \\
            &= 
            \frac{1}{u_n^2} 
            \left(
                1 + \frac{u_n^2}{3} + \frac{u_n^4}{15} + o\left(u_n^4\right)
            \right)
        \end{align}

        On déduit alors directement 
        \begin{equation}
            u_{n+1}^\beta - u_n^\beta - \frac{1}{3} \sim \frac{u_n^2}{15} 
        \end{equation}
        
        Et en injectant l'équivalent de $u_n$ on déduit 

        \begin{equation}
            u_{n+1}^\beta - u_n^\beta - \frac{1}{3} \sim \frac{1}{5 n} 
        \end{equation}

        Ce qui en utilisant le théorème de sommation des équivalents 
        (positifs) donne alors (en utiliant $H_n$)

        \begin{equation}
            u_n^\beta - \frac{n}{3} \sim \frac{1}{5} \log n 
        \end{equation}

        De là on déduit 

        \begin{align}
            u_n &= \left( \frac{n}{3} + \frac{\log n}{5} + o(\log n) \right)^{-\frac{1}{2}}
            \\
            &= \sqrt{\frac{3}{n}} \left( 1 + \frac{3 \log n}{5 n} + o\left(
            \frac{\log n}{n} \right) \right)^{-\frac{1}{2}} \\
            &\sim 
            \sqrt{\frac{3}{n}} - \sqrt{\frac{3}{n}} \frac{1}{2} \frac{3 \log n}{5 n} + o\left(
            \frac{\log n}{n\sqrt{n}} \right) \\
        \end{align}
\end{description}

\subsection{Remarques}

\begin{description}
    \item[La vision "accroissements finis" du FGN]

        Quand on écrit $u_{n+1} - u_n \sim -a u_n^\alpha$
        on écrit 

        \begin{equation}
            \left(u_{n+1} - u_n\right) u_n^{-\alpha} \sim -a
        \end{equation}

        Or $g' : x \mapsto x^{-\alpha}$ est la dérivée à une constante 
        près de $g : x \mapsto x^{1 - \alpha}$. On a donc approximativement
        en utilisant le théorème des accroissements finis~:

        \begin{equation}
            -a \sim \left(u_{n+1} - u_n\right) g'\left(u_n\right)
            \approx
            g\left(u_{n+1}\right) - g \left(u_n\right)
        \end{equation}

        Il devient alors naturel d'étudier $g\left(u_n\right)$...

    \item[Il est beaucoup plus simple de prendre comme exemple]
        $f(x) = \log(1 +x)$ puisque les développements font intervenir 
        beaucoup moins de fractions ... 
\end{description}
