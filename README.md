# AGRÉGATION MATHÉMATIQUES OPTION INFORMATIQUE 

1. Recueil de développements d'info et de maths
2. Les polycopiés et liens vers des sites web intéressants 
3. Construction d'une bibliographie générale 
4. Organisation claire des différentes parties du programme 
5. Système automatisé de build pour les références croisées 


## POUR L'INSTANT 

1. Seulement quelques développements 
2. Le tout n'est pas très propre 
3. Pas d'uniformité dans les fichiers `.tex` (classe générale)
4. Présence d'un `Makefile` tout pourri qui fait le job 

## À Faire 

1. Un découpage propre des différentes parties du dépôt
2. Une bibliographie séparée avec `bibtex`
3. Un système de compilation un peu moins con 
