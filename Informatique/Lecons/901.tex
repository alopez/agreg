\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{complexity}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs, wasysym}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{hyperref}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows,matrix}
\usepackage{pdfpages}
\usepackage{epigraph}
\usepackage{palatino}
\usepackage{url}
\usepackage{faktor}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{todonotes}



%%%% Format

\setlength{\columnsep}{2cm}

\newcommand{\lname}{L901 — Structures de données}
\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}



\newcommand{\Lin}[1]{\mathcal{L}\left(#1\right)}
\newcommand{\Comm}[1]{\mathcal{C}\left(#1\right)}
\newcommand{\GLin}[1]{\operatorname{GL}\left(#1\right)}
\newcommand{\Image}{\operatorname{Im}}
\newcommand{\Ker}{\operatorname{Ker}}
\newcommand{\trace}{\operatorname{tr}}
\newcommand{\cycl}[2]{\left\langle #1 \right\rangle_{#2}}
\newcommand{\PAL}{\ComplexityFont{PAL}}
\newcommand{\CONNECTED}{\ComplexityFont{CONNECTED}}
\newcommand{\TRIANGLEFREE}{\ComplexityFont{TRIANGLEFREE}}
\newcommand{\CTLSAT}{\ComplexityFont{CTLSAT}}
\newcommand{\HORNSAT}{\ComplexityFont{HORNSAT}}
\newcommand{\PATH}{\ComplexityFont{PATH}}
\newcommand{\PRIME}{\ComplexityFont{PRIME}}
\newcommand{\TQBF}{\ComplexityFont{TQBF}}



\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}
\newenvironment{references}{\textbf{Références}\begin{quote}\begin{compactitem}}{\end{compactitem}\end{quote}}

\newcommand{\Dev}[1]{$\RHD$ \textbf{DEV} #1 $\LHD$}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{application}
\newtheorem{application}[definition]{Application}

\theoremstyle{corollaire}
\newtheorem{corollaire}[definition]{Corollaire}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}
 
\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}


%%%% Corps du fichier
\title{\lname}
\date{}


\begin{document}

\begin{center}
    \fbox{\begin{minipage}{0.7\linewidth}
            \centering
            \hrule
            \vspace{3pt}
            \huge
            \lname 
            \vspace{3pt}
            \hrule
    \end{minipage}}
\end{center}


\section{Modèle(s) de calcul et résultats élémentaires}

\subsection{Rappels sur les machines de Turing}

On admet connue la définition d'ume machine de Turing 
déterministe, non-déterministe, alternante.
Par convention, les machines possèdent une bande d'entrée 
en lecture seule, une bande de sortie en écriture seule,
et $k$ bandes de travail. 

\begin{definition}[Complexité d'un run]
    Soit $M$ une machine de Turing, 
    on dit que $M$ calcule en temps $T$
    si et seulement si pour toute entrée $x$
    l'exécution de $\langle M, x \rangle$
    se fait en moins de  $T(|x|) + C$ transitions,
    pour toute branche. 

	On dit que $M$ calcule en espace $S$
	si et seulement si pour toute entrée $x$
	la somme des tailles des bandes de travail 
	est toujours inférieure à $S(|x|) + C$ durant 
    l'exécution de $\langle M, x \rangle$, quelque 
    soit la branche.
\end{definition}

\begin{propriete}[Simulation]
    Si $M$ est une machine de Turing à $k$ bandes 
    qui calcule en temps $T$ et en espace $S$, alors 
    il existe une machine de Turing à une bande 
    \emph{avec un nouvel alphabet de travail} qui calcule en temps 
    $O(T^2)$ et en espace $O(S)$.

    De plus la nouvelle machine est déterministe si la précédente l'était.
\end{propriete}

\begin{theoreme}[Simulation universelle]
    Il existe une machine de Turing $U$ telle que 
    pour toute paire $(x,i)$ où $i$ est un code 
    pour la machine de Turing $M_i$, on ait

    \begin{equation*}
        U(x,i) = M_i (x)
    \end{equation*}
    
    De plus si $M_i$ calcule en $T$ étapes sur l'entrée 
    $x$, alors $U(x,i)$ calcule en $C_i T \log T$ étapes.
    Avec $C_i$ un nombre indépendant de l'entrée $x$ 
    et du temps $T$.

    De plus la nouvelle machine est déterministe si la précédente l'était.
\end{theoreme}

\begin{definition}[\PAL]
    Le langage $\PAL$ des palindromes 
    sur $\Sigma = \{0,1\}$ est l'ensemble des $u \in \Sigma^*$
    tel que $\overline{u} = u$.
\end{definition}

\begin{propriete}[Gain des deux bandes]
    On a les propriétés suivantes sur le langage $\PAL$
    \begin{compactenum}[(i)]
    \item Il existe une machine déterministe à deux bandes
        $M$ qui reconnaît $\PAL$ en temps $O(n)$.
    \item Toute machine déterministe à une bande qui reconnaît 
        $\PAL$ calcule en temps $\Omega (n^2)$.
    \end{compactenum}

    Cette propriété reste vraie pour les machines non-déterministes.
\end{propriete}

\begin{definition}[Fonction propre]
    On distingue deux types de fonctions propres
    \begin{center}
        \begin{minipage}{0.9\linewidth}
            \begin{description}
                \item[Fonction propre en temps]
                    Une fonction $T$ est propre en temps 
                    si et seulement si \begin{inparaenum}[(a)]
                    \item $T(n) \geq n$
                    \item il existe une machine qui calcule 
                        la fonction $x \mapsto [T(|x|)]_2$
                        en temps $O (T(|x|))$
                    \item $T$ est croissante 
                    \end{inparaenum}
                \item[Fonction propre en espace]
                    Une fonction $T$ est propre en temps 
                    si et seulement si \begin{inparaenum}[(a)]
                    \item $T(n) \geq \log n$
                    \item il existe une machine qui calcule 
                        la fonction $x \mapsto [T(|x|)]_2$
                        en espace $O (T(|x|))$
                    \item $T$ est croissante 
                    \end{inparaenum}
            \end{description}
        \end{minipage}
    \end{center}
\end{definition}

\begin{propriete}[Speedup linéaire]
    Soit $M$ une machine qui \emph{décide} en temps $T$
    et en espace $S$. Quitte à changer l'alphabet 
    des bandes de travail, on peut l'adapter pour 
    qu'elle fonctionne en temps inférieur à $n + C T(n)$ 
    et en espace inférieur à $C S (n)$, quelque soit $0 < C < 1$.
\end{propriete}


\begin{corollaire}[Robustesse]
    On va donc définir les classes de complexité à une 
    constante multiplicative près, et ignorer 
    les conditions sur le nombre de bandes ou l'alphabet 
    d'entrée dans une grande partie des cas.
\end{corollaire}

\subsection{Classes de complexité en temps}

\begin{definition}[Complexité temporelle]
	Un langage $L$ est dans $\DTIME (T)$ 
    si et seulement s'il existe une machine 
    de Turing déterministe $M$ et une constante $c > 0$ 
    vérifiant~:
    \begin{inparaenum}[(i)]
    \item $\mathcal{L} (M) = L$
    \item $M$ calcule en temps $c \cdot T$
    \end{inparaenum}
\end{definition}

\begin{remarque}
    Si $f \leq g$ alors $\DTIME (f) \subseteq \DTIME (g)$
\end{remarque}

\begin{propriete}[Inclusion stricte]
    Si $f$ et $g$ sont des fonctions propres en temps
    telles que $f(n)\log f(n) = o(g(n))$
    alors 
    \begin{equation*}
        \DTIME (f) \subsetneq \DTIME (g)
    \end{equation*}
\end{propriete}


\begin{definition}[Classes de complexité usuelles]
    \begin{align*}
        \P   &= \bigcup_{c \in \mathbb{N}} \DTIME (n^c) \\
        \EXP &= \bigcup_{c \in \mathbb{N}} \DTIME (\exp (n^c)) \\
    \end{align*}
\end{definition}

\begin{remarque}
    $\P \subsetneq \EXP$
\end{remarque}

\begin{exemple}
    $\PAL \in \P$, $\CONNECTED \in \P$, $\TRIANGLEFREE \in \P$
\end{exemple}


\begin{theoreme}[AKS, Admis]
    $\PRIME \in \P$
\end{theoreme}

\begin{exemple}
    $\CTLSAT \in \EXP$
\end{exemple}

\subsection{Classes de complexité en espace}

\begin{definition}[Complexité spatiale]
    Un langage $L$ est dans $\DSPACE (S)$ si et seulement 
    s'il existe une machine de Turing déterministe $M$
    et une constante $c > 0$ vérifiant~:
    \begin{inparaenum}[(i)]
    \item $\mathcal{L}(M) = L$
    \item $M$ calcule en espace $c \cdot S$
    \item $M$ termine sur toute entrée 
    \end{inparaenum}
\end{definition}

\begin{remarque}
    Si $f \leq g$ alors $\DSPACE (f) \subseteq \DSPACE (g)$
\end{remarque}

\begin{propriete}[Inclusion stricte]
    Si $f$ et $g$ sont des fonctions propres en espace 
    telles que $f = o (g)$ alors 
    \begin{equation*}
        \DSPACE (f) \subsetneq \DSPACE (g)
    \end{equation*}
\end{propriete}

\begin{exemple}
    \begin{align*}
        \ComplexityFont{L} &= \DSPACE (\log) \\
        \PSPACE &= \bigcup_{c \in \mathbb{N}} \DSPACE (n^c)
    \end{align*}
\end{exemple}

\begin{exemple}
    Les graphes des opérations arithmétiques 
    élémentaires sont dans $\ComplexityFont{L}$.
\end{exemple}

\begin{exemple}
    Trouver s'il existe une stratégie gagnante 
    pour un jeu est souvent dans $\PSPACE$.
    C'est le cas du jeu de la géographie par exemple.
    Le problème de l'universalité d'un automate non-déterministe 
    est dans $\PSPACE$.
\end{exemple}



\subsection{Les conséquences du non-déterminisme}

\begin{definition}[Complexité]
    Les définitions de $\DTIME$ et $\DSPACE$ 
    s'adaptent aux machines non-déterministes
    en substituant textuellement "déterministe"
    par "non-déterministe".
\end{definition}

\begin{propriete}[Inclusion stricte]
    Si $f$ et $g$ sont des fonctions propres 
    telles que $f(n+1) = o(g(n))$
    alors 
    \begin{equation*}
        \NTIME (f) \subsetneq \NTIME (g)
    \end{equation*}
\end{propriete}

\begin{exemple}
    On peut ainsi définir la classe $\NP$,
    contenant le problème $\ComplexityFont{ISOMORPHISM}$, 
    qui à priori n'est pas dans $\P$.
    Cette classe contient aussi beaucoup de problèmes 
    connus~: le voyageur de commerce, cycle hamiltonien,
    ou la satisfiabilité d'une formule en CNF.
\end{exemple}

\begin{exemple}
    On peut de même introduire les variantes 
    de $\L$ et $\PSPACE$, respectivement $\NL$ 
    et $\NPSPACE$. L'exemple typique de problème dans
    $\NL$ est $\PATH$
\end{exemple}

\begin{definition}[Complémentaire]
    On note $\co X = \{ \overline{L} ~|~ L \in X \}$. 
    Les classes déterministes sont stables par 
    $\co$.
\end{definition}

\begin{remarque}
    On confond souvent $\overline{L}$ avec $\overline{L} \cap V$
    où $V$ est l'ensemble des codages \emph{valides} pour 
    le problème $L$.
\end{remarque}


\begin{exemple}
    $\PRIME \in \co\NP$ de manière évidente. 
    On peut montrer que $\PRIME \in \NP$ avec 
    un peu de travail
\end{exemple}

\begin{propriete}[Inclusions]
    \textbf{TODO: c'est plutôt dans la partie graphe de 
    configuration ?}
    Pour toute fonction propre en espace $f$ 
    \begin{equation*}
        \DTIME (f) \subseteq \SPACE (f)
        \subseteq \NSPACE (f) \subseteq \DTIME\left( 2^{O (f)}\right)
    \end{equation*}
\end{propriete}

\begin{exemple}
    \begin{equation*}
        \P \subseteq \PSPACE \subseteq \NPSPACE \subseteq \EXP
    \end{equation*}
\end{exemple}


\section{De la complétude}

\subsection{Notion de réduction}

\begin{definition}[Réduction dans $\P$]
    Soient $A$ et $B$ deux problèmes dans $\P$,
    on dit que $A$ se réduit à $B$ s'il existe 
    une fonction $f$ qui transforme une instance $x$ 
    du problème $A$ en une instance $y$ du problème $B$ 
    en espace logarithmique, telle que $x \in A \iff y \in B$.
\end{definition}

\begin{remarque}
    On peut adapter cette notion à la réduction dans 
    une classe par rapport à une autre classe. Par exemple 
    les réductions dans $\NP$ par rapport à $\P$.
\end{remarque}

\begin{remarque}[De la nécessité d'avoir une complexité inférieure]
    Tout problème dans $\P$ se réduit à tout problème dans $\P$
    via des réductions polynômiales
\end{remarque}

\begin{remarque}
    $\ComplexityFont{L}$ est stable par composition, faire des réductions 
    en espace logarithmique a donc un sens.
\end{remarque}

\begin{definition}[Complétude]
    Un langage $L$ est $\P$-complet s'il est 
    dans $\P$ et si tout problème de $\P$ peut s'y réduire.
    Cette définition s'adapte pour les autres classes.
\end{definition}

\begin{theoreme}
    Le langage suivant est $\P$-complet
    \begin{equation*}
        L = \{ (M,x,1^t) ~|~ M \text{ déterministe }, \langle M, x \rangle 
        \text{ accepte en temps } \leq t \}
    \end{equation*}
\end{theoreme}

\begin{theoreme}
    Le langage suivant est $\NP$-complet
    \begin{equation*}
        L = \{ (M,x,1^t) ~|~ \langle M, x \rangle 
        \text{ accepte en temps } \leq t \}
    \end{equation*}
\end{theoreme}

\begin{remarque}
    Ces théorèmes s'adaptent pour $\PSPACE$,
    $\EXP$ ...
\end{remarque}

\begin{theoreme}[Réduction et via padding]
    Si $\P = \NP$ alors $\EXP = \NEXP$ 
\end{theoreme}

\begin{theoreme}[Baker, Gill, Solovay]
    Il existe un oracle $B$ tel que 
    $\P^B \neq \NP^B$
\end{theoreme}

\subsection{Autour du graphe des configurations}

\begin{theoreme}
    $\PATH$ est $\NL$-complet
\end{theoreme}

\begin{remarque}
    Si on se limite aux graphes acycliques 
    le problème reste $\NL$-complet
\end{remarque}

\begin{theoreme}[Savitch]
        $\PATH \in \DSPACE (\log^2 )$
\end{theoreme}

\begin{corollaire}
    Pour toute fonction propre en espace $S$ 
    $\NSPACE (S) \subseteq \DSPACE (S^2)$
    et en particulier $\NPSPACE = \PSPACE$
\end{corollaire}

\begin{theoreme}[Immerman-Szelepcsényi]
        $\overline{\PATH} \in \NL$
\end{theoreme}

\begin{corollaire}
    $\ComplexityFont{2SAT}$ est $\NL$-complet
\end{corollaire}

\begin{corollaire}
    Pour toute fonction propre en espace $S$,
    $\NSPACE (S) = \co \NSPACE (S)$
\end{corollaire}

\subsection{L'épiphanie logique}

\begin{center}
    \begin{tabular}{c||c|c|c|c|c}
        \textbf{Classe} & \NL & \P & \NP & \PSPACE & \EXP 
        \\ \hline 
        \textbf{Problème Complet} & 
            \ComplexityFont{2SAT} & 
            \HORNSAT & 
            \SAT & 
            \ComplexityFont{QBF} &
            \CTLSAT
    \end{tabular}
\end{center}

\begin{remarque}
    Sur une machine RAM, $\HORNSAT$ se résout 
    en temps linéaire.
\end{remarque}

\begin{theoreme}
    S'il existe un langage $\NP$-complet 
    sur un alphabet unaire alors $\P = \NP$. 
    C'est une conséquence du fait que $\SAT$
    est $\NP$-complet.
\end{theoreme}


\begin{remarque}
    On peut adapter cette preuve pour 
    déduire que si tout langage unaire 
    de $\NP$ est dans $\P$, alors $\EXP = \NEXP$
\end{remarque}



\section{Limites du modèle de calcul}

\subsection{De l'arbitraire à l'aléatoire}

\begin{definition}[Machine de Turing randomisée]
    Une machine de Turing randomisée est une machine 
    de Turing avec une bande d'advice sur l'alphabet $\{0,1\}$. 
    Le contenu de la bande d'advice suit une loi uniforme à taille fixée.
\end{definition}

\begin{definition}[\BPP]
    Un langage $L$ est dans $\BPP$ si et seulement s'il existe une
    machine de Turing randomisée $M$ qui calcule en temps polynomial
    et qui vérifie $\mathbb{P}_r ( M(x,r) = L(x) )
    \geq \frac{2}{3}$
\end{definition}

\begin{definition}[$\RP$ et $\co\RP$] 
    Un langage $L$ est dans $\RP$ si et seulement s'il existe une 
    machine de Turing randomisée $M$ qui calcule en temps polynomial
    et qui vérifie~: \begin{compactenum}[(i)]
    \item Si $x \in L$, alors $\mathbb{P}_r (M (x,r) = L(x)) \geq \frac{2}{3}$
    \item Si $x \not \in L$, alors $\mathbb{P}_r (M(x,r) = \neg L(x)) = 0$
    \end{compactenum}
\end{definition}


\begin{exemple}
    Les tests probabilistes de primalité (Miller-Rabin, Solovay-Strassen) sont 
    dans $\co\RP$. Ce que l'on peut traduire $\PRIME \in \co\RP$.

    L'algorithme de Berlekamp probabiliste
    qui détermine si un polynôme est irréductible 
    sur un corps fini est dans $\co\RP$. \textbf{Attention,
    comment est donné le corps fini, le polynôme ... subtil !}
\end{exemple}

\begin{theoreme}[Admis]
    Tester si un polynôme est nul est dans $\RP$ et a priori
    n'est pas dans $\P$ ni $\NP$-dur.
\end{theoreme}

\begin{propriete}[Réduction de l'erreur]
    On peut réduire l'erreur d'un algorithme dans $\BPP$
    en l'exécutant un nombre polynomial de fois 
    et en sélectionnant le résultat majoritaire.
    Plus précisément, il existe une machine $M$ 
    dans $\BPP$ qui vérifie de plus~:
    \begin{equation*}
        \mathbb{P}_r \left( M(x,r) = L(x) \right) 
        \geq 
        1 - 2^{-|x|}
    \end{equation*}
\end{propriete}

\begin{remarque}
    Ce théorème s'adapte pour $\RP$ et $\co\RP$.
\end{remarque}

\begin{remarque}
    Il n'est pas clair que $\BPP$ possède un problème 
    complet. Toutefois si $\BPP = P$, ce qui est une 
    conjecture classique, alors il en existerait bien.
\end{remarque}

\subsection{La hiérarchie polynomiale}

\begin{definition}
    Soit $\ComplexityFont{C}$ une classe 
    de complexité, on définit $\exists\ComplexityFont{C}$
    comme suit~: $L \in \exists\ComplexityFont{C}$ 
    si et seulement s'il existe un $K \in \ComplexityFont{C}$
    et un polynôme $P \in \mathbb{N}[X]$
    tel que $x \in L \iff \exists y, |y| \leq P(|x|) \wedge (x,y) \in K$

    On adapte de manière naturelle la définition à $\forall\ComplexityFont{C}$.
\end{definition}

\begin{propriete}
    On constate que $\exists\P = \NP$ et $\forall\P = \co\NP$.
\end{propriete}

\begin{definition}
    On définit une hiérarchie comme suit~:
    Le niveau zéro est $\Sigma_p^0 = \Pi_p^0 = \P$,
    pour calculer le niveau suivant on ajoute une alternace 
    de quantificateurs $\Sigma_p^{i+1} = \exists \Pi_p^i$,
    et à chaque niveau il y a la classe et son complémentaire $\Pi_p^i = \co\Sigma_p^i$.
    On note $\PH = \bigcup_i \Sigma_p^i$
\end{definition}

\begin{propriete}
    Le problème $\SAT_i$, c'est-à-dire une formule CNF avec $i$ alternances 
    de quantificateurs en partant de $\exists$ est $\Sigma_p^i$-complet
\end{propriete}

\begin{propriete}
    $\PH \subseteq \PSPACE$
\end{propriete}

\begin{theoreme}[Effondrement]
    Si $\Sigma_p^i = \Sigma_p^{i+1}$ alors $\PH = \Sigma_p^i$
\end{theoreme}

\begin{application}
    S'il existe un problème complet pour $\PH$ alors 
    $\PH$ s'effondre sur un $\Sigma_p^i$. En particulier 
    on déduirait $\PH \neq \PSPACE$
\end{application}


Liens avec \PSPACE et \EXP.

\begin{theoreme}[Sipser-Gacs]
    $\BPP$ est dans $\Sigma_p^2 \cap \Pi_p^2$. Cela 
    s'interprète comme un résultat de dérandomisation
\end{theoreme}

\subsection{divers}

Les machines alternantes $\AP = \PSPACE$ ?

\begin{exemple}[Attention, automate boustrophédon]
    Une machine avec une seule bande qui n'écrit pas 
    sur son entrée reconnaît en réalité un langage rationnel.
\end{exemple}

\begin{theoreme}[Admis]
    Une machine de Turing qui calcule en espace $o(\log\log n)$
    reconnaît un langage rationnel. 
\end{theoreme}

\clearpage
\appendix 

\section{Figures}

\todo[inline]{Faire de joli dessins !}
\begin{equation*}
    \ComplexityFont{L} 
    \subseteq \NL = \co\NL
    \subseteq \P
    \subseteq \NP
    \subseteq \PSPACE = \NPSPACE = \co\NPSPACE
    \subseteq \EXP
\end{equation*}

\begin{equation*}
    \begin{array}{lll}
        \P &\subseteq \NP    & ... \\
           &\subseteq \co\NP & ... 
    \end{array}
\end{equation*}


\begin{equation*}
    P \subseteq \ZPP \subseteq \RP \subseteq \BPP \subseteq 
    \Sigma_p^2 \cap \Pi_p^2
\end{equation*}

\clearpage

\begin{references}
\item Barak Arora 
\end{references}

\begin{center}
\fbox{\begin{minipage}{0.9\linewidth}
        \textbf{Développements}
        \begin{enumerate}
            \item Immermann-Szelepcsényi 
            \item Sipser-Gacs 
        \end{enumerate}
\end{minipage}}
\end{center}

\textbf{Motivation~:} 
L'objectif est d'étudier de manière abstraite la notion 
même de complexité. En effet, elle semble être très variable 
selon les modèles de calculs~: algorithmes parallélisés, circuits,
lambda-termes. On constate d'ailleurs empiriquement que mesurer 
la complexité d'un algorithme dépend crucialement du coût "estimé"
des opérations jugées "élémentaires". 

Dans un premier temps, on explique pourquoi le formalisme 
des machines de Turing permet une vision \emph{élémentaire}
de la notion de calcul, et on justifie son utilisation 
pour la définition de la complexité. Notamment on justifie 
que les coûts de simulation permettent de retrouver 
naturellement les complexités "attendues" d'algorithmes classiques.
Tout au long du cours, on 
compare les machines de Turing \emph{déterministes}
avec les machines de Turing \emph{non-déterministes}, une différence 
qui, à priori, change réellement la complexité des problèmes.

On essaie ensuite de comprendre les relations entres les différentes 
classes introduites, notamment en introduisant la notion de problème 
complet pour une classe. Cela permet d'énoncer la forme générale 
du problème complet "classique" pour les classes $\P$, $\PSPACE$
et autres. Enfin, le problème de l'accessibilité dans un graphe $\PATH$
et son lien avec le graphe des configurations permet de prouver 
les théorèmes de Savitch et Immermann-Szelepcsényi, fournissant 
de véritables résultats sur la classification que nous avions construite.
Une dernière classe de problèmes complets est étudiée~: ceux de satisfiabilité. 
Ils sont l'occasion de donner l'exemple de $\HORNSAT$ qui est un problème 
complet pour $\P$ tout en ayant un algorithme en temps linéaire 
sur une machine RAM. Les logiques fournissent de plus des problèmes 
pour lesquels il est ensuite facile de se réduire~: on les utilise 
par exemple pour montrer que des résultats de $\NP$-complétude. Ce 
résultat ne devrait pas surprendre, au vu des liens entre les modèles 
de calcul et les logiques. 

À ce stade du cours, on a un certain nombre d'inclusions sur 
les classes bien connues, quelques égalités et deux résultats 
d'inclusion strictes. Toutefois on peut remettre en question 
les choix faits dans les modèles de calcul. Par exemple 
la classe $\P$, représentante "canonique" des calculs "efficaces"
n'est pas toujours praticable, comme le montre l'exemple d'AKS.
Dans la pratique, les algorithmes randomisés sont particulièrement 
efficaces, et bien plus naturels pour considérer ce que devrait 
être un "algorithme rapide". Il se trouve que la classe $\RP$ ainsi 
est représente le continuum entre~: il existe un certificat, et 
toute chaîne est un certificat, en quantifiant sur la proportion 
de certificats qui permettent de conclure. 
La considération des classes de complexité randomisée se restreint 
toutefois à $\RP$, $\co\RP$ et $\BPP$
pour lesquelles la conception d'algorithme est particulièrement 
intuitive. En prenant une autre direction, on peut se demander où sont 
les problèmes entre $\SAT$ et $\TQBF$, c'est l'occasion d'introduire 
la hiérarchie polynômiale et d'évoquer très rapidement les machines 
alternantes, généralisation naturelle des machines non-déterministes.
Le dernier résultat, ouvrant sur un cours de complexité plus avancée
concerne le lien, à priori non trivial, entre la hiérarchie ainsi 
introduite et les classes randomisées, par le truchement d'une 
méthode \emph{fondamentale}~: la dérandomisation. Un des derniers 
succès en date de cette dernière méthode est la preuve de $\SL =
\ComplexityFont{L}$ qui dépasse de loin le cadre de ce cours.

\tableofcontents
\end{document}

