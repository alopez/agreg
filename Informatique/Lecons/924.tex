\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{complexity}
\usepackage{hyperref}
%\usepackage{enumitem}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}
\usepackage{palatino}
\usepackage{pdfpages}
\usepackage{epigraph}

%%%% Format

\setlength{\columnsep}{2cm}

\newcommand{\lname}{L924 - Théories \& Modèles en logique du premier ordre}

\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}

\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}
 
\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}


%%%% Corps du fichier
\title{\lname}
\date{}



\begin{document}

\maketitle

\clearpage

\section{Théories}

\subsection{Rappels}

On se place dans le cadre de la logique du premier ordre 
et en particulier dans le système de la déduction naturelle
avec l'axiome du tiers exclus.

\begin{definition}[Théorie]
    Une théorie est la donnée d'un langage 
    $\mathcal{L}$ contenant des symboles 
    de fonction et de prédicats ainsi 
    qu'une liste de formules appelées \emph{axiomes}.
\end{definition}

\begin{exemple}[Théorie des groupes]
    Sur le langage $\{ e, *, =  \}$
    où $e$ est une constante, $*$ une 
    fonction d'arité $2$ et $=$ un prédicat binaire.

    \begin{itemize}
        \item $\forall x, \exists y, x * y = e$
        \item $\forall x, x * e = e$
        \item $\forall x, \forall y, \forall z, (x * y) * z = x * (y * z)$
    \end{itemize}
\end{exemple}

\begin{remarque}
    On peut ajouter au langage une fonction unaire 
    $i$ qui correspond à la fonction inverse 
    en écrivant $\forall x, x * i(x) = e$.
\end{remarque}

\begin{exemple}[Théorie de l'égalité]
    Étant donné une théorie $\mathcal{T}$ sur 
    un langage $\mathcal{L}$, on peut 
    ajouter le symbole $=$ à ce langage et les
    axiomes suivants~:

    \begin{itemize}
        \item Pour toute 
            formule $A$, l'axiome 
            $\forall x, \forall y, x = y \implies (x/z) A = (y / z) A$
        \item L'axiome $\forall x, x = x$
    \end{itemize}
\end{exemple}

\begin{remarque}
    L'ensemble des axiomes est infini, mais les axiomes 
    sont ajoutés selon un \emph{schéma}. De plus 
    il existe 
    un algorithme qui étant donné une formule 
    dit si cette formule est une instance de ce schéma 
    d'axiomes.
\end{remarque}

\begin{exemple}[Théorie de l'arithmétique]
    On dispose des constantes $0,1$, 
    des symboles de fonction $S,+,*$ 
    et des symboles de prédicats $N,=$.

    Les axiomes sont les suivants~:

    \begin{enumerate}
        \item $N(0)$
        \item $\forall x, N(x) \implies N(S(x))$
        \item $\forall x, N(x) \implies \neg (S(x) = 0)$
        \item $\forall x, \forall y, N(x) \wedge N(y) \wedge S(x) = S(y)
            \implies x = y$
        \item $\forall x, N(x) \implies 0 + x = x$
        \item $\forall x, \forall y, N(x) \wedge N(y) \implies (x + S(y) = S(x +
            y))$
        \item $\forall x, N(x) \implies 0 * x = 0$
        \item $\forall x, \forall y, N(x) \wedge N(y) \implies (x * S(y) = (x *
            y) + x)$

        \item Pour toute formule $A$ avec une variable libre $x$,
            l'axiome suivant~: 
            \begin{equation*}
            A(0) \implies \left(\forall x, N(x) \implies A (x) \implies A(S(x))\right) \implies
            \left(\forall x, N(x) \implies A(x)\right)
            \end{equation*}
    \end{enumerate}
\end{exemple}

\subsection{Propriétés des théories du premier ordre}

\begin{definition}[Cohérence]
    Une théorie $\mathcal{T}$ est dite \emph{cohérente}
    si $\mathcal{T} \vdash \bot$ n'est pas un séquent 
    démontrable
\end{definition}

\begin{definition}[Complétude]
    Une théorie $\mathcal{T}$ est dite \emph{complète}
    si pour toute formule $\phi$, soit $\mathcal{T} \vdash \phi$
    est démontrable, soit $\mathcal{T} \vdash \neg \phi$ est 
    démontrable
\end{definition}

\begin{remarque}
    On constate la différence entre $\mathcal{T} \vdash \phi \vee \neg \phi$
    est démontrable 
    et $\mathcal{T} \vdash \phi$ est démontrable ou $\mathcal{T} \vdash \neg
    \phi$ est démontrable.
\end{remarque}

\begin{definition}[Décidabilité]
    Une théorie $\mathcal{T}$ est dite \emph{décidable}
    s'il existe un algorithme qui étant donné une formule 
    $\phi$ écrite sur le langage de $\mathcal{T}$
    détermine si le séquent $\mathcal{T} \vdash \phi$ 
    est démontrable
\end{definition}

\begin{remarque}
    Une théorie complète dont l'ensemble d'axiomes 
    est récursivement énumérable est toujours décidable,
    une théorie incohérente est toujours décidable.
    En particulier si $\mathcal{T}$ n'est pas décidable,
    il n'est pas toujours vrai qu'ajouter un axiome 
    conserve l'indécidabilité.
\end{remarque}

\begin{propriete}[Compacité]
    Si le séquent $\mathcal{T} \vdash \phi$ est démontrable 
    alors il existe un sous ensemble fini $\mathcal{T}'$ 
    de $\mathcal{T}$ tel que $\mathcal{T}' \vdash \phi$
    est démontrable.
\end{propriete}

\subsection{Méthodes de décision}

\begin{definition}[Élimination des quantificateurs]
    Une théorie $\mathcal{T}$ satisfait l'élimination 
    des quantificateurs si pour toute formule $\phi$
    sans quantificateurs avec variables libres $\{x_1, \dots, x_n, y\}$
    il existe une formule $\psi$ sans quantificateurs 
    avec ses variables libres dans l'ensemble $\{ x_1, \dots, x_n\}$
    telle que  le séquent suivant soit démontrable~:

    \begin{equation*}
        \mathcal{T} \vdash \exists y. \phi \iff \psi
    \end{equation*}
\end{definition}

\begin{lemme}[Décision via l'élimination des quantificateurs]
    Si $\mathcal{T}$ satisfait l'élimination des quantificateurs 
    et que la théorie $\mathcal{T}$ est décidable quand elle est 
    restreinte aux formules sans quantificateurs, alors $\mathcal{T}$
    est décidable
\end{lemme}

\begin{exemple}[Théorie des ordres denses]
    La théorie des ordres denses non bornés 
    est décidable et complète. 
\end{exemple}

\begin{exemple}[Modèle $(\mathbb{N},+,<)$]
    La théorie $\{ \phi \text{ close} ~|~ (\mathbb{N},+,<) \vDash \phi \}$
    où $\phi$ est écrite sur le langage $0,1,+,<$
    est décidable.
\end{exemple}

\begin{remarque}
    Il est parfois possible d'ajouter 
    des prédicats au langage pour pour 
    permettre l'élimination des quantificateurs. On utilise 
    alors le lemme suivant~: pour toute formule sur le langage 
    étendu, il existe une formule équivalente sur le langage 
    non-étendu.
\end{remarque}

\subsection{Résultats d'indécidabilité}

\begin{theoreme}[L'arithmétique de Peano est indécidable]
\end{theoreme}

\begin{theoreme}[Indécidabilité forte]
    Toute théorie cohérente et dont les axiomes 
    sont récursivement énumérables et qui 
    peut exprimer l'arithmétique de Peano
    est indécidable, et ne peut pas démontrer sa propre 
    cohérence.
\end{theoreme}

\begin{exemple}
    La théorie $\{ \phi \text{ close} ~|~ (\mathbb{N},+,*,<) \vDash \phi \}$
    où $\phi$ est une formule écrite sur le langage $0,1,+,*,<$
    est indécidable.
\end{exemple}

\begin{propriete}
    Il n'est pas possible d'écrire de formule du premier ordre 
    $\phi$ dans $(\mathbb{N},+,<)$ qui permette d'exprimer 
    la multiplication $*$
\end{propriete}

\section{Modèles}

\begin{definition}[Modèle]
    Un modèle $\mathcal{M}$ d'une théorie $\mathcal{T}$
    est un ensemble \emph{non vide} qui vérifie~:
    \begin{compactenum}
    \item À tout symbole de fonction d'arité $n$ du langage 
        correspond une fonction de $\mathcal{M}^n \to \mathcal{M}$
    \item À tout symbole de prédicat d'arité $n$ du langage 
        correspond une fonction de $\mathcal{M}^n \to \mathbb{B}$
    \end{compactenum}
    
    On possède donc naturellement un morphisme d'interprétation 
    $[ \cdot ]\sigma$ où $\sigma$ est une 
    fonction qui associe un élément de $\mathcal{M}$ à chaque variable.

    On dit qu'une formule close $\phi$ est valide dans $\mathcal{M}$
    (noté $\mathcal{M} \vDash \phi$) si et seulement si son interprétation 
    est $1$.
\end{definition}

\begin{propriete}[Modèles égalitaires]
    Pour toute théorie $\mathcal{T}$ qui inclus la théorie de l'égalité,
    on peut quotienter les modèles de $\mathcal{T}$ pour que 
    le symbole d'égalité $=$ soit interprété exactement comme 
    l'égalité des éléments dans le modèle. 
\end{propriete}


\subsection{Théorème de correction et conséquences}

\begin{theoreme}[Correction]
    Si $\mathcal{T} \vdash \phi$ est démontrable,
    alors pour tout modèle $\mathcal{M}$ de $\mathcal{T}$
    $\mathcal{M} \vDash \phi$.
\end{theoreme}


\begin{exemple}[Critère de cohérence]
    Une théorie $\mathcal{T}$ qui admet un modèle 
    ne peut pas démontrer $\bot$
\end{exemple}

\begin{exemple}[Une théorie étrange]
    On note $\mathcal{T} = \{ P(c) \vee Q(c) \}$
    avec $c$ une constante et $P,Q$ deux prédicats.
    En utilisant le théorème de correction, ni $P(c)$
    ni $\neg P(c)$ n'est démontrable, mais la théorie
    est décidable.
\end{exemple}

\subsection{Théorème de complétude et conséquences}

\begin{theoreme}[Complétude]
    Si pour tout modèle $\mathcal{M}$ de $\mathcal{T}$
    $\mathcal{M} \vDash \phi$ alors $\mathcal{T} \vdash \phi$
    est démontrable.
    De manière équivalente si $\mathcal{T}$ est cohérente 
    alors elle possède un modèle.
\end{theoreme}


\begin{remarque}
    Il y a donc une correspondance parfaite entre 
    prouvable et vrai dans tout modèle. En particulier l'ensemble 
    des propriétés vraies dans tout modèle est récursivement 
    énumérable.
\end{remarque}

\begin{exemple}[Cohérence relative]
    Soit $\mathcal{T}'$ une extension d'une théorie $\mathcal{T}$
    qui vérifie que tout modèle de $\mathcal{T}$ peut se 
    transformer un modèle de $\mathcal{T}'$.
    Alors si $\mathcal{T}$ est cohérente, $\mathcal{T}'$ est cohérente.

    Cela s'applique par exemple aux méthodes pour montrer 
    que ZF+C est cohérente relativement à ZF. 
\end{exemple}

\begin{exemple}[Décidabilité via les modèles finis]
    Soit $\mathcal{T}$ une théorie qui vérifie la propriété 
    suivante~: pour toute formule $\phi$, si $\mathcal{T} \vdash \phi$ est non-démontrable 
    alors il existe un modèle fini $\mathcal{M}$ de $\mathcal{T}$ tel que
    $\mathcal{M} \vdash \neg \phi$.
    Alors $\mathcal{T}$ est décidable.

    Cela s'applique par exemple aux formules du calcul propositionnel.
\end{exemple}

\begin{theoreme}[Löwenheim-Skolem]
    Si $\mathcal{T}$ possède un langage avec un nombre dénombrable 
    de symboles, et que $\mathcal{T}$ possède un modèle, alors 
    elle possède un modèle dénombrable.

    Si $\mathcal{T}$ possède un langage avec un nombre dénombrable 
    de symboles, et que $\mathcal{T}$ possède un modèle infini,
    alors elle possède un modèle infini de toute cardinalité.
\end{theoreme}

\begin{exemple}
    Il existe des groupes de toute cardinalité.
\end{exemple}

\subsection{Exemples d'utilisation des modèles}
    
\begin{lemme}[Compacité]
    Si toute sous théorie fini de $\mathcal{T}$ admet 
    un modèle alors $\mathcal{T}$ admet un modèle
\end{lemme}


\begin{exemple}[Applications de la compacité]
    ~
    \begin{compactenum}
    \item Soit $G = (V,E)$ un graphe infini, $G$ est $k$-coloriable 
    si et seulement si toute partie finie de $G$ est $k$-coloriable
    \item Tout ensemble peut être muni d'un ordre total
    \end{compactenum}
\end{exemple}

\begin{lemme}[Existence des entiers non-standards]
    En utilisant le fait que LTL peut se traduire dans
    la logique du premier ordre et en remarquant que LTL 
    n'est pas compacte.
\end{lemme}


\begin{theoreme}[Décidabilité de l'arithmétique de Presburger]
    La théorie au premier ordre de $(\mathbb{N},+)$ est décidable.
\end{theoreme}

\section{Utilisation plus poussée des modèles}

\begin{definition}[Équivalence élémentaire]
    Deux modèles $\mathcal{M}$ et $\mathcal{N}$
    sont dit \emph{élémentairement équivalents}
    quand pour toute formule \emph{close} $\phi$,
    $\mathcal{M} \vDash \phi$ si et seulement si 
    $\mathcal{N} \vDash \psi$.
\end{definition}

\begin{remarque}
    Deux modèles \emph{isomorphes} sont 
    élémentairement
    équivalents.
\end{remarque}

\begin{exemple}
    Une théorie $\mathcal{T}$ est complète 
    si et seulement si tous ses modèles sont 
    élémentairement équivalents.
\end{exemple}

\begin{theoreme}
    La théorie des corps algébriquement 
    clos admet l'élimination des quantificateurs 
\end{theoreme}

\begin{exemple}
    Deux corps algébriquement clos sont  
    élémentairement équivalents
    si et seulement si ils ont même caractéristique.
\end{exemple}

\subsection{Vers la résolution}

\begin{definition}[Skolemisation partielle]
Soit $\phi$ une formule de la forme suivante où $\psi$ est sans quantificateurs 
\begin{equation*}
    \forall x_1, \dots, \forall x_n, \exists x_{n+1}, Q_{n+2} x_{n+2}, \dots ,
    Q_p x_p \psi 
\end{equation*}
\end{definition}

On introduit un nouveau symbole de fonction $f$ d'arité $n$
et on note $\hat{\phi}$ la skolémisation partielle de $\phi$~:
\begin{equation*}
    \hat{\phi} =
    \forall x_1, \dots, \forall x_n, Q_{n+2} x_{n+2}, \dots ,
    Q_p x_p (x_{n+1} / f(x_1, \dots, x_n))\psi 
\end{equation*}

\begin{lemme}[Skolemisation et satisfiabilité]
    Avec les notations de la définition précédente,
    la formule $\phi$ est satisfiable si et seulement si 
    la formule $\hat{\phi}$ est satisfiable.
\end{lemme}

\begin{propriete}[Recherche de contradiction]
    Si $\mathcal{T}$ est une théorie et $\phi$ une formule les propositions
    suivantes sont équivalentes~:

    \begin{compactenum}[(i)]
    \item $\mathcal{T} \vdash \phi$ est démontrable
    \item $\mathcal{T}, \neg \phi \vdash \bot$ est démontrable
    \item $\mathcal{T}, \neg \phi$ n'a pas de modèle 
    \item $\mathcal{T}, \widehat{\neg \phi}$ n'a pas de modèle
    \end{compactenum}
\end{propriete}

\begin{exemple}
    On peut supposer qu'il existe un symbole de fonction $\square^{-1}$
    dans la théorie des groupes, ou bien qu'il existe 
    une constante $\emptyset$ dans ZF
\end{exemple}

\subsection{Jeu d'Ehrenfeucht-Fraïssé}

On se limite désormais au cas où le langage est 
\emph{fini}. On suppose de plus 
qu'il ne contient que des symboles 
de prédicats et des constantes.

\begin{exemple}
    La théorie des ordres linéaires
\end{exemple}

\begin{definition}[Jeu]
    Soient $\mathcal{U}$ 
    et $\mathcal{V}$ deux modèles
    de domaines respectifs $U$ et $V$.

    Le jeu $G_n(\mathcal{U},\mathcal{V})$
    est un jeu à deux joueurs $J1$ et $J2$
    qui jouent $n$ coup en suivant les règles 
    suivantes~:
    \begin{compactenum}
    \item Le joueur $J1$ choisit l'une des deux
        structures et un élément dans celle-ci 
    \item Le joueur $J2$ choisit un élément 
        dans l'autre structure
    \end{compactenum}
    
    Une partie est la donnée de la suite 
    des $n$ coups de $J1$ et $J2$
    notée $(p,q)$.
\end{definition}

\begin{definition}[Condition de gain]
    Le joueur $J2$ gagne un jeu 
    dans $G_n(\mathcal{U},\mathcal{V})$
    si et seulement si 
    la partie $(p,q)$ définit 
    un isomorphisme partiel de $\mathcal{U}$
    dans $\mathcal{V}$
\end{definition}

\begin{theoreme}
    Si $\mathcal{U}$ et $\mathcal{V}$
    sont deux modèles d'une théorie $\mathcal{T}$
    alors les propriétés suivantes sont 
    équivalentes~:

    \begin{compactenum}
    \item $\mathcal{U}$ et $\mathcal{V}$
        satisfont les mêmes formules 
        avec moins de $n$ quantificateurs 
    \item Le joueur $J2$ gagne le jeu 
        $G_n(\mathcal{U}, \mathcal{V})$.
    \end{compactenum}
\end{theoreme}

\begin{exemple}
    La théorie des ordres linéaires ne permet 
    pas d'écrire une formule $\phi$ 
    qui n'est vérifiée que dans des modèles 
    avec un nombre pair d'éléments.
\end{exemple}

\begin{exemple}
    Pour l'arithmétique de Peano, les structures 
    $\mathbb{N}$ et $\mathbb{N} \oplus \mathbb{N}$
    sont élémentairement équivalentes.
\end{exemple}

\begin{exemple}
    La clôture transitive d'une relation 
    n'est pas définissable en 
    logique du premier ordre
\end{exemple}

\end{document}
