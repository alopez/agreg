\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{complexity}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs, wasysym}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{hyperref}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows,matrix}
\usepackage{pdfpages}
\usepackage{epigraph}
\usepackage{fourier}
\usepackage{url}
\usepackage{faktor}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{todonotes}
\usepackage{bussproofs}



%%%% Format

\setlength{\columnsep}{2cm}

\newcommand{\lname}{L930 - Sémantique des langages de programmation}
\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}



\newcommand{\Lin}[1]{\mathcal{L}\left(#1\right)}
\newcommand{\Comm}[1]{\mathcal{C}\left(#1\right)}
\newcommand{\GLin}[1]{\operatorname{GL}\left(#1\right)}
\newcommand{\Image}{\operatorname{Im}}
\newcommand{\Ker}{\operatorname{Ker}}
\newcommand{\trace}{\operatorname{tr}}
\newcommand{\cycl}[2]{\left\langle #1 \right\rangle_{#2}}
\newcommand{\PAL}{\ComplexityFont{PAL}}
\newcommand{\CONNECTED}{\ComplexityFont{CONNECTED}}
\newcommand{\TRIANGLEFREE}{\ComplexityFont{TRIANGLEFREE}}
\newcommand{\CTLSAT}{\ComplexityFont{CTLSAT}}
\newcommand{\HORNSAT}{\ComplexityFont{HORNSAT}}
\newcommand{\PATH}{\ComplexityFont{PATH}}
\newcommand{\PRIME}{\ComplexityFont{PRIME}}
\newcommand{\TQBF}{\ComplexityFont{TQBF}}



\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{jury}{\textbf{Rapport de Jury}\begin{quote}\itshape}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}
\newenvironment{references}{\textbf{Références}\begin{quote}\begin{compactitem}}{\end{compactitem}\end{quote}}

\newcommand{\Dev}[1]{$\RHD$ \textbf{DEV} #1 $\LHD$}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{application}
\newtheorem{application}[definition]{Application}

\theoremstyle{corollaire}
\newtheorem{corollaire}[definition]{Corollaire}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}
 
\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}



%%%%% SPECIAL SEMANTICS %%%%%%

\DeclareMathOperator{\IMPWP}{wp}
\DeclareMathOperator{\IMPIF}{\mathbf{if}}
\DeclareMathOperator{\IMPTHEN}{\mathbf{then}}
\DeclareMathOperator{\IMPELSE}{\mathbf{else}}
\DeclareMathOperator{\IMPWHILE}{\mathbf{while}}
\DeclareMathOperator{\IMPSKIP}{\mathbf{skip}}
\DeclareMathOperator{\IMPDO}{\mathbf{do}}
\DeclareMathOperator{\IMPEND}{\mathbf{end}}
\newcommand{\lassign}[2]{#1 := #2}
\newcommand{\lif}[3]{\IMPIF ~ #1 ~ \IMPTHEN ~#2~ \IMPELSE ~ #3}
\newcommand{\lwhile}[2]{\IMPWHILE ~ #1 ~ \IMPDO ~ #2~ \IMPEND}
\newcommand{\sem}[2]{\llbracket #1 \rrbracket_{#2}}
\newcommand{\Ereduces}[3]{ (#1, #2) \Downarrow #3}
\newcommand{\Creduces}[3]{ (#1, #2) \Downarrow #3}
\newcommand{\lwp}[2]{\IMPWP(#1)(#2)}
\newcommand{\Hoare}[3]{\{ #1 \} ~ #2 ~ \{ #3 \}}

%%%%% SPECIAL SEMANTICS %%%%%%


%%%% Corps du fichier
\title{\lname}
\date{}


\begin{document}

\begin{center}
    \fbox{\begin{minipage}{0.7\linewidth}
            \centering
            \hrule
            \vspace{3pt}
            \huge
            \lname 
            \vspace{3pt}
            \hrule
    \end{minipage}}
\end{center}


\begin{jury}
    L’objectif est de formaliser ce qu’est un programme : introduction des sémantiques opérationelle et dénotationelle, 
    dans le but de pouvoir faire des preuves de programmes, des preuves d’équivalence, des preuves de correction de traduction. 
    Ces notions sont typiquement introduites sur un langage de programmation (impératif) jouet. On peut tout à fait se limiter à 
    un langage qui ne nécessite pas l’introduction des CPOs et des théorèmes de point fixe généraux. En revanche, on s’attend ici 
    à ce que les liens entre sémantique opérationelle et dénotationelle soient étudiés (toujours dans le cas d’un langage jouet). 
    Il est aussi important que la leçon présente des exemples d’utilisation des notions introduites, comme des preuves d’équivalence 
    de programmes ou des preuves de correction de programmes.
\end{jury}

\newpage

\textbf{Commentaire}
\begin{quote}
    \itshape
    
    L'objectif est de fournir une sémantique 
    formelle à un langage jouet et de la relier 
    à des observations sur des langages de 
    programmations concrets.
    On donne la définition de IMP, un langage 
    très simple, pour immédiatement justifier 
    de sa puissance en écrivant l'algoritme d'Euclide
    pour le PGCD, et en évoquant brièvement que 
    ce langage est Turing-complet.

    Dans un premier temps on considère des expressions 
    arithmétiques (sans effet de bord) ce qui constitue 
    la première partie. 

    Ce qui permet de comparer les différentes approches~:
    dénonationelle (ici la plus claire), petit pas (qui permet 
    de contrôler l'évaluation de l'expression) 
    et grand pas (intermédiaire).

    Cela permet d'évoquer la notion d'équivalence 
    de deux expressions, une notion qui coincide 
    pour les trois sémantiques. On a alors la possibilité 
    de prouver des théorèmes comme "l'associativité de +"
    ou sa commutativité.



    
\end{quote}
\newpage

\section{Sémantique opérationnelle}

\subsection{Le langage IMP}

\begin{remarque}
    On utilise un langage jouet 
    pour simplifier~: les entiers sont naturels,
    et c'est le seul type du langage. 
\end{remarque}

\begin{align*}
    e &:= e + e ~|~ \underline{n}
                ~|~ - e 
                ~|~ x \\
    c &:= \lassign{x}{e} ~|~ \lif{e}{c}{c}
                        ~|~ \IMPSKIP 
                        ~|~ c ; c 
                        ~|~ \lwhile{e}{c} \\
\end{align*}

\begin{exemple}[Euclide]
    $\lwhile{a}{
        \lif{a - b}{a := a - b}{b := b - a}}$
\end{exemple}

\begin{remarque}
    Il n'y a pas de notion d'entrée/sortie dans le programme,
    tout repose donc sur la notion \emph{d'environnement}
\end{remarque}

\begin{definition}[Environment]
    On note $\rho : \mathcal{X} \to \mathbb{N}$ un environnement. 
\end{definition}

\begin{definition}[Dénotation des expressions]
    On interprète les expressions comme des fonctions 
    des environnements dans les entiers naturels~:

    \begin{align*}
        \sem{x}{\rho} &= \rho (x) \\
        \sem{\underline{n}}{\rho} &= n \\
        \sem{e + e'}{\rho} &= \sem{e}{\rho} + \sem{e'}{\rho} \\
        \sem{- e}{\rho} &= \max (0, - \sem{e}{\rho}) \\
    \end{align*}
\end{definition}

\begin{exemple}
    $\sem{(x+\underline{1}) + (\underline{2} + y)}{\rho} = \rho(x) + 1 + 2 + \rho (y)$
\end{exemple}

On peut vouloir donner une sémantique plus \emph{calculatoire}, 
qui permet par exemple de spécifier \emph{comment} un compilateur doit 
faire son évaluation plus que le résultat attendu.

\begin{definition}[Petits pas des expressions]
    On note $C$ pour un contexte, c'est-à-dire un terme 
    avec variable $\square$ apparaissant une unique fois,
    et $C[e]$ pour le terme 
    obtenu en substituant $\square$ par $e$ dans $C$.

    \begin{align*}
        (\rho, x) &\mapsto (\rho, \rho(x)) \\
        (\rho, \underline{n} + \underline{n'}) &\mapsto (\rho,
        \underline{n+n'}) \\
        (\rho, - \underline{n}) &\mapsto (\rho, \underline{-n})
        \\
    \end{align*}
    \begin{align*}
        (\rho, C[e]) &\longrightarrow (\rho, C[e']) & \text{ si } (\rho, e) \mapsto
        (\rho, e')
    \end{align*}
\end{definition}

\begin{exemple}
On peut spécifier la forme des contextes pour guider l'évaluation, par exemple
la définition suivante force à évaluer de "gauche à droite" les expressions 
arithmétiques, et rend le système de réécriture déterministe.
\begin{align*}
    C := \square ~|~ C + e ~|~ \underline{n} + C ~|~ 
                 ~|~ - C 
\end{align*}
\end{exemple}

\begin{exemple}
    Pour tout $\rho, x, y, z$, $(\rho, (x + y) + z) \longrightarrow^* (\rho, n)$
    si et seulement si $(\rho, x + (z + y)) \longrightarrow^* (\rho, n)$.
\end{exemple}

\begin{definition}[Grand pas des expressions]
    \begin{multicols}{2}
    \begin{prooftree}
        \AxiomC{}
        \UnaryInfC{$\Ereduces{\rho}{\underline{n}}{n} $}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{}
        \UnaryInfC{$\Ereduces{\rho}{x}{\rho(x)}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Ereduces{\rho}{e}{n}$}
        \AxiomC{$\Ereduces{\rho}{e'}{n'}$}
        \BinaryInfC{$\Ereduces{\rho}{e + e'}{n + n'}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Ereduces{\rho}{e}{n}$}
        \UnaryInfC{$\Ereduces{\rho}{- e}{- n}$}
    \end{prooftree}
    \end{multicols}
\end{definition}

\begin{exemple}
    Les expressions $\underline{1} + x$ et $\underline{1} + y$
    n'ont pas la même sémantique.
\end{exemple}

\begin{theoreme}[Équivalence sur les expressions]
    Pour tout environnement $\rho$ et tout expression $e$~:
    \begin{equation*}
        (\rho,e) \longrightarrow^* (\rho, \underline{n})
        \iff
        \Ereduces{\rho}{e}{n} 
        \iff
        \sem{e}{\rho} = n
    \end{equation*}
\end{theoreme}


\subsection{Sémantique à petits pas}

\begin{definition}[Sémantique à petits pas pour IMP]
    \begin{align*}
        (\rho, \IMPSKIP;c) &\rightarrow (\rho,c) \\
        (\rho, c;c') &\rightarrow (\rho',c'';c') & \text{ si } (\rho, c)
        \rightarrow (\rho', c'') \\
        (\rho, \lassign{x}{e}) &\rightarrow (\rho[x \mapsto \sem{e}{\rho}], \IMPSKIP) \\
        (\rho, \lif{e}{c}{c'}) &\rightarrow (\rho, c) & \text{ si }
        \sem{e}{\rho} \neq 0 \\
        (\rho, \lif{e}{c}{c'}) &\rightarrow (\rho, c') & \text{ si }
        \sem{e}{\rho} = 0 \\
        (\rho, \lwhile{e}{c}) &\rightarrow (\rho, \IMPSKIP) & \text{ si }
        \sem{e}{\rho} = 0 \\
        (\rho, \lwhile{e}{c}) &\rightarrow (\rho, c; \lwhile{e}{c}) & \text{ si }
        \sem{e}{\rho} \neq 0 \\
    \end{align*}
\end{definition}

\begin{theoreme}[Progrès]
    Les seules configurations sans successeurs pour $\rightarrow$
    sont de la forme $(\rho, \IMPSKIP)$. De plus la réduction $\rightarrow$
    est déterministe.
\end{theoreme}

\begin{theoreme}[Euclide]
    L'algorithme d'Euclide tel que codé termine pour tout environnement 
    $\rho$ tel que $\rho(a), \rho(b) \geq 0$, et à la fin de l'exécution 
    $\rho' (b)$ vaut le PGCD de $\rho(a)$ et $\rho(b)$. De plus, aucune 
    autre variable de $\rho$ n'est modifiée durant l'exécution.
\end{theoreme}

\begin{remarque}
    Il existe des programmes qui ne terminent pas, par exemple 
    le programme suivant~: $\Omega = \lwhile{1}{\IMPSKIP}$.
\end{remarque}

\begin{definition}[Contexte]
    Un contexte $C$ est produit via la grammaire
    \begin{equation*}
        C := \square ~|~ C ; c ~|~ c ; C ~|~ 
            \lif{e}{C}{c} ~|~ \lif{e}{c}{C} 
            ~|~ \lwhile{e}{C}
    \end{equation*}
\end{definition}

\begin{definition}[Équivalence contextuelle]
    On dit que $A$ et $B$ sont équivalents sous contexte 
    si et seulement si 
    \begin{equation*}
        \forall C, \forall \rho, \rho', 
        (\rho, C[A]) \rightarrow^* (\rho', \IMPSKIP)
        \iff
        (\rho, C[B]) \rightarrow^* (\rho', \IMPSKIP)
    \end{equation*}

    C'est une relation de congruence sur les programmes IMP.
\end{definition}

\begin{remarque} 
    L'équivalence contextuelle est la propriété 
    qui doit être préservée lorsque du code optimisé est produit.
    Cela dit précisément que le programme général se comportera
    de la même manière. On peut sans changer la notion 
    demander seulement l'équivalence des terminaisons de $A$ et $B$.
\end{remarque}

\begin{theoreme}
    Deux programmes $A$ et $B$ sont contextuellement 
    équivalents si et seulement si 
    \begin{equation*}
        \forall \rho, \rho',
        (\rho, A) \rightarrow^* (\rho', \IMPSKIP)
        \iff
        (\rho, B) \rightarrow^* (\rho', \IMPSKIP)
    \end{equation*}
\end{theoreme}

\begin{exemple}
    Les deux programmes suivants sont contextuellement équivalents

    \begin{equation*}
        \lif{e}{(c ; \lwhile{e}{c})}{\IMPSKIP}
        \quad \quad \quad \quad 
        \lwhile{e}{c}
    \end{equation*}
\end{exemple}

\begin{exemple}
    Tout programme qui ne termine pas est contextuellement 
    équivalent à $\Omega$.
\end{exemple}

\subsection{Sémantique à grands pas}


\begin{definition}
    \begin{multicols}{2}
    \begin{prooftree}
        \AxiomC{}
        \UnaryInfC{$\Creduces{\rho}{\IMPSKIP}{\rho}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Ereduces{\rho}{e}{n}$}
        \UnaryInfC{$\Creduces{\rho}{\lassign{x}{e}}{\rho[x \mapsto n}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Ereduces{\rho}{e}{0}$}
        \AxiomC{$\Creduces{\rho}{c'}{\rho'}$}
        \BinaryInfC{$\Creduces{\rho}{\lif{e}{c}{c'}}{\rho'}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Ereduces{\rho}{e}{n \neq 0}$}
        \AxiomC{$\Creduces{\rho}{c}{\rho'}$}
        \BinaryInfC{$\Creduces{\rho}{\lif{e}{c}{c'}}{\rho'}$}
    \end{prooftree}

    \begin{prooftree}
        \AxiomC{$\Ereduces{\rho}{e}{0}$}
        \UnaryInfC{$\Creduces{\rho}{\lwhile{e}{c}}{\rho}$}
    \end{prooftree}
    \end{multicols}
    \begin{prooftree}
        \AxiomC{$\Ereduces{\rho}{e}{n \neq 0}$}
        \AxiomC{$\Creduces{\rho}{c}{\rho'}$}
        \AxiomC{$\Creduces{\rho'}{\lwhile{e}{c}}{\rho''}$}
        \TrinaryInfC{$\Creduces{\rho}{\lwhile{e}{c}}{\rho''}$}
    \end{prooftree}
\end{definition}

\begin{theoreme}
    Le programme suivant calcule bien 
    le $n$-ème terme de la suite de Fibonaci 
    qui est alors enregistré dans $\rho(a)$.
    \begin{equation*}
        a := \underline{1};
        b := \underline{1};
        \lwhile{n}{ n := n - \underline{1} ; 
                    t := a + b;
                    a := b;
                    b := t } 
    \end{equation*}
    De plus il ne modifie que les variables $a,b,t,n$.
\end{theoreme}

\begin{lemme}
    Pour tout $\rho, \rho'$, pour tout $c, c'$.
    Si $(\rho, c) \rightarrow (\rho', c')$ 
    et $\Creduces{\rho'}{c'}{\rho''}$ alors 
    $\Creduces{\rho}{c}{\rho''}$.
\end{lemme}

\begin{theoreme}[Correction]
    Quelque soient $\rho$ et $c$,
    si $(\rho, c) \rightarrow^* (\rho', \IMPSKIP)$ 
    alors $\Creduces{\rho}{c}{\rho'}$.
\end{theoreme}


\begin{theoreme}[Adéquation]
    Quelque soient $\rho$ et $c$,
    si $\Creduces{\rho}{c}{\rho'}$.
    alors $(\rho, c) \rightarrow^* (\rho', \IMPSKIP)$ 
\end{theoreme}

\begin{remarque}
    Les deux sémantiques coïncident, et donc 
    donnent en particulier la même notion 
    d'équivalence contextuelle sur les programmes.
\end{remarque}

\begin{remarque}
    Un programme qui ne termine pas dans la sémantique à petit 
    pas est un programme qui n'a \emph{pas} de dérivation 
    dans la sémantique à grands pas.
\end{remarque}



\section{Sémantique dénotationnelle}

\begin{remarque}
    On ne peut pas définir le while par induction 
    sur la structure comme dans les expressions !
    Toutefois on désire avoir une sémantique 
    \emph{compositionnelle} et définie par \emph{induction}.
\end{remarque}

\begin{definition}[Treillis complet]
    Un treillis complet est un ensemble ordonné $(X, \leq)$  
non vide tel que toute famille $F \subseteq X$ a une borne
supérieure et inférieure.
\end{definition}

\begin{theoreme}[Point fixe de Tarski]
    Soit $(X, \leq)$ un treillis complet. Soit $f : X \to X$ une fonction 
    monotone, alors $f$ a un plus petit et un plus grand point fixe.
\end{theoreme}

\begin{exemple}
    Attention, on ne peut pas toujours l'atteindre 
    par induction !!
\end{exemple}

\begin{definition}[Domaine de Scott]
    Un ordre partiel complet, ou dcpo 
    est un ensemble ordonné $(X,\leq)$
    dans lequel toute famille dirigée 
    possède un sup. Il est dit pointé 
    s'il possède un élément minimal noté alors $\bot$.
\end{definition}

\begin{exemple}
    L'ensemble $\mathbb{N}_\bot$ est un dcpo pointé.
    L'ensemble $E_\bot$ des environnements auquel 
    on ajoute $\bot$ est un dcpo pointé.
\end{exemple}

\begin{theoreme}[Point fixe de Scott]
    Soit $f$ une fonction de $X$ dans $X$ qui 
    vérifie $\sup_i f(x_i) = f (\sup_i x_i)$ pour toute 
    famille dirigée $(x_i)$, alors $f$ possède un plus 
    petit point fixe et il est obtenu comme $\sup_i f^i (\bot)$.
\end{theoreme}

\begin{definition}[Sémantique opérationnelle]
    \begin{align*}
        \sem{\IMPSKIP}{\rho} &= \rho \\
        \sem{x := e}{\rho} &= \rho[ x \mapsto \sem{e}{\rho} ] \\
        \sem{c;c'}{\rho} &= \begin{cases}
                    \bot & \text{ si } \sem{c}{\rho} = \bot \\
                    \sem{c'}{\sem{c}{\rho}} & \text{ sinon }  \\
            \end{cases} \\
        \sem{\lif{e}{c}{c'}}{\rho} &= 
            \begin{cases}
                \sem{c}{\rho} & \text{ si } \sem{e}{\rho} \neq 0 \\
                \sem{c'}{\rho} & \text{ si } \sem{e}{\rho}   =  0 \\
            \end{cases} \\
        \sem{\lwhile{e}{c}}{\rho} &= \operatorname{lfp} (F_{e,c}) (\rho)
    \end{align*}

    Avec $F_{e,c}$ la fonctionnelle Scott-continue suivante~: 
    \begin{equation*}
        F_{e,c} (f) (\rho) = \begin{cases}
            \rho & \text{ si } \sem{e}{\rho} = 0 \\
            \bot & \text{ si } \sem{e}{\rho} \neq 0 \text{ et } \sem{c}{\rho} =
            \bot \\
            f (\sem{c}{\rho}) & \text{ sinon }
        \end{cases}
    \end{equation*}
\end{definition}

\begin{lemme}[Point fixe]
    Si $\sem{e}{\rho} = 0$ alors
    $\sem{\lwhile{e}{c}}{\rho} = \sem{c ; \lwhile{e}{c}}{\rho}$.
\end{lemme}

\begin{theoreme}[Correction]
    Pour tout $\rho$ et $c$,
    si $\Creduces{\rho}{c}{\rho'}$ alors 
    $\sem{c}{\rho} = \rho' \neq \bot$.
\end{theoreme}

\begin{remarque}
    Ne se sert pas du fait que c'est le plus 
    petit point fixe, seulement du fait que c'est un 
    point fixe quelconque.
\end{remarque}

\begin{theoreme}[Adéquation]
    Pour tout $\rho$ et $c$,
    si $\sem{c}{\rho} = \rho' \neq \bot$, alors 
    $\Creduces{\rho}{c}{\rho'}$.
\end{theoreme}

\begin{remarque}
    Ici on se sert effectivement de la propriété 
    de plus petit point fixe.
\end{remarque}

\begin{theoreme}[Full abstraction]
    Les programmes $A$ et $B$ sont contextuellement
    équivalents si et seulement s'ils ont la même 
    sémantique dénotationnelle.
\end{theoreme}

\begin{exemple}[Factorielle]
    Le programme suivant calcule la factorielle 
    (preuve par composition des sémantiques)
    \begin{equation*}
        a := \underline{1} 
        ;
        \lwhile{n}{
            m := n; 
            S := \underline{0};
            \lwhile{m}{
                m := m-\underline{1};
                S := a + S;};
            a := S;
            n := n -1}
    \end{equation*}

    \begin{verbatim}
def fact (n):
    a = 1
    while n != 0:
        m = n 
        S = 0
        while m != 0:
            S = S + a
            m = m - 1
        a = S
        n = n - 1
    \end{verbatim}
    
    Sa sémantique dénotationelle est précisément
    celle qui à $\rho$ associe $\rho'$ vérifiant~:

    \begin{align*}
        \rho'(a) &= \rho(a) ! \\
        \rho'(S) &= \rho(a) ! \\
        \rho'(n) &= 0 \\
        \rho'(m) &= 0
    \end{align*}
\end{exemple}



\section{Applications aux preuves de programmes}

\subsection{Optimisations} 

\begin{definition}
    On note $FV(c)$ (resp. $FV(e)$) les variables 
    \emph{utilisées dans les calculs} et 
    $DV(c)$ les variables \emph{qui sont modifiées par le programme}.
\end{definition}
\begin{propriete}
    Si $x \not \in FV(c)$ et 
    $\sem{c}{\rho} \neq \bot$ alors $\sem{c}{\rho} (x) = \rho(x)$
\end{propriete}

\begin{exemple}[Unused Variable Elimination]
    Si $x \not \in FV(c)$ alors $x := e; c$ 
    est équivalent à $c ; x := e$. De plus, 
    $x := e; x := e'$ est équivalent à $x := e'$.
\end{exemple}

\begin{exemple}[Transitivity]
    Si $x \not \in DV(c)$ alors $y := x ; c$
    est équivalent à $c$ où l'on substitue $x$ à $y$.
\end{exemple}

\begin{exemple}[Common Subexpression Elimination]
    Si $FV(e) \cap DV(c) = \emptyset$, $x \not \in FV(e)$
    et $z$ est globalement inutilisé alors~:

    \begin{equation*}
        x := e ; c ; y := e ; z := 0
        \equiv_{ctx}
        z := e ; x := z ; c ; y := z ; z := 0 
    \end{equation*}
\end{exemple}

\begin{propriete}
    En posant $f(X) = \lif{e}{ c; X }{\IMPSKIP}$ on a
    \begin{equation*}
        \sem{\lwhile{e}{c}}{\rho} 
        = 
        \sup_i f^i (\Omega)
    \end{equation*}
\end{propriete}

\begin{propriete}[Déroulage de boucle]
    Si $\sem{\lwhile{e}{c}}{\rho} \neq \bot$ alors 
    il existe un $n$ entier tel que 
    \begin{equation*}
        \sem{\lwhile{e}{c}}{\rho} = \sem{ \underbrace{c ; \dots ; c}_n }{\rho}
    \end{equation*}
\end{propriete}


\begin{exemple}
    Si on a la garantie que le $n$ en question 
    est toujours pair, alors on peut 
    remplacer $\lwhile{e}{c}$ par 
    $\lwhile{e}{c ; c}$ et gagner 
    environ $n/2$ comparaisons.
\end{exemple}

\begin{remarque}
    Il y a de nombreuses autres optimisations de programmes, 
    comme \emph{loop invariant hoisting} ou \emph{branch prediction}.
\end{remarque}


\subsection{Logique de Hoare}

\begin{definition}
    On considère le langage logique de l'arithmétique
    au premier ordre. On différencie dans les formules 
    deux types de variables, les variables \emph{de programme}
    et les variables \emph{logiques}. L'interprétation 
    d'une formule est donc l'interprétation standard dans 
    $\mathbb{N}$ qui nécessite l'environment $\rho$ 
    du programme et une interprétation $I$ des variables 
    logiques. On prend comme convention que 
    $\bot \models^I A$ pour tout $I$ et tout $A$.
\end{definition}

\begin{definition}
    On écrit $\models \Hoare{P}{c}{Q}$ pour 
    signifier que $\forall \rho, \forall I, 
    \rho \models^I P \implies \sem{c}{\rho} \models^I Q$.
\end{definition}

\begin{definition}[Logique de Hoare]
    \begin{multicols}{2}
    \begin{prooftree}
        \AxiomC{}
        \UnaryInfC{$\Hoare{P}{\IMPSKIP}{P}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{}
        \UnaryInfC{$\Hoare{P[x \mapsto e]}{\lassign{x}{e}}{P}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Hoare{P}{c}{Q'}$}
        \AxiomC{$\Hoare{Q'}{c'}{Q}$}
        \BinaryInfC{$\Hoare{P}{c ; c'}{Q}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Hoare{P \wedge e \neq 0}{c}{Q}$}
        \AxiomC{$\Hoare{P \wedge e =    0}{c'}{Q}$}
        \BinaryInfC{$\Hoare{P}{\lif{e}{c}{c'}}{Q}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$\Hoare{P \wedge e \neq 0}{c}{P}$}
        \UnaryInfC{$\Hoare{P}{\lwhile{e}{c}}{P \wedge e = 0}$}
    \end{prooftree}
    \begin{prooftree}
        \AxiomC{$P \implies P'$}
        \AxiomC{$\Hoare{P'}{c}{Q'}$}
        \AxiomC{$Q' \implies Q$}
        \TrinaryInfC{$\Hoare{P}{c}{Q}$}
    \end{prooftree}
    \end{multicols}
\end{definition}

\begin{lemme}
    $ \rho[ x \mapsto \sem{e}{\rho}] \models^I A$
    si et seulement si 
    $\rho \models^I A[ x \mapsto e]$
\end{lemme}

\begin{theoreme}[Correction]
    Si le séquent $\vdash \Hoare{P}{c}{Q}$ est 
    démontrable alors 
    $\models \Hoare{P}{c}{Q}$.
\end{theoreme}

\begin{exemple}
    On peut reprendre avec le formalisme de Hoare
    les preuves des algorithmes précédents,
    et se rendre compte que c'est bien exactement 
    ce qui nous intéressait.

    En particulier on note l'utilité des variables 
    logiques pour l'algorithme d'Euclide
\end{exemple}

\begin{definition}[Weakest precondition]
    Une formule logique $P$ une plus faible précondition 
    de $c$ et $A$ si et seulement si 
    \begin{equation*}
        \forall \rho, \forall I, 
        \sigma \models^I P
        \iff
        \sem{c}{\sigma} \models^I A
    \end{equation*}
\end{definition}


\begin{center}
\fbox{\begin{minipage}{0.8\linewidth}
\begin{theoreme}
    Pour toute formule $A$ plus faible 
    précondition de $c$ et $B$, le séquent 
    $\vdash \Hoare{A}{c}{B}$
    est démontrable.
\end{theoreme}
\end{minipage}}
\end{center}


\begin{theoreme}
    Pour tout $A,c,B$ tels que 
    $\models \Hoare{A}{c}{B}$
    le séquent 
    $\vdash \Hoare{A}{c}{B}$
    est démontrable.
\end{theoreme}

\begin{theoreme}
    On peut construire explicitement 
    une formule dans notre 
    langage logique $\lwp{c}{A}$ 
    pour tout $c$ et $A$ qui est une plus faible 
    précondition de $A$.
\end{theoreme}

\begin{remarque}
    Déterminer si $\lwp{c}{P} \iff Q$ est indécidable, 
    il suffit par exemple de considérer $\lwp{c}{\bot}  \iff \top$.
\end{remarque}



\section{Variations dans le modèle de calcul}

\begin{enumerate}
    \item Variations dans la sémantique opérationnelle~:
        non-déterminisme, probabilités.

    \item Dans la sémantique dénotationnelle... construction 
        de powerdomains ?!

    \item Passage à la continuation... vers des constructions 
        étendues pour les langages
\end{enumerate}


\end{document}

