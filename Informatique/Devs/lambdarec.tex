\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs}
\usepackage [a4paper]{geometry}
\geometry{left=3.5cm, right=3.5cm,top=3.5cm ,bottom=4cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{complexity}
\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}
\usepackage{palatino}
\usepackage{pdfpages}
\usepackage{epigraph}

%%%% Format

\setlength{\headheight}{35pt}
\lhead{Développements informatique \\ Confluence de la $\beta$-reduction.}
\rhead{Gaëtan \bsc{Douéneau-Tabot} \\ \small{\textsf{gaetan.doueneau@ens-paris-saclay.fr}}}
\chead{}
\rfoot{}
\pagestyle{fancy}

\theoremstyle{plain}
\newtheorem{theorem}{Théorème}
\newtheorem{lemma}[theorem]{Lemme}
\newtheorem{corollary}[theorem]{Corollaire}
\newtheorem{proposition}[theorem]{Proposition}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Définition}
\newtheorem{example}[theorem]{Exemple}
\newtheorem{remark}[theorem]{Remarque}
\newtheorem{application}[theorem]{Application}

\newcommand{\rb}{\rightarrow_{\beta}}
\newcommand{\es}{\vspace{1\baselineskip}}
\newcommand{\ds}{\vspace{0.4\baselineskip}}

\newcommand{\mb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mf}[1]{\mathfrak{#1}}

% Exercices

\newcommand{\pre}{\subsection*{Prérequis}}
\newcommand{\dev}{\subsection*{Développement}}
\newcommand{\rem}{\subsection*{Remarques}}

%%%% Corps du fichier


\begin{document}

\begin{center}
\underline{\LARGE Confluence de la $\beta$-réduction}

\emph{Adapté depuis Krivine, Lambda-calcul, types et modèles}
\end{center}




\pre


\begin{definition}[$\beta$-réduction] La $\beta$-réduction est la plus petite relation sur les $\lambda$-termes vérifiant les propriétés suivantes :

\begin{itemize}

\item si $u \rb u'$ alors $\lambda x. u \rb \lambda x. u'$ ;

\item si $u \rb u'$ et  alors $v u \rb v u'$ et $u' v \rb u' v$ ;

\item $(\lambda x. u) v \rb u[v/x]$.

\end{itemize}

\end{definition}

\begin{remark}
Les substitutions se font modulo $\alpha$-renommage\dots{} pas de captures.
\end{remark}

\begin{remark}
Définir "plus petite relation" est équivalent à dire "en utilisant un nombre fini de fois les règles données" (arbre de preuve).
\end{remark}


\dev

\begin{theorem}[Church, Rosser] La $\beta$-réduction est confluente.

\end{theorem}

\begin{definition}[Réduction parallèle] On définit la réduction parallèle $\Rightarrow$ comme la plus petite relation sur les $\lambda$-termes vérifiant les propriétés suivantes :

\begin{itemize}

\item $x \Rightarrow x$ si $x$ est une variable ;

\item si $u \Rightarrow u'$ alors $\lambda x. u \Rightarrow \lambda x. u'$ ;

\item si $u \Rightarrow u'$ et  $v \Rightarrow v'$ alors $u v \Rightarrow u' v'$ ;

\item si $u \Rightarrow u'$ et  $v \Rightarrow v'$ alors $(\lambda x. u) v \Rightarrow u' [ v'/x]$.

\end{itemize}

\end{definition}

Intuitivement cette réduction autorise à contracter en une seule étape plusieurs redex à l'intérieur du terme. Plus précisément on a le lemme suivant.

\begin{lemma} \label{lem:clot}

$\rightarrow_{\beta} \subseteq \Rightarrow \subseteq \rightarrow_{\beta}^*$

\end{lemma}

\begin{proof} \item

\begin{enumerate}

\item $\rightarrow_{\beta} \subseteq \Rightarrow$ est clair par restriction des règles.

\item  On commence par montrer que la clôture $\rb^*$ passe au contexte.

\begin{lemma} \label{lem:betaet} Si $u \rb^* u'$ alors $\lambda x. u \rb^* \lambda x. u'$ et $vu \rb^* vu'$ et $uv \rb^* u'v$.

\end{lemma}
\begin{proof} On montre par induction sur $n$ que si $u \rb^n u'$ alors \dots{} (simple).

\end{proof}


Maintenant montrons par induction sur la structure de $t$ que si $t \Rightarrow t'$ alors $t \rightarrow_\beta^* t'$


\begin{itemize}

\item si $t = x$ c'est vrai car $t' = x$ ;

\item si $t = \lambda x. u$ alors $t' = \lambda x. u'$ où $u \Rightarrow u'$. Par hypothèse d'induction il vient $u \rightarrow_{\beta}^* u'$ et on conclut que $\lambda x. u \rightarrow_{\beta}^* \lambda x. u'$ par le Lemme \ref{lem:betaet} ;

\item si $t = u v$ et que la dernière règle appliquée était $ u v \Rightarrow u' v'$, alors l'induction fournit $u \rb^* u'$ et $v \rb^* v'$. Par le Lemme \ref{lem:betaet} il vient $uv \rb^* u' v \rb^* u'v'$ ;

\item si $t = (\lambda x. u) v$ et que la règle était $t \Rightarrow u'[v'/x]$, alors $u \rb^* u'$ et $v \rb^* v'$ par hypothèse d'induction. Donc $(\lambda x. u) v \rb^* (\lambda x. u') v \rb^* (\lambda x. u') v' \rb u'[v'/x]$ modulo utilisation du Lemme \ref{lem:betaet} pour les deux premières étapes.
\end{itemize}

\end{enumerate}

\end{proof}

On en déduit que $\Rightarrow^* = \rb^*$. Il suffit donc de montrer la confluence de $\Rightarrow$ pour obtenir ce que l'on veut. En fait on va prouver que $\Rightarrow$ est fortement confluente.


\begin{lemma}[Substitution] \label{lem:subst} Si $t \Rightarrow t'$ et $s \Rightarrow s'$ alors $t[s/x] \Rightarrow t'[s'/x]$

\end{lemma}

\begin{proof} \item On montre le résultat par induction sur la structure de $t$.

\begin{itemize}

\item si $t$ est une variable c'est clair ;

\item si $t = \lambda y. u$, alors $t' = \lambda y. u'$ avec $u \Rightarrow u'$. Par induction on a $u[s/x] \Rightarrow u'[s'/x]$ et donc $\lambda y. (u[s/x])\Rightarrow \lambda y. (u'[s'/x])$. Comme $y$ est liée on peut supposer que $y \neq x$ ($\alpha$-équivalence) d'où $\lambda y. (u[s/x]) =(\lambda y. u)[s/x]$ et $\lambda y. (u[s'/x]) =(\lambda y. u')[s'/x]$ ;

\item si $t = u v$ et la dernière règle était $ u v \Rightarrow u' v'$, alors $u[s/x] \Rightarrow u'[s'/x]$ et $v[s/x] \Rightarrow v'[s'/x]$ par induction. D'où $(u[s/x])(v[s/x]) \Rightarrow (u'[s'/x])(v'[s'/x])$ ce qui conclut en factorisant les substitutions ;

\item si $t= (\lambda y. u) v$ et la règle était $t \Rightarrow u'[v'/y]$ alors $u[s/x] \Rightarrow u'[s'/x]$ et $v[s/x] \Rightarrow v'[s'/x]$ par induction. Donc $(\lambda y. (u[s/x]))v[s/x] \Rightarrow (u'[s'/x][v'[s'/x]/y])$.

D'une part on peut supposer que $y \neq x$ dans $(\lambda y. (u[s/x]))v[s/x]$ (par $\alpha$-équivalence car $y$ liée), et alors $(\lambda y. (u[s/x]))v[s/x] = ((\lambda y. u)v)[s/x]$ en factorisant.

Pour l'autre terme, on a $(u'[s'/x][v'[s'/x]/y]) = (u'[v'/y])[s'/x]$ dès que $y$ n'est pas libre dans $s'$. C'est effectivement le cas car $y$ n'est pas libre dans $s$ (pour substituer $t[s/x]$ sans capture) et $s \Rightarrow s'$ préserve cette propriété.

\end{itemize}

\end{proof}

Il ne reste plus qu'à conclure la preuve du théorème. On va montrer par induction sur $t$ que si $t \Rightarrow t'$ et
$t \Rightarrow t''$ alors il existe $s$ tel que $t' \Rightarrow s$ et $t'' \Rightarrow s$.

\begin{itemize}

\item si $t$ est une variable c'est immédiat ;

\item si $t = \lambda x u$ alors $t' = \lambda x. u'$ et $t'' = \lambda x. u''$. Par induction il existe $s_u$ tel que $u' \Rightarrow s_u$ et $u'' \Rightarrow s_u$, et alors $\lambda x. t' \Rightarrow \lambda x. s_u$ et $\lambda x. t'' \Rightarrow \lambda x. s$ ;

\item si $t = u v$ et les règles donnent $t' = u' v'$ et $t'' = u'' v''$ et par induction il existe $s_u$ et $s_v$ tel que $u' \Rightarrow s_u$ et $u'' \Rightarrow s_u$ et $v' \Rightarrow s_v$ et $v'' \Rightarrow s_v$. D'où $t' \Rightarrow s_u s_v$ et $t'' \Rightarrow s_u s_v$ ;

\item si $t = (\lambda x. u) v$ il reste deux cas non traités (le troisième est symétrique) :

\begin{itemize}

\item $t' = u'[v'/x]$ et $t'' = u''[v''/x]$. Par induction il existe $s_u$ et $s_v$ tels que $u' \Rightarrow s_u$ et $u'' \Rightarrow s_u$ et $v' \Rightarrow s_v$ et $v'' \Rightarrow s_v$. Alors en vertu du Lemme \ref{lem:subst}, $u'[v'/x] \Rightarrow s_u[s_v/x]$ et $u''[v''/x] \Rightarrow s_u[s_v/x]$, ce qui conclut.

\item $t' = u'[v'/x]$ et $t'' = (\lambda x. u'')v''$. Par induction il existe $s_u$ et $s_v$ tels que $u' \Rightarrow s_u$ et $u'' \Rightarrow s_u$ et $v' \Rightarrow s_v$ et $v'' \Rightarrow s_v$. Alors en vertu du Lemme \ref{lem:subst} $u'[v'/x] \Rightarrow s_u[s_v/x]$. D'autre part $(\lambda x. u'') v'' \Rightarrow s_u [s_v/x]$, ce qui conclut.

\end{itemize}

\end{itemize}

\rem

\begin{enumerate}

\item Il serait bon à l'oral d'admettre le Lemme \ref{lem:clot} (qui n'est pas trivial quand même), ainsi que le Lemme \ref{lem:subst} peut-être en partie.

\item On a donc unicité des formes normales quand elles existent. Liens avec la standardisation et le codage des entiers de Church.

\item Et si on essayait de montrer que $\rb$ est fortement confluente ? C'est faux ! En effet soit $ t:= ((\lambda x. (x x))((\lambda y. y)z)$ alors $t \rb ((\lambda x. (x x))z)$ et $t \rb ((\lambda y. y)z)((\lambda y. y)z)$ mais on ne peut pas les faire confluer en une seule étape.

\item Et si on essayait de montrer que $\rightarrow_{\beta}$ est localement confluente et termine (pour appliquer Newman) ? C'est faux elle ne termine pas ! Considérer $\Omega := (\lambda x. (x x))(\lambda x. (x x))$.

\item La $\beta \eta$-réduction est également confluente.

\item Soit $\rightarrow$ une relation de réécriture, et $\equiv$ la plus petite relation d'équivalence qui la contient. On dit que $\rightarrow$ vérifie la propriété de Church-Rosser lorsque $t \equiv t'$ implique l'existence de $s$ tel que $t \rightarrow s$ et $t' \rightarrow s$. Cette propriété est équivalente à la confluence.
 \end{enumerate}


\end{document}
