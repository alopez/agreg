\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{complexity}
\usepackage{hyperref}
%\usepackage{enumitem}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}
\usepackage{palatino}
\usepackage{pdfpages}
\usepackage{epigraph}

%%%% Format

\setlength{\columnsep}{2cm}

\newcommand{\lname}{Automate Boustrophédon}

\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}

\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}
 
\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}


%%%% Corps du fichier
\title{\lname}
\author{\textsc{Aliaume Lopez}}


\begin{document}

\maketitle

\vspace{1em}
\hrule
\vspace{1em}

\begin{tabular}{ll}
    \textbf{Difficulté~:} & $\star\star$ \\
    \textbf{Technicité~:} & $\star\star\star$ \\
    \textbf{Durée~:} & $\star\star$ \\
    \textbf{Recasages~:} & \cite{L909}, \cite{L913} \\
\end{tabular}

\vspace{1em}
\hrule
\vspace{1em}

\begin{ouverture}
    On a dans le plan de leçon les égalités suivantes~:

    \begin{equation*}
        \operatorname{Rec}_{\text{NFA}} \left(\Sigma^*\right)
        =
        \operatorname{Rec}_{\text{DFA}} \left(\Sigma^*\right)
        =
        \operatorname{Rec}_{\text{AFA}} \left(\Sigma^*\right)
        =
        \operatorname{Rat} \left(\Sigma^*\right)
    \end{equation*}

    Ce sont différentes manières d'exprimer une classe de langage 
    très robuste (stabilité par opérations élémentaires) et 
    relativement puissante (analyse lexicale). 

    L'objectif du développement est de montrer que modifier 
    légèrement les automates autorisés ne change pas la classe 
    de langage qui peuvent-êtres reconnus.
\end{ouverture}

\section{Définitions Préliminaires}

\begin{definition}[Automate Boustrophédon]
    Un automate boustrophédon non-déterministe est un quintuplet 
    $A = \langle \Sigma, Q, \delta, I, F \rangle$
    tel que~:

    \begin{compactenum}[(i)]
    \item $\Sigma$ est l'alphabet fini d'entrée 
    \item $Q$ est un ensemble fini d'états
    \item $\delta$ est une partie 
        de $Q \times (\Sigma \uplus \{ \bot \}) \times Q \times \{ \pm 1
        \}$
    \end{compactenum}
\end{definition}

\begin{definition}[Run sur un mot]
    Un run d'un automate boustrophédon $A$ sur un mot $w$ 
    en partant d'un état $q$ et d'une position $p \in \mathbb{Z}$
    est une suite $(q_n, p_n) \in Q\times \mathbb{Z}$
    qui vérifie~:

    \begin{compactenum}[(i)]
    \item La suite possède au moins un élément
    \item Le premier élément de la suite est $(q,p)$
    \item Si $(q_1, k_1) : (q_2, k_2)$ sont deux éléments 
        successifs dans la suite alors

        \begin{equation*}
            (q_1, w[k_1], q_2, k_2 - k_1) \in \delta
        \end{equation*}

        Avec pour convention $w[k] = \bot$ si $k$ n'est pas une position 
        valide dans $w$ (sortie des bords)
    \item Si une lettre $\bot$ est lue dans un état non final,
        alors la prochaine lettre est nécessairement dans le mot $w$
    \end{compactenum}
\end{definition}

\begin{definition}[Acceptation]
    Un mot $w \in \Sigma^*$ est accepté par un automate 
    boustrophédon $A$ si et seulement s'il existe 
    un run de $A$ sur $w$ partant d'un état initial 
    et terminant en position $|w|$ dans un état final 
    spécifique $q_f$.
\end{definition}

\begin{remarque}
    En particulier un automate peut accepter le mot vide si l'état 
    initial est acceptant. 
\end{remarque}

\section{Preuve du théorème}

Soit $A = \langle \Sigma, Q, \delta, I, F \rangle$
un automate boustrophédon que l'on suppose \emph{complet} pour simplifier 
la preuve.
On note $L = \mathcal{L} (A)$.
On va montrer que $L$ est saturé par une congruence d'indice fini. 

\begin{definition}
    Soit $w \in \Sigma^*$,
    on définit un run \emph{dans} $w$
    de $(q,p)$ à $(q',p')$ comme un run
    partant de $(q,p)$ et finissant en 
    $(q',p')$ tel que toutes les positions 
    sauf éventuellement la dernière soient 
    dans $w$.

    \begin{equation*}
        \lambda^{\rightarrow} (w) = 
        \left\{ 
            (q,q') \in Q^2 ~|~
            \exists r \text{ run de A dans } w
            \text{ de } (q,0) \text{ à } (q',|w|)
        \right\}
    \end{equation*}
    
    \begin{equation*}
        \lambda^{\leftarrow} (w) = 
        \left\{ 
            (q,q') \in Q^2 ~|~
            \exists r \text{ run de A dans } w
            \text{ de } (q,|w|-1) \text{ à } (q',-1)
        \right\}
    \end{equation*}

    \begin{equation*}
        \lambda^{\hookleftarrow} (w) = 
        \left\{ 
            (q,q') \in Q^2 ~|~
            \exists r \text{ run de A dans } w
            \text{ de } (q,0) \text{ à } (q',-1)
        \right\}
    \end{equation*}

    \begin{equation*}
        \lambda^{\hookrightarrow} (w) = 
        \left\{ 
            (q,q') \in Q^2 ~|~
            \exists r \text{ run de A dans } w
            \text{ de } (q,|w|-1) \text{ à } (q',|w|)
        \right\}
    \end{equation*}
\end{definition}


\begin{exemple}[Sur le mot vide]
    \begin{equation*}
        \lambda^{\rightarrow}(\varepsilon) = 
        \lambda^{\leftarrow}(\varepsilon) = 
        \lambda^{\hookrightarrow}(\varepsilon) = 
        \lambda^{\hookleftarrow}(\varepsilon) = 
        \emptyset 
    \end{equation*}
\end{exemple}

\begin{exemple}[Sur le mot d'une lettre]
    \begin{equation*}
        \lambda^{\rightarrow}(a) = 
        \left\{ 
            (q,q') \in Q^2 ~|~ 
            (q,a,q',+1) \in \delta
        \right\}
    \end{equation*}

    \begin{equation*}
        \lambda^{\leftarrow}(a) = 
        \left\{ 
            (q,q') \in Q^2 ~|~ 
            (q,a,q',-1) \in \delta
        \right\}
    \end{equation*}

    \begin{equation*}
        \lambda^{\hookrightarrow}(a) = 
        \lambda^{\leftarrow}(a)
    \end{equation*}

    \begin{equation*}
        \lambda^{\hookleftarrow}(a) = 
        \lambda^{\rightarrow}(a)
    \end{equation*}
\end{exemple}


\begin{definition}[Relation sur $\Sigma^*$]
    Si $w$ et $w'$ sont deux mots alors~:
    \begin{equation*}
        w \sim w' 
        \iff
        \forall x \in \{ \rightarrow, \leftarrow, \hookrightarrow,
        \hookleftarrow \}, \lambda^x (w) = \lambda^x (w')
    \end{equation*}

\end{definition}

\begin{propriete}[Relation d'équivalence d'indice fini]
    ~
    \begin{compactenum}
    \item La relation $\sim$ est une relation d'équivalence
    \item La relation $\sim$ est d'indice inférieur à $2^{ 4 |Q|^2}$
    \end{compactenum}
\end{propriete}

\begin{lemme}[La relation $\sim$ est une congruence]
    Attention, la congruence se fait 
    sur $\Sigma \uplus \{ \bot \}$
\end{lemme}

\begin{proof}
    Soient $u_1 \sim u_2$ et $v_1 \sim v_2$ tous différents de $\varepsilon$.
    Montrons par exemple que $\lambda^{\rightarrow} (u_1v_1) = 
    \lambda^{\rightarrow} (u_2 v_2)$\footnote{Les autres 
    cas se traitent de manière similaire}.

    On montre le résultat suivant par induction sur les 
    runs de $A$~:
    \begin{equation*}
        \lambda^{\rightarrow} (u_1 v_1) 
        \subseteq \lambda^{\rightarrow} (u_1) 
        (\lambda^{\hookleftarrow} (v_1) \lambda^{\hookrightarrow} (u_1))^*
        \lambda^{\rightarrow} (v_1)
    \end{equation*}
    
    On montre par induction l'inclusion réciproque~:
    \begin{equation*}
        \lambda^{\rightarrow} (u_1) 
        (\lambda^{\hookleftarrow} (v_1) \lambda^{\hookrightarrow} (u_1))^*
        \lambda^{\rightarrow} (v_1)
        \subseteq 
        \lambda^{\rightarrow} (u_1 v_1) 
    \end{equation*}

    Cela permet de conclure.

    \textbf{Pour les cas contenant $\varepsilon$~:}
    Si $u_1 = \varepsilon$, alors $u_1 v_1 = v_1$ 
    et donc $\lambda^{\rightarrow} (u_1 v_1) = \lambda^{\rightarrow} (v_1) 
    = \lambda^{\rightarrow}(v_2)$. Quitte à supposer l'automate complet,
    $\lambda^{\rightarrow}(u_2) = \emptyset$ implique $u_2 = \varepsilon$.
    On peut alors conclure. Les autres cas se traitent de manière 
    similaire.
\end{proof}

\begin{lemme}[La relation $\sim$ sature $L$]
\end{lemme}

\begin{proof}
    Soit $w \sim w'$ et $w \in L - \{ \varepsilon \}$,
    on a un run acceptant pour $w$ 
    qui part de $(q_0,0)$ et termine 
    en $(q_f,|w|)$.     
    Cela revient à dire que 
    \begin{equation*}
        (q_0, q_f) 
        \in 
        \lambda^{\rightarrow}(w) 
        (\lambda^{\hookleftarrow} (w) \lambda^{\hookrightarrow} (w))^*
        \lambda^{\rightarrow} (w)
    \end{equation*}

    On déduit donc  par la congruence 
    \begin{equation*}
        (q_0, q_f) 
        \in 
        \lambda^{\rightarrow}(w') 
        (\lambda^{\hookleftarrow} (w') \lambda^{\hookrightarrow} (w'))^*
        \lambda^{\rightarrow} (w')
    \end{equation*}

    Or à partir de cela on peut reconstruire un
    calcul acceptant de l'automate $A$ sur $w'$.
    On a donc bien $w' \in L$.

    Si $w = \varepsilon$, alors $w \sim w'$ force $w' = \varepsilon$. On 
    a donc $w' = w \in L$.
\end{proof}

\section{Annexe palindromes}

\begin{lemme}
    Un automate déterministe qui reconnait mots dont 
    les préfixes de taille $n$ sont des palindromes
    possède au moins $2^{n/2}$ états si $n$ est impair
    (sur l'alphabet $\{ a,b \}$)
\end{lemme}

\begin{proof}
    Si $n = 2 m + 1$ on construit 
    $f : \Sigma^m \to Q$
    qui à $u$ associe le $m+1$-ème état 
    dans un run de l'automate sur $u 0 \overline{u}$.

    Cette fonction est injective et cela donne directement 
    la minoration attendue.
\end{proof}

\begin{lemme}
    On peut construire un automate boustrophédon 
    qui reconnait mots dont les préfixes 
    de taille $n$ sont des palindromes 
    avec $n(n+1)/2 + 1$ états
\end{lemme}

\begin{remarque}[Serge]
Via Wikipedia, j'ai appris que la complexité de la déterminisation
d'u automate boustrophédon est un problème ouvert depuis un papier STOC 1978 :
polynomiale ou exponentielle ?
\end{remarque}

\section{Annexe Effectivité}

On peut effectivement construire les classes d'équivalence 
comme des quadruplets de parties de $Q^2$.

L'état initial est la classe du mot $\varepsilon$
qui a déjà été calculée. Les états finaux sont 
les états $(A,B,C,D)$ tels que 

\begin{equation*}
    \exists q_0 \in I,
    (q_0, q_f) \in A( C D )^* A
\end{equation*}

Ce qui se calcule simplement car on sait calculer 
la clôture transitive d'une relation.

De plus, on sait que $[ua] = [u] \cdot [a]$ 
et comme on a calculé la classe d'une lettre 
on a un moyen effectif pour calculer la classe 
d'un mot $w$.

\appendix

\bibliographystyle{abstract}
\bibliography{ressources}

\end{document}
