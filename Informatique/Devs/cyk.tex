\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{complexity}
\usepackage{hyperref}
%\usepackage{enumitem}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}
\usepackage{palatino}
\usepackage{pdfpages}
\usepackage{epigraph}
\usepackage{algorithm}
\usepackage{algorithmic}

%%%% Format

\setlength{\columnsep}{2cm}

\newcommand{\lname}{Intersection d'un langage algébrique et rationnel.
Application à CYK}

\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Gabriel Lebouder}}
\chead{}
\rfoot{}
\pagestyle{fancy}

\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}

\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}

\renewcommand{\A}{\mathcal A}
\renewcommand{\L}{\mathcal L}
\newcommand{\xr}{\xrightarrow}

%%%% Corps du fichier
\title{\lname}
\author{\textsc{Aliaume Lopez}}


\begin{document}

\maketitle

\vspace{1em}
\hrule
\vspace{1em}

\begin{tabular}{ll}
    \textbf{Difficulté~:} & $\star$ \\
    \textbf{Technicité~:} & $\star$ \\
    \textbf{Durée~:} & $\star\star\star$ \\
    \textbf{Recasages~:} & \cite{L923}
\end{tabular}

\vspace{1em}
\hrule
\vspace{1em}

\begin{ouverture}
    On veut déterminer si un programme est syntaxiquement valide. Cela
    correspond à l'appartenance à une grammaire algébrique. Pour montrer
    cette appartenance, on peut démontrer de manière plus générale
    que l'intersection d'un langage rationnel (qui correspondra par exemple
    au mot trivial) et d'une grammaire algébrique (syntaxe du langage de
    programmation) est encore une grammaire algébrique. On pourra ensuite
    tester la vacuité de cette grammaire. Cela démontre une fois de plus
    que les propriétés de clôture sont souvent utiles pour trouver des
    algorithmes efficaces.
\end{ouverture}

\section{Intersection}

On fixe dans toute la suite $\Sigma$ un alphabet fini non vide.

\begin{definition}[Rappel : Automate normalisé]
  Un automate $\A = \langle \Sigma, Q, \delta, I, F \rangle$ est dit
  normalisé si :
  \begin{itemize}
    \item $I$ est un singleton $\{i\}$
    \item $\delta \cap (Q \times \Sigma \times I) = \emptyset$
    \item $F$ est un singleton $\{f\}$
    \item $\delta \cap (F \times \Sigma \times Q) = \emptyset$
  \end{itemize}
\end{definition}

\begin{lemme}[Normalisation d'un automate]
  On peut normaliser un automate en temps linéaire.
\end{lemme}
\begin{proof}
  Soit $\A = \langle \Sigma,Q, \delta, I, F \rangle$
  un automate quelconque.\\
  On construit $\A' = \langle \Sigma, Q', \delta', \{i_0\}, \{f_0\} \rangle$
  défini par :
  \begin{itemize}
    \item $i_0$ et $f_0$ des états frais
    \item $Q' = Q \cup \{i_0, f_0\}$
    \item $\delta' =
      \delta \cup
      \{ (i_0,a,q)\ |\ \exists i\in I, (i,a,q)\in\delta \} \cup
      \{ (q,a,f_0)\ |\ \exists f\in F, (q,a,f)\in F \}$
  \end{itemize}
  Par construction, cet automate est normalisé, et la construcion
  est bien linéaire en la taille de l'automate initial. De plus
  on a $|\A'|\leq |\A|*3$.
  Enfin, cet automate a bien pour langage le langage de $\A$ :
  \begin{itemize}
    \item $\L(\A) \subset \L(\A')$ : soit $w\in\L(\A)$\\
      Soit $i \xr[]{w_1} q_1 \xr[]{w_2} \cdots \xr[]{w_{n-1}} q_n \xr[]{w_{n-1}} f$
      une dérivation de $w$ dans $\A$.\\
      Alors $i_0 \xr{w_1} q_1 \xr{w_2} \cdots \xr{w_{n-1}} q_n \xr{w_n} f_0$
      est une dérivation de $w$ dans $\A'$.\\
      Ainsi, $w\in\L(\A')$
    \item $\L(\A') \subset \L(\A)$ : soit $w\in\L(\A')$\\
      Soit $i_0 \xr{w_1} q_1 \xr{w_2} \cdots \xr{w_{n-1}} q_n \xr{w_n} f_0$
      une dérivation de $w$ dans $\A'$.\\
      Soient maintenant $i\in I$ et $f\in F$ tels que
      $(i, w_1, q_1)\in\delta$ et $(q_n, w_{n-1}, f)\in\delta$\\
      Alors $i \xr[]{w_1} q_1 \xr[]{w_2} \cdots \xr[]{w_{n-1}} q_n \xr[]{w_n} f$
      est une dérivation de $w$ dans $\A$.\\
      Ainsi, $w\in\L(\A)$
  \end{itemize}
\end{proof}

\begin{definition}[Rappel : Forme normale de Chomsky]
  Une Grammaire $G = \langle \Sigma, V, P \rangle$ est dite
  sous forme normale de Chomsky si $P\subset V\times (V^2+\Sigma).$
\end{definition}

\begin{lemme}[Mise sous forme normale de Chomsky]
  Une grammaire ne reconnaissant pas le mot vide peut être
  mise sous forme normale de Chomsky en temps linéaire.
\end{lemme}
\begin{proof}
  On va seulement donner ici les éléments principaux
  de la construction de la grammaire équivalente :
  \begin{itemize}
    \item Élimination de l'axiome dans les membres droits des règles.\\
      Ceci en ajoutant un nouveau symbole $S_0 \to S$.\\
      Celà double au plus la taille de la grammaire.
    \item Suppression des lettres terminales dans les membres
      droits des règles de longueur au moins 2\
      Ceci en ajoutant des nouvelles variables $N_a \to a$.
      Cela double au plus la taille de la grammaire.
    \item Suppression des membres droits ayant au plus deux symboles.\\
      Ceci en construisant $n-1$ nouveaux symboles pour chaque règle de taille $n$.
      Cela triple au plus la taille de la grammaire.
    \item Élimination des $\epsilon$-règles.\\
      Ceci se fait en recharchant dans un premier temps les variables se
      réduisant en $\epsilon$, en supprimant les $\epsilon$-règles, puis
      pour chaque règle contenant une ou plusieurs de ces variables, en
      ajoutant cette règle par toutes les règles obtenables en supprimant
      une ou plusieurs de ces variables.
    \item Élimination des règles unités.
  \end{itemize}
\end{proof}


\begin{definition}[Grammaire d'intersection]
  Soit $\A = \langle \Sigma, Q, \delta, \{i\}, \{f\} \rangle$ un automate normalisé et $G=\langle \Sigma, V, P\rangle$ une grammaire sous forme normale de Chomsky.\\
  On définit $G_{\A} = \langle \Sigma, V_{\A}, P_{\A} \rangle$
\end{definition}

%\begin{definition}[Grammaire d'intersection]
%    Soit $A = \langle \Sigma, Q, \delta, q_0, F \rangle$
%    un automate fini et $G = \langle \Sigma, V, P \rangle$
%    une grammaire.
%
%    On définit $G_A = \langle \Sigma, V_A, P_A \rangle$ la grammaire suivante~:
%
%    \begin{equation*}
%        \begin{cases}
%            V' =& \left\{ X_{p,q} ~|~ X \in (V \uplus \Sigma), p \in Q, q \in Q
%            \right\} \uplus \\
%                & \{ \hat{S} \} \\
%            P' =& \left\{ a_{p,q} \to a ~|~ p \overset{a}{\to} q \right\} \uplus \\
%                & \left\{ X_{p,q} \to X^1_{p,p_1} \dots X^i_{p_n,q} ~|~ X \to
%                        X^1 \dots X^i, (q,p_1, \dots, p_n, q) \right\} \uplus \\
%                        & \left\{ \hat{S} \to S_{q_0, q_f} ~|~ q_f \in F
%                    \right\}
%        \end{cases}
%    \end{equation*}
%\end{definition}


\begin{lemme}[Première inclusion]
    \begin{equation*}
        \mathcal{L}(G) \cap \mathcal{L}(A) \subseteq \mathcal{L} (G_A)
    \end{equation*}
\end{lemme}

\begin{lemme}[Deuxième inclusion]
    \begin{equation*}
        \mathcal{L} (G_A) \subseteq \mathcal{L}(G) \cap \mathcal{L}(A)
    \end{equation*}
\end{lemme}

\section{Algorithme CYK}

\begin{lemme}[Forme Normale de Chomsky]
    On peut mettre une grammaire en forme normale
    de Chomsky en temps linéaire ?
\end{lemme}

\begin{lemme}[Vacuité]
    On peut tester la vacuité d'une grammaire algébrique
    en temps linéaire
\end{lemme}

\begin{lemme}[CYK]
    L'algorithme CYK consiste, à partir d'une grammaire
    en forme normale de chomsky $G$ à tester la vaciuité
    de l'intersection de cette grammaire avec l'automate $A$
    correspondant à un mot $m$
\end{lemme}

\appendix

\bibliographystyle{abstract}
\bibliography{ressources}



\end{document}
