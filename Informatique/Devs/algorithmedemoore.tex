\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{complexity}
\usepackage{hyperref}
%\usepackage{enumitem}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}
\usepackage{palatino}
\usepackage{pdfpages}
\usepackage{epigraph}
\usepackage{algorithm}
\usepackage{algorithmic}

%%%% Format
\setlength{\columnsep}{2cm}

\newcommand{\lname}{Algorithme de Moore}

\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}

\setlength{\headheight}{35pt}
\lhead{Algorithme de Moore}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}

\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}


\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}
 
\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}


%%%% Corps du fichier
\title{\lname}
\author{\textsc{Aliaume Lopez}}


%%%% Corps du fichier


\begin{document}
\maketitle

\vspace{1em}
\hrule
\vspace{1em}

\begin{tabular}{ll}
    \textbf{Difficulté~:} & $\star$ \\
    \textbf{Technicité~:} & $\star$ \\
    \textbf{Durée~:} & $\star\star\star$ \\
    \textbf{Recasages~:} & \cite{L916}, \cite{L924}
\end{tabular}

\vspace{1em}
\hrule
\vspace{1em}

\begin{ouverture}
    L'algorithme de Moore de minimisation d'un automate fini est un algorithme
    qui calcule l'automate fini déterministe complet minimal équivalent à un automate 
    fini déterministe complet donné.
    
    Dans ce cadre on fera attention aux propriétés suivantes:
    \begin{enumerate}
        \item Les automates sont \emph{déterministes} et \emph{complets}. 
        \item L'automate minimal est l'automate déteriminste complet 
            qui reconnaît le même langage avec un nombre minimal 
            d'états. Il est unique à isomorphisme près.
    \end{enumerate}

    L'intérêt d'un tel algorithme est multiple:
    \begin{enumerate}
        \item Cet algorithme calcule un représentant \emph{canonique} 
            pour un langage rationnel donné (comparaison de langages etc)
        \item Ce représentant est de taille minimale dans la classe 
            d'automate considérés (compression, rapidité d'exécution etc)
    \end{enumerate}
\end{ouverture}

\subsection*{Définitions préliminaires}

\begin{definition}[Automate déterministe complet]
    Un automate déterministe complet $\mathcal{A}$
    est défini par un ensemble fini d'états $Q$
    un alphabet d'entrée $\Sigma$, un état initial $q_0 \in Q$,
    un ensemble d'états finaux $F \subseteq Q$ et une fonction 
    (totale) de transition $\delta : Q \times \Sigma \to Q$.

    On note $\mathcal{A} = (Q, \Sigma, \delta, q_0, F)$. De plus 
    si $q \in Q$ et $a \in \Sigma$ on note $q \cdot a$ la valeur 
    $\delta(q,a)$. Si $w$ est un mot fini sur $\Sigma$ on étend 
    la notation de la manière suivante par induction 
    sur $w$~: \begin{inparaenum}[(i)]
    \item $q \cdot \varepsilon = q$ \item $q \cdot (aw) = (q \cdot a) \cdot w$
    \end{inparaenum}
\end{definition}

\begin{definition}[Langage d'un état]
    Ayant fixé un automate déterministe complet $\mathcal{A}$, 
    on note $L_q$ l'ensemble des mots sur $\Sigma^*$
    acceptés par l'état $q$~:

    \begin{equation*}
        L_q = \{ w \in \Sigma^* ~|~ q \cdot w \in F \}
    \end{equation*}
\end{definition}

\begin{remarque}
    Dans un automate complet déterministe $\mathcal{A}$
    $q \in F$ si et seulement si $\varepsilon \in L_q$.
\end{remarque}

\begin{definition}[Résiduels]
    Si $L$ est une partie de $\Sigma^*$ et $w \in \Sigma^*$
    on définit le résiduel de $L$ par $w$ comme suit~:

    \begin{equation*}
        w^{-1} L = \{ u \in \Sigma^* ~|~ wu \in L \}
    \end{equation*}
\end{definition}

\begin{definition}[Automate des résiduels]
    Si $L$ est une partie de $\Sigma^*$ on définit l'automate 
    des résiduels $\mathcal{A}_L$ comme suit~:

    \begin{compactenum}
        \item $Q = \{ w^{-1} L ~|~ w \in \Sigma^* \}$
        \item $F = \{ q \in Q ~|~ \varepsilon \in q \}$
        \item $q_0 = L$
        \item $\delta(q,a) = a^{-1} q$ 
    \end{compactenum}

    Si $Q$ est fini c'est un automate déterministe complet.
\end{definition}

\begin{lemme}
    L'automate des résiduels reconnaît exactement le langage $L$
\end{lemme}

\begin{lemme}
    Si on se place dans un automate déterministe complet $\mathcal{A}$
    alors pour tout état $q$ et tout mot $w$ on a l'égalité 
    suivante $L_{q \cdot w} = w^{-1} L_q$.
\end{lemme}

\begin{lemme}
    Si un langage $L$ est reconnaissable par un automate 
    fini déterministe alors l'ensemble de ses résiduels est fini.
\end{lemme}

\begin{definition}[Morphisme]
    Si $\mathcal{A_1}$ et $\mathcal{A_2}$ sont des automates 
    déterministes complets, on dit qu'une application 
    $\phi : Q_1 \to Q_2$ est un morphisme quand~:

    \begin{compactenum}
        \item $\phi (q \cdot a) = \phi(q) \cdot a$
        \item $\forall q \in F_1, \phi (q) \in F_2$
    \end{compactenum}
\end{definition}

\begin{lemme}[Des morphismes]
    Si $\phi$ est un morphisme entre les automates $\mathcal{A}_1$ 
    et $\mathcal{A}_2$ alors le langage reconnu par $\mathcal{A}_1$ est inclus 
    dans le langage reconnu par $\mathcal{A}_2$.

    De plus si $\phi$ est un isomorphisme alors les langages 
    reconnus par les deux automates sont égaux.
\end{lemme}

\begin{lemme}
    Si $L$ est un langage reconnaissable l'automate $\mathcal{A}_L$
    possède un nombre minimal d'états parmis les automates déterministes 
    complets et est unique à renommage des états près. 
    
    De plus 
    il existe un morphisme (surjectif) canonique $\phi_L$ de tout automate $\mathcal{A}$
    reconnaissant $L$ vers $\mathcal{A}_L$ définit par~:

    \begin{equation*}
        \phi_L (q) = L_q
    \end{equation*}
\end{lemme}

\subsection*{Développement}

Dans cette section, automate est synonyme d'automate fini déterministe complet.

L'objectif est de réaliser un calcul effectif de l'automate minimal 
sans avoir à calculer les résiduels du langage (ce qui est coûteux).
Pour cela on va réaliser l'automate minimal à partir d'un automate 
quelconque en utilisant un \emph{quotient} dont le calcul est plus simple.

\begin{definition}[Congruence]
    Une relation d'équivalence est une congruence/bisimulation 
    sur un automate $\mathcal{A}$ est une relation qui vérifie~:
    
    \begin{enumerate}
        \item 
            \begin{equation*}
                p \sim q \implies (p \in F \iff q \in F)
            \end{equation*}
        \item 
            \begin{equation*}
                p \sim q \implies \forall a \in \Sigma, p\cdot a \sim q \cdot a 
            \end{equation*}
    \end{enumerate}
\end{definition}

\begin{lemme}[Puissance des congruences]
    Dans un automate $\mathcal{A}$ fixé, 
    et si $(q,q') \in Q^2$ alors~:

    \begin{equation*}
        L_q = L_{q'} \iff \exists R \textrm{ congruence}, qRq'
    \end{equation*}
\end{lemme}

\begin{proof}
    ~
    \begin{description}
        \item[Sens implique~:] On construit 
            une congruence appellée la congruence de Nerode
            comme suit~:

            \begin{equation*}
                R = \{ (q,q') \in Q^2 ~|~ L_q = L_{q'} \}
            \end{equation*}

            Par construction $R$ est symétrique réflexive et transitive, et 
            si $q R q'$ alors $q \in F$ implique $\varepsilon \in L_q$
            ce qui veut dire que $\varepsilon \in L_{q'}$ ce qui 
            force $q' \in F$.

            De plus si $L_q = L_{q'}$ et $a \in \Sigma$ alors~:
            
            \begin{equation*}
                L_{q \cdot a} = a^{-1} L_q = a^{-1} L_{q'} = L_{q' \cdot a}
            \end{equation*}

            Et donc $(q\cdot a) R (q' \cdot a)$. On peut conclure 
            que $R$ est une congruence, et on remarque que $q R q'$.

        \item[Sens réciproque~:]
            Soit $R$ une congruence, et $q R q'$. 
            Soit $w \in L_q$, on sait que $q \cdot w \in F$, or 
            par induction sur $w$ comme $R$ est une congruence 
            cela veut dire que $q' \cdot w \in F$, on conclut 
            donc $w \in L_q \implies w \in L_{q'}$. Comme la relation 
            est symétrique l'autre inclusion suit directement.

    \end{description}
\end{proof}

\begin{definition}[Automate quotient]
    Soit $\mathcal{A} = (Q, \Sigma, \delta, q_0, F)$ un automate 
    et $\sim$ une relation congruence sur $\mathcal{A}$.
    Alors l'application $\delta$ passe au quotient et on peut définir 
    $\bar{\delta} : Q/\sim \times \Sigma \to Q/\sim$, ainsi que l'automate 
    $\bar{\mathcal{A}} = (Q/\sim, \Sigma, \bar{\delta}, [q_0], F/\sim)$.
    On a noté $[q_0]$ la classe de $q_0$ pour $\sim$ dans $Q / \sim$.
\end{definition}

\begin{lemme}[Quotient de Nerode]
    Si $\mathcal{A}$ est un automate et $R$ est la congruence de Nerode sur 
    $\mathcal{A}$ alors l'automate quotient $\bar{\mathcal{A}}$
    reconnait le même langage que $\mathcal{A}$.
\end{lemme}

\begin{proof}
    Notons $L$ le langage reconnu par l'automate $\mathcal{A}$.
    On sait qu'il existe un morphisme $\phi_L$ surjectif qui 
    relie $\mathcal{A}$ avec $\mathcal{A}_L$.

    \begin{enumerate}[(i)]
        \item
            Comme la congruence de Nerode est une congruence et 
            cela permet de construire le quotient $\bar{\phi_L}$ 
            et de conserver la propriété de morphisme.

        \item 
            De plus, le morphisme $\bar{\phi_L}$ est \emph{injectif}
            car $\phi_L (q) = \phi_L (q')$ si et seulement si $L_q = L_{q'}$
            ce qui veut dire que $q \sim q'$. Ainsi $\bar{\phi_L}$ est un
            isomorphisme et les langages reconnus sont identiques.
    \end{enumerate}
\end{proof}




Il reste simplement à savoir calculer l'équivalence de Nerode sur un automate
donné. C'est ce que fait l'algorithme en figure 1. 

\begin{lemme}[Correction et temps de calcul]
    L'algorithme de Moore calcule la congruence de Nerode et 
    termine en temps $\mathcal{O}(n^2)$.
\end{lemme}

\begin{proof}
    ~
    \begin{description}
        \item[Correction] À chaque étape l'algorithme 
            calcule une partition des états $Q$ qui vérifie 
            \begin{enumerate}[(i)]
                \item Si $q$ est dans la même classe que $q'$ alors 
                    $q \in F \iff q' \in F$

                \item Comme c'est une partition, la relation définie 
                    est automatiquement une relation d'équivalence à 
                    chaque étape

                \item À l'étape $n$ on peut constater que $qRq'$ 
                    si et seulement si pour tout mot $w$ de taille plus 
                    petite que $n$, $q \cdot w \in F \iff q' \cdot w \in F$.

                \item À la sortie de la boucle 
                    la relation $R$ définie par la partition vérifie 
                    la propriété suivante~:

                    \begin{equation*}
                        \forall a \in \Sigma, 
                        \forall qRq', 
                        (q \cdot a) R (q' \cdot a)
                    \end{equation*}
            \end{enumerate}

            On peut déduire que l'algorithme calcule une congruence.
            De plus il calcule exactement la congruence de Nerode.

        \item[Complexité] À chaque tour de boucle le nombre d'ensembles dans la
            partition augmente strictement, mais ce nombre est borné par $|Q|$
            et donc l'algorithme termine. On constate que l'on fait au plus
            $|Q|$ tours dans la boucle, et donc la complexité si la séparation 
            est faite naïvement est en~: 

            \begin{equation*}
                \mathcal{O}( |Q| \times |\Sigma| \times |Q|^2 )
            \end{equation*}
    \end{description}
\end{proof}

\begin{figure}
\begin{verbatim}
    P := { F, F^c } 
    While a modification is made 
        For a in Sigma
            For C class in P
                Split C with a in P
\end{verbatim}
\caption{Algorithme de Moore}
\end{figure}

\begin{conclusion}
    On a montré que l'existence d'un automate minimal est non seulement 
    un outil théorique intéressant, mais aussi un outil \emph{constructif}.
    Cette construction n'étant pas très coûteuse, elle peut s'avérer
    intéressante pour répondre à un grand nombre de problèmes sur les 
    automates (équivalence de langage, apériodicité...)
\end{conclusion}

\begin{ouverture}
\begin{remarque}
    Il est possible d'améliorer la complexité en considérant une structure 
    de données différente. On numérote les états de $Q$ par des entiers
    entre $1$ et $|Q|$, et la partition $P$ est représentée par un tableau 
    de $|Q|$ éléments où les partitions sont des segments, dont les bornes
    sont maintenues dans une liste séparée $I$, on conserve de plus un tableau 
    de taille $|Q|$ qui à un élément associe son représentant canonique
    (d'indice minimal dans son segment).

    De cette manière, une opération de raffinement peut se faire avec une
    adaptation du tri par bit de poids faible. Pour séparer les éléments 
    par une lettre $a$ à l'intérieur d'une partition, on considère le premier 
    élément de la partition et on compare simplement les représentants 
    canoniques en les mettant à jour au fur et à mesure de la progression.

    On peut alors obtenir un temps $|Q|$ pour la séparation par une lettre
    $a \in \Sigma$ ce qui donne à l'algorithme total une complexité 
    $\mathcal{O} \left( |\Sigma| \times |Q|^2 \right)$.
\end{remarque}

\begin{remarque}
    Il est possible d'utiliser l'algorithme de Hoprcoft qui permet d'avoir une 
    meilleure complexité grâce à un raffinement plus intelligent des partitions.
    La complexité obtenue est alors $\mathcal{O}(|\Sigma| | Q| \ln |Q| )$.
\end{remarque}

\begin{remarque}
    Il est possible d'utiliser une version plus faible des résultats précédents 
    pour calculer, non pas l'équivalence de Nerode, mais la plus petite 
    congruence qui identifie deux états, ce qui permet de faire des tests 
    plus rapides: c'est la théorie des bisimulations dans les automates.
\end{remarque}
\end{ouverture}


\appendix

\bibliographystyle{abstract}
\bibliography{ressources}

\end{document}
