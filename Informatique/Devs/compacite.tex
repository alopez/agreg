\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{complexity}
\usepackage{hyperref}
%\usepackage{enumitem}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}
\usepackage{palatino}
\usepackage{pdfpages}
\usepackage{epigraph}
\usepackage{algorithm}
\usepackage{algorithmic}

%%%% Format

\setlength{\columnsep}{2cm}

\newcommand{\lname}{Compacité logique}

\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}

\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}
 
\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}


%%%% Corps du fichier
\title{\lname}
\author{\textsc{Aliaume Lopez}}


\begin{document}

\maketitle

\vspace{1em}
\hrule
\vspace{1em}

\begin{tabular}{ll}
    \textbf{Difficulté~:} & $\star$ \\
    \textbf{Technicité~:} & $\star$ \\
    \textbf{Durée~:} & $\star\star\star$ \\
    \textbf{Recasages~:} & \cite{L916}, \cite{L924}
\end{tabular}

\vspace{1em}
\hrule
\vspace{1em}

\begin{ouverture}
    Faire une ouverture 
\end{ouverture}

\section{Introduction}

\begin{definition}[Théorème de compacité]
    Soit $\mathcal{T}$ une théorie, alors 
    les deux propositions suivantes sont 
    équivalentes~:

    \begin{compactenum}[(i)]
    \item La théorie $\mathcal{T}$ a un modèle 
    \item Toute sous théorie finie de $\mathcal{T}$
        a un modèle 
    \end{compactenum}
\end{definition}

\begin{remarque}
    Ce résultat utilise le théorème de complétude,
    qui lui-même dépend (dans le cas général)
    de l'axiome du choix.
\end{remarque}

\section{Applications}

\begin{lemme}[Encoder un ordre total]
    Il existe une fonction $f$ qui à tout ensemble $E$ associe 
    une théorie $f(E)$ et qui vérifie les propriétés~:

    \begin{compactenum}
        \item L'ensemble $E$ est totalement ordonnable si et seulement si 
            $f(E)$ admet un modèle 
        \item La fonction $f$ est croissante pour l'inclusion 
        \item Pour toute sous partie finie $\mathcal{T}$ de $f(E)$
            il existe une partie finie $E'$ 
            de $E$ telle que $\mathcal{T} \subseteq f(E')$.
    \end{compactenum}
\end{lemme}

\begin{proof}
    On construit la fonction et les propriétés à vérifier seront admises.
    Soit $E$ un ensemble, 
    on construit les formules suivantes~:

    \begin{compactenum}
    \item Pour $(x,y) \in E^2$, la formule $x \leq y \vee y \leq x$
    \item Pour $(x,y,z) \in E^3$, la formule $x \leq y \wedge y \leq z \implies
        x \leq z$
    \item Pour $x \in E$, la formule $x \leq x$
    \item Pour $(x,y) \in E^2$, la formule $x \leq y \wedge y \leq x \implies x
        = y$
    \end{compactenum}

    Cette théorie admet un modèle si et seulement si l'ensemble admet 
    un ordre total de manière évidente.
\end{proof}

\begin{theoreme}[Ensembles totalement ordonnés]
    Tout ensemble peut être muni d'un ordre total
\end{theoreme}
\begin{proof}
    Soit $E$ un ensemble, montrons que $f(E)$ admet un modèle.
    Soit $\mathcal{T}$ une sous-théorie \emph{finie} de $f(E)$, on peut la 
    compléter en une théorie \emph{finie} $f(E')$ avec $E'$ un sous 
    ensemble fini de $E$. Comme tout ensemble fini est totalement 
    ordonnable, on sait que $f(E')$ admet un modèle, et donc $\mathcal{T}$
    admet un modèle. On peut alors conclure. 
\end{proof}

\begin{theoreme}[Coloriage]
    Un graphe est $k$-coloriable si et seulement 
    si toute partie finie de ce graphe 
    est $k$-coloriable 
\end{theoreme}

\begin{proof}
    On construit une fonction exactement comme dans le cas des ensembles 
    totalement ordonnés. Pour cela, à un graphe $G = (V,E)$ et un nombre $k$
    de couleurs on associe 
    l'ensemble de formules suivant~:

    \begin{compactenum}
    \item Pour $v \in V$, $\bigvee_{i = 1}^k c_k (v)$
    \item Pour $v \in V$, pour $1 \leq i \leq k$, $c_i (v) \implies \bigwedge_{j
        \neq i} \neg c_j (v)$
    \item Pour $(u,v) \in E$, $e(u,v)$
    \item Pour $(u,v) \not \in E$, $\neg e(u,v)$
    \item Pour $(u,v) \in V$, $1 \leq i \leq k$, $e(u,v) \wedge c_i (v) \implies
        \neg c_i(u)$
    \end{compactenum}
\end{proof}

\section{Annexe compacité}

\begin{propriete}[LTL n'est pas compacte]
\end{propriete}

\textbf{Question~:} La logique du second ordre est-elle compacte ?

\section{Annexe ordres et coloriages}

\begin{itemize}
    \item L'axiome du choix donne déjà un bon ordre 
        sur tout ensemble, et donc en particulier
        un ordre total
    \item Tout graphe planaire est $4$-coloriable
    \item Si on se restreint à la logique propositionnelle,
        alors le théorème de compacité est en effet 
        une application du théorème de Tychonoff sur 
        des arbres sémantiques.
\end{itemize}


\appendix

\bibliographystyle{abstract}
\bibliography{ressources}



\end{document}
