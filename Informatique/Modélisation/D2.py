
import itertools

EXAMPLES = [
        ("441", True),
        ("433", False),
        ("432", False),
        ("333", True),
        ("423", True),
        ("3333", True),
        ("4233", True),
        ("4413", True),
        ("5313", True),
        ("5340", True) ] 

def wordToArray (s):
    """ Convert a string s
        into an array of integers 
        between 0 and 9
    """
    return [ (ord (a) - 48) % 9 for a in s ] 

def isPermutation (t):
    """ Check if the array t 
        is a bijection 
        from {0,..., len(t) - 1} 
        into {0,..., len(t) - 1}

        O(n)
    """
    b = [ False for i in t ]
    for i in t:
        b[i] = True
    
    return all(b)

def permutationTest (w):
    """ Checks if the word w 
        is a "jonglable" word 
        using the permutation test

        O (|w|)
    """
    p = len (w)
    s = [ (a + k) % p for (a,k) in enumerate (w) ]

    return isPermutation (s)

def numberOfExpectedBalls (w):
    """ Finds the expected number 
        of balls to use with the 
        word w
    """
    k = [ a for a in w ] # copy
    m = len (k)
    b = 0
    for (i,a) in enumerate (k):
        if a == None:
            pass 
        elif a == 0:
            k[i] = None
        else:
            b += 1
            for j in range (i, m, a):
                k[j] = None
    return b


def meanTest (w):
    """ Checks if the word w 
        is a "jonglable" word 
        using the mean test 
    """
    size = len (w)
    mean = sum (w) / len (w)

    if mean.is_integer ():
        m = int (mean) # force conversion 
        return (m == numberOfExpectedBalls (w))
    else:
        return False

def checkTesterWith (testFunc, examples):
    fails = 0
    for (word,expectedResult) in examples:
        res = testFunc (wordToArray (word))
        if res != expectedResult:
            fails += 1
            print ("Failure on {} ... expecting {} returned {}".format
                    (word,expectedResult,res))
    print ("{} failures on {} examples".format (fails, len (examples)))



########
#####
### ENUMERATE BP(b,p)
####
#######

def wordEnumerate (p):
    # Starting word 
    i = 0
    while i < 10**p:
        s    = str (i)
        word = "0" * (p - len (s)) + s
        w    = wordToArray (word)
        yield w 
        i += 1

def testMeanEfficiency (p):
    errs  = 0
    count = 0
    for w in wordEnumerate (p):
        count += 1
        if permutationTest (w) != meanTest (w):
            errs += 1

    print ("{} errors on {} words ({} %)".format (errs,count, int(errs / count *
        100)))
# PRECOMPILED RESULTS
# testMeanEfficiency (6) =  69891 errors on  1000000 words (6 %)
# testMeanEfficiency (7) = 516693 errors on 10000000 words (5 %) 



def naiveBPEnumerate (b,p):
    for w in wordEnumerate (p):
        if sum (w) == b*p and permutationTest (w):
            yield w 

# 49 results for (3,3)

def permBPEnumerate (b,p):
    # For each permutation 
    for permutation in itertools.permutations (list (range (p))):
        # Enumerate the pre-image of the permutation
        sols = [ ]
        for i in range (p):
            ap = [ ] 
            A = permutation[i] - i
            # 0 <= σ(i) < p
            # 0 <= i    < p
            # donc -p <= σ(i) - i < p
            for k in range (-A, 9- A + 1):
                if k % p == 0 and 0 <= A + k and A + k <= 9:
                    ap.append (A + k)
            sols.append (ap)

        # Construct all the possible arrangements
        # for preimage in itertools.product (*sols):
            # l = list (preimage)
            # if sum (l) == b * p:
                # yield l

        exists = any ([ sum (list (preimage)) == p * b
                for preimage in itertools.product (*sols) ])
                
        if not exists:
            print ("La permutation {} n'a pas d'antécédent".format
                    (permutation))

# 37 results for (3,3)
