import math
import itertools
import random

def partition(collection):
    if len(collection) == 1:
        yield [ collection ]
        return

    first = collection[0]
    for smaller in partition(collection[1:]):
        # insert `first` in each of the subpartition's subsets
        for n, subset in enumerate(smaller):
            yield smaller[:n] + [[ first ] + subset]  + smaller[n+1:]
        # put `first` in its own subset 
        yield [ [ first ] ] + smaller

##############################
### VÉRIFIER QUE QUELQUE CHOSE
### EST BIEN UN RANGEMENT
##############################

def isRangement (X,c,f):
    """ Vérifie que f est un rangement 
        d'un ensemble à $n$ éléments 
        dans un ensemble à $k$ éléments
    """
    assert (len (X) == len (f))

    # Détermine la taille de l'espace d'arrivée 
    k     = max ( fi for fi in f ) + 1

    # Détermine si la fonction est surjective  
    T     = [ False for i in range (k) ]
    for i,fi in enumerate (f):
        T[fi] = True

    surjective = all (T) # Toutes les images sont atteintes

    # Détermine si la fonction vérifie 
    # la propriété de respect des tailles 
    Q     = [ 0 for i in range (k) ]
    for i,fi in enumerate (f):
        Q[fi] += X[i]

    tailleBlocs = all ( j <= c for j in Q)

    return surjective and tailleBlocs 

##################################
###
### UNE PREMIÈRE RÉSOLUTION NAIVE 
###
##################################

def naiveSolver (X,c):
    """ Solveur naïf qui énumère les partitions """
    def findWhere (part, num):
        for i,parti in enumerate (part):
            if num in parti:
                return i
    
    m = None

    # On peut directement énumérer sur $f$
    # pour ne pas s'embêter ... ?!
    for p in partition (list (range (len (X)))):
        f = [ findWhere (p,i) for i in range (len (X)) ] 
        if isRangement (X,c,f):
            if m != None:
                m = min (len (p),m)
            else:
                m = len (p)
    return m

EXEMPLES = [
        [1, 1, 5, 3, 3],
        [2, 5, 4, 4, 2],
        [3, 2, 2, 3, 2],
        [2, 4, 5, 5, 2],
        [4, 4, 1, 4, 3, 4],
        [2, 4, 1, 1, 2, 5]
        ]





############################
###
### DES TESTS RANDOMISÉS 
###
############################

def randomCheckPropDouble ():
    """ Test randomisé de la propriété
        « il n'est pas toujours optimal 
          de grouper par deux les 
          éléments »
    """
    i = 0
    while i <= 10000:
        n = random.randint (1,5)
        c = random.randint (2,10)

        X = [ random.randint (1,c - 1)    for i in range (n) ]
        Y = [ random.randint (1,c - X[i]) for i in range (n) ]
        print ("X : {} / Y : {}".format (X,Y))

        p1 = naiveSolver (X + Y, c)
        p2 = naiveSolver ([ X[i]+Y[i] for i in range (n) ], c)

        if p1 >= p2:
            pass
        else:
            print ("Contre Exemple : X = {} | Y = {} | c = {} (p1 = {} / p2 = {})".format (X,Y,c,p1,p2))
            return True


def rechercheC1C2C3 ():
    """ Recherche l'exemple donné dans la partie 
        I qui doit vérifier les propriétés 
        C1/C2/C3 avec 6 éléments 
    """
    i = 0
    while i <= 10000: 
        X1 = random.randint (1,40)
        X2 = random.randint (1,40)
        X3 = random.randint (1,40)
        X  = [X1,X2,X3]
        c  = X1 + X2 + X3

        Y  = [ random.randint (1,40) for i in range (4) ] 

        p1 = naiveSolver (Y, c)
        p2 = naiveSolver (X + Y, c)

        if p1 != None and p2 != None and p1 +1 >  p2:
            print ("[TEST{}] X = {} / c = {} (p1 = {}, p2 = {})".format (i,X+Y,c,p1,p2))
            return True

        i = i+1







#######################
### 
### LES EXERCICES DE
### PROGRAMMATION 
### DEMANDÉS 
###
#######################




def nextFit (X,c):
    """ Algorithme NextFit """
    k    = 1
    curr = 0

    for i in X:
        if curr + i <= c:
            curr = curr + i
        elif i <= c:
            curr = i
            k    = k + 1
        else:
            return None # instance impossible 
    return k


def firstFit (X,c):
    """ Algorithme FirstFit """
    b = [0] 

    def findFirstFit (i):
        for k,bk in enumerate(b):
            if bk + i <= c:
                return k 
        return None
    
    for i in X:
        k = findFirstFit (i)
        if k == None:
            if i <= c:
                b.append (i)
            else:
                return None # instance impossible  
        else:
            b[k] = b[k] + i

    return len (b)



















########################
####
#### CONSTRUCTION 
#### DES CONTRE 
#### EXEMPLES 
####
########################
            

def instanceDec6Ratio3Sur2 ():
    """ On cherche une instance 
        de taille 6 décroissance 
        telle que ω(X,c) = 2 
        et C_eff (X,c) = 3
    """
    i = 0
    while i < 100000:
        X = [ random.randint (1,20) for i in range (6) ]
        X.sort ()
        X.reverse ()
        c = random.randint (1,20)

        if firstFit (X,c) == 3 and naiveSolver (X,c) == 2:
            print ("[TEST{}] X = {} / c = {}".format (i, X,c))
            return True

        i += 1
         
