\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts, amsmath, amssymb, mathrsfs}
\usepackage[a4paper,landscape,twocolumn]{geometry}
\geometry{left=1cm, right=1cm,top=1cm ,bottom=2.5cm}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{complexity}
\usepackage{hyperref}
%\usepackage{enumitem}
\usepackage{paralist}
\usepackage{epigraph}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}
\usepackage{palatino}
\usepackage{pdfpages}
\usepackage{epigraph}
\usepackage{complexity}
\usepackage{algorithm}
\usepackage{algorithmic}

%%%% Format

\setlength{\columnsep}{2cm}

\newcommand{\lname}{Modélisation — 2012 — Sujet D4}

\setlength{\headheight}{35pt}
\lhead{\lname}
\rhead{\textsc{Aliaume Lopez}}
\chead{}
\rfoot{}
\pagestyle{fancy}

\newenvironment{contexte}{\textbf{Contexte}\begin{quote}\itshape}{\end{quote}}
\newenvironment{conclusion}{\textbf{Conclusion}\begin{quote}}{\end{quote}}
\newenvironment{ouverture}{\textbf{Ouverture}\begin{quote}}{\end{quote}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{exemple}
\newtheorem{exemple}[definition]{Exemple}

\theoremstyle{theoreme}
\newtheorem{theoreme}[definition]{Théorème}

\theoremstyle{lemme}
\newtheorem{lemme}[definition]{Lemme}

\theoremstyle{propriete}
\newtheorem{propriete}[definition]{Propriété}
 
\theoremstyle{remarque}
\newtheorem*{remarque}{Remarque}


%%%% Corps du fichier
\title{\lname}
\date{}


\begin{document}

\maketitle 

\section{Définition du problème}

\begin{definition}[Rangement]
    Soit $c$ un entier et $(X_i)_{1 \leq i \leq n}$ 
    une suite d'entiers entre $1$ et $c$.
    La fonction $f : \{ 1, \dots, n \} \to \{ 1, \dots, p \}$
    est un rangement quand 
    \begin{compactenum}
    \item Elle est \emph{surjective}
    \item Pour tout $1 \leq k \leq p$,
        \begin{equation*}
            \sum_{i \in f^{-1}(\{k \})} \leq c
        \end{equation*}
    \end{compactenum}
\end{definition}


\begin{definition}[Bin Packing]
    ~
    \begin{description}
        \item[Entrée] Un entier $c$ et une suite $(X_i)_{1 \leq i \leq n}$ d'entiers 
            compris entre $1$ et $c$
        \item[Sortie] Un rangement $f$ avec un ensemble d'arrivé 
            \emph{minimal}
    \end{description}
\end{definition}

\begin{definition}[Bin Packing Décision]
    ~
    \begin{description}
        \item[Entrée] Un entier $c$,  une suite $(X_i)_{1 \leq i \leq n}$ d'entiers 
            compris entre $1$ et $c$ et un entier $k$
        \item[Sortie] Il existe un rangement de $(X,c)$ avec 
            comme ensemble d'arrivée $\{ 1, \dots, k \}$
    \end{description}
\end{definition}

\begin{exemple}
    ~
    \begin{enumerate}
        \item Répartir des données sur de multiples disques 
        \item Répartir des publicités dans des émissions radio 
            avec une taille bornée pour les "pauses"
        \item Répartir un chargement pour faire le moins d'aller-retours
            (remplir un ascenceur, un camion de déménagement)
        \item Faire de l'ordonnancement de processus
    \end{enumerate}
\end{exemple}

\section{Résultats théoriques}

On note $||f||$ la taille de l'ensemble 
d'arrivée d'un rangement $f$.
On note $\omega(X,c)$ la taille minimale 
de l'ensemble d'arrivée d'un rangement.


\begin{propriete}[Bin Packing Décision est dans \NP]
\end{propriete}

\begin{proof}
    Soient $(X,c,k)$ une instance du problème,
    cette instance est positive si et seulement s'il 
    existe $f : \{ 1, \dots, n \} \to \{ 1, \dots, k \}$
    qui est un rangement. Or un rangement est en particulier 
    une \emph{partition} de $X$ en $k$ sous ensembles.
    
    On peut "énumérer" les partitions en temps 
    polynômial grâce au non-déterminisme.
    \begin{center}
        \begin{algorithmic}
            \STATE $f$ array of size $n$
            \FOR{ $1 \leq i \leq n$ }
            \STATE $f(i) \leftarrow \text{choice}(1,k)$
            \ENDFOR
        \end{algorithmic}
    \end{center}

    Par la suite vérifier si une partition $f$ est un 
    rangement se fait en temps polynômial puisqu'il suffit 
    de vérifier \begin{inparaenum} \item la surjectivité \item
    la compatibilité des classes avec la taille $c$ \end{inparaenum}.
\end{proof}

\begin{propriete}[Bin Packing est dans \NP]
    Il s'agit de répondre le plus petit $\omega(X,c)$,
    mais en énumérant les $k$ tels que $(X,c,k)$ 
    est une solution de \textsc{BinPackingDecision}
    on peut conclure en temps polynômial.
\end{propriete}


\begin{propriete}[Bin Packing Décision est \NP-complet]
\end{propriete}

\begin{proof}
    Soit $S, w : S \to \mathbb{N}$ une instance du problème de partition.
    On pose $X = w$ et $2c = \sum_{s \in S} w(s)$. On demande alors 
    si l'instance $(X,c,2)$ possède une solution. Cette transformation 
    est bien polynômiale. 

    Si $(X,c,2)$ a une solution, alors il existe $f$ qui est un rangement 
    avec espace d'arrivée $\{ 1, 2 \}$. On peut donc déduire que 
    $f^{-1}(\{1\})$ et $f^{-1}(\{2\})$ répondent au problème de partition,
    car $\sum f^{-1}(\{ 1 \}) + \sum f^{-1}(\{ 2 \}) = 2c$
    et chacun est plus petit que $c$, donc ils sont tous deux égaux à $c$.

    Réciproquement, si le problème de partition a une solution, il suffit 
    d'utiliser cette partition pour $f$, qui vérifie automatiquement les 
    propriétés de rangement.
\end{proof}

\begin{lemme}[Approximation maximale]
    S'il existe un algorithme d'approximation du \textsc{BinPacking}
    polynômial avec un facteur d'approximation strictement 
    inférieur à $3/2$, alors $\P = \NP$.
\end{lemme}

\begin{proof}
    On reprend la réduction précédente avec l'algorithme 
    d'approximaiton. Il ne peut répondre $3$ ou plus 
    quand $2$ sont optimales, et donc peut résoudre 
    \textsc{Partition}
\end{proof}


\begin{propriete}[Il existe un algorithme pseudo-polynomial pour $k=2$]
    En utilisant de la programmation dynamique.
\end{propriete}

\section{Optimalité et majorations}


\begin{propriete}[Encadrement]
    Si $V(X)$ est la somme des $X_i$,
    alors $V(X) \leq c \omega (X,c) \leq 2 V(X)$
\end{propriete}

\begin{proof}
    Soit $f$ un rangement de $(X,c)$
    avec $||f|| = p$.

    \begin{equation*}
        V(X) = \sum_{k = 1}^p \sum_{i \in f^{-1}(\{ k \})} X_i
            \leq pc 
    \end{equation*}

    Donc pour tout $f$ on a $V(X) \leq c ||f||$.

    Pour la deuxième inégalité on note $B_i$ le paquet $f^{-1}(\{ i \})$
    et $V(B_i)$ pour la somme des volumes des éléments de $B_i$.
    Soit $f$ un rangement optimal pour $(X,c)$. Soit $1 \leq i \leq
    \omega(X,c) = p$, soit $j = i+1 \mod p$.

    Si $V(B_i) + V(B_j) \leq c$ on a une contradiction, car on peut fusionner 
    les boîtes $i$ et $j$ pour obtenir un rangement de taille strictement 
    inféreur. Ainsi, pour chaque boîte $B_i$,
    $V(B_i) + V(B_j) > c$.

    En sommant cette inégalité on obtient alors~:

    \begin{equation*}
        2 V(X) = \sum_{i = 1}^p V(B_i) + \sum_{i = 1}^p V(B_i) \geq p c = c \omega
        (X,c) 
    \end{equation*}
\end{proof}

\begin{propriete}[Monotonie]
    Si $X \leq Y$ alors $\omega (X,c) \leq \omega (Y,c)$
\end{propriete}

\begin{proof}
    Soit $f$ un rangement pour $(Y,c)$, il est aisé 
    de vérifier que $f$ est un rangement pour $(X,c)$,
    donc $\omega(X,c) \leq ||f||$ ce qui permet de conclure.
\end{proof}

\begin{propriete}[Groupement]
    Si $X$ et $Y$ sont deux familles de même taille avec
    $2 \leq X_i + Y_i \leq c$ alors~:

    \begin{equation*}
        \omega (X \cdot Y, c) \leq \omega ( (X_i + Y_i), c)
    \end{equation*}

    Cette inégalité peut être stricte !
\end{propriete}

\begin{proof}
    Un rangement pour $((X_i + Y_i), c)$ est toujours 
    un rangement pour $(X \cdot Y, c)$, on a donc 
    bien l'inégalité attendue.

    Cette inégalité est stricte (contre exemple trouvé automatiquement)
    $X = [2, 6, 1, 1]$, $Y = [1, 2, 7, 6]$, $X+Y = [3,8,8,7]$, $c = 9$.
    Le rangement optimal pour $X+Y$ utilise $4$ cases car on ne peut 
    pas placer deux éléments dans une même case. Le rangement optimal 
    pour $X \cdot Y$ utilise moins de 3 cases.
\end{proof}

\begin{propriete}[Non-remplissage]
    Pour tout $n \geq 6$ il existe une séquence qui vérifie $C1$, $C2$ et $C3$
\end{propriete}

\begin{proof}
    Pour $n = 6$ on prend $X_1 = X_2 = X_3 = 1$ 
    pour simplifier les notations, et on pose $c = 3$.
    Il suffit de remarquer que $X_4 = X_5 = X_6 = 1.6$ 
    permet de compléter la suite pour convenir.

    Plus généralement, on pose $X_1 = X_2 = X_3 = 1$,
    $c = 3$ et $X_i = 1.6$ pour $i \geq 4$.
    Le rangement optimal pour la suite tronquée utilise $n - 3$
    boîtes, et on peut ranger en $n - 3$ boîtes toute la suite aussi.
    Cela permet de conclure.
\end{proof}

\section{Next Fit}

\begin{lemme}[Analyse de \textsc{NextFit}]
    Si $p = \omega (X,c)$ alors Next Fit n'utilise pas plus 
    que $2 p$ paquets.

    Il existe une séquence qui force Next Fit à utiliser 
    $2p - 2$ paquets.
\end{lemme}

\begin{proof}
    Soit $f$ le remplissage fait par Next Fit, on note $(B_i)$
    la partition sous-jacente à $f$ qui contient $k$ boîtes.
    Si $B_i$ et $B_{i+1}$ sont deux paquets adjacents alors 
    $V(B_i) + V(B_{i+1}) \geq c + 1$. On peut donc conclure 
    qu'au plus la moitié de l'espace est vide et donc 
    $2 V(X) \geq c k$. En utilisant la minoration de $\omega (X,c)$
    on déduit alors $k \leq 2 \omega(X,c)$, ce qui prouve 
    que l'algorithme est une $2$-approximation



    Pour construire une mauvaise instance de taille $N$ 
    il suffit de poser $c = 2$ et d'alterner 
    les éléments $X_i = 1$ pour $i$ pair et $X_i = i / N$ pour $i$ impair.
    Sur une telle instance \textsc{NextFit} utilise $N/2$ paquets, alors 
    qu'une solution optimale commence par grouper les pairs par deux 
    pour former $N/4$ paquets, et ensuite peut construire un dernier paquet 
    contenant tous les impairs. 

    On a donc une instance où \emph{NextFit} donne $N/2$ et ou la solution 
    optimale est $N/4 + 1$ ce qui montre le théorème.
\end{proof}

On remarque que la preuve s'adapte pour montrer qu'on peut toujours 
forcer $\emph{NextFit}$ à avoir un ratio plus grand que $2 - \varepsilon$.


\section{First Fit}

\begin{lemme}[Analyse de \textsc{FirstFit}]
    Le ratio de \textsc{FirstFit} est plus petit que $2$.
    Il existe une instance telle que le ratio d'approximation 
    soit exactement $5/3 = 10/6$.
\end{lemme}
\begin{proof}
    Au plus une boîte peut être plus qu'à moitié vide à chaque étape, 
    car sinon elles seraient fusionnées. Cela montre que le ratio
    de \textsc{FirstFit} est au plus $2$.

    Pour construire l'instance on considère $M$ un entier naturel une 
    suite de $6M$ items de taille $1/7 + e$, puis $6M$ items de taille 
    $1/3 + e$ puis $6M$ items de taille $1/2 + e$ avec $e$ un petit nombre.

    La stratégie optimale est de remplir chaque paquet avec un 
    des éléments de chaque type et demande $6M$ paquets.
    
    En lançant l'algorithme \textsc{FirstFit} on commence par 
    les objets de taille $1/7$,
    puis deux par deux dans un paquet les objets de taille $1/3$,
    puis construit un paquet par objet de type $1/2$. Au total 
    il construit $1M + 6M / 2 + 6M$ paquets, c'est-à-dire $10M$ 
    paquets. 
\end{proof}

\section{First Fit avec instance décroissante}

\begin{remarque}
    Les algorithmes précédents se font en ligne, alors que celui-ci 
    suppose par exemple d'avoir trié l'instance avant.
\end{remarque}


\begin{proposition}[Instance particulière]
    Sur l'instance $X = [12, 10, 5, 5, 4, 4]$,
    $c = 20$ on a $\omega (X,c) = 2$ et $C_{eff} (X,c) = 3$
\end{proposition}
\begin{proof}
    Trouvé par calcul sur ordinateur.
\end{proof}

\begin{proposition}[Instance particulière]
    Ça donne une idée pour construire une instance générale 
\end{proposition}


\end{document}
