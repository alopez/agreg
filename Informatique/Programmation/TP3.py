##################################
########### QUICKHULL ############
##################################

##################################
####### Fonctions utiles #########
##################################

def rev(t):
    return [t[len(t)-i] for i in range(1,len(t)+1)]

def vect(x_1,x_2):
    assert x_1[0]<=x_2[0]
    return [x_2[0]-x_1[0],x_2[1]-x_1[1]]

#print vect([1,2],[2,3])

def det(vect1,vect2):
    return vect1[0]*vect2[1]-vect1[1]*vect2[0]

#print det([1,2],[2,3])
#print det([1,1],[1,3])

def scal(vect1,vect2):
    return vect1[0]*vect2[0]+vect1[1]*vect2[1]

#print scal([2,0],[1,1])
#print scal([2,0],[2,1])

def tri_absc(t):
    if len(t)==0 or len(t)==1:
        return t
    else:
        t_tri=[]
        t_1=tri_absc(t[:len(t)/2])
        t_2=tri_absc(t[len(t)/2:])
        i,j=0,0
        while i<len(t_1) or j<len(t_2):
            if i==len(t_1):
                t_tri.append(t_2[j])
                j+=1
            elif j==len(t_2):
                t_tri.append(t_1[i])
                i+=1
            elif t_1[i][0]>t_2[j][0]:
                t_tri.append(t_2[j])
                j+=1
            else:
                t_tri.append(t_1[i])
                i+=1
        return t_tri

#print tri_absc([[4,0],[3,0],[2,-4],[1,-1],[1,-5],[2,-1]])

def extraire(t,ind,b):
    x_1=t[ind[0]]
    x_2=t[ind[len(ind)-1]]
    assert x_1[0]<=x_2[0]
    #On extrait d'un sous-tableau trie t[ind] les points situes:
    # - au-dessus de la droite (x_1,x_2) si b=0
    # - en-dessous de la droite si b=1
    extr=[ind[0]]
    vect1=vect(x_1,x_2)
    for i in range(len(ind)):
        vect2=vect(x_1,t[ind[i]])
        if det(vect1,vect2)>0 and b==1:
            extr.append(ind[i])
        if det(vect1,vect2)<0 and b==0:
            extr.append(ind[i])
    extr.append(ind[len(ind)-1])
    return extr

#print extraire([[0,0],[1,5],[1,-2],[2,4],[2,1],[3,0]],[0,1,2,3,4,5],1)
#print extraire([[0,0],[1,5],[1,-2],[2,4],[2,-1],[3,0]],[0,1,2,3,4,5],1)
#print extraire([[0,0],[1,5],[1,-2],[2,4],[2,1],[3,0]],[0,1,2,3,4,5],0)
#print extraire([[0,0],[1,5],[1,-2],[2,4],[2,-1],[3,0]],[0,1,2,3,4,5],0)

##################################
######### L'algorithme ########### 
##################################

def quickhull_rec(t,ind,b):
    if len(ind)==2:
        return ind
    else:
        x_1=t[ind[0]]
        x_2=t[ind[len(ind)-1]]
        vect1=vect(x_1,x_2)
        max=0
        detmax=0
        scalmin=0
        for i in range(len(ind)):
            vect2=vect(x_1,t[ind[i]])
            deter=det(vect1,vect2)
            if deter>detmax and b==1:
                max=i
                detmax=det(vect1,vect2)
                scalmin=scal(vect1,vect2)
            if deter<detmax and b==0:
                max=i
                detmax=det(vect1,vect2)
                scalmin=scal(vect1,vect2)
            if deter==detmax:
                prodscal=scal(vect1,vect2)
                if (prodscal<scalmin) and b==1:
                    max=i
                    detmax=det(vect1,vect2)
                    scalmin=scal(vect1,vect2)
                if prodscal>scalmin and b==0:
                    max=i
                    detmax=det(vect1,vect2)
                    scalmin=scal(vect1,vect2)
        ind1=extraire(t,ind[:max],b)
        ind2=extraire(t,ind[max:],b)
        quick_1=quickhull_rec(t,ind1,b)
        quick_2=quickhull_rec(t,ind2,b)
        return quick_1[:len(quick_1)-1]+quick_2

#print quickhull_rec([[0,0],[1,1],[1,5],[2,4],[2,1],[3,0]],[0,1,2,3,4,5],1)
#print quickhull_rec([[0,0],[1,-1],[1,-5],[2,-4],[2,-1],[3,0]],[0,1,2,3,4,5],0)

def quickhull(t):
    ind_tot=[i for i in range(len(t))]
    ind1=extraire(t,ind_tot,1)
    ind2=extraire(t,ind_tot,0)
    quick_1=quickhull_rec(t,ind1,1)
    quick_2=rev(quickhull_rec(t,ind2,0))
    return quick_1[:len(quick_1)-1]+quick_2[:len(quick_2)-1]

#print quickhull([[0,0],[1,1],[1,-1],[1,5],[1,-6],[2,4],[2,-1],[2,1],[3,0]])
