import random
import math
import json 

###
# 
# Un automate non-déterministe
#
###
class Automate:
    def __init__ (self, 
                  etats,        # Ensemble d'états sous forme de SET
                  delta,        # Dictionnaire qui à un état associe un ensemble de transitions (si déterministe)
                  initial,      # "Ensemble" d'états initiaux (si non déterministe)
                  finaux,       # Ensemble d'états finaux sous forme de SET
                  ND):          # Est-ce que l'automate est non-déterministe ?
        self.etats   = etats
        self.delta   = delta
        self.initial = initial
        self.finaux  = finaux
        self.ND      = ND 

    def getAlphabet (self):
        l = set ()
        for q,d in self.delta.items ():
            for a,p in d.items ():
                l.add (a)
        return l

    def getTransition (self,etat,lettre):
        return self.delta[etat][lettre]

    def ndTrans (self, etats, lettre):
        transitions = set ()
        for state in etats:
            try:
                testTrans = self.getTransition (state,lettre)
                transitions = transitions.union (testTrans)
            except KeyError:
                pass
        return transitions

    def isFinalState (self, etat):
        return etat in self.finaux

    def determinisation (self):
        if not self.ND:
            return self

        alphabet  = self.getAlphabet ()
        newStates = set ()
        newDelta  = {} 
        newInit   = self.initial
        newFinal  = set ()
        pile      = [newInit]
        
        while pile != []:
            etat = pile.pop ()
            for a in alphabet:
                nouvelEtat = self.ndTrans (etat, a)
                if nouvelEtat in newStates:
                    pass
                else:
                    pile.append (nouvelEtat)
            newStates.add (etat)

        return newStates

    def printAutomate (self):
        print ("digraph finite_state_machine {")
        print ("\trankdir=LR;")
        print ("\tsize=\"8,5\"")

        print ("node [shape = doublecircle];")
        
        codage = list (self.etats)

        for etat in self.finaux:
            print ("LR_{} [label=\"{}\"];".format (codage.index (etat),
                                                   str (etat)))

        print ("node [shape = circle];")
        for etat in self.etats:
            if etat not in self.finaux:
                print ("LR_{} [label=\"{}\"];".format (codage.index (etat),
                                                       str (etat)))
        

        for q,d in self.delta.items ():
            for a,p in d.items ():
                print ("\tLR_{} -> LR_{} [label = \"{}\"];".format (
                    codage.index (q), codage.index (p), a))
        print ("}")






####### LES RUNNERS ########

class Runner:
    def reInitialise (self):
        self.current = self.automate.initial 

    def run (self, word, debug=False):
        i = 0
        if debug:
            print ("Q{} = {}".format (i, self.current), end=" ")

        for a in word:
            if not self.step (a):
                return False
            if debug:
                print ("--{}--> {}".format (a,self.current), end=" ")

        if debug and self.isFinished ():
            print ("accepting".format (i))
        return self.isFinished ()


class DetRunner (Runner):
    """ S'occupe d'un "run" de l'automate """
    def __init__ (self, automate):
        if automate.ND:
            raise ValueError ("Expected deterministic automaton, given a non-deterministic one")

        self.automate = automate
        self.current  = automate.initial

    def step (self, lettre):
        try:
            transition   = self.automate.getTransition(self.current, lettre)
            self.current = transition
            return True
        except KeyError:
            return False

    def isFinished (self):
        return self.automate.isFinalState (self.current)


class NonDetRunner(Runner):
    """ S'occupe d'un "run" de l'automate 
        En calculant l'automate des parties 
        à la volée 
    """
    def __init__ (self, automate):
        if not automate.ND:
            raise ValueError ("Expected non-deterministic automaton, given a deterministic one")

        self.automate = automate
        self.current  = automate.initial

    def step (self, lettre):
        self.current = self.automate.ndTrans (self.current, lettre)
        return True

    def isFinished (self):
        return any (self.automate.isFinalState (x)
                    for x in self.current)

#######
# Bébé automate 
#######

def construitExemple1 ():
    etats = { "q0", "q1" }
    delta = {
                "q0" : { "a" : { "q0" },
                         "b" : { "q0", "q1" } },
            }
    finaux = { "q1" }

    return Automate (etats, delta, { "q0" }, finaux, True)

def automateDeSimonND (motif,alphabet):
    m        = len (motif)
    q0       = "" # varepsilon 
    delta    = {} 

    curr = q0
    delta[q0] = {}
    for a in motif:
        delta[curr][a] = { curr + a } 
        curr = curr + a
        delta[curr] = {}

    for a in alphabet:
        if a in delta[""]:
            delta[""][a].add (q0)
        else:
            delta[""][a] = { "" }
        
    A = Automate (
            { motif[:i] for i in range (m) },
            delta,
            { q0 },
            { motif },
            True) 
    return A


def automateDeSimon (motif,alphabet):
    """ Calcule l'automate déterminisé des motifs """
    m = len (motif)
    q0 = ""

    A = Automate ({ motif[:i] for i in range (m+1) }, # États
            { q0 : # varepsilon 
                { a : ""  for a in alphabet } },
            q0, # initial 
            { motif },
            False)

    currentState = q0
    for a in motif: # pour chaque lettre 
        newState = currentState + a
        A.delta[newState] = dict ()
        oldNewState = A.delta[currentState][a]
        A.delta[currentState][a] = newState

        for lettre,etat in  (A.delta[oldNewState]).items ():
            A.delta[newState][lettre] = etat

        currentState = newState

    return A






