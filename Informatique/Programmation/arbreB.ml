
(*
 * Fonctions utilitaires 
 * parce qu'Ocaml c'est nul 
 *)
let maybe (f : 'a -> 'b) (d : 'b) (x : 'a option) = 
  match x with 
    | None   -> d 
    | Some y -> f y;;

let safeHead (l : 'a list) = match l with 
  | []      -> None
  | x :: xs -> Some x;;


(*** 
   * B-Tree 
   *
   *)

(*
 * Un arbre est soit une feuille 
 * soit un noeud qui contient N fils 
 * « séparés » par N-1 balises 
 *)
type 'a arbre = 
  | Feuille of 'a 
  | Noeud   of 'a noeud 
and 'a noeud = { 
    fils    : ('a arbre list);
    balises : 'a list;
  };;

(*
 * Un contexte est soit vide (on est à la racine
 * de l'arbre) soit contient nos voisins directs 
 * à gauche et à droite, ainsi que le contexte du noeud père
 *)
type 'a ctx = 
  | Racine 
  | Fils of 'a ctx * 'a fils
and 'a fils = {
    (* Voisins à gauche *)
    vGauche : ('a arbre list);
    (* Voisins à droite *)
    vDroite : ('a arbre list);
    (* Balises *)
    bGauche : 'a list;
    bDroite : 'a list
};;

(*
 * Une position dans l'arbre 
 * c'est la position qu'on regarde actuellement
 * et le contexte dans lequel on se trouve 
 *)
type 'a loc = {
    loc : 'a arbre;
    ctx : 'a ctx;
};;

exception InvalidMove;;


(****** MAINTENANT ON PASSE AUX OPÉRATIONS DE DÉPLACEMENT *******)

(* Transforer un arbre en position *)
let locate   arbre  = { loc = arbre; ctx = Racine } ;;
(* Observer l'arbre à la position courante *)
let observe  zipper = zipper.loc;; 

(* Substituer l'arbre à la position courante *)
let substitute (arbre : 'a arbre) (zipper : 'a loc) = 
  { zipper with 
        loc = arbre };;

(* 
 * Est-ce que la valeur est actuellement 
 * `in range` ? (ie. peut-on insérer ici ...)
 *)
let isInRange (x : 'a) (zipper : 'a loc) = 
  match zipper.ctx with 
    | Racine      -> true    (* ... mmh ... bonne question ... *) 
    | Fils (_, f) -> maybe ((>=) x) true (safeHead f.bGauche) &&
                     maybe ((<) x) true (safeHead f.bDroite) ;;

(*
 * Quel est le nombre de fils du noeud parent ? 
 * (nombre de voisins + 1)
 * *) 
let branchingFactor (zipper : 'a loc) = 
  match zipper.ctx with 
    | Racine -> 0
    | Fils (_,f) -> List.length f.bGauche + List.length f.bDroite + 1;;

(** Les opérations de déplacement peuvent lever 
  * une exception InvalidMove 
  *)
let moveDown (zipper : 'a loc) = match zipper.loc with
  | Feuille _ -> raise InvalidMove 
  | Noeud n   -> 
      begin 
        match n.fils with 
          | []      -> raise InvalidMove 
          | x :: xs -> {
              loc = x; (* le nouvel arbre *)
              ctx = Fils (zipper.ctx, {
                    vGauche = [];
                    vDroite = xs;
                    bGauche = [];
                    bDroite = n.balises
                })
            };
      end;;

let moveUp (zipper : 'a loc) = 
  match zipper.ctx with 
    | Racine           -> raise InvalidMove 
    | Fils (parent, f) -> 
        { 
              ctx = parent;
              loc = Noeud ({
                fils    = (List.rev f.vGauche) @ [zipper.loc] @ f.vDroite;
                balises = (List.rev f.bGauche) @ f.bDroite
              })
        };;

let rec goTop (zipper : 'a loc) = 
  try 
    goTop (moveUp zipper)
  with 
    | InvalidMove -> zipper;;

let moveLeft (zipper : 'a loc) = 
  match zipper.ctx with 
    | Racine           -> raise InvalidMove 
    | Fils (parent, f) -> 
        begin match (f.vGauche,f.bGauche) with 
          | (v :: vs, b :: bs) ->
              {
                ctx = Fils (parent, {
                    vGauche = vs;
                    vDroite = zipper.loc :: f.vDroite;
                    bGauche = bs;
                    bDroite = b :: f.bDroite;
                });
                loc = v
              }
          | _ -> raise InvalidMove 
        end;;


let moveRight (zipper : 'a loc) = 
  match zipper.ctx with 
    | Racine           -> raise InvalidMove 
    | Fils (parent, f) -> 
        begin match (f.vDroite,f.bDroite) with 
          | (v :: vs, b :: bs) ->
              {
                ctx = Fils (parent, {
                    vDroite = vs;
                    vGauche = zipper.loc :: f.vGauche;
                    bDroite = bs;
                    bGauche = b :: f.bGauche;
                });
                loc = v
              }
          | _ -> raise InvalidMove 
        end;;

(*** Insère à DROITE de la position courante ***)
let insertNodeR (n : 'a arbre) (b : 'a) (zipper : 'a loc) = 
  match zipper.ctx with 
    | Racine -> 
            {
                zipper with 
                    ctx = Fils (Racine, {
                      vGauche = [];
                      vDroite = [n];
                      bGauche = [];
                      bDroite = [b];
                    })
            }
    | Fils (parent, f) -> 
        { 
            zipper with 
                ctx = Fils (parent, {
                  f with 
                      vDroite = n :: (f.vDroite);
                      bDroite = b :: f.bDroite;
                })
        };;
(*** Insère à GAUCHE de la position courante ***)
let insertNodeL (n : 'a arbre) (b : 'a) (zipper : 'a loc) = 
  match zipper.ctx with 
    | Racine -> 
            {
                zipper with 
                    ctx = Fils (Racine, {
                      vGauche = [n];
                      vDroite = [];
                      bGauche = [b];
                      bDroite = [];
                    })
            }
    | Fils (parent, f) -> 
        { 
            zipper with 
                ctx = Fils (parent, {
                  f with 
                      vGauche = n :: f.vGauche;
                      bGauche = b :: f.bGauche;
                })
        };;


(*** L'OPÉRATION TRÈS IMPORTANTE QUI COUPE UN ARBRE EN DEUX 
   * et l'amène au parent 
   *)
let splitTree (zipper : 'a loc) = match zipper.ctx with 
  | Racine -> failwith "[splitTree] splitting root" 
  | Fils (parent, f) -> 
      (* l'arbre à gauche *)
      let a1 = Noeud ({ balises = List.rev (List.tl f.bGauche) ; 
                        fils    = List.rev f.vGauche })
      (* l'arbre à droite *)
      in let a2 = Noeud ({ balises = f.bDroite;
                           fils    = zipper.loc :: f.vDroite })
      in 
        zipper 
          |> moveUp                               (* on considère le père *) 
          |> substitute a1                        (* on remplace le père par a1 *)
          |> insertNodeR a2 (List.hd f.bGauche);; (* on ajoute à côté de lui a2 *)



(*
 * PFIOU, C'EST TERMINÉ, ON PEUT 
 * TRAVAILLER SUR LES B ARBRES MAINTENANT 
 *)

(* Fait un déplacement dans l'arbre pour se rapprocher 
 * de la feuille recherchée 
 *)
let trouvePositionStep (x : 'a) (zipper : 'a loc) = 
  match zipper.ctx with 
      (* Si c'est la racine on doit descendre *)
    | Racine          -> moveDown zipper
    | Fils (parent,f) ->
        if isInRange x zipper then 
          moveDown zipper
        else
          moveRight zipper;;

(*
 * Se positionne sur la feuille qui ressemble le 
 * plus à l'élément recherché
 *)
let rec trouvePosition (x : 'a) (zipper : 'a loc) = 
  try 
    trouvePosition x (trouvePositionStep x zipper)
  with 
    | InvalidMove -> zipper;;

(*
 * Ajoute une feuille ... On est supposément dans 
 * un état où tous nos voisins sont déjà des feuilles 
 *)
let ajouteFeuille (x : 'a) (zipper : 'a loc) = 
  match zipper.loc with 
    | Feuille y -> 
        begin 
          if y <= x then 
            insertNodeR (Feuille x) x zipper
          else
            insertNodeL (Feuille x) x zipper
          end
    | Noeud n -> raise InvalidMove;;

(* 
 * Se déplace jusqu'au noeud "médian" 
 *
 * Ce code est hyper impératif juste pour montrer 
 * que la méthode "zipper" permet aussi un style 
 * très impératif (puisque précisément on parle de 
 * "pointeur" sur des "positions") 
 *)
let elementMedian (zipper : 'a loc) = 
  match zipper.ctx with 
    | Racine -> failwith "[elementMedian] On ne peut pas bouger sur la racine"
    | Fils (parent,f) -> 
        let b = branchingFactor zipper in 
          let l1 = ref (List.length f.vGauche) in 
          let z  = ref zipper in 
          while !l1 < b/2 || !l1 > b/2 do
            if !l1 < b/2 then begin 
              z := moveRight !z;
              incr l1
            end else begin 
              z := moveLeft !z;
              decr l1
            end
          done; !z;;

(* 
 * Fait l'équilibrage de l'arbre en remontant 
 *)
let rec equilibrageInsertion a b (zipper : 'a loc) = 
  (* Si on a moins de b fils alors tout est bon *)
  if branchingFactor zipper <= b then 
    zipper
  else begin 
    zipper 
      |> elementMedian              (* on se déplace au médian *)
      |> splitTree                  (* on coupe *) 
      |> equilibrageInsertion a b   (* et on essaie de ré-équilibrer *) 
  end;;



let insertionClef (x : 'a) (zipper : 'a loc) = 
  zipper 
    |> goTop                      (* On doit bien partir de la racine !! *)
    |> trouvePosition x           (* On se rend à la position d'insertion *)
    |> ajouteFeuille  x           (* On ajoute la feuille *)
    |> equilibrageInsertion 2 4;; (** changer ici le type d'équilibrage voulu **)

let contientClef (x : 'a) (zipper : 'a loc) = 
  zipper 
       |> goTop 
       |> trouvePosition x
       |> begin fun z -> match z.loc with 
          | Feuille y when y = x -> true
          | _                    -> false end;;

let example () = 
  Feuille 8  (* Remarquons que les arbres vides ne sont pas traités par ce code *)
    |> locate 
    |> insertionClef 11
    |> insertionClef 10
    |> insertionClef 9
    |> insertionClef 90
    |> insertionClef 80
    |> insertionClef 20
    |> insertionClef 12
    |> goTop 
    |> observe;;

