import math



####### TAS BINAIRE  #######
# 
# ATTENTION, POUR L'INSTANT
# C'EST UNE STRUCTURE 
# PERSISTANTE
#
############################




def pile_new (x):
    return [x]

def pile_pop (P,sommet,poids):
    m = min (P,key = lambda x: poids[x])

    return (m, [ x for x in P if x != m ] )

def pile_update_or_add (P,sommet,distance):
    return [sommet] + [ x for x in P if x != sommet ] 

def pile_est_vide (P):
    return P==[]


####### CODAGE DU GRAPHE #########
#                                #
# Une matrice n par n            #
# contenant des nombres          #
# représentant le poids          #
# des arêtes. Le poids math.inf  #
# signifie qu'il n'y a pas       #
# d'arête entre les deux sommets #
#                                #
##################################

# Graphe d'exemple
G = [ [ math.inf for i in range (4) ] 
                 for i in range (4) ] 
G[0][0] = 3
G[0][1] = 1
G[1][3] = 11
G[0][2] = 5
G[2][3] = 0
G[3][3] = 3


# Itère sur les sommets d'un graphe G
def sommets (G):
    k = 0
    for i in G:
        yield k
        k += 1



def dijkstra (G,s,t):
    """
        ENTRÉES:
            G : un graphe orienté avec des poids (int matrix)
            s : un sommet de départ              (int)
            t : un sommet d'arrivée              (int)

        SORTIE:
            p : un chemin de s à t de longueur minimale
    """
    # GREEN     : les sommets dont la distance est assurée 
    GREEN = { s }   
    # P         : la file d'attente de traitement
    P     = pile_new (s)
    # D         : la distance à s calculée jusqu'ici 
    D     = [ math.inf for i in sommets (G) ] 
    D[s]  = 0
    # M         : la maman du sommet (le sommet qui mène à lui dans un chemin optimal)
    M     = [ None for i in sommets (G) ] 

    while not pile_est_vide (P):
        print ("PILE     : {}".format (P))
        print ("DISTANCE : {}".format (D))
        print ("MAMAN    : {}".format (M))

        sommet,P = pile_pop (P,s,D)

        # NOTE: de toute manière on est sous la forme 
        # d'une matrice, donc il faut lire toute la matrice 
        for voisin in sommets(G):
            if voisin not in GREEN and voisin != sommet:
                newD = min (D[voisin], D[sommet] + G[sommet][voisin])
                if newD < D[voisin]:
                    D[voisin] = newD
                    P = pile_update_or_add (P,voisin,D[voisin])
                    M[voisin] = sommet
        GREEN.add (sommet)
    return D


