
(* Les sommets sont représentés par des entiers
 * On les suppose entre 0 et n-1 où n est le
 * nombre de sommets*)
(* Un -1 dans la matrice d'adjacence représente
 * deux sommets non liés *)
type graph = int array array;;
type cmpr  = Equal | FirstIsBelow | FirstIsAbove ;;
exception Finish;;



(* Définir un tas binaire *)
type 'a tas  = {
  tableau      : 'a array;
  cmpfunc      : 'a -> 'a -> cmpr;
  mutable size : int ;
  msize        : int ;
};;

(* FONCTIONS NON SAFE *)
let filsGauche i = 2 * i + 1  ;;
let filsDroit  i = 2 * i + 2  ;;
(* SAFE *)
let parent     i = match i with
  | 0 -> 0
  | _ -> (i - 1) / 2;;

(** UNSAFE **)
let swap t i j =
  let tmp = t.(i) in
    t.(i) <- t.(j);
    t.(j) <- tmp;;

let tasCreate default cmpfunc msize =
  {
    tableau = Array.init msize (fun i -> default);
    cmpfunc = cmpfunc;
    size    = 0;
    msize   = msize;
  };;

let tasFindMin tas = match tas.size with
  | 0 -> None
  | _ -> Some (tas.tableau.(0));;


let tasAppend tas elem =
  let t = tas.tableau in
  let compareWithParent pos = tas.cmpfunc
                     t.(pos)
                     t.(parent pos)
  in
  let rec correction pos = match compareWithParent pos with
    | Equal        -> ()
    | FirstIsAbove -> ()
    | FirstIsBelow -> swap t pos (parent pos); correction (parent pos)
  in
    t.(tas.size) <- elem;
    tas.size <- 1+tas.size;
    correction (tas.size - 1);;

(* Attention, remove min plante si c'est
 * le dernier élément
 *)
let tasRemoveMinUnsafe tas =
  let t = tas.tableau in
  let compare a b = tas.cmpfunc t.(a) t.(b) in
  let argmin  a b = match compare a b with
    | Equal -> a
    | FirstIsBelow -> a
    | FirstIsAbove -> b
  in
  let isSmallerThan func pos =
    if filsGauche pos < tas.size then
      begin
      match compare pos (func pos) with
        | Equal        -> None
        | FirstIsBelow -> None
        | FirstIsAbove -> Some (func pos)
      end
    else
      None
  in
  let rec correction pos = match (isSmallerThan filsGauche pos, isSmallerThan filsDroit pos) with
    | None,None      -> ()
    | Some x,None    -> swap t pos x; correction x
    | None, Some x   -> swap t pos x; correction x
    | Some x, Some y ->
        let z = argmin x y in
          swap t pos z; correction z
  in
    t.(0)    <- t.(tas.size - 1);
    tas.size <- tas.size -1;
    correction 0;;

let tasRemoveMin tas = match tas.size with
  | 0 -> ()
  | 1 -> tas.size <- 0;
  | _ -> tasRemoveMinUnsafe tas;;

let testTas =
  let cmpfunc i j =
    if i < j then FirstIsBelow
    else if i > j then FirstIsAbove
    else Equal
  in tasCreate (-1) cmpfunc 10;;

let testingTas () =
  for i = 1 to 10 do
    tasAppend testTas (Random.int 100)
  done;
  for i = 1 to 10 do
      begin
      match tasFindMin testTas with
        | None   -> print_string "EMPTY"
        | Some x -> print_int x;
                    print_string "    ";
      end;
      tasRemoveMin testTas
  done;;

(* Ajout d'un sommet dans la pile de priorité
 * Fait la màj si le sommet était déjà présent
 * avec une valeur plus basse
 * La pile est supposée sans doublon de sommet *)
let rec ajout_pile s w added pile =
  match !pile with
  |[] -> (
          pile := [(s,w)];
          true
    )
  |(s',w')::q ->
      if s' = s then (
        pile := ( (s,min w w')::q ) ;
        w < w'
      )
      else if w' < w then (
        let pilerec = ref q in
        let b = ajout_pile s w added pilerec in
        pile := (s',w'):: !pilerec ;
        b
      )
      else if added then
        let pilerec = ref q in
        let b = ajout_pile s w added pilerec in
        b
      else (
        let pilerec = ref q in
        let b = ajout_pile s w true pilerec in
        pile := (s,w)::(s',w'):: !pilerec;
        b
      );;

let mini_pile pile =
  match pile with
  |[] -> raise Finish
  |t::q -> t

let rec del_pile s pile =
  match pile with
  |[] -> []
  |(s',w)::q -> if s' = s then q else (s',w)::(del_pile s q)

let init_pile s =
  [s,0]

let dijkstra s graphe =
  let n = Array.length graphe in
  let parents = Array.make n (-1) in
  let pile = ref (init_pile s) in
  try (
    while true do
      let sommet,w = mini_pile !pile in
      let () = pile := del_pile sommet !pile in
      for voisin = 0 to n-1 do
        let w' = graphe.(sommet).(voisin) in
        if w' >=0 && ajout_pile voisin (w'+w) false pile then
            parents.(voisin) <- sommet
      done ;
    done ;
    parents
  )
  with Finish -> parents



let graphe = Array.make_matrix 4 4 (-1);;
let testocaml =
  graphe.(0).(1) <- 1;
  graphe.(1).(3) <- 11;
  graphe.(0).(2) <- 5;
  graphe.(2).(3) <- 0;
  dijkstra 0 graphe;;




