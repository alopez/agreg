
(* les graphes (orientés) sont donnés sous forme de tableaux de voisins *)
type graph = int list array

type lit = Pos of int | Neg of int
type clause = lit list
type cnf = clause list

(* on pourra faire un parser quand on aura le temps *)


let build_cnf graphe =
  (* k = nombre de sommets *)
  let k = Array.length graphe in

  (* le fait qu'on soit sur le sommet s à l'instant t est codé
   * par la variable Var(s + k*t) *)

  (* renvoie la clause correspondant au fait que le sommet s
   * apparaisse au moins une fois dans le parcours *)
  let rec une_fois s acc =
    match acc with
    | -1 -> []
    |_ -> Pos(s + k*acc) :: (une_fois s (acc - 1))
  in
  let une_fois s = une_fois s (k-1) in

  (* renvoie la liste de clauses correspondant au fait que le sommet s
   * n'apparaisse pas plus d'une fois dans le parcours *)
  let rec pas_plus s acc1 acc2 =
    if acc2 = 0
    then []
    else
      if acc1 = acc2
      then pas_plus s 0 (acc2-1)
      else [Neg(s + k*acc1) ; Neg(s + k*acc2)] :: (pas_plus s (acc1 + 1)) acc2
  in
  let pas_plus s = pas_plus s 0 (k-1) in

  (* renvoie la formule : "sommet s apparait 1 fois et 1 seule dans le parcours" *)
  let exact s = [une_fois s] @ (pas_plus s) in

  (* renvoie la formule "chaque sommet apparait ..." *)
  let exact =
    let l = ref [] in
    for s = 1 to k do
      l := (exact s) @ !l ;
    done ;
    !l
  in

  (* renvoie la clause correspondant au fait que si s est vu à t, le suivant est un voisin de s *)
  let next s t =
    let l = List.map (fun x -> Pos(x + k*(t+1))) graphe.(s-1) in
    Neg(s + k*t) :: l
  in
  let next =
    let l = ref [] in
    for s = 1 to k do
      for t = 0 to (k-1) do
        l := (next s t) :: !l ;
      done ;
    done ;
    !l
  in

  exact @ next


let print_cnf graphe =
  let cnf = build_cnf graphe in
  let k = Array.length graphe in

  let print_lit lit =
    (match lit with
    |Pos(i) -> print_int i
    |Neg(i) -> (print_char '-' ; print_int i)
    ) ;
    print_char ' '
  in

  let print_clause clause = List.iter print_lit clause ; print_string "0\n" in

  let print_cnf cnf =
    print_string "p cnf " ;
    print_int (k-1 + k*k) ;
    print_char ' ' ;
    print_int (List.length cnf) ;
    print_newline () ;
    List.iter print_clause cnf
  in

  print_cnf cnf


let g1 = [| [2] ; [1] ; [2] ; [3] ; [3;4] |]
let () = print_cnf g1

