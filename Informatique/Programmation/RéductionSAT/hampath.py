from subprocess import call
import subprocess
import random

def disj (l):
    return " ".join (l)

def conj (l):
    return (" 0\n".join (l)) + " 0"

def neg (l):
    return "-" + l

class Graph:
    def __init__ (self,n):
        self._matrix = [ [ False for i in range (n) ]
                                 for i in range (n) ] 
        self._size   = n 

    def addAretes (self, *aretes):
        for (u,v) in aretes:
            try:
                self._matrix[u][v] = True
            except:
                pass

    def _encodeVar (self,u,k):
        return "{}".format (1 + u + k * self._size)

    def _decodeVar (self,l):
        u = (l - 1) % self._size 
        k = (l - u - 1) // self._size 
        return (u,k)


    def _toHamSAT (self):
        sommetExiste = []
        sommetUnique = []
        tempsExiste  = []
        # tempsUnique  = []
        respectArete = []

        for u in range (self._size): 
            for k in range (self._size):
                for l in range (self._size):
                    if k != l:
                        sommetUnique.append (disj ([ neg (self._encodeVar (u,k)),
                                                     neg (self._encodeVar (u,l))
                                                     ]))
        # for k in range (self._size): 
            # for u in range (self._size):
                # for v in range (self._size):
                    # if u != v:
                        # tempsUnique.append (disj ([ neg (self._encodeVar (u,k)),
                                                     # neg (self._encodeVar (v,k))
                                                     # ]))

        for u in range (self._size):
            sommetExiste.append (disj ([ 
                    self._encodeVar (u,k)
                    for k in range (self._size)
                ]))

        for k in range (self._size):
            tempsExiste.append (disj ([ 
                    self._encodeVar (u,k)
                    for u in range (self._size)
                ]))

        for u in range (self._size):
            for v in range (self._size):
                for k in range (self._size - 1):
                    if not self._matrix[u][v]:
                        respectArete.append (disj ([ neg (self._encodeVar (u,k)),
                                                     neg (self._encodeVar (v,k+1)) ]))

        forms = sommetUnique + sommetExiste +  respectArete + tempsExiste # + tempsUnique
        BEGIN = "p cnf {} {}".format (self._size * self._size, 
                                        len (forms))

        return "{}\n{}".format (BEGIN, conj(forms))

    def _runMiniSat (self,ifile,ofile):
        with open (ifile, "w") as i:
            i.write (self._toHamSAT ())

        call (["minisat", "-verb=0", ifile, ofile])

        with open (ofile,"r") as o:
            return o.readlines ()

    def _parseMiniSat (self, lines):
        if lines[0] == "SAT\n":
            numbers = (lines[1][:-2]).split ()
            numbers = [ int(num)
                            for num in numbers 
                            if num[0] != "-" ]
            numbers = [ self._decodeVar (num)
                        for num in numbers ] 
            chemin  = []
            for k in range (self._size):
                for (u,l) in numbers:
                    if l == k:
                        chemin.append (u)
            return chemin
        else:
            return None


    def _printSolvedGraph (self): 
        a = self._runMiniSat ("hampath.sat", "hampath.sol")
        chemin = self._parseMiniSat (a)

        if chemin == None:
            with open ("hampath.dot", "w") as o:
                o.write ("digraph G {\n")

                for u in range (self._size):
                    o.write ("\tN{} ; \n".format (u))

                for u in range (self._size):
                    for v in range (self._size):
                        if self._matrix[u][v]:
                            o.write ("\tN{} -> N{}; \n".format (u,v))

                o.write ("}")
        else:
            indices = [ 0 for i in range (self._size) ]
            for (i,u) in enumerate (chemin):
                indices[u] = i

            with open ("hampath.dot", "w") as o:
                o.write ("digraph G {\n")

                for u in range (self._size):
                    o.write ("\tN{} [label=\"{}\"]; \n".format (u,indices[u]))

                for u in range (self._size):
                    for v in range (self._size):
                        if self._matrix[u][v]:
                            if indices[u] + 1 == indices[v]:
                                o.write ("\tN{} -> N{} [color=\"red\"];\n".format (u,v))
                            else:
                                o.write ("\tN{} -> N{}; \n".format (u,v))

                o.write ("}")

    def createPdfSolution (self):
        self._printSolvedGraph ()
        call (["dot", "-Tpdf", "hampath.dot", "-ohampath.pdf"])
        call (["open", "hampath.pdf"])


villes = ["BREST",       # 0
          "RENNES",      # 1
          "NANTES",      # 2
          "PARIS",       # 3
          "LILLE",       # 4
          "STRASBOURG",  # 5
          "BORDEAUX",    # 6
          "TOULOUSE",    # 7
          "MONTPELLIER", # 8
          "LYON",        # 9
          "MARSEILLE"]   # 10

num = len (villes)

g = Graph (num)

def addConnection (a,b):
    g.addAretes ((villes.index (a), villes.index (b)))
    g.addAretes ((villes.index (b), villes.index (a)))

arcs = [("BORDEAUX"  , "NANTES"),
("NANTES"    , "BREST"),
("NANTES"    , "RENNES"),
("RENNES"    , "PARIS"),
("PARIS"     , "LILLE"),
("PARIS"     , "STRASBOURG"),
("PARIS"     , "TOULOUSE"),
("PARIS"     , "LYON"),
("PARIS"     , "NANTES"),
("BREST"     , "RENNES"),
("BORDEAUX"  , "TOULOUSE"),
("TOULOUSE"  , "MONTPELLIER"),
("MARSEILLE" , "MONTPELLIER"),
("MARSEILLE" , "LYON"),
("LYON" , "STRASBOURG")]

for (a,b) in arcs:
    i = villes.index (a)
    j = villes.index (b)
    g._matrix[i][j] = True
    g._matrix[j][i] = True


# for i in range (100):
    # n1 = random.randint (0,num-1)
    # n2 = random.randint (0,num-1)
    # b  = random.randint (0,100)
    # if b >= 30:
        # g.addAretes ((n1,n2))

g.createPdfSolution ()


