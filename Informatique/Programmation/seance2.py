import math 
import time 
import gc

def isArraySorted (t):
    """ Récupère un tableau t 
        et dit s'il est déjà 
        trié ou non 
    """
    if len (t) <= 1:
        return True
    else:
        for i,v in enumerate (t[:-1]):
            if v > t[i+1]:
                return False
        return True


def isArraySorted_test ():
    t1 = [ i for i in range (10) ]
    t2 = [ 2,3,8,1]
    t3 = [ 1, 2, 1 ]
    t4 = [ 1,1,1,1,2,1 ]
    t5 = [ 1,2, 2, 4, 3, 8, 8 ] 

    assert (isArraySorted (t1))
    assert (not isArraySorted (t2))
    assert (not isArraySorted (t3))
    assert (not isArraySorted (t4))
    assert (not isArraySorted (t5))
                
    print ("All tests passed successfully")


###### UNE FONCTION DE RECHERCHE DICHOTOMIQUE 

def rechercheDichotomique (t,e):
    N = len (t)
    def aux (i,j):
        """ Dichotomie sur le tableau [i,j] 
            avec j exclus !  
        """
        if j <= i + 1:
            if t[i] == e:
                return i
            else:
                return (-1)
        else:
            k = int ((i+j) / 2)
            if t[k] == e:
                return k
            elif t[k] < e:
                return aux (k, j)
            else:
                return aux (i, k)
    return aux (0, N) 

def rechercheDichotomique3 (t,e):
    N = len (t)
    i,j = 0,N
    while True:
        if j <= i + 1:
            if t[i] == e:
                return i
            else:
                return (-1)
        else:
            k = int ((i+j) / 2)
            if t[k] == e:
                return k
            elif t[k] < e:
                i,j = k,j
            else:
                i,j = i,k




def rechercheDichotomique2 (t,e):
    """ Version avec des appels récursifs 
        qui est plus simple 
    """
    N = len (t) 
    if N == 0:
        return (-1)
    elif N == 1:
        if t[0] == e:
            return 0
        else:
            return (-1)
    else:
        k = int (N / 2)
        if t[k] == e:
            return k
        elif t[k] < e:
            return k + 1 + rechercheDichotomique2 (t[k+1:], e)
        else:
            return rechercheDichotomique2 (t[:k], e)

def rechercheDichotomique_test ():
    t1 = [ 1, 8, 9, 10 ] 
    t2 = [ 1,2, 7,8 ,9, 10 ] 
    
    assert (rechercheDichotomique (t1, 1) == 0)
    assert (rechercheDichotomique (t1, 4) == -1)
    assert (rechercheDichotomique (t1, 10) == 3)

    assert (rechercheDichotomique (t2, 1) == 0)
    assert (rechercheDichotomique (t2, 4) == -1)
    assert (rechercheDichotomique (t2, 10) == 5)


#### LA FONCTION FACTORIELLE 

def factorielleIter (n):
    P = 1
    for i in range (1,n+1):
        P = P * i
    return P

def factorielleRec (n):
    if n <= 0:
        return 1
    else:
        return n * factorielleRec (n-1)

#### Écrire une fonction qui découpe un entier en produits 
### de facteurs premiers 

def decompostionFacteursPremiers (n):
    T = [] 
    for p in range (2,n+1): 
        NP = 0 
        while n % p == 0:
            n = int (n / p)
            NP = NP + 1
        if NP != 0:
            T.append ((p,NP))

    return T


#### Listes doublement chaînées 

class DoubleLinkedList:
    def __init__ (self,val):
        self.before = None
        self.after  = None
        self.value  = val

    def concatAfter (self,l):
        if l == None:
            pass
        if self.after == None:
            self.after = l
            l.before   = self
        else:
            self.after.concatAfter (l) # appel récursif 

    def concatBefore (self,l):
        if l == None:
            pass
        if self.before == None:
            self.before = l
            l.after     = self
        else:
            self.before.concatBefore (l) # appel récursif 

    def removeHead (self):
        g = self.before
        d = self.after 

        if d != None:
            d.before = g

        if g != None:
            g.after = d

        return (self.value, d)

    def goLeft (self):
        return self.after 

    def goRight (self):
        return self.before


class SDList:
    def __init__ (self):
        self.head = None
        self.tail = None

    def append (self, e):
        L = DoubleLinkedList (e)
        if self.head is None:
            self.head = L
            self.tail = L
        else:
            self.tail.concatAfter (L)
            self.tail = L

    def prepend (self,e):
        L = DoubleLinkedList (e)
        if self.head is None:
            self.head = L
            self.tail = L
        else:
            self.head.concatBefore (L)
            self.head = L

    def remove (self, e):
        p = self.head 
        while p != None:
            if p.value == e:
                (_,new) = p.removeHead ()
                p = new

    def __str__ (self):
        T = []
        p = self.head 
        while p != None:
            T.append (p.value)
            p = p.after

        return str (T)



# suivant 
N = 1000
T = [ [ 0 for i in range (N) ] for j in range (N) ] 

for i in range (N):
    T[i] = 0
    time.sleep (1)
    gc.collect (0)



