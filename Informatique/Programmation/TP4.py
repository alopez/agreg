# Séance de TP 4
import math
import drawingkit

def plscPrecalcul (u,v):
    """ Calcul de la plus longue sous-séquence 
        commune de (u,v) 
    """
    T = [ [ 0 for j in v ] 
              for i in u ]

    T[0][0] = int (u[0] == v[0])

    for i in range (1, len (u)):
        if u[i] == v[0]:
            T[i][0] = 1
        else:
            T[i][0] = T[i-1][0]

    for j in range (1, len (v)):
        if v[j] == u[0]:
            T[0][j] = 1
        else:
            T[0][j] = T[0][j-1]

    for i in range (1, len (u)):
        for j in range (1, len (v)):
            T[i][j] = max (T [i][j-1], T[i-1][j])
            if u[i] == v[j]:
                T[i][j] = max (1 + T[i-1][j-1], T[i][j])

    return T

def plsc (u,v):
    T = plscPrecalcul (u,v)

    print (T)
    p = []
    i,j = (len (u) - 1, len (v) - 1)

    def accs (i,j):
        if i < 0 or j < 0:
            return 0
        else:
            return T[i][j]

    while 0 <= i and 0 <= j:
        print ("({},{}) : {}".format (i,j,p))
        if T[i][j] == max (accs(i-1,j),accs(i,j-1)):
            if accs(i,j-1) > accs(i-1,j):
                j = j-1
            else:
                i = i-1
        else:
            p.append (u[i])
            i = i-1
            j = j-1

    p.reverse()
    return p 

def plscTD (u,v):
    T = [ [ None for j in v ] 
                 for i in u ]
    def aux (i,j):
        if i < 0 or j < 0:
            return (0,"")

        if T[i][j] == None:
            if u[i] == v[j]:
                taille,chaine = aux(i-1,j-1)
                T[i][j] = (taille + 1, chaine + u[i])
            else:
                T[i][j] = max ([ aux(i-1,j), aux (j-1,i) ], key=lambda x: x[0])

        return T[i][j] 

    return aux (len(u)-1,len(v)-1)


###### Calculer un point de Fermat-Ticolli

def fermatT (pts):
    """
        Trouve un point qui minimise somme des distances 
        aux autres points 

        f(x) = Σ |x - y_i|

        Δf(x) = Σ (x - y_i) / | x - y_i | 
    """

    # construction des fonctions intermédiaires
    def moins (x,y):
        return (x[0] - y[0], x[1] - y[1])

    def norme (x):
        return math.sqrt (x[0] * x[0] + x[1] * x[1] )

    def f(x):
        return sum ([ norme (moins(x,y)) for y in pts ])

    
    def grad (x):
        c0 = sum ([ moins (x,y)[0] / norme (moins (x,y))
                    for y in pts ])
        c1 = sum ([ moins (x,y)[1] / norme (moins (x,y))
                    for y in pts ])

        return (c0,c1)

    x01 = sum ([ y[0] for y in pts ]) / len (pts)
    x02 = sum ([ y[1] for y in pts ]) / len (pts)

    # initialisation au barycentre des points 
    # x  = (x01, x02)
    x = (100,100)

    # Drawing
    dt = drawingkit.MyDrawingKit ()

    for y in pts:
        dt.drawPoint (y)

    m = dt.min 
    M = dt.max 

    for i in range (m[0],M[0]):
        for j in range (m[1],M[1]):
            v = int (f((i,j))) % 10
            if v <= 5:
                dt.drawPoint ((i,j), "black")
            else:
                dt.drawPoint ((i,j), "white")

    dt.drawPoint (x,"red")

    # initialisation de la tolérance et du pas 
    # en fonction du problème 
    gradN     = grad (x)
    tolerance = 0.01
    pas = norme (gradN) 

    N         = len (pts) / tolerance

    while norme(gradN) > tolerance and N > 0:
        print ("grad = {} / xN = {}".format (gradN,x))
        xN    = moins (x, (pas * gradN[0], pas*gradN[1]) )
        gradN = grad(xN)
        dt.drawPoint (xN, "red")
        dt.drawLine (x,xN, "red")

        pas = norme (gradN) 

        x = xN
        N = N - 1
       

         

    dt.finalDrawing ()


    
