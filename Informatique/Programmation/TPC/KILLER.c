#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>
#include <sys/types.h>

int childpid;

void
signal_handler (int numero_signal)
{
	// Quand on reçoit le signal
	printf ("YOU TRIED TO KILL ME %d ... SIGKILL\n", childpid);
	sleep (2);
	printf ("BON, JE VAIS TUER %d\n", childpid);
	kill (childpid,SIGKILL);		
}


void 
essaie_de_tuer ()
{
	sleep (1);
	printf ("Je suis le processus fils, mon papa c'est %d, moi je suis %d\n", 
					getppid (),
					getpid  ());
	sleep (5);
	printf ("Je vais tuer mon père %d\n", getppid ());
	kill (getppid (), SIGTERM);
}

int 
main (int argc, char** argv)
{
	signal (SIGTERM, signal_handler);

	int pid = fork ();
	
	if (pid == 0) {
		essaie_de_tuer ();
	} else {
		childpid = pid;
	}

	while (1) {

	}

	return 0;
}
