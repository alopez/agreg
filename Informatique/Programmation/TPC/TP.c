#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>

/*
 * Construit un tableau n*m
 * d'entiers 
 */
int*
construire_tableau (int n, int m)
{
	int* tableau = malloc ( sizeof (int) * n * m );
	return tableau;
}

int
indice_tableau (int n, int m, int i, int j)
{
	int ptr;
	ptr = j + m*i;
	if (ptr < n * m) {
			return ptr;
	} else {
			return 0;
	}
}

int**
construire_matrice (int n, int m)
{
	int** matrice = malloc (sizeof (int*) * n);
	for (int i = 0; i < m; i++) {
		matrice[i] = malloc (sizeof (int) * m);
	}
	return matrice;
}

void
print_tableau (int n, int m, int* tableau)
{
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j ++) {
			printf ("%07d ", tableau[indice_tableau(n,m,i,j)]);
		}
		printf ("\n");
	}
	printf ("\n");
}

void
print_matrice (int n, int m, int** matrice)
{
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j ++) {
			printf ("%07d ", matrice[i][j]);
		}
		printf ("\n");
	}
	printf ("\n");
}

int
main (int argc, char** argv)
{
	/* LE TEST POUR LE TABLEAU 
	 * SUR UNE SEULE LIGNE 
	 */
	{
			int n = 50;
			int m = 10;
			int* test = construire_tableau (n,m);
			for (int i = 0; i < n *m; i ++) {
				test[i] = i;
			}
			print_tableau (n,m,test);
	}
	/* LE TEST POUR LE TABLEAU
	 * EN DEUX DIMENSIONS
	 */
	{
			int n = 50;
			int m = 10;
			int** test = construire_matrice (n,m);
	}

	while (1) {
		int* test = malloc (sizeof (int) * 100000000);	
		test[100] = 0;
		sleep (1);
		free (test);
	};
	
	return 0;
}

