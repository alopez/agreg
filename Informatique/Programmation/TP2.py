import sys
import time
import turtle
from memory_profiler import profile
import gc

def verification_tri_tableau(t):
    b=True
    #On parcourt le tableau pour verifier qu'il est trie
    for i in range(len(t)-1):
        if t[i]>t[i+1]:
            #Si un element est plus grand que son successeur, c'est faux
            b=False
    #print(b)
    return(b)

#print verification_tri_tableau([1,2,3])
#print verification_tri_tableau([1,3,2])

def dichotomie(t,x):
    n=len(t)
    def dichotomie_rec(t,i,j,x):
        #Le cas du tableau vide
        if i>j:
            return (False,0)
        #Le cas du tableau a un element
        if j==i:
            if t[i]==x:
                return (True,0)
            else:
                return (False,0)
        #Sinon, on cherche dans quelle zone du tableau l'element est
        if t[i+(j-i+1)/2]==x:
            return (True,i+(j-i+1)/2)
        elif t[i+(j-i+1)/2]>x:
            return dichotomie_rec(t,i,(j-i+1)/2,x)
        else:
            return dichotomie_rec(t,(j-i+1)/2,j,x)
    (b,val)=dichotomie_rec(t,0,n-1,x)
    if b:
        return val
    else:
        return None
    
# print dichotomie([],3)
# print dichotomie([2],3)
# print dichotomie([3],3)
# print dichotomie([1,3,4,5,7,8,8,9],3)


def factorielle_imperative(n):
    factorielle=1
    for i in range(1,n+1):
        factorielle*=i
    return factorielle

# print factorielle_imperative(0)
# print factorielle_imperative(1)
# print factorielle_imperative(6)

def factorielle_recursive(n):
    if n==0 or n==1:
        return 1
    else:
        return n*factorielle_recursive(n-1)

# print factorielle_imperative(0)
# print factorielle_imperative(1)
# print factorielle_imperative(6)

def decomposition(n):
    if n==0:
        return None
    decomp=[]
    i=2
    while n!=1:
       if n%i==0:
           n=n/i
           decomp+=[i]
       else:
           i+=1
    return decomp

# print decomposition(0)
# print decomposition(1)
# print decomposition(6)
# print decomposition(3564)

class Liste():
    def __init__(self, head = None, tail = None):
        self.head=head
        self.tail=tail
    def __str__(self):
        if (self.tail==None):
            return "[]"
        else:
            return str(self.head)+":"+str(self.tail)

def liste(n):
    l=Liste()
    for i in range(1,n+1):
        l=Liste(i,l)
    return l

def listepy(n):
    l=[]
    for i in range(1,n+1):
        l.append(i)
    return l

#print liste(0)
#print liste(5)

def mul_liste1_rec(l):
    if l.head==None:
        return 1
    else:
        return l.head*mul_liste1_rec(l.tail)

def fact_liste1(n):
    return mul_liste1_rec(liste(n))

#print fact_liste1(12)
def test(n):
    t=time.clock()
    l=liste(n)
    return time.clock()-t

def testpy(n):
    t=time.clock()
    l=listepy(n)
    return time.clock()-t

def timetest(n):
    gc.disable ()

    for i in range(1,n+1):
        t = [ 0 for i in range (1000) ]
        gc.collect ()

    gc.collect ()
    time.sleep (10)
    gc.collect ()

    gc.disable ()
    for i in range(1,n+1):
        t = [ 0 for i in range (1000) ]

    gc.collect ()
    time.sleep (10)
    gc.collect( )

    gc.enable ()
    for i in range(1,n+1):
        t = [ 0 for i in range (1000) ]

    # for i in range(1,n+1):
        # tpy.append(testpy(100*i))
        # gc.collect ()
    # maxi=max(tpy[n-1],t[n-1])
    # echelle_v=300/maxi
    # echelle_h=1200/n
    # turtle.color("white")
    # turtle.goto(-600,-200)
    # turtle.color("blue")
    # for i in range(1,n):
        # turtle.goto(echelle_h*i-600,echelle_v*tpy[i]-200)
    # turtle.penup()
    # turtle.goto(-600,-200)
    # turtle.pendown()
    # turtle.color("red")
    # for i in range(1,n):
        # turtle.goto(echelle_h*i-600,echelle_v*t[i]-200)

timetest(600)
# turtle.mainloop()
