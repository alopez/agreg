# Drawing kit pour enveloppe convexe 


import turtle 
import time
import random
import math

POINT   = 0
LINE    = 1
class MyDrawingKit:

    def __init__ (self):
        self.actions = []
        self.max     = [0,0]
        self.min     = [0,0]

    def drawPoint (self,p, color="blue"):
        self.actions.append ((POINT,color, p))
        self.__UpdateBounds (p)

    def drawLine (self, p1, p2, color="blue"):
        self.actions.append ((LINE, color, p1, p2))
        self.__UpdateBounds (p1)
        self.__UpdateBounds (p2)

    def drawPoly (self, l, color="blue"):
        print (l)
        for (pi,pj) in zip (l, l[1:] + l):
            self.drawLine (pi,pj,color)

    def __UpdateBounds (self,p):
        self.min = [ min(self.min[i], p[i])
                     for i in range (2) ]
        self.max = [ max(self.max[i], p[i])
                     for i in range (2) ]


    def finalDrawing (self):
        # Calcule la taille du dessin
        sizeX = self.max[0] - self.min[0]
        sizeY = self.max[1] - self.min[1]

        print ("EXTREME POINTS {} / {}".format (self.min, self.max))

        # Calcule l'échelle en X et Y 
        scaleX = 500 / sizeX
        scaleY = 500 / sizeY
        turtle.setup(800, 800)
        turtle.colormode(255)
        turtle.speed (0)
        turtle.penup ()

        def rescale (p):
            return [ (p[0] - self.min[0]) * scaleX - 250, 
                     (p[1] - self.min[1]) * scaleY - 250]

        # self.actions.sort (key=lambda x: x[0])
        for a in self.actions:
            print ("Drawing : {}".format (a))
            if a[0] == POINT:
                _,color,p = a
                print ("Point {} rescaled to {}".format (p, rescale(p)))
                turtle.goto (*rescale(p))
                turtle.dot (min (1*scaleX, 10),color)

            elif a[0] == LINE:
                _,color,p1,p2 = a
                print ("Point {} rescaled to {}".format (p1, rescale(p1)))
                print ("Point {} rescaled to {}".format (p2, rescale(p2)))
                turtle.goto (*rescale(p1))
                turtle.pendown ()
                turtle.color (color)
                turtle.goto (*rescale (p2))
                turtle.penup ()
                # time.sleep (1)

        turtle.mainloop ()



def generateRandomPoints (N):
    radius = [ random.randint (0,100) for i in range (N) ]
    angle  = [ random.randint (0,100) for i in range (N) ] 

    return [ [ int (radius[i] * math.cos (angle[i])),
               int (radius[i] * math.sin (angle[i])) ]
             for i in range (N) ]



##################################
########### QUICKHULL ############
##################################

##################################
####### Fonctions utiles #########
##################################

# def rev(t):
    # return [t[len(t)-i] for i in range(1,len(t)+1)]

# def vect(x_1,x_2):
    # assert x_1[0]<=x_2[0]
    # return [x_2[0]-x_1[0],x_2[1]-x_1[1]]

# #print vect([1,2],[2,3])

# def det(vect1,vect2):
    # return vect1[0]*vect2[1]-vect1[1]*vect2[0]

# #print det([1,2],[2,3])
# #print det([1,1],[1,3])

# def scal(vect1,vect2):
    # return vect1[0]*vect2[0]+vect1[1]*vect2[1]

# #print scal([2,0],[1,1])
# #print scal([2,0],[2,1])

# def tri_absc(t):
    # if len(t)==0 or len(t)==1:
        # return t
    # else:
        # t_tri=[]
        # k = int (len(t) / 2)
        # t_1=tri_absc(t[:k])
        # t_2=tri_absc(t[k:])
        # i,j=0,0
        # while i<len(t_1) or j<len(t_2):
            # if i==len(t_1):
                # t_tri.append(t_2[j])
                # j+=1
            # elif j==len(t_2):
                # t_tri.append(t_1[i])
                # i+=1
            # elif t_1[i][0]>t_2[j][0]:
                # t_tri.append(t_2[j])
                # j+=1
            # else:
                # t_tri.append(t_1[i])
                # i+=1
        # return t_tri

# #print tri_absc([[4,0],[3,0],[2,-4],[1,-1],[1,-5],[2,-1]])


# def extraire(t,ind,b):
    # x_1=t[ind[0]]
    # x_2=t[ind[len(ind)-1]]
    # assert x_1[0]<=x_2[0]
    # #On extrait d'un sous-tableau trie t[ind] les points situes:
    # # - au-dessus de la droite (x_1,x_2) si b=0
    # # - en-dessous de la droite si b=1
    # extr=[ind[0]]
    # vect1=vect(x_1,x_2)
    # for i in range(len(ind)):
        # vect2=vect(x_1,t[ind[i]])
        # if det(vect1,vect2)>0 and b==1:
            # extr.append(ind[i])
        # if det(vect1,vect2)<0 and b==0:
            # extr.append(ind[i])
    # extr.append(ind[len(ind)-1])
    # return extr

# #print extraire([[0,0],[1,5],[1,-2],[2,4],[2,1],[3,0]],[0,1,2,3,4,5],1)
# #print extraire([[0,0],[1,5],[1,-2],[2,4],[2,-1],[3,0]],[0,1,2,3,4,5],1)
# #print extraire([[0,0],[1,5],[1,-2],[2,4],[2,1],[3,0]],[0,1,2,3,4,5],0)
# #print extraire([[0,0],[1,5],[1,-2],[2,4],[2,-1],[3,0]],[0,1,2,3,4,5],0)

# ##################################
# ######### L'algorithme ########### 
# ##################################

# dt = MyDrawingKit()
# L = generateRandomPoints (100)
# L = tri_absc (L)
# for p in L:
    # dt.drawPoint (p)

# def quickhull_rec(t,ind,b):
    # if len(ind)==2:
        # return ind
    # else:
        # x_1=t[ind[0]]
        # x_2=t[ind[len(ind)-1]]
        # vect1=vect(x_1,x_2)
        # max=0
        # detmax=0
        # scalmin=0
        # for i in range(len(ind)):
            # vect2=vect(x_1,t[ind[i]])
            # deter=det(vect1,vect2)
            # if deter>detmax and b==1:
                # max=i
                # detmax=det(vect1,vect2)
                # scalmin=scal(vect1,vect2)
            # if deter<detmax and b==0:
                # max=i
                # detmax=det(vect1,vect2)
                # scalmin=scal(vect1,vect2)
            # if deter==detmax:
                # prodscal=scal(vect1,vect2)
                # if (prodscal<scalmin) and b==1:
                    # max=i
                    # detmax=det(vect1,vect2)
                    # scalmin=scal(vect1,vect2)
                # if prodscal>scalmin and b==0:
                    # max=i
                    # detmax=det(vect1,vect2)
                    # scalmin=scal(vect1,vect2)
        # ind1=extraire(t,ind[:max],b)
        # ind2=extraire(t,ind[max:],b)

        
        # color = (random.randint (60,200),
                 # random.randint (60,200),
                 # random.randint (60,200))
                 
        # dt.drawPoly ([ t[ind[0]], t[ind[max]], t[ind[-1]]],
            # color)

        # quick_1=quickhull_rec(t,ind1,b)
        # quick_2=quickhull_rec(t,ind2,b)
        # return quick_1[:len(quick_1)-1]+quick_2

# #print quickhull_rec([[0,0],[1,1],[1,5],[2,4],[2,1],[3,0]],[0,1,2,3,4,5],1)
# #print quickhull_rec([[0,0],[1,-1],[1,-5],[2,-4],[2,-1],[3,0]],[0,1,2,3,4,5],0)

# def quickhull(t):
    # ind_tot=[i for i in range(len(t))]
    # ind1=extraire(t,ind_tot,1)
    # ind2=extraire(t,ind_tot,0)

    # quick_1=quickhull_rec(t,ind1,1)
    # quick_2=rev(quickhull_rec(t,ind2,0))
    # return quick_1[:len(quick_1)-1]+quick_2[:len(quick_2)-1]

# #print quickhull([[0,0],[1,1],[1,-1],[1,5],[1,-6],[2,4],[2,-1],[2,1],[3,0]])

# E = quickhull(L)

# dt.drawPoly ([ L[i] for i in E ], "red")
# dt.finalDrawing()
